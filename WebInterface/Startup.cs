﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace WebInterface
{
	public class Startup
	{

		public Startup(IHostingEnvironment env)
		{
			//var builder = new ConfigurationBuilder()
			//	.SetBasePath(env.ContentRootPath)
			//	.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
			//	.AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
			//	.AddEnvironmentVariables();
			//Configuration = builder.Build();
		}

		private string GetSwaggerXMLPath()
		{
			var app = Microsoft.Extensions.PlatformAbstractions.PlatformServices.Default.Application;
			return System.IO.Path.Combine(app.ApplicationBasePath, "WebInterface.xml");
		}

		public IConfigurationRoot Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			//services.AddAuthentication(options => new ActiveDirectoryCookieOptions());
			// Add framework services.
			services.AddMvc().AddJsonOptions(jsonOptions =>
			{
				jsonOptions.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
			});

			services.AddSwaggerGen(options =>
			{
				options.SingleApiVersion(new Swashbuckle.Swagger.Model.Info
				{
					Version = "v1",
					Title = "Geo Search API",
					Description = "A simple api",
					TermsOfService = "None"
				});
				options.IncludeXmlComments(GetSwaggerXMLPath());
				options.DescribeAllEnumsAsStrings();
			});
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
		{
			//loggerFactory.AddConsole(Configuration.GetSection("Logging"));
			//loggerFactory.AddDebug();
			//app.UseCookieAuthentication(new ActiveDirectoryCookieOptions().ApplicationCookie);

			app.UseCors("*");

			//if (env.IsDevelopment())
			//{
			//    app.UseDeveloperExceptionPage();
			//    app.UseBrowserLink();
			//}
			//else
			//{
			//    app.UseExceptionHandler("/Home/Index");
			//}

			app.UseStaticFiles();

			app.UseSwagger();
			app.UseSwaggerUi();

			app.UseMvc(routes =>
			{
				routes.MapRoute(
					"SheriffApp",
					"{*url}",
					new { controller = "Home", action = "Index" }
				);
			});
		}
	}
}
