import React from 'react'
import ReactDOM from 'react-dom'
import TestUtils from 'react-addons-test-utils'
import expect from 'expect'
import jsdom from 'mocha-jsdom'

import dom from '../dom'

dom('<html><body><div id="container"></div></body></html>')
var $, container

describe('Testing a Panel Container', function () {

  before(function () {
    $ = jsdom.rerequire('jquery')
    container = $('#container')[0]
  })

  it("empty", function () {

    ReactDOM.render(
      <div id="testing">A title</div>,
      container
    );

    expect($('#testing').html()).toEqual("A title");
  })

  it("title", function () {

    ReactDOM.render(
      <div id="testing">A title</div>,
      container
    );

    expect($('#testing').html()).toEqual("A title");
  })

  it("content", function () {

    ReactDOM.render(
      <div id="testing">A title</div>,
      container
    );

    expect($('#testing').html()).toEqual("A title");
  })

  it("title + content", function () {

    ReactDOM.render(
      <div id="testing">A title</div>,
      container
    );

    expect($('#testing').html()).toEqual("A title");
  })
})
