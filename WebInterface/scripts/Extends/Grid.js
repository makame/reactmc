"use strict";
const React = require('react');
const Core_1 = require('../Core');
const Grid_1 = require('../Core/Components/Grid');
class GridItemTriggerModel extends Grid_1.GridItemModel {
    constructor(initializer) {
        super();
        this.filters = {
            not_in: {
                check(value, filter_value, field, context, page, application) {
                    return value != filter_value;
                },
                render(valueLink, field, content, context, page, application) {
                    return (React.createElement("div", {className: "padding-1 text right"}, 
                        React.createElement("span", {className: "text thick padding-right-1"}, "Значение"), 
                        React.createElement(Core_1.Trigger, {valueLink: valueLink})));
                }
            },
            totaly: {
                check(value, filter_value, field, context, page, application) {
                    filter_value = filter_value !== false && filter_value !== true ? false : filter_value;
                    return value === filter_value;
                },
                render(valueLink, field, content, context, page, application) {
                    return (React.createElement("div", {className: "padding-1 text right"}, 
                        React.createElement("span", {className: "text thick padding-right-1"}, "Значение"), 
                        React.createElement(Core_1.Trigger, {valueLink: valueLink})));
                }
            },
        };
        if (initializer === undefined) {
            return;
        }
        Object.keys(initializer).forEach((key, index) => {
            this[key] = initializer[key];
        });
    }
    render(value, field, content, context, page, application) {
        return React.createElement("div", null, value ? 'Да' : 'Нет');
    }
    create_render(valueLink, field, messages, content, context, page, application) {
        return React.createElement(Core_1.Trigger, {valueLink: valueLink, className: (messages.error ? ' error' : '') + (messages.enabled ? '' : ' disabled')});
    }
    edit_render(valueLink, field, messages, content, context, page, application) {
        return React.createElement(Core_1.Trigger, {valueLink: valueLink, className: (messages.error ? ' error' : '') + (messages.enabled ? '' : ' disabled')});
    }
}
exports.GridItemTriggerModel = GridItemTriggerModel;
class GridItemTextModel extends Grid_1.GridItemModel {
    constructor(initializer) {
        super();
        this.classEdit = "padding-0";
        this.classCreate = "padding-0";
        this.filters = {
            contains: {
                check(value, filter_value, field, context, page, application) {
                    return !filter_value || (value && filter_value && value.toLowerCase().indexOf(filter_value.toLowerCase()) >= 0);
                },
                render(valueLink, field, content, context, page, application) {
                    return (React.createElement("div", {className: "padding-px1"}, 
                        React.createElement("div", {className: "sp-input-group sp-sm"}, 
                            React.createElement("input", {value: valueLink.value ? valueLink.value : '', onChange: event => valueLink.requestChange(event.target.value), type: "text", className: "sp-form-control", placeholder: "Фильтр"})
                        )
                    ));
                }
            },
            not_in: {
                check(value, filter_value, field, context, page, application) {
                    return !filter_value || (value && filter_value && value.toLowerCase().indexOf(filter_value.toLowerCase()) < 0);
                },
                render(valueLink, field, content, context, page, application) {
                    return (React.createElement("div", {className: "padding-px1"}, 
                        React.createElement("div", {className: "sp-input-group sp-sm"}, 
                            React.createElement("input", {value: valueLink.value ? valueLink.value : '', onChange: event => valueLink.requestChange(event.target.value), type: "text", className: "sp-form-control", placeholder: "Фильтр"})
                        )
                    ));
                }
            },
            totaly: {
                check(value, filter_value, field, context, page, application) {
                    return value && filter_value && value.toLowerCase() == filter_value.toLowerCase();
                },
                render(valueLink, field, content, context, page, application) {
                    return (React.createElement("div", {className: "padding-1 text right"}, 
                        React.createElement("span", {className: "text thick padding-right-1"}, "Значение"), 
                        React.createElement(Core_1.Trigger, {valueLink: valueLink})));
                }
            },
            null: {
                check(value, filter_value, field, context, page, application) {
                    return !value;
                },
            },
            not_null: {
                check(value, filter_value, field, context, page, application) {
                    return !!value;
                },
            },
        };
        if (initializer === undefined) {
            return;
        }
        Object.keys(initializer).forEach((key, index) => {
            this[key] = initializer[key];
        });
    }
    render(value, field, content, context, page, application) {
        return React.createElement("div", null, value);
    }
    create_render(valueLink, field, messages, content, context, page, application) {
        return React.createElement("div", {className: "sp-input-group margin-px1" + (messages.error ? ' error' : '') + (messages.enabled ? '' : ' disabled')}, 
            React.createElement("input", {type: "text", className: "sp-form-control", placeholder: this.placeholder, value: valueLink.value ? valueLink.value : '', onChange: event => valueLink.requestChange(event.target.value)})
        );
    }
    edit_render(valueLink, field, messages, content, context, page, application) {
        return React.createElement("div", {className: "sp-input-group margin-px1" + (messages.error ? ' error' : '') + (messages.enabled ? '' : ' disabled')}, 
            React.createElement("input", {type: "text", className: "sp-form-control", placeholder: this.placeholder, value: valueLink.value ? valueLink.value : '', onChange: event => valueLink.requestChange(event.target.value)})
        );
    }
}
exports.GridItemTextModel = GridItemTextModel;
class GridItemSelectModel extends Grid_1.GridItemModel {
    constructor(initializer) {
        super();
        this.classEdit = "padding-0";
        this.classCreate = "padding-0";
        this.filters = {
            totaly: {
                check(value, filter_value, field, context, page, application) {
                    return value && filter_value && (value === filter_value || value.id == filter_value.id);
                },
                render(valueLink, field, content, context, page, application) {
                    return (React.createElement(Core_1.Select, {valueLink: valueLink, icon: this.icon, data: this.data, data_type: this.data_type, search: this.search, request: this.request, default: this.default, asInput: this.as_input, render: this.render_element}));
                }
            },
            null: {
                check(value, filter_value, field, context, page, application) {
                    return !value;
                },
            },
            not_null: {
                check(value, filter_value, field, context, page, application) {
                    return !!value;
                },
            },
        };
        if (initializer === undefined) {
            return;
        }
        Object.keys(initializer).forEach((key, index) => {
            this[key] = initializer[key];
        });
    }
    render(value, field, content, context, page, application) {
        return React.createElement("div", null, value ? value.name : 'Нет значения');
    }
    create_render(valueLink, field, messages, content, context, page, application) {
        return React.createElement(Core_1.Select, {valueLink: valueLink, icon: this.icon, className: (this.class ? (' ' + this.class) : '') + (messages.error ? 'error' : '') + (messages.enabled ? '' : ' disabled'), data: this.data, data_type: this.data_type, search: this.search, request: this.request, default: this.default, asInput: this.as_input, render: this.render_element});
    }
    edit_render(valueLink, field, messages, content, context, page, application) {
        return React.createElement(Core_1.Select, {valueLink: valueLink, icon: this.icon, className: (this.class ? (' ' + this.class) : '') + (messages.error ? 'error' : '') + (messages.enabled ? '' : ' disabled'), data: this.data, data_type: this.data_type, search: this.search, request: this.request, default: this.default, asInput: this.as_input, render: this.render_element});
    }
}
exports.GridItemSelectModel = GridItemSelectModel;
class GridItemMSelectModel extends Grid_1.GridItemModel {
    constructor(initializer) {
        super();
        this.classEdit = "padding-0";
        this.classCreate = "padding-0";
        this.filters = {
            null: {
                check(value, filter_value, field, context, page, application) {
                    return !value;
                },
            },
            not_null: {
                check(value, filter_value, field, context, page, application) {
                    return !!value;
                },
            },
        };
        if (initializer === undefined) {
            return;
        }
        Object.keys(initializer).forEach((key, index) => {
            this[key] = initializer[key];
        });
    }
    render(value, field, content, context, page, application) {
        return React.createElement("div", null, value.length > 0 ? value.map((item) => { return item.name; }).join(', ') : 'Нет значений');
    }
    create_render(valueLink, field, messages, content, context, page, application) {
        return React.createElement(Core_1.MSelect, {valueLink: valueLink, icon: this.icon, className: (this.class ? (' ' + this.class) : '') + (messages.error ? 'error' : '') + (messages.enabled ? '' : ' disabled'), data: this.data, data_type: this.data_type, search: this.search, request: this.request, asInput: this.as_input, render: this.render_element});
    }
    edit_render(valueLink, field, messages, content, context, page, application) {
        return React.createElement(Core_1.MSelect, {valueLink: valueLink, icon: this.icon, className: (this.class ? (' ' + this.class) : '') + (messages.error ? 'error' : '') + (messages.enabled ? '' : ' disabled'), data: this.data, data_type: this.data_type, search: this.search, request: this.request, asInput: this.as_input, render: this.render_element});
    }
}
exports.GridItemMSelectModel = GridItemMSelectModel;
//# sourceMappingURL=Grid.js.map