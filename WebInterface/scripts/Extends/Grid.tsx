import * as React from 'react'
import * as moment from 'moment';

import FontAwesome = require('react-fontawesome')

import {
  IValueLink, Map, MapPro, ContentModel, ContextModel, RouteModel, ApplicationModel,
  Select, ISelectData, SelectDataType, SelectSearchType,
  MSelect, IMSelectData,
  Trigger,
  DatePicker,
  PeriodPicker
} from '../Core'

import { Grid, GridMessages, IGridItemFilter, IGridItemModel, GridItemModel } from '../Core/Components/Grid';

export interface IGridItemTriggerModel extends IGridItemModel<boolean> {
}

export class GridItemTriggerModel extends GridItemModel<boolean> implements IGridItemTriggerModel {

  constructor(initializer?: IGridItemTriggerModel) {
    super()
    if (initializer === undefined) {
      return;
    }
    Object.keys(initializer).forEach((key, index) => {
      this[key] = initializer[key];
    });
  }

  filters: Map<IGridItemFilter<boolean>> = {
      not_in: {
        check(value: boolean, filter_value: any, field: GridItemModel<boolean>, context: ContextModel, page: RouteModel, application: ApplicationModel): boolean {
            return value != filter_value
        },

        render(valueLink: IValueLink<any>, field: GridItemModel<boolean>, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {
            return (
                <div className="padding-1 text right">
                    <span className="text thick padding-right-1">Значение</span>
                    <Trigger valueLink={valueLink}></Trigger>
                </div>
            )
        }
      },
      totaly: {
        check(value: boolean, filter_value: any, field: GridItemModel<boolean>, context: ContextModel, page: RouteModel, application: ApplicationModel): boolean {
            filter_value = filter_value !== false && filter_value !== true ? false : filter_value
            return value === filter_value
        },

        render(valueLink: IValueLink<any>, field: GridItemModel<boolean>, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {
            return (
                <div className="padding-1 text right">
                    <span className="text thick padding-right-1">Значение</span>
                    <Trigger valueLink={valueLink}></Trigger>
                </div>
            )
        }
      },
  }
  
  render(value: boolean, field: GridItemModel<boolean>, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {
      return <div>{value ? 'Да' : 'Нет'}</div>
  }

  create_render(valueLink: IValueLink<boolean>, field: GridItemModel<boolean>, messages: GridMessages, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {
      return <Trigger valueLink={valueLink} className={(messages.error ? ' error' : '') + (messages.enabled ? '' : ' disabled')}/>
  }
  
  edit_render(valueLink: IValueLink<boolean>, field: GridItemModel<boolean>, messages: GridMessages, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {
      return <Trigger valueLink={valueLink} className={(messages.error ? ' error' : '') + (messages.enabled ? '' : ' disabled')}/>
  }
}

export interface IGridItemTextModel extends IGridItemModel<string> {
  icon?: string
  placeholder?: string
}

export class GridItemTextModel extends GridItemModel<string> implements IGridItemTextModel {
  icon?: string
  placeholder?: string
  classEdit = "padding-0"
  classCreate = "padding-0"

  constructor(initializer?: IGridItemTextModel) {
    super()
    if (initializer === undefined) {
      return;
    }
    Object.keys(initializer).forEach((key, index) => {
      this[key] = initializer[key];
    });
  }

  filters: Map<IGridItemFilter<string>> = {
    contains: {
        check(value: string, filter_value: any, field: GridItemModel<string>, context: ContextModel, page: RouteModel, application: ApplicationModel): boolean {
            return !filter_value || (value && filter_value && value.toLowerCase().indexOf(filter_value.toLowerCase()) >= 0)
        },

        render(valueLink: IValueLink<any>, field: GridItemModel<string>, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {
            return (
            <div className="padding-px1">
                <div className={"sp-input-group sp-sm"}>
                <input value={valueLink.value ? valueLink.value : ''} onChange={event => valueLink.requestChange((event.target as HTMLInputElement).value)} type="text" className="sp-form-control" placeholder="Фильтр"/>
                </div>
            </div>
            )
        }
    },
    not_in: {
        check(value: string, filter_value: any, field: GridItemModel<string>, context: ContextModel, page: RouteModel, application: ApplicationModel): boolean {
            return !filter_value || (value && filter_value && value.toLowerCase().indexOf(filter_value.toLowerCase()) < 0)
        },

        render(valueLink: IValueLink<any>, field: GridItemModel<string>, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {
            return (
            <div className="padding-px1">
                <div className={"sp-input-group sp-sm"}>
                <input value={valueLink.value ? valueLink.value : ''} onChange={event => valueLink.requestChange((event.target as HTMLInputElement).value)} type="text" className="sp-form-control" placeholder="Фильтр"/>
                </div>
            </div>
            )
        }
    },
    totaly: {
        check(value: string, filter_value: any, field: GridItemModel<string>, context: ContextModel, page: RouteModel, application: ApplicationModel): boolean {
            return value && filter_value && value.toLowerCase() == filter_value.toLowerCase()
        },

        render(valueLink: IValueLink<any>, field: GridItemModel<string>, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {
            return (
                <div className="padding-1 text right">
                    <span className="text thick padding-right-1">Значение</span>
                    <Trigger valueLink={valueLink}></Trigger>
                </div>
            )
        }
    },
    null: {
        check(value: string, filter_value: any, field: GridItemModel<string>, context: ContextModel, page: RouteModel, application: ApplicationModel): boolean {
            return !value
        },
    },
    not_null: {
        check(value: string, filter_value: any, field: GridItemModel<string>, context: ContextModel, page: RouteModel, application: ApplicationModel): boolean {
            return !!value
        },
    },
  }
  
  render(value: string, field: GridItemModel<string>, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {
      return <div>{value}</div>
  }

  create_render(valueLink: IValueLink<string>, field: GridItemModel<string>, messages: GridMessages, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {
      return <div className={"sp-input-group margin-px1" + (messages.error ? ' error' : '') + (messages.enabled ? '' : ' disabled')}>
        <input type="text" className="sp-form-control" placeholder={this.placeholder} value={valueLink.value ? valueLink.value : ''} onChange={event => valueLink.requestChange((event.target as HTMLInputElement).value)}/>
      </div>
  }
  
  edit_render(valueLink: IValueLink<string>, field: GridItemModel<string>, messages: GridMessages, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {
      return <div className={"sp-input-group margin-px1" + (messages.error ? ' error' : '') + (messages.enabled ? '' : ' disabled')}>
        <input type="text" className="sp-form-control" placeholder={this.placeholder} value={valueLink.value ? valueLink.value : ''} onChange={event => valueLink.requestChange((event.target as HTMLInputElement).value)}/>
      </div>
  }
}

export interface IGridItemSelectModel extends IGridItemModel<ISelectData> {
  icon?: string
  class?: string
  
  data?: ISelectData[]
  data_type?: SelectDataType
  search?: SelectSearchType
  request?: (search_string: string) => Promise<ISelectData[]> | ISelectData[]
  default?: ISelectData
  as_input?: boolean
  render_element?: (value: ISelectData, clear: () => void, updating: boolean) => JSX.Element
}

export class GridItemSelectModel extends GridItemModel<ISelectData> implements IGridItemSelectModel {
  icon?: string
  class?: string
  
  classEdit = "padding-0"
  classCreate = "padding-0"
  
  data?: ISelectData[]
  data_type?: SelectDataType
  search?: SelectSearchType
  request?: (search_string: string) => Promise<ISelectData[]> | ISelectData[]
  default?: ISelectData
  as_input?: boolean
  render_element?: (value: ISelectData, clear: () => void, updating: boolean) => JSX.Element

  constructor(initializer?: IGridItemSelectModel) {
    super()
    if (initializer === undefined) {
      return;
    }
    Object.keys(initializer).forEach((key, index) => {
      this[key] = initializer[key];
    });
  }

  filters: Map<IGridItemFilter<ISelectData>> = {
    // contains: {
    //     check(value: ISelectData, filter_value: any, field: GridItemModel<ISelectData>, context: ContextModel, page: RouteModel, application: ApplicationModel): boolean {
    //         return !filter_value || (value && filter_value && value.toLowerCase().indexOf(filter_value.toLowerCase()) >= 0)
    //     },

    //     render(valueLink: IValueLink<any>, field: GridItemModel<ISelectData>, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {
    //         return (
    //         <div className="padding-px1">
    //             <div className={"sp-input-group sp-sm"}>
    //             <input value={valueLink.value ? valueLink.value : ''} onChange={event => valueLink.requestChange((event.target as HTMLInputElement).value)} type="text" className="sp-form-control" placeholder="Фильтр"/>
    //             </div>
    //         </div>
    //         )
    //     }
    // },
    // not_in: {
    //     check(value: ISelectData, filter_value: any, field: GridItemModel<ISelectData>, context: ContextModel, page: RouteModel, application: ApplicationModel): boolean {
    //         return !filter_value || (value && filter_value && value.toLowerCase().indexOf(filter_value.toLowerCase()) < 0)
    //     },

    //     render(valueLink: IValueLink<any>, field: GridItemModel<ISelectData>, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {
    //         return (
    //         <div className="padding-px1">
    //             <div className={"sp-input-group sp-sm"}>
    //             <input value={valueLink.value ? valueLink.value : ''} onChange={event => valueLink.requestChange((event.target as HTMLInputElement).value)} type="text" className="sp-form-control" placeholder="Фильтр"/>
    //             </div>
    //         </div>
    //         )
    //     }
    // },
    totaly: {
        check(value: ISelectData, filter_value: any, field: GridItemModel<ISelectData>, context: ContextModel, page: RouteModel, application: ApplicationModel): boolean {
            return value && filter_value && (value === filter_value || value.id == filter_value.id)
        },

        render(valueLink: IValueLink<any>, field: GridItemModel<ISelectData>, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {
            return (
                <Select valueLink={valueLink} icon={this.icon}
                data={this.data} data_type={this.data_type} search={this.search} request={this.request} default={this.default} asInput={this.as_input} render={this.render_element}/>
            )
        }
    },
    null: {
        check(value: ISelectData, filter_value: any, field: GridItemModel<ISelectData>, context: ContextModel, page: RouteModel, application: ApplicationModel): boolean {
            return !value
        },
    },
    not_null: {
        check(value: ISelectData, filter_value: any, field: GridItemModel<ISelectData>, context: ContextModel, page: RouteModel, application: ApplicationModel): boolean {
            return !!value
        },
    },
  }
  
  render(value: ISelectData, field: GridItemModel<ISelectData>, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {
      return <div>{value ? value.name : 'Нет значения'}</div>
  }

  create_render(valueLink: IValueLink<ISelectData>, field: GridItemModel<ISelectData>, messages: GridMessages, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {
      return <Select valueLink={valueLink} icon={this.icon} className={(this.class ? (' ' + this.class) : '') + (messages.error ? 'error' : '') + (messages.enabled ? '' : ' disabled')}
        data={this.data} data_type={this.data_type} search={this.search} request={this.request} default={this.default} asInput={this.as_input} render={this.render_element}/>
  }
  
  edit_render(valueLink: IValueLink<ISelectData>, field: GridItemModel<ISelectData>, messages: GridMessages, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {
      return <Select valueLink={valueLink} icon={this.icon} className={(this.class ? (' ' + this.class) : '') + (messages.error ? 'error' : '') + (messages.enabled ? '' : ' disabled')}
        data={this.data} data_type={this.data_type} search={this.search} request={this.request} default={this.default} asInput={this.as_input} render={this.render_element}/>
  }
}

export interface IGridItemMSelectModel extends IGridItemModel<IMSelectData[]> {
  icon?: string
  class?: string
  
  data?: IMSelectData[]
  data_type?: SelectDataType
  search?: SelectSearchType
  request?: (search_string: string) => Promise<IMSelectData[]> | IMSelectData[]
  default?: IMSelectData[]
  as_input?: boolean
  render_element?: (value: IMSelectData[], clear: () => void, updating: boolean) => JSX.Element
}

export class GridItemMSelectModel extends GridItemModel<IMSelectData[]> implements IGridItemMSelectModel {
  icon?: string
  class?: string
  
  classEdit = "padding-0"
  classCreate = "padding-0"
  
  data?: IMSelectData[]
  data_type?: SelectDataType
  search?: SelectSearchType
  request?: (search_string: string) => Promise<IMSelectData[]> | IMSelectData[]
  default?: IMSelectData[]
  as_input?: boolean
  render_element?: (value: IMSelectData[], clear: () => void, updating: boolean) => JSX.Element

  constructor(initializer?: IGridItemSelectModel) {
    super()
    if (initializer === undefined) {
      return;
    }
    Object.keys(initializer).forEach((key, index) => {
      this[key] = initializer[key];
    });
  }

  filters: Map<IGridItemFilter<IMSelectData[]>> = {
    // contains: {
    //     check(value: ISelectData, filter_value: any, field: GridItemModel<ISelectData>, context: ContextModel, page: RouteModel, application: ApplicationModel): boolean {
    //         return !filter_value || (value && filter_value && value.toLowerCase().indexOf(filter_value.toLowerCase()) >= 0)
    //     },

    //     render(valueLink: IValueLink<any>, field: GridItemModel<ISelectData>, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {
    //         return (
    //         <div className="padding-px1">
    //             <div className={"sp-input-group sp-sm"}>
    //             <input value={valueLink.value ? valueLink.value : ''} onChange={event => valueLink.requestChange((event.target as HTMLInputElement).value)} type="text" className="sp-form-control" placeholder="Фильтр"/>
    //             </div>
    //         </div>
    //         )
    //     }
    // },
    // not_in: {
    //     check(value: ISelectData, filter_value: any, field: GridItemModel<ISelectData>, context: ContextModel, page: RouteModel, application: ApplicationModel): boolean {
    //         return !filter_value || (value && filter_value && value.toLowerCase().indexOf(filter_value.toLowerCase()) < 0)
    //     },

    //     render(valueLink: IValueLink<any>, field: GridItemModel<ISelectData>, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {
    //         return (
    //         <div className="padding-px1">
    //             <div className={"sp-input-group sp-sm"}>
    //             <input value={valueLink.value ? valueLink.value : ''} onChange={event => valueLink.requestChange((event.target as HTMLInputElement).value)} type="text" className="sp-form-control" placeholder="Фильтр"/>
    //             </div>
    //         </div>
    //         )
    //     }
    // },
    // totaly: {
    //     check(value: IMSelectData[], filter_value: any, field: GridItemModel<IMSelectData[]>, context: ContextModel, page: RouteModel, application: ApplicationModel): boolean {
    //         return value && filter_value && (value === filter_value)
    //     },

    //     render(valueLink: IValueLink<any>, field: GridItemModel<IMSelectData[]>, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {
    //         return (
    //             <Select valueLink={valueLink} icon={this.icon}
    //             data={this.data} data_type={this.data_type} search={this.search} request={this.request} default={this.default} asInput={this.as_input} render={this.render_element}/>
    //         )
    //     }
    // },
    null: {
        check(value: IMSelectData[], filter_value: any, field: GridItemModel<IMSelectData[]>, context: ContextModel, page: RouteModel, application: ApplicationModel): boolean {
            return !value
        },
    },
    not_null: {
        check(value: IMSelectData[], filter_value: any, field: GridItemModel<IMSelectData[]>, context: ContextModel, page: RouteModel, application: ApplicationModel): boolean {
            return !!value
        },
    },
  }
  
  render(value: IMSelectData[], field: GridItemModel<IMSelectData[]>, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {
      return <div>{value.length > 0 ? value.map((item)=>{return item.name}).join(', ') : 'Нет значений'}</div>
  }

  create_render(valueLink: IValueLink<IMSelectData[]>, field: GridItemModel<IMSelectData[]>, messages: GridMessages, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {
      return <MSelect valueLink={valueLink} icon={this.icon} className={(this.class ? (' ' + this.class) : '') + (messages.error ? 'error' : '') + (messages.enabled ? '' : ' disabled')}
        data={this.data} data_type={this.data_type} search={this.search} request={this.request} asInput={this.as_input} render={this.render_element}/>

  }
  
  edit_render(valueLink: IValueLink<IMSelectData[]>, field: GridItemModel<IMSelectData[]>, messages: GridMessages, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {
      return <MSelect valueLink={valueLink} icon={this.icon} className={(this.class ? (' ' + this.class) : '') + (messages.error ? 'error' : '') + (messages.enabled ? '' : ' disabled')}
        data={this.data} data_type={this.data_type} search={this.search} request={this.request} asInput={this.as_input} render={this.render_element}/>
  }
}