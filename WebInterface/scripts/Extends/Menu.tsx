import * as React from 'react'
import * as moment from 'moment';

import FontAwesome = require('react-fontawesome')

import {
  IValueLink, Map, MapPro, ContentModel, ContextModel, RouteModel, ApplicationModel,
  DatePicker
} from '../Core'

import { IMenuItemModel } from '../Core/Components/Menu'

export class MenuItemSpaceModel implements IMenuItemModel {
  render(name: string, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) {
    return (
      <div key={name} className="sp-space">
      </div>
    )
  }
}

export class MenuItemDividerModel implements IMenuItemModel {
  render(name: string, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) {
    return (
      <div key={name} className="sp-divider">
      </div>
    )
  }
}

export interface IMenuItemHeaderModel {
  text: string
}

export class MenuItemHeaderModel implements IMenuItemHeaderModel, IMenuItemModel {
  text: string

  constructor(initializer?: IMenuItemHeaderModel) {
    if (initializer === undefined) {
      return;
    }
    Object.keys(initializer).forEach((key, index) => {
      this[key] = initializer[key];
    });
  }

  render(name: string, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) {
    return (
      <div key={name} className="sp-header">
        {this.text}
      </div>
    )
  }
}

export interface IMenuItemInputModel {
  value?: string
  icon?: string
  placeholder?: string
}

export class MenuItemInputModel implements IMenuItemInputModel, IMenuItemModel {
  value?: string
  icon?: string
  placeholder?: string

  constructor(initializer?: IMenuItemInputModel) {
    if (initializer === undefined) {
      return;
    }
    Object.keys(initializer).forEach((key, index) => {
      this[key] = initializer[key];
    });
  }

  render(name: string, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) {
    let requestChange = (event) => {
      this.value = event.target.value
      page.update()
    }

    return (
      <div key={name} className="sp-input">
        {(this.icon ? <FontAwesome name={this.icon} /> : null)}
        <input type="text" placeholder={this.placeholder} value={this.value} onChange={requestChange}/>
      </div>
    )
  }
}

export interface IMenuItemButtonModel {
  label?: string
  icon?: string
  placeholder?: string
  loading?: boolean
  on_click: (context: ContextModel, page: RouteModel, application: ApplicationModel) => Promise<void> | void
}

export class MenuItemButtonModel implements IMenuItemButtonModel, IMenuItemModel {
  label?: string
  icon?: string
  placeholder?: string
  loading?: boolean
  on_click: (context: ContextModel, page: RouteModel, application: ApplicationModel) => Promise<void> | void

  constructor(initializer?: IMenuItemButtonModel) {
    if (initializer === undefined) {
      return;
    }
    Object.keys(initializer).forEach((key, index) => {
      this[key] = initializer[key];
    });
  }

  render(name: string, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) {
    return (
      <div key={name} className={"sp-item" + (this.loading ? ' loading' : '')} onClick={async ()=>{
          this.loading = true
          page.update()
          try {
            await this.on_click(context, page, application)
          }
          catch (message) {
            console.error(message)
          }
          this.loading = false
          page.update()
        }}>
        {(this.icon ? <FontAwesome name={this.icon} /> : null)}
        {this.label}
      </div>
    )
  }
}

export interface IMenuItemDateModel {
  get?: (content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => moment.Moment
  set?: (newValue: moment.Moment, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => void
  value?: moment.Moment
  icon?: string
}

export class MenuItemDateModel implements IMenuItemDateModel, IMenuItemModel {
  get?: (content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => moment.Moment
  set?: (newValue: moment.Moment, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => void
  value?: moment.Moment
  icon?: string

  constructor(initializer?: IMenuItemDateModel) {
    if (initializer === undefined) {
      return;
    }
    Object.keys(initializer).forEach((key, index) => {
      this[key] = initializer[key];
    });
  }

  render(name: string, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) {
    if (this.get) {
      this.value = this.get(content, context, page, application)
    }
    let valueLink: IValueLink<moment.Moment> = {
      value: this.value,
      requestChange: (newValue) => {
        if (this.set) {
          this.set(newValue, content, context, page, application)
        }
        else {
          this.value = newValue
          page.update()
        }
      }
    }

    return (
      <DatePicker key={name} valueLink={valueLink} render={
          (value, clear, format)=>{
            var selected = "Не выбрано"

            if (value) {
              selected = value.format("YYYY-MM-DD")
            }

            return (
              <div tabIndex={0} className={"sp-item"}>
                {(this.icon ? <FontAwesome name={this.icon} /> : null)}
                {selected}
              </div>
            )
          }
        }>
      </DatePicker>
    )
  }
}

export interface MenuItemEnumModelItem {
  id: any
  name: string
  icon?: string
}

export interface IMenuItemEnumModel {
  get?: (item: IMenuItemModel, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => MenuItemEnumModelItem
  set?: (item: IMenuItemModel, newValue: MenuItemEnumModelItem, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => void
  value?: MenuItemEnumModelItem
  enums?: MenuItemEnumModelItem[]
}

export class MenuItemEnumModel implements IMenuItemEnumModel, IMenuItemModel {
  get?: (item: IMenuItemModel, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => MenuItemEnumModelItem
  set?: (item: IMenuItemModel, newValue: MenuItemEnumModelItem, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => void
  value?: MenuItemEnumModelItem
  enums?: MenuItemEnumModelItem[]

  constructor(initializer?: IMenuItemEnumModel) {
    if (initializer === undefined) {
      return;
    }
    Object.keys(initializer).forEach((key, index) => {
      this[key] = initializer[key];
    });
  }

  render(name: string, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) {
    if (this.get) {
      this.value = this.get(this, content, context, page, application)
    }
    let valueLink = {
      value: this.value,
      requestChange: (newValue) => {
        if (this.set) {
          this.set(this, newValue, content, context, page, application)
        }
        else {
          this.value = newValue
          page.update()
        }
      }
    }

    let enums = this.enums.map((row) => {
      return (
        <div key={row.id} className={"sp-item" + (valueLink.value === row || (valueLink.value && valueLink.value.id == row.id) ? ' active' : '')}
          onClick={()=>{valueLink.requestChange(row)}}>
          {row.name}
        </div>
      )
    })

    return (
      <div key={name}>
        <div className="sp-enum">
          {enums}
        </div>
      </div>
    )
  }
}