"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const React = require('react');
const FontAwesome = require('react-fontawesome');
const Core_1 = require('../Core');
class MenuItemSpaceModel {
    render(name, content, context, page, application) {
        return (React.createElement("div", {key: name, className: "sp-space"}));
    }
}
exports.MenuItemSpaceModel = MenuItemSpaceModel;
class MenuItemDividerModel {
    render(name, content, context, page, application) {
        return (React.createElement("div", {key: name, className: "sp-divider"}));
    }
}
exports.MenuItemDividerModel = MenuItemDividerModel;
class MenuItemHeaderModel {
    constructor(initializer) {
        if (initializer === undefined) {
            return;
        }
        Object.keys(initializer).forEach((key, index) => {
            this[key] = initializer[key];
        });
    }
    render(name, content, context, page, application) {
        return (React.createElement("div", {key: name, className: "sp-header"}, this.text));
    }
}
exports.MenuItemHeaderModel = MenuItemHeaderModel;
class MenuItemInputModel {
    constructor(initializer) {
        if (initializer === undefined) {
            return;
        }
        Object.keys(initializer).forEach((key, index) => {
            this[key] = initializer[key];
        });
    }
    render(name, content, context, page, application) {
        let requestChange = (event) => {
            this.value = event.target.value;
            page.update();
        };
        return (React.createElement("div", {key: name, className: "sp-input"}, 
            (this.icon ? React.createElement(FontAwesome, {name: this.icon}) : null), 
            React.createElement("input", {type: "text", placeholder: this.placeholder, value: this.value, onChange: requestChange})));
    }
}
exports.MenuItemInputModel = MenuItemInputModel;
class MenuItemButtonModel {
    constructor(initializer) {
        if (initializer === undefined) {
            return;
        }
        Object.keys(initializer).forEach((key, index) => {
            this[key] = initializer[key];
        });
    }
    render(name, content, context, page, application) {
        return (React.createElement("div", {key: name, className: "sp-item" + (this.loading ? ' loading' : ''), onClick: () => __awaiter(this, void 0, void 0, function* () {
            this.loading = true;
            page.update();
            try {
                yield this.on_click(context, page, application);
            }
            catch (message) {
                console.error(message);
            }
            this.loading = false;
            page.update();
        })}, 
            (this.icon ? React.createElement(FontAwesome, {name: this.icon}) : null), 
            this.label));
    }
}
exports.MenuItemButtonModel = MenuItemButtonModel;
class MenuItemDateModel {
    constructor(initializer) {
        if (initializer === undefined) {
            return;
        }
        Object.keys(initializer).forEach((key, index) => {
            this[key] = initializer[key];
        });
    }
    render(name, content, context, page, application) {
        if (this.get) {
            this.value = this.get(content, context, page, application);
        }
        let valueLink = {
            value: this.value,
            requestChange: (newValue) => {
                if (this.set) {
                    this.set(newValue, content, context, page, application);
                }
                else {
                    this.value = newValue;
                    page.update();
                }
            }
        };
        return (React.createElement(Core_1.DatePicker, {key: name, valueLink: valueLink, render: (value, clear, format) => {
            var selected = "Не выбрано";
            if (value) {
                selected = value.format("YYYY-MM-DD");
            }
            return (React.createElement("div", {tabIndex: 0, className: "sp-item"}, 
                (this.icon ? React.createElement(FontAwesome, {name: this.icon}) : null), 
                selected));
        }}));
    }
}
exports.MenuItemDateModel = MenuItemDateModel;
class MenuItemEnumModel {
    constructor(initializer) {
        if (initializer === undefined) {
            return;
        }
        Object.keys(initializer).forEach((key, index) => {
            this[key] = initializer[key];
        });
    }
    render(name, content, context, page, application) {
        if (this.get) {
            this.value = this.get(this, content, context, page, application);
        }
        let valueLink = {
            value: this.value,
            requestChange: (newValue) => {
                if (this.set) {
                    this.set(this, newValue, content, context, page, application);
                }
                else {
                    this.value = newValue;
                    page.update();
                }
            }
        };
        let enums = this.enums.map((row) => {
            return (React.createElement("div", {key: row.id, className: "sp-item" + (valueLink.value === row || (valueLink.value && valueLink.value.id == row.id) ? ' active' : ''), onClick: () => { valueLink.requestChange(row); }}, row.name));
        });
        return (React.createElement("div", {key: name}, 
            React.createElement("div", {className: "sp-enum"}, enums)
        ));
    }
}
exports.MenuItemEnumModel = MenuItemEnumModel;
//# sourceMappingURL=Menu.js.map