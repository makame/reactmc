import * as React from 'react'
import * as moment from 'moment';

import FontAwesome = require('react-fontawesome')

import {
  IValueLink, Map, MapPro, ContentModel, ContextModel, RouteModel, ApplicationModel,
  Select, ISelectData, SelectDataType, SelectSearchType,
  MSelect, IMSelectData,
  Trigger,
  DatePicker,
  PeriodPicker
} from '../Core'

import { Form, IFormLink, IFormItemModel, FormItemModel } from '../Core/Components/Form';

export interface IFormItemGroupModel extends IFormItemModel<Map<any>> {
  items: Map<FormItemModel<any>>
}

export class FormItemGroupModel extends FormItemModel<Map<any>> {
  items: Map<FormItemModel<any>>

  constructor(initializer?: IFormItemGroupModel) {
    super()
    if (initializer === undefined) {
      return;
    }
    Object.keys(initializer).forEach((key, index) => {
      this[key] = initializer[key];
    });
  }

  render(name: string, valueLink: IValueLink<Map<any>>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel, containerFactory: (contaner: string, inner: JSX.Element, context: ContextModel) => JSX.Element): JSX.Element {
    return (
      React.createElement(Form, { value: valueLink.value, form: this.items, model: { value: valueLink.value, form: this.items, parent: model }, content: content, context, page, application, containerFactory })
    )
  }
}

export interface IFormItemInputAddonTextModel {
  icon?: string
  text?: string
  class?: string
}

export interface IFormItemInputAddonModelRender {
  render(name: string, field: FormItemModel<string>, valueLink: IValueLink<string>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element
}

export class FormItemInputAddonTextModel implements IFormItemInputAddonModelRender, IFormItemInputAddonTextModel {
  icon?: string
  text?: string
  class?: string

  constructor(initializer?: IFormItemInputAddonTextModel) {
    if (initializer === undefined) {
      return;
    }
    Object.keys(initializer).forEach((key, index) => {
      this[key] = initializer[key];
    });
  }

  render(name: string, field: FormItemModel<string>, valueLink: IValueLink<string>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {
    let icon = this.icon ? <FontAwesome name={this.icon}/> : null
    let text = this.text

    return (<div key={name} className={"sp-input-group-addon" + (this.class ? (" " + this.class) : '')}>{icon}{text}</div>)
  }
}

export interface IFormItemInputAddonButtonModel {
  icon?: string
  text?: string
  class?: string
}

export class FormItemInputAddonButtonModel implements IFormItemInputAddonModelRender, IFormItemInputAddonButtonModel {
  icon?: string
  text?: string
  on_click?: (name: string, field: FormItemModel<string>, valueLink: IValueLink<string>, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => Promise<void> | void
  loading?: boolean
  class?: string

  enabled?(name: string, field: FormItemModel<string>, valueLink: IValueLink<string>, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): boolean

  constructor(initializer?: IFormItemInputAddonButtonModel) {
    if (initializer === undefined) {
      return;
    }
    Object.keys(initializer).forEach((key, index) => {
      this[key] = initializer[key];
    });
  }

  render(name: string, field: FormItemModel<string>, valueLink: IValueLink<string>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {
    let icon = this.icon ? <FontAwesome name={this.icon}/> : null
    let text = this.text
    let btn_enabled = !enabled ? false : (this.enabled ? this.enabled(name, field, valueLink, model, content, context, page, application) : true)
    
    return (
      <div key={name} tabIndex={enabled ? 0 : -1} className={"sp-input-group-btn" + (!btn_enabled ? " disabled" : '') + (this.loading ? " loading" : '') + (this.class ? (" " + this.class) : '')}
        onClick={
          async () => {
            if (this.on_click) {
              this.loading = true
              page.update()
              await this.on_click(name, field, valueLink, model, content, context, page, application)
              this.loading = true
              page.update()
            }
          }
        }>
        {icon}
        {text ? (<span>{text}</span>) : null}
      </div>
    )
  }
}

export interface IFormItemInputModel extends IFormItemModel<string> {
  left?: Map<IFormItemInputAddonModelRender>
  right?: Map<IFormItemInputAddonModelRender>
  placeholder?: string
  icon?: string
  class?: string
  type?: string
}

export class FormItemInputModel extends FormItemModel<string> implements IFormItemInputModel {
  left?: Map<IFormItemInputAddonModelRender>
  right?: Map<IFormItemInputAddonModelRender>
  placeholder?: string
  icon?: string
  class?: string
  type?: string = "text"

  constructor(initializer?: IFormItemInputModel) {
    super()
    if (initializer === undefined) {
      return;
    }
    Object.keys(initializer).forEach((key, index) => {
      this[key] = initializer[key];
    });
  }

  render(name: string, valueLink: IValueLink<string>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) {
    let left = []
    let right = []

    if (this.left) {
      MapPro.each(this.left, (addon, index) => {
        left.push(addon.render(index, this, valueLink, enabled, model, content, context, page, application))
      })
    }

    if (this.right) {
      MapPro.each(this.right, (addon, index) => {
        right.push(addon.render(index, this, valueLink, enabled, model, content, context, page, application))
      })
    }

    let icon = this.icon ? <div className={"sp-input-icon"}><FontAwesome name={this.icon}/></div> : null

    return (
      <div className={"sp-input-group" + (this.class ? (" " + this.class) : '') + (this.messages.error ? ' error' : '') + (enabled ? '' : ' disabled')}>
        {left}
        {icon}
        <input tabIndex={enabled ? 0 : -1} type={this.type} className="sp-form-control" placeholder={this.placeholder} value={valueLink.value} onChange={(event) => valueLink.requestChange((event.target as HTMLInputElement).value)}/>
        {right}
      </div>
    )
  }
}

export interface IFormItemRowAddonTextModel {
  icon?: string
  text?: string
  class?: string
}

export interface IFormItemRowAddonModelRender {
  render(name: string, field: FormItemModel<Map<any>>, valueLink: IValueLink<Map<any>>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element
}

export class FormItemRowAddonTextModel implements IFormItemRowAddonModelRender, IFormItemRowAddonTextModel {
  icon?: string
  text?: string
  class?: string

  constructor(initializer?: IFormItemRowAddonTextModel) {
    if (initializer === undefined) {
      return;
    }
    Object.keys(initializer).forEach((key, index) => {
      this[key] = initializer[key];
    });
  }

  render(name: string, field: FormItemModel<Map<any>>, valueLink: IValueLink<Map<any>>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {
    let icon = this.icon ? <FontAwesome name={this.icon}/> : null
    let text = this.text

    return (<div key={name} className={"sp-input-group-addon" + (this.class ? (" " + this.class) : '')}>{icon}{text}</div>)
  }
}

export interface IFormItemRowAddonButtonModel {
  icon?: string
  text?: string
  on_click?: (name: string, field: FormItemModel<Map<any>>, valueLink: IValueLink<Map<any>>, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => Promise<void> | void
  loading?: boolean
  class?: string

  enabled?(name: string, field: FormItemModel<Map<any>>, valueLink: IValueLink<Map<any>>, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): boolean
}

export class FormItemRowAddonButtonModel implements IFormItemRowAddonModelRender, IFormItemRowAddonButtonModel {
  icon?: string
  text?: string
  on_click?: (name: string, field: FormItemModel<Map<any>>, valueLink: IValueLink<Map<any>>, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => Promise<void> | void
  loading?: boolean
  class?: string

  enabled?(name: string, field: FormItemModel<Map<any>>, valueLink: IValueLink<Map<any>>, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): boolean

  render(name: string, field: FormItemModel<Map<any>>, valueLink: IValueLink<Map<any>>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {
    let icon = this.icon ? <FontAwesome name={this.icon}/> : null
    let text = this.text
    let btn_enabled = !enabled ? false : (this.enabled ? this.enabled(name, field, valueLink, model, content, context, page, application) : true)
    
    return (
      <div key={name} tabIndex={btn_enabled ? 0 : -1} className={"sp-input-group-btn" + (!btn_enabled ? " disabled" : '') + (this.loading ? " loading" : '') + (this.class ? (" " + this.class) : '')}
        onClick={
          async () => {
            if (this.on_click) {
              this.loading = true
              page.update()
              await this.on_click(name, field, valueLink, model, content, context, page, application)
              this.loading = true
              page.update()
            }
          }
        }>
        {icon}
        {text ? (<span>{text}</span>) : null}
      </div>
    )
  }
}

export interface IFormItemRowAddonInputModel {
  icon?: string
  placeholder?: string
  class?: string
  type?: string

  enabled?(name: string, field: FormItemModel<Map<any>>, valueLink: IValueLink<Map<any>>, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): boolean
}

export class FormItemRowAddonInputModel implements IFormItemRowAddonModelRender, IFormItemRowAddonInputModel {
  icon?: string
  placeholder?: string
  class?: string
  type?: string = "text"

  render(name: string, field: FormItemModel<Map<any>>, valueLink: IValueLink<Map<any>>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {
    let icon = this.icon ? <FontAwesome name={this.icon}/> : null

    return (
      <input key={name} tabIndex={enabled ? 0 : -1} type={this.type} className="sp-form-control" value={valueLink.value[name] as string} placeholder={this.placeholder} onChange={(event) => {
          valueLink.value[name] = (event.target as HTMLInputElement).value
          valueLink.requestChange(valueLink.value)
        }
      }/>
    )
  }
}

export class FormItemRowModel extends FormItemModel<Map<any>> {
  items?: Map<IFormItemRowAddonModelRender>
  class?: string

  render(name: string, valueLink: IValueLink<Map<any>>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) {
    let items = []

    MapPro.each(this.items, (addon, addon_name) => {
      items.push(addon.render(addon_name, this, valueLink, enabled, model, content, context, page, application))
    })

    return (
      <div className={"sp-input-group" + (this.class ? (' ' + this.class) : '') + (this.messages.error ? ' error' : '') + (enabled ? '' : ' disabled')}>
        {items}
      </div>
    )
  }
}

export interface IFormItemButtonModelRender {
  if?: (name: string, field: FormItemModel<Map<any>>, valueLink: IValueLink<Map<any>>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => boolean
  render(name: string, field: FormItemModel<Map<any>>, valueLink: IValueLink<Map<any>>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element
}

export interface IFormItemButtonModel {
  icon?: string
  text?: string
  on_click?: (name: string, field: FormItemModel<Map<any>>, valueLink: IValueLink<Map<any>>, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => Promise<void> | void
  loading?: boolean
  class?: string

  enabled?(name: string, field: FormItemModel<Map<any>>, valueLink: IValueLink<Map<any>>, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): boolean
}

export class FormItemButtonModel implements IFormItemButtonModelRender, IFormItemButtonModel {
  icon?: string
  text?: string
  on_click?: (name: string, field: FormItemModel<Map<any>>, valueLink: IValueLink<Map<any>>, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => Promise<void> | void
  loading?: boolean
  class?: string

  if?: (name: string, field: FormItemModel<Map<any>>, valueLink: IValueLink<Map<any>>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => boolean
  enabled?(name: string, field: FormItemModel<Map<any>>, valueLink: IValueLink<Map<any>>, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): boolean

  constructor(initializer?: IFormItemButtonModel) {
    if (initializer === undefined) {
      return;
    }
    Object.keys(initializer).forEach((key, index) => {
      this[key] = initializer[key];
    });
  }
  
  render(name: string, field: FormItemModel<Map<any>>, valueLink: IValueLink<Map<any>>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {
    let icon = this.icon ? <FontAwesome name={this.icon}/> : null
    let text = this.text
    let btn_enabled = !enabled ? false : (this.enabled ? this.enabled(name, field, valueLink, model, content, context, page, application) : true)

    return (
      <div key={name} tabIndex={btn_enabled ? 0 : -1} className={"btn" + (!btn_enabled ? " disabled" : '') + (this.loading ? " loading" : '') + (this.class ? (" " + this.class) : '')}
        onClick={
          async () => {
            if (this.on_click) {
              this.loading = true
              page.update()
              await this.on_click(name, field, valueLink, model, content, context, page, application)
              this.loading = true
              page.update()
            }
          }
        }>
        {icon}
        {text ? (<span>{text}</span>) : null}
      </div>
    )
  }
}

export interface IFormItemButtonsModel extends IFormItemModel<Map<any>> {
  items?: Map<IFormItemButtonModelRender>
  class?: string
}

export class FormItemButtonsModel extends FormItemModel<Map<any>> implements IFormItemButtonsModel{
  items?: Map<IFormItemButtonModelRender>
  class?: string

  constructor(initializer?: IFormItemButtonsModel) {
    super()
    if (initializer === undefined) {
      return;
    }
    Object.keys(initializer).forEach((key, index) => {
      this[key] = initializer[key];
    });
  }

  render(name: string, valueLink: IValueLink<Map<any>>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) {
    let items = []

    MapPro.each(this.items, (button, button_name) => {
      if (!button.if || button.if(button_name, this, valueLink, enabled, model, content, context, page, application)) {
        items.push(button.render(button_name, this, valueLink, enabled, model, content, context, page, application))
      }
    })

    return (
      <div className={"sp-inlines" + (this.class ? (' ' + this.class) : '') + (this.messages.error ? ' error' : '') + (enabled ? '' : ' disabled')}>
        {items}
      </div>
    )
  }
}

export interface IFormItemTriggerModel extends IFormItemModel<boolean> {
  icon?: string
  class?: string
  text?: string
}

export class FormItemTriggerModel extends FormItemModel<boolean> implements IFormItemTriggerModel {
  icon?: string
  class?: string
  text?: string

  constructor(initializer?: IFormItemTriggerModel) {
    super()
    if (initializer === undefined) {
      return;
    }
    Object.keys(initializer).forEach((key, index) => {
      this[key] = initializer[key];
    });
  }

  render(name: string, valueLink: IValueLink<boolean>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) {
    return (
      <Trigger valueLink={valueLink} icon={this.icon} text={this.text} className={(this.class ? (' ' + this.class) : '') + (this.messages.error ? 'error' : '') + (enabled ? '' : ' disabled')}/>
    )
  }
}

export interface IFormItemSelectAddonTextModel {
  icon?: string
  text?: string
  class?: string
}

export interface IFormItemSelectAddonModelRender {
  render(name: string, field: FormItemModel<ISelectData>, valueLink: IValueLink<ISelectData>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element
}

export class FormItemSelectAddonTextModel implements IFormItemSelectAddonModelRender, IFormItemSelectAddonTextModel {
  icon?: string
  text?: string
  class?: string

  constructor(initializer?: IFormItemSelectAddonTextModel) {
    if (initializer === undefined) {
      return;
    }
    Object.keys(initializer).forEach((key, index) => {
      this[key] = initializer[key];
    });
  }

  render(name: string, field: FormItemModel<ISelectData>, valueLink: IValueLink<ISelectData>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {
    let icon = this.icon ? <FontAwesome name={this.icon}/> : null
    let text = this.text

    return (<div key={name} className={"sp-input-group-addon" + (this.class ? (" " + this.class) : '')}>{icon}{text}</div>)
  }
}

export interface IFormItemSelectAddonButtonModel {
  icon?: string
  text?: string
  class?: string
}

export class FormItemSelectAddonButtonModel implements IFormItemSelectAddonModelRender, IFormItemSelectAddonButtonModel {
  icon?: string
  text?: string
  on_click?: (name: string, field: FormItemModel<ISelectData>, valueLink: IValueLink<ISelectData>, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => Promise<void> | void
  loading?: boolean
  class?: string

  enabled?(name: string, field: FormItemModel<ISelectData>, valueLink: IValueLink<ISelectData>, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): boolean

  constructor(initializer?: IFormItemSelectAddonButtonModel) {
    if (initializer === undefined) {
      return;
    }
    Object.keys(initializer).forEach((key, index) => {
      this[key] = initializer[key];
    });
  }

  render(name: string, field: FormItemModel<ISelectData>, valueLink: IValueLink<ISelectData>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {
    let icon = this.icon ? <FontAwesome name={this.icon}/> : null
    let text = this.text
    let btn_enabled = !enabled ? false : (this.enabled ? this.enabled(name, field, valueLink, model, content, context, page, application) : true)
    
    return (
      <div key={name} tabIndex={enabled ? 0 : -1} className={"sp-input-group-btn" + (!btn_enabled ? " disabled" : '') + (this.loading ? " loading" : '') + (this.class ? (" " + this.class) : '')}
        onClick={
          async () => {
            if (this.on_click) {
              this.loading = true
              page.update()
              await this.on_click(name, field, valueLink, model, content, context, page, application)
              this.loading = true
              page.update()
            }
          }
        }>
        {icon}
        {text ? (<span>{text}</span>) : null}
      </div>
    )
  }
}

export interface IFormItemSelectModel extends IFormItemModel<ISelectData> {
  left?: Map<IFormItemSelectAddonModelRender>
  right?: Map<IFormItemSelectAddonModelRender>
  icon?: string
  class?: string
  
  data?: ISelectData[]
  data_type?: SelectDataType
  search?: SelectSearchType
  request?: (search_string: string) => Promise<ISelectData[]> | ISelectData[]
  default?: ISelectData
  asInput?: boolean
  render_element?: (value: ISelectData, clear: () => void, updating: boolean) => JSX.Element
}

export class FormItemSelectModel extends FormItemModel<ISelectData> implements IFormItemSelectModel {
  left?: Map<IFormItemSelectAddonModelRender>
  right?: Map<IFormItemSelectAddonModelRender>
  icon?: string
  class?: string
  
  data?: ISelectData[]
  data_type?: SelectDataType
  search?: SelectSearchType
  request?: (search_string: string) => Promise<ISelectData[]> | ISelectData[]
  default?: ISelectData
  as_input?: boolean
  render_element?: (value: ISelectData, clear: () => void, updating: boolean) => JSX.Element

  constructor(initializer?: IFormItemSelectModel) {
    super()
    if (initializer === undefined) {
      return;
    }
    Object.keys(initializer).forEach((key, index) => {
      this[key] = initializer[key];
    });
  }

  render(name: string, valueLink: IValueLink<ISelectData>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) {
    let left: JSX.Element[] = []
    let right: JSX.Element[] = []

    if (this.left) {
      MapPro.each(this.left, (addon, index) => {
        left.push(addon.render(index, this, valueLink, enabled, model, content, context, page, application))
      })
    }

    if (this.right) {
      MapPro.each(this.right, (addon, index) => {
        right.push(addon.render(index, this, valueLink, enabled, model, content, context, page, application))
      })
    }
    
    return (
      <Select valueLink={valueLink} icon={this.icon} className={(this.class ? (' ' + this.class) : '') + (this.messages.error ? 'error' : '') + (enabled ? '' : ' disabled')}
        data={this.data} data_type={this.data_type} search={this.search} request={this.request} default={this.default} asInput={this.as_input} render={this.render_element} left={left} right={right}/>
    )
  }
}

export interface IFormItemMSelectAddonTextModel {
  icon?: string
  text?: string
  class?: string
}

export interface IFormItemMSelectAddonModelRender {
  render(name: string, field: FormItemModel<IMSelectData[]>, valueLink: IValueLink<IMSelectData[]>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element
}

export class FormItemMSelectAddonTextModel implements IFormItemMSelectAddonModelRender, IFormItemMSelectAddonTextModel {
  icon?: string
  text?: string
  class?: string

  constructor(initializer?: IFormItemMSelectAddonTextModel) {
    if (initializer === undefined) {
      return;
    }
    Object.keys(initializer).forEach((key, index) => {
      this[key] = initializer[key];
    });
  }

  render(name: string, field: FormItemModel<IMSelectData[]>, valueLink: IValueLink<IMSelectData[]>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {
    let icon = this.icon ? <FontAwesome name={this.icon}/> : null
    let text = this.text

    return (<div key={name} className={"sp-input-group-addon" + (this.class ? (" " + this.class) : '')}>{icon}{text}</div>)
  }
}

export interface IFormItemMSelectAddonButtonModel {
  icon?: string
  text?: string
  class?: string
}

export class FormItemMSelectAddonButtonModel implements IFormItemMSelectAddonModelRender, IFormItemMSelectAddonButtonModel {
  icon?: string
  text?: string
  on_click?: (name: string, field: FormItemModel<IMSelectData[]>, valueLink: IValueLink<IMSelectData[]>, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => Promise<void> | void
  loading?: boolean
  class?: string

  enabled?(name: string, field: FormItemModel<IMSelectData[]>, valueLink: IValueLink<IMSelectData[]>, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): boolean

  constructor(initializer?: IFormItemMSelectAddonButtonModel) {
    if (initializer === undefined) {
      return;
    }
    Object.keys(initializer).forEach((key, index) => {
      this[key] = initializer[key];
    });
  }

  render(name: string, field: FormItemModel<IMSelectData[]>, valueLink: IValueLink<IMSelectData[]>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {
    let icon = this.icon ? <FontAwesome name={this.icon}/> : null
    let text = this.text
    let btn_enabled = !enabled ? false : (this.enabled ? this.enabled(name, field, valueLink, model, content, context, page, application) : true)
    
    return (
      <div key={name} tabIndex={enabled ? 0 : -1} className={"sp-input-group-btn" + (!btn_enabled ? " disabled" : '') + (this.loading ? " loading" : '') + (this.class ? (" " + this.class) : '')}
        onClick={
          async () => {
            if (this.on_click) {
              this.loading = true
              page.update()
              await this.on_click(name, field, valueLink, model, content, context, page, application)
              this.loading = true
              page.update()
            }
          }
        }>
        {icon}
        {text ? (<span>{text}</span>) : null}
      </div>
    )
  }
}

export interface IFormItemMSelectModel extends IFormItemModel<IMSelectData[]> {
  left?: Map<IFormItemMSelectAddonModelRender>
  right?: Map<IFormItemMSelectAddonModelRender>
  icon?: string
  class?: string
  
  data?: IMSelectData[]
  data_type?: SelectDataType
  search?: SelectSearchType
  request?: (search_string: string) => Promise<IMSelectData[]> | IMSelectData[]
  default?: IMSelectData[]
  asInput?: boolean
  render_element?: (value: IMSelectData[], clear: () => void, updating: boolean) => JSX.Element
}

export class FormItemMSelectModel extends FormItemModel<IMSelectData[]> implements IFormItemMSelectModel {
  left?: Map<IFormItemMSelectAddonModelRender>
  right?: Map<IFormItemMSelectAddonModelRender>
  icon?: string
  class?: string
  
  data?: IMSelectData[]
  data_type?: SelectDataType
  search?: SelectSearchType
  request?: (search_string: string) => Promise<IMSelectData[]> | IMSelectData[]
  default?: IMSelectData[]
  as_input?: boolean
  render_element?: (value: IMSelectData[], clear: () => void, updating: boolean) => JSX.Element

  constructor(initializer?: IFormItemMSelectModel) {
    super()
    if (initializer === undefined) {
      return;
    }
    Object.keys(initializer).forEach((key, index) => {
      this[key] = initializer[key];
    });
  }

  render(name: string, valueLink: IValueLink<IMSelectData[]>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) {
    let left: JSX.Element[] = []
    let right: JSX.Element[] = []

    if (this.left) {
      MapPro.each(this.left, (addon, index) => {
        left.push(addon.render(index, this, valueLink, enabled, model, content, context, page, application))
      })
    }

    if (this.right) {
      MapPro.each(this.right, (addon, index) => {
        right.push(addon.render(index, this, valueLink, enabled, model, content, context, page, application))
      })
    }
    
    return (
      <MSelect valueLink={valueLink} icon={this.icon} className={(this.class ? (' ' + this.class) : '') + (this.messages.error ? 'error' : '') + (enabled ? '' : ' disabled')}
        data={this.data} data_type={this.data_type} search={this.search} request={this.request} asInput={this.as_input} render={this.render_element} left={left} right={right}/>
    )
  }
}