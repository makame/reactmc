"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const React = require('react');
const FontAwesome = require('react-fontawesome');
const Core_1 = require('../Core');
const Form_1 = require('../Core/Components/Form');
class FormItemGroupModel extends Form_1.FormItemModel {
    constructor(initializer) {
        super();
        if (initializer === undefined) {
            return;
        }
        Object.keys(initializer).forEach((key, index) => {
            this[key] = initializer[key];
        });
    }
    render(name, valueLink, enabled, model, content, context, page, application, containerFactory) {
        return (React.createElement(Form_1.Form, { value: valueLink.value, form: this.items, model: { value: valueLink.value, form: this.items, parent: model }, content: content, context, page, application, containerFactory }));
    }
}
exports.FormItemGroupModel = FormItemGroupModel;
class FormItemInputAddonTextModel {
    constructor(initializer) {
        if (initializer === undefined) {
            return;
        }
        Object.keys(initializer).forEach((key, index) => {
            this[key] = initializer[key];
        });
    }
    render(name, field, valueLink, enabled, model, content, context, page, application) {
        let icon = this.icon ? React.createElement(FontAwesome, {name: this.icon}) : null;
        let text = this.text;
        return (React.createElement("div", {key: name, className: "sp-input-group-addon" + (this.class ? (" " + this.class) : '')}, 
            icon, 
            text));
    }
}
exports.FormItemInputAddonTextModel = FormItemInputAddonTextModel;
class FormItemInputAddonButtonModel {
    constructor(initializer) {
        if (initializer === undefined) {
            return;
        }
        Object.keys(initializer).forEach((key, index) => {
            this[key] = initializer[key];
        });
    }
    render(name, field, valueLink, enabled, model, content, context, page, application) {
        let icon = this.icon ? React.createElement(FontAwesome, {name: this.icon}) : null;
        let text = this.text;
        let btn_enabled = !enabled ? false : (this.enabled ? this.enabled(name, field, valueLink, model, content, context, page, application) : true);
        return (React.createElement("div", {key: name, tabIndex: enabled ? 0 : -1, className: "sp-input-group-btn" + (!btn_enabled ? " disabled" : '') + (this.loading ? " loading" : '') + (this.class ? (" " + this.class) : ''), onClick: () => __awaiter(this, void 0, void 0, function* () {
            if (this.on_click) {
                this.loading = true;
                page.update();
                yield this.on_click(name, field, valueLink, model, content, context, page, application);
                this.loading = true;
                page.update();
            }
        })}, 
            icon, 
            text ? (React.createElement("span", null, text)) : null));
    }
}
exports.FormItemInputAddonButtonModel = FormItemInputAddonButtonModel;
class FormItemInputModel extends Form_1.FormItemModel {
    constructor(initializer) {
        super();
        this.type = "text";
        if (initializer === undefined) {
            return;
        }
        Object.keys(initializer).forEach((key, index) => {
            this[key] = initializer[key];
        });
    }
    render(name, valueLink, enabled, model, content, context, page, application) {
        let left = [];
        let right = [];
        if (this.left) {
            Core_1.MapPro.each(this.left, (addon, index) => {
                left.push(addon.render(index, this, valueLink, enabled, model, content, context, page, application));
            });
        }
        if (this.right) {
            Core_1.MapPro.each(this.right, (addon, index) => {
                right.push(addon.render(index, this, valueLink, enabled, model, content, context, page, application));
            });
        }
        let icon = this.icon ? React.createElement("div", {className: "sp-input-icon"}, 
            React.createElement(FontAwesome, {name: this.icon})
        ) : null;
        return (React.createElement("div", {className: "sp-input-group" + (this.class ? (" " + this.class) : '') + (this.messages.error ? ' error' : '') + (enabled ? '' : ' disabled')}, 
            left, 
            icon, 
            React.createElement("input", {tabIndex: enabled ? 0 : -1, type: this.type, className: "sp-form-control", placeholder: this.placeholder, value: valueLink.value, onChange: (event) => valueLink.requestChange(event.target.value)}), 
            right));
    }
}
exports.FormItemInputModel = FormItemInputModel;
class FormItemRowAddonTextModel {
    constructor(initializer) {
        if (initializer === undefined) {
            return;
        }
        Object.keys(initializer).forEach((key, index) => {
            this[key] = initializer[key];
        });
    }
    render(name, field, valueLink, enabled, model, content, context, page, application) {
        let icon = this.icon ? React.createElement(FontAwesome, {name: this.icon}) : null;
        let text = this.text;
        return (React.createElement("div", {key: name, className: "sp-input-group-addon" + (this.class ? (" " + this.class) : '')}, 
            icon, 
            text));
    }
}
exports.FormItemRowAddonTextModel = FormItemRowAddonTextModel;
class FormItemRowAddonButtonModel {
    render(name, field, valueLink, enabled, model, content, context, page, application) {
        let icon = this.icon ? React.createElement(FontAwesome, {name: this.icon}) : null;
        let text = this.text;
        let btn_enabled = !enabled ? false : (this.enabled ? this.enabled(name, field, valueLink, model, content, context, page, application) : true);
        return (React.createElement("div", {key: name, tabIndex: btn_enabled ? 0 : -1, className: "sp-input-group-btn" + (!btn_enabled ? " disabled" : '') + (this.loading ? " loading" : '') + (this.class ? (" " + this.class) : ''), onClick: () => __awaiter(this, void 0, void 0, function* () {
            if (this.on_click) {
                this.loading = true;
                page.update();
                yield this.on_click(name, field, valueLink, model, content, context, page, application);
                this.loading = true;
                page.update();
            }
        })}, 
            icon, 
            text ? (React.createElement("span", null, text)) : null));
    }
}
exports.FormItemRowAddonButtonModel = FormItemRowAddonButtonModel;
class FormItemRowAddonInputModel {
    constructor() {
        this.type = "text";
    }
    render(name, field, valueLink, enabled, model, content, context, page, application) {
        let icon = this.icon ? React.createElement(FontAwesome, {name: this.icon}) : null;
        return (React.createElement("input", {key: name, tabIndex: enabled ? 0 : -1, type: this.type, className: "sp-form-control", value: valueLink.value[name], placeholder: this.placeholder, onChange: (event) => {
            valueLink.value[name] = event.target.value;
            valueLink.requestChange(valueLink.value);
        }}));
    }
}
exports.FormItemRowAddonInputModel = FormItemRowAddonInputModel;
class FormItemRowModel extends Form_1.FormItemModel {
    render(name, valueLink, enabled, model, content, context, page, application) {
        let items = [];
        Core_1.MapPro.each(this.items, (addon, addon_name) => {
            items.push(addon.render(addon_name, this, valueLink, enabled, model, content, context, page, application));
        });
        return (React.createElement("div", {className: "sp-input-group" + (this.class ? (' ' + this.class) : '') + (this.messages.error ? ' error' : '') + (enabled ? '' : ' disabled')}, items));
    }
}
exports.FormItemRowModel = FormItemRowModel;
class FormItemButtonModel {
    constructor(initializer) {
        if (initializer === undefined) {
            return;
        }
        Object.keys(initializer).forEach((key, index) => {
            this[key] = initializer[key];
        });
    }
    render(name, field, valueLink, enabled, model, content, context, page, application) {
        let icon = this.icon ? React.createElement(FontAwesome, {name: this.icon}) : null;
        let text = this.text;
        let btn_enabled = !enabled ? false : (this.enabled ? this.enabled(name, field, valueLink, model, content, context, page, application) : true);
        return (React.createElement("div", {key: name, tabIndex: btn_enabled ? 0 : -1, className: "btn" + (!btn_enabled ? " disabled" : '') + (this.loading ? " loading" : '') + (this.class ? (" " + this.class) : ''), onClick: () => __awaiter(this, void 0, void 0, function* () {
            if (this.on_click) {
                this.loading = true;
                page.update();
                yield this.on_click(name, field, valueLink, model, content, context, page, application);
                this.loading = true;
                page.update();
            }
        })}, 
            icon, 
            text ? (React.createElement("span", null, text)) : null));
    }
}
exports.FormItemButtonModel = FormItemButtonModel;
class FormItemButtonsModel extends Form_1.FormItemModel {
    constructor(initializer) {
        super();
        if (initializer === undefined) {
            return;
        }
        Object.keys(initializer).forEach((key, index) => {
            this[key] = initializer[key];
        });
    }
    render(name, valueLink, enabled, model, content, context, page, application) {
        let items = [];
        Core_1.MapPro.each(this.items, (button, button_name) => {
            if (!button.if || button.if(button_name, this, valueLink, enabled, model, content, context, page, application)) {
                items.push(button.render(button_name, this, valueLink, enabled, model, content, context, page, application));
            }
        });
        return (React.createElement("div", {className: "sp-inlines" + (this.class ? (' ' + this.class) : '') + (this.messages.error ? ' error' : '') + (enabled ? '' : ' disabled')}, items));
    }
}
exports.FormItemButtonsModel = FormItemButtonsModel;
class FormItemTriggerModel extends Form_1.FormItemModel {
    constructor(initializer) {
        super();
        if (initializer === undefined) {
            return;
        }
        Object.keys(initializer).forEach((key, index) => {
            this[key] = initializer[key];
        });
    }
    render(name, valueLink, enabled, model, content, context, page, application) {
        return (React.createElement(Core_1.Trigger, {valueLink: valueLink, icon: this.icon, text: this.text, className: (this.class ? (' ' + this.class) : '') + (this.messages.error ? 'error' : '') + (enabled ? '' : ' disabled')}));
    }
}
exports.FormItemTriggerModel = FormItemTriggerModel;
class FormItemSelectAddonTextModel {
    constructor(initializer) {
        if (initializer === undefined) {
            return;
        }
        Object.keys(initializer).forEach((key, index) => {
            this[key] = initializer[key];
        });
    }
    render(name, field, valueLink, enabled, model, content, context, page, application) {
        let icon = this.icon ? React.createElement(FontAwesome, {name: this.icon}) : null;
        let text = this.text;
        return (React.createElement("div", {key: name, className: "sp-input-group-addon" + (this.class ? (" " + this.class) : '')}, 
            icon, 
            text));
    }
}
exports.FormItemSelectAddonTextModel = FormItemSelectAddonTextModel;
class FormItemSelectAddonButtonModel {
    constructor(initializer) {
        if (initializer === undefined) {
            return;
        }
        Object.keys(initializer).forEach((key, index) => {
            this[key] = initializer[key];
        });
    }
    render(name, field, valueLink, enabled, model, content, context, page, application) {
        let icon = this.icon ? React.createElement(FontAwesome, {name: this.icon}) : null;
        let text = this.text;
        let btn_enabled = !enabled ? false : (this.enabled ? this.enabled(name, field, valueLink, model, content, context, page, application) : true);
        return (React.createElement("div", {key: name, tabIndex: enabled ? 0 : -1, className: "sp-input-group-btn" + (!btn_enabled ? " disabled" : '') + (this.loading ? " loading" : '') + (this.class ? (" " + this.class) : ''), onClick: () => __awaiter(this, void 0, void 0, function* () {
            if (this.on_click) {
                this.loading = true;
                page.update();
                yield this.on_click(name, field, valueLink, model, content, context, page, application);
                this.loading = true;
                page.update();
            }
        })}, 
            icon, 
            text ? (React.createElement("span", null, text)) : null));
    }
}
exports.FormItemSelectAddonButtonModel = FormItemSelectAddonButtonModel;
class FormItemSelectModel extends Form_1.FormItemModel {
    constructor(initializer) {
        super();
        if (initializer === undefined) {
            return;
        }
        Object.keys(initializer).forEach((key, index) => {
            this[key] = initializer[key];
        });
    }
    render(name, valueLink, enabled, model, content, context, page, application) {
        let left = [];
        let right = [];
        if (this.left) {
            Core_1.MapPro.each(this.left, (addon, index) => {
                left.push(addon.render(index, this, valueLink, enabled, model, content, context, page, application));
            });
        }
        if (this.right) {
            Core_1.MapPro.each(this.right, (addon, index) => {
                right.push(addon.render(index, this, valueLink, enabled, model, content, context, page, application));
            });
        }
        return (React.createElement(Core_1.Select, {valueLink: valueLink, icon: this.icon, className: (this.class ? (' ' + this.class) : '') + (this.messages.error ? 'error' : '') + (enabled ? '' : ' disabled'), data: this.data, data_type: this.data_type, search: this.search, request: this.request, default: this.default, asInput: this.as_input, render: this.render_element, left: left, right: right}));
    }
}
exports.FormItemSelectModel = FormItemSelectModel;
class FormItemMSelectAddonTextModel {
    constructor(initializer) {
        if (initializer === undefined) {
            return;
        }
        Object.keys(initializer).forEach((key, index) => {
            this[key] = initializer[key];
        });
    }
    render(name, field, valueLink, enabled, model, content, context, page, application) {
        let icon = this.icon ? React.createElement(FontAwesome, {name: this.icon}) : null;
        let text = this.text;
        return (React.createElement("div", {key: name, className: "sp-input-group-addon" + (this.class ? (" " + this.class) : '')}, 
            icon, 
            text));
    }
}
exports.FormItemMSelectAddonTextModel = FormItemMSelectAddonTextModel;
class FormItemMSelectAddonButtonModel {
    constructor(initializer) {
        if (initializer === undefined) {
            return;
        }
        Object.keys(initializer).forEach((key, index) => {
            this[key] = initializer[key];
        });
    }
    render(name, field, valueLink, enabled, model, content, context, page, application) {
        let icon = this.icon ? React.createElement(FontAwesome, {name: this.icon}) : null;
        let text = this.text;
        let btn_enabled = !enabled ? false : (this.enabled ? this.enabled(name, field, valueLink, model, content, context, page, application) : true);
        return (React.createElement("div", {key: name, tabIndex: enabled ? 0 : -1, className: "sp-input-group-btn" + (!btn_enabled ? " disabled" : '') + (this.loading ? " loading" : '') + (this.class ? (" " + this.class) : ''), onClick: () => __awaiter(this, void 0, void 0, function* () {
            if (this.on_click) {
                this.loading = true;
                page.update();
                yield this.on_click(name, field, valueLink, model, content, context, page, application);
                this.loading = true;
                page.update();
            }
        })}, 
            icon, 
            text ? (React.createElement("span", null, text)) : null));
    }
}
exports.FormItemMSelectAddonButtonModel = FormItemMSelectAddonButtonModel;
class FormItemMSelectModel extends Form_1.FormItemModel {
    constructor(initializer) {
        super();
        if (initializer === undefined) {
            return;
        }
        Object.keys(initializer).forEach((key, index) => {
            this[key] = initializer[key];
        });
    }
    render(name, valueLink, enabled, model, content, context, page, application) {
        let left = [];
        let right = [];
        if (this.left) {
            Core_1.MapPro.each(this.left, (addon, index) => {
                left.push(addon.render(index, this, valueLink, enabled, model, content, context, page, application));
            });
        }
        if (this.right) {
            Core_1.MapPro.each(this.right, (addon, index) => {
                right.push(addon.render(index, this, valueLink, enabled, model, content, context, page, application));
            });
        }
        return (React.createElement(Core_1.MSelect, {valueLink: valueLink, icon: this.icon, className: (this.class ? (' ' + this.class) : '') + (this.messages.error ? 'error' : '') + (enabled ? '' : ' disabled'), data: this.data, data_type: this.data_type, search: this.search, request: this.request, asInput: this.as_input, render: this.render_element, left: left, right: right}));
    }
}
exports.FormItemMSelectModel = FormItemMSelectModel;
//# sourceMappingURL=Form.js.map