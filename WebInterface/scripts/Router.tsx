import * as React from 'react';
import { render } from 'react-dom';
// /// <amd-dependency path="jquery"/>
// import * as Velocity from 'velocity-animate'
const Velocity = require('velocity-animate')
$.fn.velocity = Velocity

// require('jquery');
// require('velocity-animate');

import { Application } from './Core';

import { application } from './Model/App';

$(window).keydown(function(e) { if (e.keyCode == 123) debugger; });

render(React.createElement(Application, { application }), document.getElementById("app-container"));

$(window).load(function() {
  $('body #loading').children().velocity('fadeOut', {duration: 100, complete: function(){$('body #loading').velocity('fadeOut', {duration: 500, complete: function(){$('body #loading').remove()}});}});
});

if (typeof window['require'] !== "undefined") {
    let electron = window['require']("electron");
    let ipcRenderer = electron.ipcRenderer;
    console.log("ipc renderer", ipcRenderer);
    let remote = electron.remote;

	document.getElementById("min-btn").addEventListener("click", function (e) {
        let window = remote.getCurrentWindow();
        window.minimize();
    });

    document.getElementById("max-btn").addEventListener("click", function (e) {
        let window = remote.getCurrentWindow();
        if (!window.isMaximized()) {
            window.maximize();
        } else {
            window.unmaximize();
        }
    });

    document.getElementById("close-btn").addEventListener("click", function (e) {
        var window = remote.getCurrentWindow();
        window.close();
    });

}