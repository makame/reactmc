"use strict";
const React = require('react');
const FontAwesome = require('react-fontawesome');
exports.containers = {
    panel: {
        render: (model, content, context, page, application) => {
            let titleSetGet = model.parameters[0];
            let titleValue = titleSetGet.get(context, page, application);
            let title = titleValue ? (React.createElement("div", {className: "sp-pane-header"}, titleValue)) : null;
            return (React.createElement("div", {className: "sp-pane" + (model.class.length > 0 ? (' ' + model.class.join(' ')) : '')}, 
                title, 
                content));
        }
    },
    labeled: {
        render: (model, content, context, page, application) => {
            let titleSetGet = model.parameters[0];
            let titleValue = titleSetGet.get(context, page, application);
            let title = titleValue ? (React.createElement("div", {className: "padding-left-1 padding-bottom-d1 text mini thick"}, titleValue)) : null;
            return (React.createElement("div", {className: model.class.join(' ')}, 
                title, 
                content));
        }
    },
    div: {
        render: (model, content, context, page, application) => {
            return (React.createElement("div", {className: model.class.join(' ')}, content));
        }
    },
    accord: {
        render: (model, content, context, page, application) => {
            let titleSetGet = model.parameters[0];
            let visibleSetGet = model.parameters[1];
            let titleValue = titleSetGet.get(context, page, application);
            let visibleValue = visibleSetGet.get(context, page, application);
            let title = (React.createElement("div", {className: "sp-pane-header sp-accord" + (visibleValue ? " active" : ""), onClick: (e) => {
                visibleSetGet.set(!visibleValue, context, page, application);
                page.update();
            }}, 
                titleValue, 
                React.createElement("div", {className: "sp-header-toolbar"}, 
                    React.createElement("div", {className: "sp-button"}, 
                        React.createElement(FontAwesome, {name: visibleValue ? "chevron-up" : "chevron-down"})
                    )
                )));
            return (React.createElement("div", {className: "sp-pane" + (model.class.length > 0 ? (' ' + model.class.join(' ')) : '')}, 
                title, 
                visibleValue ? content : null));
        }
    },
};
//# sourceMappingURL=Containers.js.map