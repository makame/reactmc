"use strict";
class METHOD {
    constructor(rest, url = '') {
        this._url = "";
        this._data = "";
        this._method = "POST";
        this._type = "json";
        this._crossDomain = false;
        this.rest = rest;
        this._url = url;
        this._data = "";
        this._method = "POST";
        this._type = "json";
        this._crossDomain = false;
    }
    cross(crossDomain) {
        this._crossDomain = crossDomain;
        return this;
    }
    url(url) {
        this._url = url;
        return this;
    }
    method(method) {
        this._method = method;
        return this;
    }
    type(type) {
        this._type = type;
        return this;
    }
    data(data) {
        this._data = data;
        return this;
    }
    do() {
        if (this._crossDomain) {
            return this.rest.addTask((resolve, reject) => {
                setTimeout(() => {
                    $.ajax({
                        method: this._method,
                        url: this._url,
                        data: this._data,
                        async: false,
                        dataType: this._type,
                        cache: true,
                        crossDomain: this._crossDomain,
                    })
                        .done(function (data) {
                        resolve(data);
                    })
                        .fail(function (error) {
                        reject(error);
                    });
                }, 10);
            });
        }
        else {
            return this.rest.addTask((resolve, reject) => {
                $.ajax({
                    method: this._method,
                    url: this._url,
                    data: this._data,
                    async: true,
                    dataType: this._type,
                    cache: true,
                    crossDomain: this._crossDomain,
                })
                    .done(function (data) {
                    resolve(data);
                })
                    .fail(function (error, message) {
                    reject({ error, message });
                });
            });
        }
    }
}
exports.METHOD = METHOD;
class GET extends METHOD {
    constructor(rest, url = '') {
        super(rest, url);
        this._method = "GET";
    }
}
exports.GET = GET;
class ONE {
    constructor(rest, one = []) {
        this._one = [];
        this.rest = rest;
        this._one = [];
        if (one) {
            this.one(one);
        }
    }
    one(one) {
        this._one.push(one);
        return this;
    }
    back(one) {
        this._one = this._one.splice(0, this._one.length - 1);
        return this;
    }
    get() {
        if (this._one.length > 0) {
            return new GET(this.rest, this.rest._enter + this._one.join('/'));
        }
        else {
            return new GET(this.rest, this.rest._enter);
        }
    }
    method() {
        if (this._one.length > 0) {
            return new METHOD(this.rest, this.rest._enter + this._one.join('/'));
        }
        else {
            return new METHOD(this.rest, this.rest._enter);
        }
    }
}
exports.ONE = ONE;
class Task {
    constructor(func, resolve, reject, run) {
        this.func = func;
        this.resolve = resolve;
        this.reject = reject;
        this.run = run;
    }
}
exports.Task = Task;
class REST {
    constructor(enter = '') {
        this.tasks = [];
        this.tasks = [];
        this._enter = enter;
    }
    new(enter) {
        return new REST(enter);
    }
    getDir() {
        let url = window.location.pathname.split('.html')[0];
        url = window.location.pathname.split('.htm')[0];
        return url.substring(0, url.lastIndexOf("/") + 1);
    }
    enter(enter) {
        if (!enter) {
            let url = window.location.pathname.split('.html')[0];
            url = window.location.pathname.split('.htm')[0];
            this._enter = url.substring(0, url.lastIndexOf("/") + 1);
        }
        else {
            this._enter = enter;
        }
        return this;
    }
    one(one) {
        return new ONE(this, [one]);
    }
    nextTask() {
        if (this.tasks.length > 0) {
            const { func, resolve, reject, run } = this.tasks[0];
            if (!run) {
                func(resolve, reject);
            }
        }
    }
    addTask(func) {
        const self = this;
        const task = new Promise((resolve, reject) => {
            this.tasks.push(new Task(func, resolve, reject, false));
            this.nextTask();
        });
        task.then(() => {
            this.tasks = this.tasks.slice(1);
            this.nextTask();
        }, () => {
            this.tasks = this.tasks.slice(1);
            this.nextTask();
        });
        return task;
    }
    get() {
        return new GET(this);
    }
    method() {
        return new METHOD(this);
    }
}
exports.REST = REST;
exports.rest = new REST();
//# sourceMappingURL=REST.js.map