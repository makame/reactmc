import * as React from 'react';
import { Router, IndexRoute, IndexRedirect, Route, useRouterHistory } from 'react-router';
import { createHistory } from 'history';
import { Modal } from './Controls/Modal';
import { Loading } from './Controls/Loading';

export interface ISetGet {
  get?: <T>(context: ContextModel, page: RouteModel, application: ApplicationModel) => T
  set?: <T>(newValue: T, context: ContextModel, page: RouteModel, application: ApplicationModel) => void | Promise<void>
}

export interface IValueLink<T> {
  value: T
  requestChange(newValue: T)
}

export class Map<T> {
  [K: string]: T
}

export class MapPro {
  static map<T, S>(map: Map<T>, iterator: (item: T, name: string, index: number) => S):S[] {
    let keys = Object.keys(map)
    let result: S[] = []
    keys.forEach((name, index)=>{
      result.push(iterator(map[name], name, index))
    })
    return result
  }

  static each<T, S>(map: Map<T>, iterator: (item: T, name: string, index: number) => S) {
    let keys = Object.keys(map)
    keys.forEach((name, index)=>{
      iterator(map[name], name, index)
    })
  }

  static find<T>(map: Map<T>, iterator: (item: T, name: string, index: number) => boolean): T {
    let keys = Object.keys(map)
    let result: T
    keys.forEach((name, index)=>{
      if (iterator(map[name], name, index) && !result) {
        result = map[name]
      }
    })
    return result
  }

  static findName<T>(map: Map<T>, iterator: (item: T, name: string, index: number) => boolean): string {
    let keys = Object.keys(map)
    let result: string
    keys.forEach((name, index)=>{
      if (iterator(map[name], name, index) && !result) {
        result = name
      }
    })
    return result
  }

  static filter<T>(map: Map<T>, iterator: (item: T, name: string, index: number) => boolean): Map<T> {
    let keys = Object.keys(map)
    let result: Map<T> = {}
    keys.forEach((name, index)=>{
      if (iterator(map[name], name, index)) {
        result[name] = map[name]
      }
    })
    return result
  }
}

export class ContextModel implements ISetGet {
  content?: ContentModel
  init?(context: ContextModel, page: RouteModel, application: ApplicationModel): Promise<any> | void
  loading?: boolean
  inited?: boolean
}

export class IContentModel extends ContextModel {
  container?: string
  loading?: boolean
  modals?: Map<ModalModel>
}

export class ContentModel extends ContextModel implements IContentModel {
  container?: string
  loading?: boolean
  modals?: Map<ModalModel>

  render(context: ContextModel,
  page: RouteModel,
  application: ApplicationModel,
  containerFactory: (contaner: string, inner: JSX.Element, context: ContextModel) => JSX.Element): JSX.Element {
    return null
  }
}

export class Init<T> {
  constructor(initializer?: T) {
    if (initializer === undefined) {
      return;
    }
    Object.keys(initializer).forEach((key, index) => {
      this[key] = initializer[key];
    });
  }
}

export class ContentModelInit<T> extends ContentModel {
  constructor(initializer?: T) {
    super()
    if (initializer === undefined) {
      return;
    }
    Object.keys(initializer).forEach((key, index) => {
      this[key] = initializer[key];
    });
  }
}

export class ModalModel {
  message?: ContentModel
  bottom?: ContentModel
  dialog?: boolean
  content: ContentModel

  class?: string

  close?: () => void

  before_close?: (close: () => void, content: ContentModel, item: ModalModel, page: RouteModel, application: ApplicationModel) => void
  on_outside?: (content: ContentModel, item: ModalModel, page: RouteModel, application: ApplicationModel) => void
}

export class RouteModel {
  routes?: Map<RouteModel>
  index?: string
  path: string
  layout?: (page: RouteModel, application: ApplicationModel, location: any, children?: JSX.Element) => JSX.Element
  page?: (page: RouteModel, application: ApplicationModel, children?: JSX.Element) => JSX.Element
  update?(): void
  check?(page: RouteModel, application: ApplicationModel): void | boolean
  condition?(page: RouteModel, application: ApplicationModel): ContentModel
  content?: ContentModel
  init?(page: RouteModel, application: ApplicationModel): Promise<any> | void
  loading?: boolean
  inited?: boolean
  params?: Map<string>
  query?: Map<string>
  location?: any
}

export class ApplicationModel {
  basename?(location: Location)
  browserHistory?: any
  initing?: boolean
  init?(application: ApplicationModel): Promise<any> | void
  routes?: Map<RouteModel>
  containers: Map<IContainerModel>
}

export interface IContainerModel {
  render(model: ContainerAdapter, content: JSX.Element, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element
}

export class ContainerAdapter {
  type: string
  class: string[]
  parameters: ISetGet[]
}

export class ComponentProps {
  page: RouteModel
  application: ApplicationModel

  content?: ContentModel
  context?: ContextModel

  params?: Map<string>
  query?: Map<string>
  location?: any
}

export class Component extends React.Component<ComponentProps, {}> {

  public static defaultProps = new ComponentProps()

  private getValue(array: string[], parent: ContextModel): ISetGet {
    if (array.length > 1) {
      return this.getValue(array.slice(1), parent[array[0]])
    }
    else if (array.length == 1)  {
      return parent[array[0]]
    }
    else {
      throw new RangeError("Context haven't current SetGet")
    }
  }

  private parseContainer(str: string, context: ContextModel): ContainerAdapter[] {
    return str.split('>').map((step1) => {
      let in_brackets_req: RegExpMatchArray = step1.match(/\((.*?)\)/)
      let in_brackets: string = in_brackets_req ? in_brackets_req[1] : ''

      let step2: string[] = step1.replace(in_brackets, '').replace('(', '').replace(')', '').split('.')
      let type: string = step2[0]

      let classes: string[] = step2.length > 1 ? step2.slice(1) : []

      let parameters: ISetGet[] = in_brackets.split(',').map((parameterRaw: string)=>{
        let parameter = parameterRaw.trim()
        if (parameter.indexOf('$') == 0) {
          let value: string[] = parameter.slice(1).split('.')
          return this.getValue(value, context)
        }
        else if (parameter.indexOf("'") == 0) {
          const pp = parameter.replace("'", '').replace("'", '')
          const result: ISetGet = {
            get: (context: ContextModel, page: RouteModel, application: ApplicationModel): string => {
              return pp
            }
          }
          return result
        }
        else {
          const result: ISetGet = {
            get: (context: ContextModel, page: RouteModel, application: ApplicationModel): string => {
              return parameter
            }
          }
          return result
        }
      })

      const adapter: ContainerAdapter = { type, class: classes, parameters }
      
      return adapter
    })
  }

  private getContainer<T>(model: ContainerAdapter, content: JSX.Element, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {
    if (model.type) {
      return application.containers[model.type].render(model, content, context, page, application)
    }
    else return content
  }

  private createContainer(array: ContainerAdapter[], content: JSX.Element, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {
    if (array.length > 0) {
      return this.createContainer(array.slice(1), this.getContainer(array[0], content, context, page, application), context, page, application)
    }
    else {
      return content
    }
  }

  private containerFactory(container: string, inner: JSX.Element, context: ContextModel, page: RouteModel, application: ApplicationModel) {
    let containerModel = this.parseContainer(container, context)

    return this.createContainer(containerModel.reverse(), inner, context, page, application)
  }

  componentWillReceiveProps(nextProps, nextContext) {
    const { content, page, application } = this.props

    page.params = nextProps.params ? nextProps.params : (page.params ? page.params : {})
    page.query = nextProps.location && nextProps.location.query ? nextProps.location.query : (page.query ? page.query : {})
    page.location = nextProps.location ? nextProps.location : page.location
  }

  async init() {
    const { context, page, application } = this.props

    if (context.init && !context.inited && !context.loading) {
      context.loading = true
      page.update()
      await context.init(context, page, application)
      context.loading = false
      context.inited = true
      page.update()
    }
  }

  componentWillMount() {
    const { content, context, page, application } = this.props

    page.params = this.props.params ? this.props.params : (page.params ? page.params : {})
    page.query = this.props.location && this.props.location.query ? this.props.location.query : (page.query ? page.query : {})
    page.location = this.props.location ? this.props.location : page.location

    this.init()
  }

  async componentWillUpdate() {
    this.init()
  }

  render() {
    const { content, context, page, application } = this.props

    let modals = content && content.modals ? MapPro.map(content.modals, (item, name) => {
      item.close = () => {
        if (item.before_close) {
          item.before_close(() => {
            delete content.modals[name]
            page.update()
          }, content, item, page, application)
        }
        else {
          delete content.modals[name]
          page.update()
        }
      }

      return (
        <Modal key={name} dialog={item.dialog} className={item.class}
          message={item.message ? React.createElement(Component, { content: item.message, context: item, page: page, application }) : null}
          bottom={item.bottom ? React.createElement(Component, { content: item.bottom, context: item, page: page, application }) : null}
          onOutsideClick={(e)=>{
            e.preventDefault()
            e.stopPropagation()
              
            if (item.on_outside) {
              item.on_outside(content, item, page, application)
            }
            else {
              item.close()
            }
          }}>
          {React.createElement(Component, { content: item.content, context: item, page: page, application })}
        </Modal>
      )
    }) : null

    let container = (element: JSX.Element): JSX.Element => {
      let inner = (
        <div className={"sp-module" + (content.loading ? ' loading' : '')}>
          {element}
          {modals}
          <div className="message">
            <div className="loader"></div>
          </div>
        </div>
      )

      if (content.container) {
        return this.containerFactory(content.container, inner, context, page, application)
      }
      else {
        return inner
      }
    }

    return container(content.render(context, page, application, (contaner: string, inner: JSX.Element, context: ContextModel)=>{
      if (contaner) {
        return this.containerFactory(contaner, inner, context, page, application)
      }
      else {
        return inner
      }
    }))
  }
}

class PageProps {
  page: RouteModel
  application: ApplicationModel
}

class Page extends React.Component<PageProps, {}> {

  public static defaultProps = new PageProps()

  async init() {
    const { page, application } = this.props

    if (page.init && !page.inited && !page.loading) {
      page.loading = true
      page.update()
      await page.init(page, application)
      page.loading = false
      page.inited = true
      page.update()
    }
  }

  componentWillMount() {
    this.init()
  }

  componentWillUpdate() {
    this.init()
  }

  render(): JSX.Element {
    const { children, page, application } = this.props

    if (page.loading) {
      return (<div><Loading/></div>)
    }
    else if (page.page) {
      return page.page(page, application)
    }
    else {
      const condition: ContentModel = page.condition ? page.condition(page, application) : null

      const content: ContentModel = condition ? condition : page.content

      if (content) {
        return React.createElement(Component, { content, context: content, page, application }, children)
      }
      else {
        return <div>{children}</div>
      }
    }
  }
}

class LayoutMask {
  page: RouteModel
  application: ApplicationModel
}

class LayoutProps {
  location?: any
}

class Layout extends React.Component<LayoutProps, {}> {

  mask?: LayoutMask

  public static defaultProps = new LayoutProps()

  componentWillMount() {
    const { page } = this.mask

    page.update = () => { this.setState({}) }
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { page, application } = this.mask

    if (page.check) return page.check(page, application) !== false
    else return true
  }

  render(): JSX.Element {
    const { location } = this.props
    const { page, application } = this.mask

    const children = React.createElement(Page, { page, application }, this.props.children)
    
    if (page.layout) {
      return page.layout(page, application, location, children)
    }
    else {
      return <div>{children}</div>
      // return children
    }
  }
}

export class ApplicationProps {
  application: ApplicationModel
}

export class ApplicationState {
  browserHistory?: any
}

export class Application extends React.Component<ApplicationProps, ApplicationState> {

  public static defaultProps = new ApplicationProps()

  state = new ApplicationState()

  private parseRoute(route: RouteModel, name: string, application: ApplicationModel) {
    var routes = []
    if (route.routes) MapPro.each(route.routes, (item, name) => {
      routes.push(this.parseRoute(item, name, application))
    })

    var index = null
    if (route.index) {
      index = (<IndexRedirect to={route.index}/>)
    }

    const params = { page: route, name, application }

    let component = class LayoutMask extends Layout {
      mask = {
        page: route,
        application: application
      }
    }

    return (
      <Route key={name} path={route.path} component={component}>
        {index}
        {routes}
      </Route>
    )
  }

  async init() {
    if (this.props.application.init) {
      this.props.application.initing = true
      await this.props.application.init(this.props.application)
      this.props.application.initing = false
      this.setState({})
    }
  }

  componentWillMount() {
    let basename = this.props.application.basename ? this.props.application.basename(window.location) : window.location.origin

    this.state.browserHistory = useRouterHistory(createHistory)({
      basename: basename
    })

    this.props.application.browserHistory = this.state.browserHistory

    this.init()

    // this.application.update = () => {this.setState({})}
  }

  render() {
    if (this.props.application.initing) {
      return (<Loading/>)
    }
    else {
      var routes = []

      MapPro.each(this.props.application.routes, (item, name) => {
        routes.push(this.parseRoute(item, name, this.props.application))
      })

      return (
        <Router history={this.state.browserHistory} routes={routes}/>
      )
    }
  }
}