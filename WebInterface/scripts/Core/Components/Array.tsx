import * as React from 'react'

import { Component, Map, MapPro, IContentModel, ContentModelInit, ContentModel, ContextModel, RouteModel, ApplicationModel } from '../Main'

export interface IArrayModel extends IContentModel {
  items: Map<ContentModel>
  class?: string
}

export class ArrayModel extends ContentModelInit<IArrayModel> implements IArrayModel {
  items: Map<ContentModel>
  class?: string

  render(context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {

    let array = MapPro.map(this.items, (item, name)=>{return React.createElement(Component, { key: name, content: item, context: item, page, application })})

    return (
      <div className={this.class ? this.class : ''}>
        {array}
      </div>
    )
  }
}
