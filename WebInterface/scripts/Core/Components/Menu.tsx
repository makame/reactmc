import * as React from 'react';

import { Map, MapPro, IContentModel, ContentModelInit, ContentModel, ContextModel, RouteModel, ApplicationModel } from '../Main'

export interface IMenuItemModel {
  render(name: string, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element
  if?: (context: ContextModel, page: RouteModel, application: ApplicationModel) => boolean
}

export interface IMenuModel extends IContentModel {
  items: Map<IMenuItemModel>
  class?: string
}

export class MenuModel extends ContentModelInit<IMenuModel> implements IMenuModel {
  items: Map<IMenuItemModel>
  class?: string

  render(context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {
    return React.createElement(Menu, { content: this, context, page, application })
  }
}

class MenuProps {
  content: MenuModel
  context: ContextModel
  page: RouteModel
  application: ApplicationModel
}

class Menu extends React.Component<MenuProps, {}> implements ContentModel {

  public static defaultProps = new MenuProps()

  render() {
    let self = this

    const {content, context, page, application} = this.props

    const array: JSX.Element[] = []
    
    MapPro.each(content.items, (item, name)=>{
      const _if = item.if ? item.if(context, page, application) : true

      if (_if) {
        array.push(item.render(name, content, context, page, application))
      }
    })

    return (
      <div className={"sp-menu" + (content.class ? (' ' + content.class) : '')}>
        <div>
          {array}
        </div>
      </div>
    )
  }
}
