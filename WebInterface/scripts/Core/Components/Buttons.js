"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const React = require('react');
const FontAwesome = require('react-fontawesome');
const Main_1 = require('../Main');
class ButtonModel {
}
exports.ButtonModel = ButtonModel;
class ButtonsModel extends Main_1.ContentModelInit {
    render(context, page, application) {
        let buttons = [];
        Main_1.MapPro.each(this.buttons, (button, name) => {
            let icon = button.icon ? React.createElement(FontAwesome, {name: button.icon}) : null;
            let text = typeof button.text === "function" ? button.text(context, page, application) : button.text;
            let enabled = button.enabled ? (typeof button.enabled === "function" ? button.enabled(context, page, application) : button.enabled) : true;
            let if_ = button.if ? (typeof button.if === "function" ? button.if(context, page, application) : button.if) : true;
            let click = () => __awaiter(this, void 0, void 0, function* () {
                if (button.on_click) {
                    button.loading = true;
                    page.update();
                    yield button.on_click(context, page, application);
                    button.loading = false;
                    page.update();
                }
            });
            if (if_)
                buttons.push(React.createElement("div", {key: name, tabIndex: enabled ? 0 : -1, disabled: !enabled, className: "btn" + (button.class ? (' ' + button.class) : '') + (!enabled ? " disabled" : '') + (button.loading ? (' ' + 'loading') : ''), onClick: click, onKeyDown: (e) => {
                    if (e.keyCode == 27) {
                        click();
                    }
                }}, 
                    icon, 
                    text ? (React.createElement("span", null, text)) : null));
        });
        return (React.createElement("div", {className: "sp-inlines" + (this.class ? (' ' + this.class) : '')}, buttons));
    }
}
exports.ButtonsModel = ButtonsModel;
//# sourceMappingURL=Buttons.js.map