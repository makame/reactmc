"use strict";
const React = require('react');
const StdAnim_1 = require('../Controls/StdAnim');
const Main_1 = require('../Main');
class FormMessages {
}
exports.FormMessages = FormMessages;
class FormItemModel {
    constructor() {
        this.messages = {};
    }
    render(name, valueLink, enabled, model, content, context, page, application, containerFactory) {
        return null;
    }
}
exports.FormItemModel = FormItemModel;
class IFormModel extends Main_1.IContentModel {
    constructor() {
        super(...arguments);
        this.value = {};
    }
}
exports.IFormModel = IFormModel;
class IFormLink {
}
exports.IFormLink = IFormLink;
class FormModel extends Main_1.ContentModelInit {
    render(context, page, application, containerFactory) {
        return (React.createElement("div", {className: this.class ? this.class : ''}, React.createElement(Form, { value: this.value, form: this.form, model: { value: this.value, form: this.form }, content: this, context, page, application, containerFactory })));
    }
}
exports.FormModel = FormModel;
class FormProps {
}
exports.FormProps = FormProps;
class FormState {
    constructor() {
        this.if = {};
        this.enabled = {};
    }
}
exports.FormState = FormState;
class Form extends React.Component {
    constructor() {
        super(...arguments);
        this.state = new FormState();
    }
    validate() {
        let self = this;
        let validated = true;
        Main_1.MapPro.each(self.props.form, (field, name) => {
            if (field.if) {
                self.state.if[name] = field.if(self.props.value[name], self.props.model, self.props.context, self.props.page, self.props.application);
            }
            else {
                self.state.if[name] = true;
            }
            if (self.state.if[name]) {
                if (field.enabled)
                    self.state.enabled[name] = field.enabled(self.props.value[name], self.props.model, self.props.context, self.props.page, self.props.application);
                else
                    self.state.enabled[name] = true;
                if (field.error)
                    field.messages.error = field.error(self.props.value[name], self.props.model, self.props.context, self.props.page, self.props.application);
                else
                    field.error = null;
                if (field.info)
                    field.messages.info = field.info(self.props.value[name], self.props.model, self.props.context, self.props.page, self.props.application);
                else
                    field.info = null;
                if (field.warning)
                    field.messages.warning = field.warning(self.props.value[name], self.props.model, self.props.context, self.props.page, self.props.application);
                else
                    field.warning = null;
            }
        });
    }
    componentWillMount() {
        let self = this;
        self.validate();
    }
    componentWillUpdate() {
        let self = this;
        self.validate();
    }
    render() {
        let self = this;
        const { value, form, model, content, context, page, application, containerFactory } = self.props;
        let rows = [];
        let i = 0;
        Main_1.MapPro.each(form, (field, name) => {
            if (self.state.if[name]) {
                const valueLink = {
                    value: value[name],
                    requestChange: (newValue) => {
                        value[name] = newValue;
                        self.validate();
                        page.update();
                    }
                };
                let error = field.messages.error;
                let info = field.messages.info;
                let warning = field.messages.warning;
                let enabled = self.props.enabled !== false && self.state.enabled[name];
                let message = [];
                if (error) {
                    message.push(React.createElement("div", {className: "padding-left-1 padding-top-1 padding-bottom-d1 text mini red", key: "error"}, error));
                }
                if (info) {
                    message.push(React.createElement("div", {className: "padding-left-1 padding-top-1 padding-bottom-d1 text mini sub", key: "info"}, info));
                }
                if (warning) {
                    message.push(React.createElement("div", {className: "padding-left-1 padding-top-1 padding-bottom-d1 text mini warning", key: "warning"}, warning));
                }
                const container = containerFactory(field.container, (React.createElement("div", null, 
                    field.render(name, valueLink, enabled, model, content, context, page, application, containerFactory), 
                    React.createElement(StdAnim_1.StdAnim, {duration: 300, type: "fadeScaleX"}, message))), self.props.context);
                rows.push(React.createElement("div", {key: name}, container));
            }
            i++;
        });
        return (React.createElement(StdAnim_1.StdAnim, {duration: 300, type: "tag"}, rows));
    }
}
Form.defaultProps = new FormProps();
exports.Form = Form;
//# sourceMappingURL=Form.js.map