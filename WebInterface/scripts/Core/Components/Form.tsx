import * as React from 'react';

import { StdAnim } from '../Controls/StdAnim'

import { Map, MapPro, IValueLink, IContentModel, ContentModelInit, ContentModel, ContextModel, RouteModel, ApplicationModel } from '../Main'

export class FormMessages {
  error?: string
  info?: string
  warning?: string
}

export interface IFormItemModel<T> {
  container?: string

  if?: (value: T, model: IFormLink, context: ContextModel, page: RouteModel, application: ApplicationModel) => boolean
  enabled?: (value: T, model: IFormLink, context: ContextModel, page: RouteModel, application: ApplicationModel) => boolean
  error?: (value: T, model: IFormLink, context: ContextModel, page: RouteModel, application: ApplicationModel) => string
  info?: (value: T, model: IFormLink, context: ContextModel, page: RouteModel, application: ApplicationModel) => string
  warning?: (value: T, model: IFormLink, context: ContextModel, page: RouteModel, application: ApplicationModel) => string
}

export class FormItemModel<T> implements IFormItemModel<T> {
  messages?: FormMessages = {}
  validated?: boolean
  container?: string

  if?: (value: T, model: IFormLink, context: ContextModel, page: RouteModel, application: ApplicationModel) => boolean
  enabled?: (value: T, model: IFormLink, context: ContextModel, page: RouteModel, application: ApplicationModel) => boolean
  error?: (value: T, model: IFormLink, context: ContextModel, page: RouteModel, application: ApplicationModel) => string
  info?: (value: T, model: IFormLink, context: ContextModel, page: RouteModel, application: ApplicationModel) => string
  warning?: (value: T, model: IFormLink, context: ContextModel, page: RouteModel, application: ApplicationModel) => string
  
  render(name: string, valueLink: IValueLink<T>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel, containerFactory: (contaner: string, inner: JSX.Element, context: ContextModel) => JSX.Element): JSX.Element {
    return null
  }
}

export class IFormModel extends IContentModel {
  class?: string
  value?: Map<any> = {}
  form: Map<FormItemModel<any>>
  validated?: boolean
}

export class IFormLink {
  value: Map<any>
  form: Map<FormItemModel<any>>
  parent?: IFormLink
}

export class FormModel extends ContentModelInit<IFormModel> {
  class?: string
  value: Map<any>
  form: Map<FormItemModel<any>>
  validated?: boolean

  render(context: ContextModel, page: RouteModel, application: ApplicationModel, containerFactory: (contaner: string, inner: JSX.Element, context: ContextModel) => JSX.Element): JSX.Element {    
    return (
      <div className={this.class ? this.class : ''}>
        {React.createElement(Form, { value: this.value, form: this.form, model: { value: this.value, form: this.form }, content: this, context, page, application, containerFactory })}
      </div>
    )
  }
}

export class FormProps {
  value: Map<any>
  form: Map<FormItemModel<any>>
  model: IFormLink
  parent?: IFormLink
  validated?: boolean
  enabled?: boolean
  containerFactory: (contaner: string, inner: JSX.Element, context: ContextModel) => JSX.Element

  content: ContentModel
  context: ContextModel
  page: RouteModel
  application: ApplicationModel
}

export class FormState {
  if: Map<boolean> = {}
  enabled: Map<boolean> = {}
}

export class Form extends React.Component<FormProps, FormState> implements ContentModel {

  public static defaultProps = new FormProps()

  state = new FormState()

  validate() {
    let self = this

    let validated = true

    MapPro.each(self.props.form, (field, name) => {

      if (field.if) {
        self.state.if[name] = field.if(self.props.value[name], self.props.model, self.props.context, self.props.page, self.props.application)
      }
      else {
        self.state.if[name] = true
      }

      if (self.state.if[name]) {
        if (field.enabled) self.state.enabled[name] = field.enabled(self.props.value[name], self.props.model, self.props.context, self.props.page, self.props.application)
        else self.state.enabled[name] = true

        if (field.error) field.messages.error = field.error(self.props.value[name], self.props.model, self.props.context, self.props.page, self.props.application)
        else field.error = null

        if (field.info) field.messages.info = field.info(self.props.value[name], self.props.model, self.props.context, self.props.page, self.props.application)
        else field.info = null

        if (field.warning) field.messages.warning = field.warning(self.props.value[name], self.props.model, self.props.context, self.props.page, self.props.application)
        else field.warning = null
      }
    })
  }

  componentWillMount() {
    let self = this

    self.validate()
  }

  componentWillUpdate() {
    let self = this

    self.validate()
  }

  render() {
    let self = this

    const { value, form, model, content, context, page, application, containerFactory } = self.props

    let rows = [];

    let i = 0

    MapPro.each(form, (field, name) => {

        if (self.state.if[name]) {
          const valueLink = {
            value: value[name],
            requestChange: (newValue) => {
              value[name] = newValue
              self.validate()
              page.update()
            }
          }

          let error = field.messages.error
          let info = field.messages.info
          let warning = field.messages.warning
          let enabled = self.props.enabled !== false && self.state.enabled[name]

          let message = []

          if (error) {
            message.push(
              <div className="padding-left-1 padding-top-1 padding-bottom-d1 text mini red" key="error">
                {error}
              </div>
            )
          }

          if (info) {
            message.push(
              <div className="padding-left-1 padding-top-1 padding-bottom-d1 text mini sub" key="info">
                {info}
              </div>
            )
          }

          if (warning) {
            message.push(
              <div className="padding-left-1 padding-top-1 padding-bottom-d1 text mini warning" key="warning">
                {warning}
              </div>
            )
          }

          const container = containerFactory(field.container, (
            <div>
              {field.render(name, valueLink, enabled, model, content, context, page, application, containerFactory)}
              <StdAnim duration={300} type="fadeScaleX">{message}</StdAnim>
            </div>
          ), self.props.context)

          rows.push(
            <div key={name}>{container}</div>
          )
        }

      i++
    })

    return (
      <StdAnim duration={300} type="tag">
        {rows}
      </StdAnim>
    );
  }
}