"use strict";
const React = require('react');
const StdAnim_1 = require('../Controls/StdAnim');
const Main_1 = require('../Main');
class TabModel extends Main_1.ContentModelInit {
}
exports.TabModel = TabModel;
class TabsModel extends Main_1.ContentModelInit {
    render(context, page, application) {
        let tabs = Main_1.MapPro.map(this.tabs, (item, name) => {
            return (React.createElement("div", {className: "sp-pane-tab" + (this.class ? (' ' + this.class) : '') + (this.active_tab == name ? ' active' : ''), key: name, onClick: (e) => {
                this.active_tab = name;
                page.update();
            }}, item.label));
        });
        let tab = this.active_tab && this.tabs[this.active_tab] ? React.createElement(Main_1.Component, { key: this.active_tab, content: this.tabs[this.active_tab].content, context: this.tabs[this.active_tab].content, page, application }) : null;
        return (React.createElement("div", {className: this.class ? this.class : ''}, 
            React.createElement("div", {className: "sp-inlines width-full"}, tabs), 
            React.createElement("div", {className: "sp-pane"}, 
                React.createElement(StdAnim_1.StdAnim, {duration: 300, type: "fade"}, tab)
            )));
    }
}
exports.TabsModel = TabsModel;
//# sourceMappingURL=Tabs.js.map