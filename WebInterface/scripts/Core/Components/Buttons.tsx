import * as React from 'react'

import FontAwesome = require('react-fontawesome')

import { Map, MapPro, IContentModel, ContentModelInit, ContextModel, RouteModel, ApplicationModel } from '../Main'

export class ButtonModel {
  text?: string
  icon?: string
  enabled?: (context: ContextModel, page: RouteModel, application: ApplicationModel) => Promise<boolean> | boolean
  on_click: (context: ContextModel, page: RouteModel, application: ApplicationModel) => Promise<void> | void
  if?: (context: ContextModel, page: RouteModel, application: ApplicationModel) => Promise<boolean> | boolean
  loading?: boolean
  class?: string
}

export interface IButtonsModel extends IContentModel {
  buttons: Map<ButtonModel>
  class?: string
}

export class ButtonsModel extends ContentModelInit<IButtonsModel> implements IButtonsModel {
  buttons: Map<ButtonModel>
  class?: string

  render(context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {
    let buttons = []

    MapPro.each(this.buttons, (button, name) => {
      let icon = button.icon ? <FontAwesome name={button.icon}/> : null
      let text = typeof button.text === "function" ? button.text(context, page, application) : button.text
      let enabled = button.enabled ? (typeof button.enabled === "function" ? button.enabled(context, page, application) : button.enabled) : true
      let if_ = button.if ? (typeof button.if === "function" ? button.if(context, page, application) : button.if) : true
      
      let click = async ()=>{
        if (button.on_click) {
          button.loading = true
          page.update()
          await button.on_click(context, page, application)
          button.loading = false
          page.update()
        }
      }

      if (if_) buttons.push(
        <div key={name} tabIndex={enabled ? 0 : -1} disabled={!enabled}
          className={"btn" + (button.class ? (' ' + button.class) : '')  + (!enabled ? " disabled" : '') + (button.loading ? (' ' + 'loading') : '')}
          onClick={click}
          onKeyDown={(e)=>{
            if (e.keyCode == 27) {
              click()
            }
          }}>
          {icon}
          {text ? (<span>{text}</span>) : null}
        </div>
      )
    })

    return (
      <div className={"sp-inlines" + (this.class ? (' ' + this.class) : '')}>
        {buttons}
      </div>
    )
  }
}