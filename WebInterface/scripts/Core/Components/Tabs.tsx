import * as React from 'react'

import { StdAnim } from '../Controls/StdAnim'

import { Component, Map, MapPro, IContentModel, ContentModelInit, ContentModel, ContextModel, RouteModel, ApplicationModel } from '../Main'

export interface ITabModel extends IContentModel {
  label?: string
  class?: string
}

export class TabModel extends ContentModelInit<ITabModel> implements ITabModel {
  label?: string
  class?: string
}

export interface ITabsModel extends IContentModel {
  tabs?: Map<TabModel>
  class?: string
  active_tab?: string
}

export class TabsModel extends ContentModelInit<ITabsModel> implements ITabsModel {
  tabs?: Map<TabModel>
  class?: string
  active_tab?: string

  render(context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {

    let tabs = MapPro.map(this.tabs, (item, name)=>{
      return (<div className={"sp-pane-tab" + (this.class ? (' ' + this.class) : '') + (this.active_tab == name ? ' active' : '')} key={name}
      onClick={(e)=>{
        this.active_tab = name
        page.update()
      }}>{item.label}</div>)
    })

    let tab = this.active_tab && this.tabs[this.active_tab] ? React.createElement(Component, { key: this.active_tab, content: this.tabs[this.active_tab].content, context: this.tabs[this.active_tab].content, page, application }) : null

    return (
      <div className={this.class ? this.class : ''}>
        <div className={"sp-inlines width-full"}>
          {tabs}
        </div>
        <div className={"sp-pane"}>
          <StdAnim duration={300} type="fade">{tab}</StdAnim>
        </div>
      </div>
    )
  }
}
