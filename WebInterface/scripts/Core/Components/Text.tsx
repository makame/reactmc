import * as React from 'react'

import { ContentModelInit, IContentModel, ContextModel, RouteModel, ApplicationModel } from '../Main'

export interface ITextModel extends IContentModel {
  value?: string
  class?: string
}

export class TextModel extends ContentModelInit<ITextModel> implements ITextModel {
  value?: string
  class?: string

  render(context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {
    return (
      <div className={this.class ? this.class : ''}>
        {this.value}
      </div>
    )
  }
}
