"use strict";
const React = require('react');
const Main_1 = require('../Main');
class MenuModel extends Main_1.ContentModelInit {
    render(context, page, application) {
        return React.createElement(Menu, { content: this, context, page, application });
    }
}
exports.MenuModel = MenuModel;
class MenuProps {
}
class Menu extends React.Component {
    render() {
        let self = this;
        const { content, context, page, application } = this.props;
        const array = [];
        Main_1.MapPro.each(content.items, (item, name) => {
            const _if = item.if ? item.if(context, page, application) : true;
            if (_if) {
                array.push(item.render(name, content, context, page, application));
            }
        });
        return (React.createElement("div", {className: "sp-menu" + (content.class ? (' ' + content.class) : '')}, 
            React.createElement("div", null, array)
        ));
    }
}
Menu.defaultProps = new MenuProps();
//# sourceMappingURL=Menu.js.map