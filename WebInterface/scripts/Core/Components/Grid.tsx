import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Dropdown } from '../Controls/Dropdown';
import { Popup } from '../Controls/Popup';
import { ConnectionPortal } from '../Controls/Portal';
import { Select } from '../Controls/Select';
import { StdAnim } from '../Controls/StdAnim';

import FontAwesome = require('react-fontawesome')

import Helper from '../Controls/Helper'

import { Map, MapPro, IValueLink, IContentModel, ContentModelInit, ContentModel, ContextModel, RouteModel, ApplicationModel } from '../Main'

let __id = 0

const uniqueId = function(item: IGridRow) {
  if (typeof item.id == "undefined") {
    item.id = ++__id;
  }

  return item
};

const uniqueIds = function(item: IGridRow[]) {
  for (let i = 0; i < item.length; i++) {
    uniqueId(item[i])
  }

  return item;
};

export type GridDataType = 'local' | 'remote' | 'remote_local'

export class GridMessages {
  error?: string
  info?: string
  warning?: string
  if?: boolean
  enabled?: boolean
}

export interface IGridItemModel<T> {
  container?: string
  filter?: string[]
  classRow?: string
  classEdit?: string
  classCreate?: string

  sortable?: boolean
  label: string

  if?: (value: T, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => boolean
  enabled?: (value: T, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => boolean
  error?: (value: T, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => string
  info?: (value: T, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => string
  warning?: (value: T, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => string
}

export interface IGridItemFilter<T> {
  check(value: T, filter_value: any, field: GridItemModel<T>, context: ContextModel, page: RouteModel, application: ApplicationModel): boolean
  render?(valueLink: IValueLink<any>, field: GridItemModel<T>, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element
}

export class GridItemModel<T> implements IGridItemModel<T> {
  container?: string
  filter?: string[]
  classRow?: string
  classEdit?: string
  classCreate?: string

  sortable?: boolean
  label: string

  if?: (value: T, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => boolean
  enabled?: (value: T, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => boolean
  error?: (value: T, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => string
  info?: (value: T, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => string
  warning?: (value: T, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => string

  filters?: Map<IGridItemFilter<T>>

  render?(value: T, field: GridItemModel<T>, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {
    return null
  }
  create_render?(valueLink: IValueLink<T>, field: GridItemModel<T>, messages: GridMessages, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {
    return null
  }
  edit_render?(valueLink: IValueLink<T>, field: GridItemModel<T>, messages: GridMessages, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element {
    return null
  }
}

export interface IGridFilterType {
  type: string
  value?: any
}

export interface IGridRow {
  [K: string]: any
  id: any
}

export interface IGridFilter {
  type: string
  value?: any
}

export class IGridValue {
  data?: IGridRow[]
  parsed_data?: IGridRow[]

  messages?: Map<Map<GridMessages>> = {}
  create_messages?: Map<GridMessages> = {}

  create?: IGridRow = { id: null }
  edit?: Map<IGridRow> = {}
  selected?: Map<IGridRow>

  sort?: string[] = []
  
  filter?: Map<IGridFilter> = {}

  per_page: number = 10
  page: number = 0
  total: number = 0

  need_update?: boolean
  loaded?: boolean
  scroll?: {
    key: any
    value: (content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => any
  }
  need_scroll?: boolean
  after_request?: boolean

  constructor(initializer?: IGridValue) {
    if (initializer === undefined) {
      return;
    }
    Object.keys(initializer).forEach((key, index) => {
      this[key] = initializer[key];
    });
  }
}

export interface IGridResponce {
  total: number
  data: IGridRow[]
}

export class GridContextCreateButton {
  text: string
  icon?: string
  if?: (row: IGridRow, context: ContextModel, page: RouteModel, application: ApplicationModel) => boolean
  enabled?: (row: IGridRow, context: ContextModel, page: RouteModel, application: ApplicationModel) => boolean
  on_click: (row: IGridRow, context: ContextModel, page: RouteModel, application: ApplicationModel) => Promise<void> | void
}

export class GridContextRowButton {
  text: string
  icon?: string
  if?: (row: IGridRow, context: ContextModel, page: RouteModel, application: ApplicationModel) => boolean
  enabled?: (row: IGridRow, context: ContextModel, page: RouteModel, application: ApplicationModel) => boolean
  on_click: (row: IGridRow, context: ContextModel, page: RouteModel, application: ApplicationModel) => Promise<void> | void
}

export class GridContextChangeButton {
  text: string
  icon?: string
  if?: (row: IGridRow, old_row: IGridRow, context: ContextModel, page: RouteModel, application: ApplicationModel) => boolean
  enabled?: (row: IGridRow, old_row: IGridRow, context: ContextModel, page: RouteModel, application: ApplicationModel) => boolean
  on_click: (row: IGridRow, old_row: IGridRow, context: ContextModel, page: RouteModel, application: ApplicationModel) => Promise<boolean> | boolean
}

export class IGrid {
  fields: Map<GridItemModel<any>>
  pagination?: boolean
  big_data?: boolean
  selectable?: boolean
  on_scroll?: (row: IGridRow, context: ContextModel, page: RouteModel, application: ApplicationModel) => void

  allow_edit?: (row: IGridRow, context: ContextModel, page: RouteModel, application: ApplicationModel) => boolean
  allow_add?: (context: ContextModel, page: RouteModel, application: ApplicationModel) => boolean

  row_context_menu?: Map<GridContextRowButton> = {}
  create_context_menu?: Map<GridContextCreateButton> = {}
  edit_context_menu?: Map<GridContextChangeButton> = {}
  
  sortNull?: string[] = []
  sort?: string[] = []
  on_click?: (row: IGridRow, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => Promise<void> | void

  data_type: GridDataType

  request?: (value: IGridValue, context: ContextModel, page: RouteModel, application: ApplicationModel) => Promise<IGridResponce> | IGridResponce

  constructor(initializer?: IGrid) {
    if (initializer === undefined) {
      return;
    }
    Object.keys(initializer).forEach((key, index) => {
      this[key] = initializer[key];
    });
  }
}

export class IGridModel extends IContentModel {
  class?: string
  value?: IGridValue = new IGridValue()
  grid: IGrid = new IGrid()
}

export class GridModel extends ContentModel {
  class?: string
  value?: IGridValue
  grid: IGrid

  constructor(initializer?: IGridModel) {
    super()
    if (initializer === undefined) {
      return;
    }
    this.class = initializer.class
    if (initializer.value) this.value = new IGridValue(initializer.value)
    if (initializer.grid) this.grid = new IGrid(initializer.grid)
  }

  render(context: ContextModel, page: RouteModel, application: ApplicationModel, containerFactory: (contaner: string, inner: JSX.Element, context: ContextModel) => JSX.Element): JSX.Element {    
    return (
      <div className={this.class ? this.class : ''}>
        {React.createElement(Grid, { value: this.value, grid: this.grid, content: this, context, page, application, containerFactory })}
      </div>
    )
  }
}

export class GridProps {
  value: IGridValue
  grid: IGrid

  containerFactory: (contaner: string, inner: JSX.Element, context: ContextModel) => JSX.Element

  content: ContentModel
  context: ContextModel
  page: RouteModel
  application: ApplicationModel
}

export class GridState {
  if: Map<boolean> = {}
  enabled: Map<boolean> = {}
  caclulate_render: boolean = true
  need_rerender: boolean = false
  updating: boolean = false
  rows_updated: boolean = false
  loading: {
    on_create: boolean
    on_edit: Map<boolean>
    on_click: Map<boolean>
  } = {
    on_create: false,
    on_edit: {},
    on_click: {}
  }
}

export class Grid extends React.Component<GridProps, GridState> implements ContentModel {

  filter_types: Map<string> = {
    contains: 'Содержит',
    totaly: 'Аналогично',
    not_in: 'Не содержит',
    null: 'Пусто',
    not_null: 'Не пусто',
  }

  public static defaultProps = new GridProps()

  state = new GridState()
  
  id: string = Helper.guid()

  refs: {
    [key: string]: any;
    tbody: HTMLElement;
  }

  items: any[] = []
  few: any[] = []
  etaloneHeight: number = 0
  etaloneOffset: number = 0
  t: number = 0
  b: number = 0

  need_on_scroll: boolean = false

  timerScroll?: number
  timeoutRenderItems?: number

  prev: boolean = false

  cleanSelect() {
    if (this.props.value.selected) {
      MapPro.each(this.props.value.selected, (item, index) => {
        let find = this.props.value.parsed_data.find((row) => {return row === item || row.id == item.id})
        if (!find) {
          delete this.props.value.selected[index]
        }
      })

      if (Object.keys(this.props.value.selected).length == 0) this.props.value.selected = null
    }

    this.props.page.update()
  }

  cleanPage() {
    if (this.props.grid.pagination && this.props.value.per_page != null) {
      if (Math.floor(this.props.value.total / this.props.value.per_page) < this.props.value.page) {
        this.props.value.page = Math.floor(this.props.value.total / this.props.value.per_page)
      }
    }
    else {
      this.props.value.page = 0
    }
  }

  onScroll() {
    if (this.props.grid.on_scroll) {
      let scrollTop = $(window).scrollTop() + $(window).height() / 2
      let finded = null;
      for (let index = 0; index < this.items.length; index++) {
        if (this.items[index].min <= scrollTop && this.items[index].max > scrollTop) {
          finded = index
          break;
        }
        else if (this.items[index].min > scrollTop) {
          break;
        }
      }

      if (finded != null) {
        this.props.grid.on_scroll(this.props.value.parsed_data[finded], this.props.context, this.props.page, this.props.application)
      }
    }

    this.need_on_scroll = false
  }

  willUpdate() {
    if (this.props.grid.big_data) {
      let top = $(window).scrollTop() - $(window).height()
      let bottom = $(window).scrollTop() + $(window).height() * 2

      let t = 0
      let b = 0

      let elems = []

      if (this.etaloneHeight > 0) for (let index = 1; index < this.items.length; index++) {
        if ((this.items[index].min ? this.items[index].min : this.etaloneHeight * index + this.etaloneOffset) > bottom) {
          b = b + (this.items[index].height ? this.items[index].height : this.etaloneHeight)
        }
        else if ((this.items[index].max ? this.items[index].max : this.etaloneHeight * (index + 1) + this.etaloneOffset) < top) {
          t = t + (this.items[index].height ? this.items[index].height : this.etaloneHeight)
        }
        else {
          elems.push(this.items[index])

          if (!this.items[index].momento || !this.items[index].updated) {
            let momento = this.renderItem(this.props.value.parsed_data[index], index)
            this.items[index].momento = momento
            this.items[index].updated = true
            this.items[index].need_info = true
          }
        }
      }

      if (this.items.length > 0 && (!this.items[0].updated || !this.items[0].momento)) {
        let momento = this.renderItem(this.props.value.parsed_data[0], 0)
        this.items[0].momento = momento
        this.items[0].updated = true
        this.items[0].need_info = true
      }

      this.t = t
      this.b = b
      this.few = elems
    }
    else {
      for (let index = 0; index < this.items.length; index++) {
        if (!this.items[index].momento || !this.items[index].updated) {
          let momento = this.renderItem(this.props.value.parsed_data[index], index)
          this.items[index].momento = momento
          this.items[index].updated = true
          this.items[index].need_info = true
        }
      }
    }
  }

  handleScroll(event) {
    window.clearTimeout(this.timerScroll);
    this.timerScroll = window.setTimeout(this.onScroll.bind(this), 150);

    this.forceUpdate()
  }

  handleScrollBind: any

  componentDidMount() {
    this.handleScrollBind = this.handleScroll.bind(this)
    window.addEventListener('scroll', this.handleScrollBind);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScrollBind);
  }

  componentWillMount() {
    window.clearTimeout(this.timeoutRenderItems)

    this.timeoutRenderItems = window.setTimeout(()=>{
      this.state.caclulate_render = true
      this.state.need_rerender = false

      this.forceUpdate()

      this.items = this.props.value.parsed_data ? this.props.value.parsed_data.map((item, index)=>{
        return {
          updated: false,
          index: index,
          id: item.id,
          data: item,
          momento: this.props.grid.big_data ? null : this.renderItem(item, index)
        }
      }) : []

      this.state.caclulate_render = false
      this.state.need_rerender = true

      this.forceUpdate()
    }, 0)

    // let filter = false
    // MapPro.each(this.props.grid.fields, (form, name) => {
    //   if (!this.props.value.filter[name]) {
    //     this.props.value.filter[name] = {
    //         type: form.filter
    //     }
    //     filter = true
    //   }
    // })

    let sort = this.props.grid.sort && this.props.grid.sort != this.props.value.sort ? this.props.grid.sort : (this.props.grid.sortNull ? this.props.grid.sortNull : [])

    this.props.value.sort = sort

    this.props.page.update()

    this.parse_data()
  }

  componentWillUpdate() {
    let curr = this.state.updating || this.props.value.need_update

    if (this.prev != curr && !curr) {

      window.clearTimeout(this.timeoutRenderItems)

      this.timeoutRenderItems = window.setTimeout(()=>{

        this.state.caclulate_render = true
        this.state.need_rerender = false

        this.forceUpdate()

        this.items = this.props.value.parsed_data.map((item, index)=>{
          return {
            updated: false,
            need_info: true,
            index: index,
            id: item.id,
            data: item
          }
        })

        this.state.caclulate_render = false
        this.state.need_rerender = true

        this.forceUpdate()
      }, 0)
    }

    this.willUpdate()

    this.prev = curr
  }

  reget_info() {
    if (this.props.grid.big_data) {
      if (this.items.length > 0 && this.items[0].momento) {
        let item = this.items[0]
        let elem = $(ReactDOM.findDOMNode(this.refs.tbody)).find('tr[id=' + this.id + item.index + ']')
        if (elem[0]) {
          let min = elem.offset().top
          let height = elem.outerHeight()
          let max = min + height

          item.elem = elem[0]
          item.min = min
          item.height = height
          item.max = max
          item.need_info = false

          this.etaloneHeight = height
          this.etaloneOffset = min
        }
      }

      this.few.forEach((item)=>{
        let elem = $(ReactDOM.findDOMNode(this.refs.tbody)).find('tr[id=' + this.id + item.index + ']')
        if (elem[0]) {
          let min = elem.offset().top
          let height = elem.outerHeight()
          let max = min + height

          item.elem = elem[0]
          item.min = min
          item.height = height
          item.max = max
          item.need_info = false
        }
      })
    }
    else {
      this.items.forEach((item)=>{
        if (!item.need_info) return
        let elem = $(ReactDOM.findDOMNode(this.refs.tbody)).find('tr[id=' + this.id + item.index + ']')
        if (elem[0]) {
          let min = elem.offset().top
          let height = elem.outerHeight()
          let max = min + height

          item.elem = elem[0]
          item.min = min
          item.height = height
          item.max = max
          item.need_info = false
        }
      })
    }
  }

  componentDidUpdate() {
    if (this.state.need_rerender) {
      this.reget_info()

      this.state.need_rerender = false
      this.state.rows_updated = true

      this.forceUpdate()
    }
    else if (this.props.grid.big_data && this.few.filter((item) => item.need_info).length > 0) {
      this.reget_info()

      this.forceUpdate()
    }
    else if (!this.props.grid.big_data && this.items.filter((item) => item.need_info).length > 0) {
      this.reget_info()

      this.forceUpdate()
    }
    else if (this.props.value.loaded && !this.state.updating && !this.props.value.need_update && this.state.rows_updated && this.items.length == this.props.value.parsed_data.length) {
      if (this.props.value.scroll && this.props.value.need_scroll) {
        const key = this.props.value.scroll.key
        const value = this.props.value.scroll.value(this.props.content, this.props.context, this.props.page, this.props.application)

        let scrollToIndex = (index) => {
          let windowHeight = $(window).height();

          let offset

          if (this.items[index].max && this.items[index].height) {
            offset = this.items[index].max - this.items[index].height / 2 - windowHeight / 2;
          }
          else {
            offset = this.etaloneHeight * (index + 1) + this.etaloneOffset - this.etaloneHeight / 2 - windowHeight / 2;
          }

          let el = $('#' + this.id + index);
          if (el[0]) {
            this.props.value.need_scroll = false
            $('html, body').animate({ scrollTop: offset }, 300, () => {
                $(el.children()).velocity({ opacity: 1.0, backgroundColor: '#e0f0ff' }, {
                    duration: 300, easing: "swing", loop: 1, complete: () => {
                $(el.children()).attr("style", "");
              }})
            });
          }
          else {
            $('html, body').animate({scrollTop: offset}, 100, ()=>{
              this.forceUpdate()
            });
          }
        }

        this.props.value.data.forEach((row, index) => {
          let isIn = row[key] == value
          if (isIn) {
            if (this.props.value.per_page == null && this.props.value.page == 0) {
              scrollToIndex(index)
            }
            else if (index < (this.props.value.page + 1) * this.props.value.per_page && index >= this.props.value.page * this.props.value.per_page) {
              scrollToIndex(index - Math.floor(index / this.props.value.per_page) * this.props.value.per_page)
            }
            else {
              this.props.value.page = Math.floor(index / this.props.value.per_page)
              this.parse_data()
            }
          }
        })
      }
    }
  }

  factory_run_recurse(func_name) {
    if (this.props.value.need_update) {
      this.props.value.need_update = false

      this[func_name]()
    }
    else {
      this.state.updating = false
      this.forceUpdate()
    }
  }

  recurse_remote_data() {
    this.state.updating = true
    this.forceUpdate()

    window.setTimeout(async () => {
      try {
        const response = await this.props.grid.request(this.props.value, this.props.context, this.props.page, this.props.application)
        this.props.value.parsed_data = uniqueIds(response.data)
        this.props.value.total = response.total
        this.props.value.page = 0
        this.cleanSelect()
        this.props.page.update()

        this.factory_run_recurse('recurse_remote_data')
      }
      catch (message) {
        console.error(message)

        this.factory_run_recurse('recurse_remote_data')
      }
    }, 300)
  }

  recurse_remote_pagination_data() {
    this.state.updating = true
    this.forceUpdate()

    window.setTimeout(async () => {
      try {
        const response = await this.props.grid.request(this.props.value, this.props.context, this.props.page, this.props.application)
        this.props.value.parsed_data = uniqueIds(response.data)
        this.props.value.total = response.total
        this.cleanPage()
        this.cleanSelect()
        this.props.page.update()

        this.factory_run_recurse('recurse_remote_pagination_data')
      }
      catch (message) {
        console.error(message)

        this.factory_run_recurse('recurse_remote_pagination_data')
      }
    }, 300)
  }

  recurse_remote_local_data() {
    this.state.updating = true
    this.forceUpdate()

    window.setTimeout(async () => {
      try {
        if (!this.props.value.loaded) {
          const response = await this.props.grid.request(this.props.value, this.props.context, this.props.page, this.props.application)
          this.props.value.data = uniqueIds(response.data)

          this.props.value.loaded = true

          this.props.value.after_request = true
        }

        let temp = this.props.value.data.filter(row => {
          return !MapPro.find(this.props.grid.fields, (form, name) => {
            if (this.props.value.filter && this.props.value.filter[name] && form.filters && form.filters[this.props.value.filter[name].type] && form.filters[this.props.value.filter[name].type].check) {
              const filter_type = this.props.value.filter[name].type
              const filter_value = this.props.value.filter[name].value
              const value = row[name]
              return !form.filters[filter_type].check(value, filter_value, form, this.props.context, this.props.page, this.props.application)
            }
            else {
              return false
            }
          })
        })

        if (this.props.value.sort && this.props.value.sort.length > 0) {
          this.props.value.sort.reverse().forEach((s) => {
            let oper = s.substring(0, 1)
            let by = s.substring(1)

            if (oper === '-') {
              temp = temp.sort((a, b) => {
                return a[by] > b[by] ? 1 : 0
              });
            }
            else {
              temp = temp.sort((a, b) => {
                return a[by] < b[by] ? 1 : 0
              });
            }
          })
          this.props.value.sort.reverse()
        }

        this.props.value.total = temp.length
        this.props.value.parsed_data = temp

        this.props.value.page = 0
        this.cleanSelect()
        this.props.page.update()

        this.factory_run_recurse('recurse_remote_local_data')
      }
      catch (message) {
        console.error(message)

        this.factory_run_recurse('recurse_remote_local_data')
      }
    }, 300)
  }

  recurse_remote_local_pagination_data() {
    this.state.updating = true
    this.forceUpdate()

    window.setTimeout(async () => {
      try {
        if (!this.props.value.loaded) {
          const response = await this.props.grid.request(this.props.value, this.props.context, this.props.page, this.props.application)
          this.props.value.data = uniqueIds(response.data)

          this.props.value.loaded = true

          this.props.value.after_request = true
        }

        let temp = this.props.value.data.filter(row => {
          return !MapPro.find(this.props.grid.fields, (form, name) => {

            if (this.props.value.filter && this.props.value.filter[name] && form.filters && form.filters[this.props.value.filter[name].type] && form.filters[this.props.value.filter[name].type].check) {
              const filter_type = this.props.value.filter[name].type
              const filter_value = this.props.value.filter[name].value
              const value = row[name]
              return !form.filters[filter_type].check(value, filter_value, form, this.props.context, this.props.page, this.props.application)
            }
            else {
              return false
            }
          })
        })

        if (this.props.value.sort && this.props.value.sort.length > 0) {
          this.props.value.sort.reverse().forEach((s) => {
            let oper = s.substring(0, 1)
            let by = s.substring(1)

            if (oper === '-') {
              temp = temp.sort((a, b) => {
                return a[by] > b[by] ? 1 : 0
              });
            }
            else {
              temp = temp.sort((a, b) => {
                return a[by] < b[by] ? 1 : 0
              });
            }
          })
          this.props.value.sort.reverse()
        }

        this.props.value.total = temp.length

        if (this.props.value.per_page != null) temp = temp.filter((item, index) => {
          return index < (this.props.value.page + 1) * this.props.value.per_page && index >= this.props.value.page * this.props.value.per_page
        })

        this.props.value.parsed_data = temp

        this.cleanPage()
        this.cleanSelect()
        this.props.page.update()

        this.factory_run_recurse('recurse_remote_local_pagination_data')
      }
      catch (message) {
        console.error(message)

        this.factory_run_recurse('recurse_remote_local_pagination_data')
      }
    }, 300)
  }

  recurse_local_data() {
    this.state.updating = true
    this.forceUpdate()

    window.setTimeout(() => {
      let temp = this.props.value.data.filter(row => {
        return !MapPro.find(this.props.grid.fields, (form, name) => {
          if (this.props.value.filter && this.props.value.filter[name] && form.filters && form.filters[this.props.value.filter[name].type] && form.filters[this.props.value.filter[name].type].check) {
            const filter_type = this.props.value.filter[name].type
            const filter_value = this.props.value.filter[name].value
            const value = row[name]
            return !form.filters[filter_type].check(value, filter_value, form, this.props.context, this.props.page, this.props.application)
          }
          else {
            return false
          }
        })
      })

      if (this.props.value.sort && this.props.value.sort.length > 0) {
        this.props.value.sort.reverse().forEach((s) => {
          let oper = s.substring(0, 1)
          let by = s.substring(1)

          if (oper === '-') {
            temp = temp.sort((a, b) => {
              return a[by] > b[by] ? 1 : 0
            });
          }
          else {
            temp = temp.sort((a, b) => {
              return a[by] < b[by] ? 1 : 0
            });
          }
        })
        this.props.value.sort.reverse()
      }

      this.props.value.total = temp.length
      this.props.value.parsed_data = temp
      this.props.value.page = 0
      this.cleanSelect()
      this.props.page.update()

      this.factory_run_recurse('recurse_local_data')
    }, 300)
  }

  recurse_local_pagination_data() {
    this.state.updating = true
    this.forceUpdate()

    window.setTimeout(() => {
      let temp = this.props.value.data.filter(row => {
        return !MapPro.find(this.props.grid.fields, (form, name) => {
          if (this.props.value.filter && this.props.value.filter[name] && form.filters && form.filters[this.props.value.filter[name].type] && form.filters[this.props.value.filter[name].type].check) {
            const filter_type = this.props.value.filter[name].type
            const filter_value = this.props.value.filter[name].value
            const value = row[name]
            return !form.filters[filter_type].check(value, filter_value, form, this.props.context, this.props.page, this.props.application)
          }
          else {
            return false
          }
        })
      })

      if (this.props.value.sort && this.props.value.sort.length > 0) {
        this.props.value.sort.reverse().forEach((s) => {
          let oper = s.substring(0, 1)
          let by = s.substring(1)

          if (oper === '-') {
            temp = temp.sort((a, b) => {
              return a[by] > b[by] ? 1 : 0
            });
          }
          else {
            temp = temp.sort((a, b) => {
              return a[by] < b[by] ? 1 : 0
            });
          }
        })
        this.props.value.sort.reverse()
      }

      this.props.value.total = temp.length

      if (this.props.value.per_page != null) temp = temp.filter((item, index) => {
        return index < (this.props.value.page + 1) * this.props.value.per_page && index >= this.props.value.page * this.props.value.per_page
      })

      this.props.value.parsed_data = temp

      this.cleanPage()
      this.cleanSelect()
      this.props.page.update()

      this.factory_run_recurse('recurse_local_pagination_data')
    }, 300)
  }

  parse_data() {
    if (this.state.updating) {
      this.props.value.need_update = true
    }
    else if (this.props.grid.data_type == 'local') {
      this.state.rows_updated = false

      if (this.props.grid.pagination) {
        this.recurse_local_pagination_data()
      }
      else {
        this.recurse_local_data()
      }
    }
    else if (this.props.grid.data_type == 'remote') {
      this.state.rows_updated = false

      if (this.props.grid.pagination) {
        this.recurse_remote_pagination_data()
      }
      else {
        this.recurse_remote_data()
      }
    }
    else if (this.props.grid.data_type == 'remote_local') {
      this.state.rows_updated = false

      if (this.props.grid.pagination) {
        this.recurse_remote_local_pagination_data()
      }
      else {
        this.recurse_remote_local_data()
      }
    }
  }

  validate(row: IGridRow): Map<GridMessages> {
    let result: Map<GridMessages> = {}

    MapPro.each(this.props.grid.fields, (form, name) => {
        let messages: GridMessages = {}

        if (form.error) {
            messages.error = form.error(row, this.props.content, this.props.context, this.props.page, this.props.application)
        }

        if (form.info) {
            messages.info = form.info(row, this.props.content, this.props.context, this.props.page, this.props.application)
        }

        if (form.warning) {
            messages.warning = form.warning(row, this.props.content, this.props.context, this.props.page, this.props.application)
        }

        if (form.if) {
            messages.if = form.if(row, this.props.content, this.props.context, this.props.page, this.props.application)
        }
        else {
            messages.if = true
        }

        if (form.enabled) {
            messages.enabled = form.enabled(row, this.props.content, this.props.context, this.props.page, this.props.application)
        }
        else {
            messages.enabled = true
        }

        result[name] = messages
    })

    return result
  }

  componentWillReceiveProps(nextProps) {
    const { grid, value } = this.props

    if ((nextProps.grid !== grid || nextProps.value !== value) || (value.need_update && !this.state.updating)) {

    //   let filter = false
    //   MapPro.each(this.props.grid.fields, (form, name) => {
    //     if (!this.props.value.filter[name]) {
    //       this.props.value.filter[name] = {}
    //       this.props.value.filter[name].type = form.filter
    //       filter = true
    //     }
    //   })

      let sort = this.props.grid.sort && this.props.grid.sort != this.props.value.sort ? this.props.grid.sort : (this.props.grid.sortNull ? this.props.grid.sortNull : [])
      this.props.value.sort = sort

      this.props.page.update()

      this.parse_data()
    }
  }

  get_filter_types(filters: string[]): Map<string> {
    return MapPro.filter(this.filter_types, (item, name)=>{return !!filters.find((row)=>{return row == name})})
  }

  renderItem(row: IGridRow, index: number): JSX.Element {
    if (!row) return null

    let cells = []

    let is_first = index == 0
    let is_last = index == this.props.value.parsed_data.length - 1

    let allow_edit = this.props.grid.allow_edit ? this.props.grid.allow_edit(row, this.props.context, this.props.page, this.props.application) : false

    if (this.props.value.edit[row.id] && allow_edit) {
        
      let validate = this.validate(this.props.value.edit[row.id])
      
      this.props.value.messages[row.id] = validate

      MapPro.each(this.props.grid.fields, (form, name) => {

        let message = []

        if (validate[name].error) {
          message.push(
            <div className="padding-left-1 padding-top-1 padding-bottom-d1 text mini red" key="error">
              {validate[name].error}
            </div>
          )
        }

        if (validate[name].info) {
          message.push(
            <div className="padding-left-1 padding-top-1 padding-bottom-d1 text mini sub" key="info">
              {validate[name].info}
            </div>
          )
        }

        if (validate[name].warning) {
          message.push(
            <div className="padding-left-1 padding-top-1 padding-bottom-d1 text mini warning" key="warning">
              {validate[name].warning}
            </div>
          )
        }

        if (form.edit_render) {
          let valueLink = {
            value: this.props.value.edit[row.id][name],
            requestChange: (newValue) => {
              this.props.value.edit[row.id][name] = newValue
              this.items[index].updated = false
              this.props.page.update()
            }
          }

          cells.push(
            <Popup key={name}
              position="bottom center"
              openOnOverMove closeOnOutMove
              openOnFocus closeOnBlur
              isHidden={message.length == 0}
              delay={100} isGhost
              activeClassName="active"
              element={
                <td key={name} className={form.classEdit ? form.classEdit : ''}>
                    {form.edit_render(valueLink, form, validate[name], this.props.content, this.props.context, this.props.page, this.props.application)}
                </td>
              }
              >
              <div className="min-width-3 width-full padding-d1">
                <StdAnim duration={300} type="fade">{message}</StdAnim>
              </div>
            </Popup>
          )
        }
        else {
          cells.push(
            <td key={name}>
            </td>
          )
        }
      })
    }
    else {
      MapPro.each(this.props.grid.fields, (form, name) => {
        if (form.render) {
          cells.push(
            <td key={name} className={form.classRow ? form.classRow : ''}>
                {form.render(row[name], form, this.props.content, this.props.context, this.props.page, this.props.application)}
            </td>
          )
        }
        else {
          cells.push(
            <td key={name}>
            </td>
          )
        }
      })
    }

    let rowMenu = []

    let connectionPortal: ConnectionPortal = {}

    if (!this.props.value.edit[row.id] && allow_edit) rowMenu.push(
      <div key="change" className="sp-item text thick padding-horizontal-d2 padding-vertical-1" onClick={()=>{
          this.props.value.edit[row.id] = Object.assign({}, row)
          this.items[index].updated = false
          this.props.page.update()
          connectionPortal.close()
        }}>
        Изменить
      </div>
    )

    if (this.props.value.edit[row.id] && allow_edit) rowMenu.push(
      <div key="cancel" className="sp-item text thick padding-horizontal-d2 padding-vertical-1" onClick={()=>{
          delete this.props.value.edit[row.id]
          this.items[index].updated = false
          this.props.page.update()
          connectionPortal.close()
        }}>
        Отмена
      </div>
    )

    if (!this.props.value.selected && this.props.grid.selectable) rowMenu.push(
      <div key="select" className="sp-item text thick padding-horizontal-d2 padding-vertical-1" onClick={()=>{
          this.props.value.selected = {}
          this.props.value.selected[row.id] = row
          this.items[index].updated = false
          this.props.page.update()
          connectionPortal.close()
        }}>
        Выбрать
      </div>
    )

    if (this.props.value.selected && this.props.grid.selectable) rowMenu.push(
      <div key="unselect" className="sp-item text thick padding-horizontal-d2 padding-vertical-1" onClick={()=>{
          this.props.value.selected = null
          this.items[index].updated = false
          this.props.page.update()
          connectionPortal.close()
        }}>
        Отменить выбор
      </div>
    )

    let onSelect = async () => {
      if (this.props.value.selected) {
        if (this.props.value.selected[row.id]) {
          delete this.props.value.selected[row.id]
          if (Object.keys(this.props.value.selected).length == 0) this.props.value.selected = null
          this.items[index].updated = false
          this.props.page.update()
        }
        else {
          this.props.value.selected[row.id] = row
          this.items[index].updated = false
          this.props.page.update()
        }
      }
      else if (!this.props.value.edit[row.id] && this.props.grid.on_click) {
        this.state.loading.on_click[row.id] = true
        this.items[index].updated = false
        this.forceUpdate()
        try {
          await this.props.grid.on_click(row, this.props.content, this.props.context, this.props.page, this.props.application)
        }
        catch (message) {
          console.error(message)
        }
        delete this.state.loading.on_click[row.id]
        this.items[index].updated = false
        this.forceUpdate()

        this.parse_data()
      }
    }

    let onKeyDown = (event) => {
      if (event.keyCode == 13 || event.keyCode == 32) {
        onSelect()
      }
      else if (event.keyCode == 40 && !is_last) $('#' + this.id + (index + 1)).focus()
      else if (event.keyCode == 38 && !is_first) $('#' + this.id + (index - 1)).focus()
      else if (event.keyCode == 37) {
        Helper.focusBack(event.target)
      }
      else if (event.keyCode == 39) {
        Helper.focusNext(event.target)
      }
    }

    if (this.props.value.edit[row.id]) {
      if (this.props.grid.edit_context_menu) MapPro.each(this.props.grid.edit_context_menu, (button, name)=>{
        let _if = button.if ? button.if(this.props.value.edit[row.id], row, this.props.context, this.props.page, this.props.application) : true
        if (_if) {
          let enabled = button.enabled ? button.enabled(this.props.value.edit[row.id], row, this.props.context, this.props.page, this.props.application) : true
          rowMenu.push(
            <div key={name} className={"sp-item text thick padding-horizontal-d2 padding-vertical-1" + (!enabled ? ' enabled' : '')} onClick={async ()=>{
                connectionPortal.close()
                this.state.updating = true
                this.state.loading.on_create = true
                this.items[index].updated = false
                this.forceUpdate()
                try {
                  const result = await button.on_click(this.props.value.edit[row.id], row, this.props.context, this.props.page, this.props.application)
                  if (result == true) {
                    delete this.props.value.edit[row.id]
                    this.items[index].updated = false
                    this.props.page.update()
                    connectionPortal.close()
                  }
                }
                catch (message) {
                  console.error(message)
                }
                this.state.updating = false
                this.state.loading.on_create = false
                this.items[index].updated = false
                this.forceUpdate()

                this.parse_data()
              }}>
              {button.text}
            </div>
          )
        }
      })
    }
    else {
      if (this.props.grid.row_context_menu) MapPro.each(this.props.grid.row_context_menu, (button, name)=>{
        let _if = button.if ? button.if(row, this.props.context, this.props.page, this.props.application) : true
        if (_if) {
          let enabled = button.enabled ? button.enabled(row, this.props.context, this.props.page, this.props.application) : true
          rowMenu.push(
            <div key={name} className="sp-item text thick padding-horizontal-d2 padding-vertical-1" onClick={async ()=>{
                connectionPortal.close()
                this.state.updating = true
                this.state.loading.on_create = true
                this.items[index].updated = false
                this.forceUpdate()
                try {
                  await button.on_click(row, this.props.context, this.props.page, this.props.application)
                }
                catch (message) {
                  console.error(message)
                }
                this.state.updating = false
                this.state.loading.on_create = false
                this.items[index].updated = false
                this.forceUpdate()

                this.parse_data()
              }}>
              {button.text}
            </div>
          )
        }
      })
    }

    if (rowMenu.length > 0) {
      return (
        <Popup key={index}
          position="bottom center"
          closeOnEsc closeOnOutsideClick
          closeOnClick
          openOnContextMenu
          connectionPortal={connectionPortal}
          activeClassName="active"
          element={
            (
              <tr id={this.id + index} tabIndex={0} className={"pointer" + (this.props.value.selected && this.props.value.selected[row.id] ? ' selected' : '') + (this.props.value.edit[row.id] ? ' unselectable' : '') + (((this.state.loading.on_edit && this.state.loading.on_edit[row.id]) || this.state.loading.on_click[row.id]) ? ' loading' : '')} onClick={onSelect} onKeyDown={onKeyDown}>
                {cells}
              </tr>
            )
          }
          >
          <div className="sp-menu-item pull-left min-width-3 width-full">
            {rowMenu}
          </div>
        </Popup>
      )
    }
    else {
      return (
        <tr id={this.id + index} key={index} className={(this.props.value.selected && this.props.value.selected[row.id] ? ' selected' : '') + (this.props.value.edit[row.id] ? ' unselectable' : '') + (((this.state.loading.on_edit && this.state.loading.on_edit[row.id]) || this.state.loading.on_click[row.id]) ? ' loading' : '')}>
          {cells}
        </tr>
      )
    }
  }

  render() {
    // const updating = this.state.updating || this.props.value.need_update || !this.state.rows_updated
    const updating = !this.state.rows_updated

    let headers = [];

    let clearSort = () => {
      let sort = this.props.grid.sort && this.props.grid.sort != this.props.value.sort ? this.props.grid.sort : (this.props.grid.sortNull ? this.props.grid.sortNull : [])
      if (sort !== this.props.value.sort) {
        this.props.value.sort = sort
        this.props.page.update()
      }

      this.parse_data()
    }

    let sortString = this.props.value.sort.length > 0 ? (
      <div key="sortString" className="padding-horizontal-d2 padding-vertical-1">
        {this.props.value.sort.join(' > ')}
      </div>
    ) : null

    let i = 0
    MapPro.each(this.props.grid.fields, (form, name) => {

      let sortContains = this.props.value.sort.find((item) => {return item.substring(1) == name})
      let sortOperator = sortContains ? sortContains.substring(0, 1) : null

      let sortSet = (operator) => {
        if (sortContains && sortOperator == operator) {
          let temp = []
          this.props.value.sort.forEach((item) => {
            if (item.substring(1) != name) {
              temp.push(item)
            }
          })
          this.props.value.sort = temp
        this.props.page.update()
        }
        else if (sortContains && sortOperator != operator) {
          let temp = []
          this.props.value.sort.forEach((item) => {
            if (item.substring(1) != name) {
              temp.push(item)
            }
            else {
              temp.push(operator + name)
            }
          })
          this.props.value.sort = temp
        this.props.page.update()
        }
        else if (!sortContains) {
          let temp = []
          this.props.value.sort.forEach((item) => {
            temp.push(item)
          })
          temp.push(operator + name)
          this.props.value.sort = temp
        this.props.page.update()
        }

        this.parse_data()
      }

      let sortPanel = form.sortable ? [
        sortString,
        (
          <div key="sortPanel" className="sp-table-layout">
            <div className="">
              <div tabIndex={0} className={"btn btn-default " + (sortContains && sortOperator == '+')} onClick={()=>{sortSet('+')}} onKeyDown={(e)=>{
                  if (e.keyCode == 13) {
                    sortSet('+')
                    return false
                  }
                  else if (e.keyCode == 37) {
                    Helper.focusBack(e.target as HTMLElement)
                  }
                  else if (e.keyCode == 39) {
                    Helper.focusNext(e.target as HTMLElement)
                  }
                }}>
                <FontAwesome name="chevron-up"/>
              </div>
              <div tabIndex={0} className={"btn btn-default " + (sortContains && sortOperator == '-')} onClick={()=>{sortSet('-')}} onKeyDown={(e)=>{
                  if (e.keyCode == 13) {
                    sortSet('-')
                    return false
                  }
                  else if (e.keyCode == 37) {
                    Helper.focusBack(e.target as HTMLElement)
                  }
                  else if (e.keyCode == 39) {
                    Helper.focusNext(e.target as HTMLElement)
                  }
                }}>
                <FontAwesome name="chevron-down"/>
              </div>
              <div tabIndex={0} className="btn btn-default" onClick={()=>{clearSort()}} onKeyDown={(e)=>{
                  if (e.keyCode == 13) {
                    clearSort()
                    return false
                  }
                  else if (e.keyCode == 37) {
                    Helper.focusBack(e.target as HTMLElement)
                  }
                  else if (e.keyCode == 39) {
                    Helper.focusNext(e.target as HTMLElement)
                  }
                }}>
                <FontAwesome name="remove"/>
              </div>
            </div>
          </div>
        )
      ] : null

      let setFilter = (value: {id: string, name: string}) => {
        if (!this.props.value.filter[name] && value) {
          this.props.value.filter[name] = { type: value.id }
        }
        else if (value) {
          this.props.value.filter[name].type = value.id
        }
        else {
          delete this.props.value.filter[name]
        }
        this.props.page.update()

        this.parse_data()
      }

      let typePanel = null
      let filterPanel = null

      let filterTypes = form.filter ? this.get_filter_types(form.filter) : new Map<string>()

      if (Object.keys(filterTypes).length > 0) {

        let filterName = this.props.value.filter[name] ? this.props.value.filter[name].type : null

        let filterValueLink: IValueLink<{id: string, name: string}> = {
          value: filterName ? {
            id: filterName,
            name: filterTypes[filterName]
          } : null,
          requestChange: setFilter.bind(this)
        }

        let filterData = MapPro.map(filterTypes, (item, name)=>{return {
            id: name,
            name: item
        }})

        typePanel = (
          <Select className="margin-px1" level={1}
            valueLink={filterValueLink} data_type='local' search='none' data={filterData}/>
        )

        if (form.filters && form.filters[filterName] && form.filters[filterName].render) {
          let filterMValueLink: IValueLink<any> = {
            value: this.props.value.filter[name].value,
            requestChange: (newValue) => {
              this.props.value.filter[name].value = newValue
              this.props.page.update()

              this.parse_data()
            }
          }

          filterPanel = form.filters[filterName].render(filterMValueLink, form, this.props.content, this.props.context, this.props.page, this.props.application)
        }
      }

      if (typePanel || filterPanel || sortPanel) {
        headers.push(
          <Dropdown
            key={name}
            ref="dropdownMenu"
            position="bottom right"
            closeOnEsc closeOnOutsideClick
            toggleOnClick
            activeClassName="active"
            element={(
              <td className={"pointer sp-dropdown2-item"}>
                <div className="" style={{display: 'inline-block'}}>
                  {form.label}
                </div>
                <FontAwesome className="sp-dropdown2-on-open" style={{float: 'right'}} name={updating ? "spinner" : "chevron-up"} pulse={updating}/>
                <FontAwesome className="sp-dropdown2-on-not-open" style={{float: 'right'}} name={updating ? "spinner" : "chevron-down"} pulse={updating}/>
              </td>
            )}
            >
            <div className="sp-dropdown2-menu pull-left min-width-3 width-full">

              {typePanel}

              {filterPanel}

              {sortPanel}
            </div>
          </Dropdown>
        )
      }
      else {
        headers.push(
          <td key={name}>{form.label}</td>
        )
      }

      i++
    })

    let count = i

    let allow_add = this.props.grid.allow_add ? this.props.grid.allow_add(this.props.context, this.props.page, this.props.application) : false

    let creation = null

    if (allow_add) {

      let create = []

      let validate = this.validate(this.props.value.create)
      this.props.value.create_messages = validate

      MapPro.each(this.props.grid.fields, (form, name) => {

        let message = []

        if (validate[name].error) {
          message.push(
            <div className="padding-left-1 padding-top-1 padding-bottom-d1 text mini red" key="error">
              {validate[name].error}
            </div>
          )
        }

        if (validate[name].info) {
          message.push(
            <div className="padding-left-1 padding-top-1 padding-bottom-d1 text mini sub" key="info">
              {validate[name].info}
            </div>
          )
        }

        if (validate[name].warning) {
          message.push(
            <div className="padding-left-1 padding-top-1 padding-bottom-d1 text mini warning" key="warning">
              {validate[name].warning}
            </div>
          )
        }

        if (form.create_render) {
          let valueLink = {
            value: this.props.value.create[name],
            requestChange: (newValue) => {
              this.props.value.create[name] = newValue
              this.props.page.update()
            }
          }

          create.push(
            <Popup key={name}
              position="bottom center"
              openOnOverMove closeOnOutMove
              openOnFocus closeOnBlur
              isHidden={message.length == 0}
              delay={100} isGhost
              activeClassName="active"
              element={
                <td key={name} className={form.classCreate ? form.classCreate : ''}>
                    {form.create_render(valueLink, form, validate[name], this.props.content, this.props.context, this.props.page, this.props.application)}
                </td>}
              >
              <div className="min-width-3 width-full padding-d1">
                <StdAnim duration={300} type="fade">{message}</StdAnim>
              </div>
            </Popup>
          )
        }
        else {
          create.push(
            <td key={name}>
            </td>
          )
        }
      })

      let connectionPortal: ConnectionPortal = {}

      let creationMenu = []

      if (this.props.grid.create_context_menu) MapPro.each(this.props.grid.create_context_menu, (button, name)=>{
        let _if = button.if ? button.if(this.props.value.create, this.props.context, this.props.page, this.props.application) : true
        if (_if) {
          let enabled = button.enabled ? button.enabled(this.props.value.create, this.props.context, this.props.page, this.props.application) : true
          creationMenu.push(
            <div key={name} className={"sp-item text thick padding-horizontal-d2 padding-vertical-1" + (!enabled ? ' disabled' : '')} onClick={async ()=>{
                connectionPortal.close()
                this.state.updating = true
                this.state.loading.on_create = true
                try {
                  await button.on_click(this.props.value.create, this.props.context, this.props.page, this.props.application)
                }
                catch (message) {
                  console.error(message)
                }
                this.state.updating = false
                this.state.loading.on_create = false
                this.forceUpdate()

                this.parse_data()
              }}>
              {button.text}
            </div>
          )
        }
      })

      if (creationMenu.length > 0) {
        creation = (
          <Popup
            position="bottom center"
            closeOnEsc closeOnOutsideClick
            closeOnClick
            openOnContextMenu
            connectionPortal={connectionPortal}
            activeClassName="active"
            element={
              (
                <tr className={"unselectable" + (this.state.loading.on_create ? ' loading' : '')}>
                  {create}
                </tr>
              )
            }
            >
            <div className="sp-menu-item pull-left min-width-3 width-full">
              {creationMenu}
            </div>
          </Popup>
        )
      }
      else {
        creation = (
          <tr className={"unselectable" + (this.state.loading.on_create ? ' loading' : '')}>
            {create}
          </tr>
        )
      }
    }

    let pagination = null

    if (this.props.grid.pagination && this.props.value.per_page != null) {
      let items = []

      let setPage = (page) => {
        this.props.value.page = page
        this.props.page.update()
        this.need_on_scroll = true

        this.parse_data()
      }

      if (this.props.value.page > 0) {
        items.push(
          <div key={0} className="sp-item" onClick={setPage.bind(this, 0)}>
            <div>1</div>
          </div>
        )
      }

      if (this.props.value.page > 1) {
        items.push(
          <div key={1} className="sp-item" onClick={setPage.bind(this, 1)}>
            <div>2</div>
          </div>
        )
      }

      if (this.props.value.page > 3) {
        items.push(
          <div key={2} className="sp-item disabled">
            <div>...</div>
          </div>
        )
      }

      if (this.props.value.page > 2) {
        items.push(
          <div key={3} className="sp-item" onClick={setPage.bind(this, this.props.value.page - 1)}>
            <div>{this.props.value.page}</div>
          </div>
        )
      }

      if (this.props.value.per_page < this.props.value.total) {
        items.push(
          <div key={4} className="sp-item active">
            <div>{this.props.value.page + 1}</div>
          </div>
        )
      }

      let maxPage = Math.ceil(this.props.value.total / this.props.value.per_page) - 1

      if (this.props.value.page < maxPage - 2) {
        items.push(
          <div key={5} className="sp-item" onClick={setPage.bind(this, this.props.value.page + 1)}>
            <div>{this.props.value.page + 2}</div>
          </div>
        )
      }

      if (this.props.value.page < maxPage - 3) {
        items.push(
          <div key={6} className="sp-item disabled">
            <div>...</div>
          </div>
        )
      }

      if (this.props.value.page < maxPage - 1) {
        items.push(
          <div key={7} className="sp-item" onClick={setPage.bind(this, maxPage - 1)}>
            <div>{maxPage}</div>
          </div>
        )
      }

      if (this.props.value.page < maxPage) {
        items.push(
          <div key={8} className="sp-item" onClick={setPage.bind(this, maxPage)}>
            <div>{maxPage + 1}</div>
          </div>
        )
      }

      pagination = items.length > 0 ? (
        <div className={"sp-pagination" + (updating ? ' loading' : '')}>
          {items}
        </div>
      ) : null
    }

    return (
      <table className="sp-table">
        <thead>
          <tr>
            {headers}
          </tr>
        </thead>
        <tbody>
          {creation}
        </tbody>
        <tbody ref="tbody">
          {this.props.grid.big_data ?
            [
              this.items.length > 0 && this.items[0].momento ? this.items[0].momento : null,
              (<tr key="top_space" className="stripes" style={{height: this.t}}>
                {this.etaloneHeight > 0 && this.t >= this.etaloneHeight ? (<td colSpan={Object.keys(this.props.grid.fields).length}>
                </td>) : null}
              </tr>),
              this.few.map(item => item.momento ? item.momento : (
                <tr key={item.index} className="stripes" style={{height: this.etaloneHeight}}>
                  <td colSpan={Object.keys(this.props.grid.fields).length}>
                  </td>
                </tr>
              )),
              (<tr key="botom_space" className="stripes" style={{height: this.b}}>
                {this.etaloneHeight > 0 && this.b >= this.etaloneHeight ? (<td colSpan={Object.keys(this.props.grid.fields).length}>
                </td>) : null}
              </tr>)
            ] : this.items.map(item => item.momento)
          }
        </tbody>
        <tfoot>
          <tr>
            <td colSpan={Object.keys(this.props.grid.fields).length} className="padding-0">
              {pagination}
            </td>
          </tr>
        </tfoot>
      </table>
    );
  }
}