"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const React = require('react');
const ReactDOM = require('react-dom');
const Dropdown_1 = require('../Controls/Dropdown');
const Popup_1 = require('../Controls/Popup');
const Select_1 = require('../Controls/Select');
const StdAnim_1 = require('../Controls/StdAnim');
const FontAwesome = require('react-fontawesome');
const Helper_1 = require('../Controls/Helper');
const Main_1 = require('../Main');
let __id = 0;
const uniqueId = function (item) {
    if (typeof item.id == "undefined") {
        item.id = ++__id;
    }
    return item;
};
const uniqueIds = function (item) {
    for (let i = 0; i < item.length; i++) {
        uniqueId(item[i]);
    }
    return item;
};
class GridMessages {
}
exports.GridMessages = GridMessages;
class GridItemModel {
    render(value, field, content, context, page, application) {
        return null;
    }
    create_render(valueLink, field, messages, content, context, page, application) {
        return null;
    }
    edit_render(valueLink, field, messages, content, context, page, application) {
        return null;
    }
}
exports.GridItemModel = GridItemModel;
class IGridValue {
    constructor(initializer) {
        this.messages = {};
        this.create_messages = {};
        this.create = { id: null };
        this.edit = {};
        this.sort = [];
        this.filter = {};
        this.per_page = 10;
        this.page = 0;
        this.total = 0;
        if (initializer === undefined) {
            return;
        }
        Object.keys(initializer).forEach((key, index) => {
            this[key] = initializer[key];
        });
    }
}
exports.IGridValue = IGridValue;
class GridContextCreateButton {
}
exports.GridContextCreateButton = GridContextCreateButton;
class GridContextRowButton {
}
exports.GridContextRowButton = GridContextRowButton;
class GridContextChangeButton {
}
exports.GridContextChangeButton = GridContextChangeButton;
class IGrid {
    constructor(initializer) {
        this.row_context_menu = {};
        this.create_context_menu = {};
        this.edit_context_menu = {};
        this.sortNull = [];
        this.sort = [];
        if (initializer === undefined) {
            return;
        }
        Object.keys(initializer).forEach((key, index) => {
            this[key] = initializer[key];
        });
    }
}
exports.IGrid = IGrid;
class IGridModel extends Main_1.IContentModel {
    constructor() {
        super(...arguments);
        this.value = new IGridValue();
        this.grid = new IGrid();
    }
}
exports.IGridModel = IGridModel;
class GridModel extends Main_1.ContentModel {
    constructor(initializer) {
        super();
        if (initializer === undefined) {
            return;
        }
        this.class = initializer.class;
        if (initializer.value)
            this.value = new IGridValue(initializer.value);
        if (initializer.grid)
            this.grid = new IGrid(initializer.grid);
    }
    render(context, page, application, containerFactory) {
        return (React.createElement("div", {className: this.class ? this.class : ''}, React.createElement(Grid, { value: this.value, grid: this.grid, content: this, context, page, application, containerFactory })));
    }
}
exports.GridModel = GridModel;
class GridProps {
}
exports.GridProps = GridProps;
class GridState {
    constructor() {
        this.if = {};
        this.enabled = {};
        this.caclulate_render = true;
        this.need_rerender = false;
        this.updating = false;
        this.rows_updated = false;
        this.loading = {
            on_create: false,
            on_edit: {},
            on_click: {}
        };
    }
}
exports.GridState = GridState;
class Grid extends React.Component {
    constructor() {
        super(...arguments);
        this.filter_types = {
            contains: 'Содержит',
            totaly: 'Аналогично',
            not_in: 'Не содержит',
            null: 'Пусто',
            not_null: 'Не пусто',
        };
        this.state = new GridState();
        this.id = Helper_1.default.guid();
        this.items = [];
        this.few = [];
        this.etaloneHeight = 0;
        this.etaloneOffset = 0;
        this.t = 0;
        this.b = 0;
        this.need_on_scroll = false;
        this.prev = false;
    }
    cleanSelect() {
        if (this.props.value.selected) {
            Main_1.MapPro.each(this.props.value.selected, (item, index) => {
                let find = this.props.value.parsed_data.find((row) => { return row === item || row.id == item.id; });
                if (!find) {
                    delete this.props.value.selected[index];
                }
            });
            if (Object.keys(this.props.value.selected).length == 0)
                this.props.value.selected = null;
        }
        this.props.page.update();
    }
    cleanPage() {
        if (this.props.grid.pagination && this.props.value.per_page != null) {
            if (Math.floor(this.props.value.total / this.props.value.per_page) < this.props.value.page) {
                this.props.value.page = Math.floor(this.props.value.total / this.props.value.per_page);
            }
        }
        else {
            this.props.value.page = 0;
        }
    }
    onScroll() {
        if (this.props.grid.on_scroll) {
            let scrollTop = $(window).scrollTop() + $(window).height() / 2;
            let finded = null;
            for (let index = 0; index < this.items.length; index++) {
                if (this.items[index].min <= scrollTop && this.items[index].max > scrollTop) {
                    finded = index;
                    break;
                }
                else if (this.items[index].min > scrollTop) {
                    break;
                }
            }
            if (finded != null) {
                this.props.grid.on_scroll(this.props.value.parsed_data[finded], this.props.context, this.props.page, this.props.application);
            }
        }
        this.need_on_scroll = false;
    }
    willUpdate() {
        if (this.props.grid.big_data) {
            let top = $(window).scrollTop() - $(window).height();
            let bottom = $(window).scrollTop() + $(window).height() * 2;
            let t = 0;
            let b = 0;
            let elems = [];
            if (this.etaloneHeight > 0)
                for (let index = 1; index < this.items.length; index++) {
                    if ((this.items[index].min ? this.items[index].min : this.etaloneHeight * index + this.etaloneOffset) > bottom) {
                        b = b + (this.items[index].height ? this.items[index].height : this.etaloneHeight);
                    }
                    else if ((this.items[index].max ? this.items[index].max : this.etaloneHeight * (index + 1) + this.etaloneOffset) < top) {
                        t = t + (this.items[index].height ? this.items[index].height : this.etaloneHeight);
                    }
                    else {
                        elems.push(this.items[index]);
                        if (!this.items[index].momento || !this.items[index].updated) {
                            let momento = this.renderItem(this.props.value.parsed_data[index], index);
                            this.items[index].momento = momento;
                            this.items[index].updated = true;
                            this.items[index].need_info = true;
                        }
                    }
                }
            if (this.items.length > 0 && (!this.items[0].updated || !this.items[0].momento)) {
                let momento = this.renderItem(this.props.value.parsed_data[0], 0);
                this.items[0].momento = momento;
                this.items[0].updated = true;
                this.items[0].need_info = true;
            }
            this.t = t;
            this.b = b;
            this.few = elems;
        }
        else {
            for (let index = 0; index < this.items.length; index++) {
                if (!this.items[index].momento || !this.items[index].updated) {
                    let momento = this.renderItem(this.props.value.parsed_data[index], index);
                    this.items[index].momento = momento;
                    this.items[index].updated = true;
                    this.items[index].need_info = true;
                }
            }
        }
    }
    handleScroll(event) {
        window.clearTimeout(this.timerScroll);
        this.timerScroll = window.setTimeout(this.onScroll.bind(this), 150);
        this.forceUpdate();
    }
    componentDidMount() {
        this.handleScrollBind = this.handleScroll.bind(this);
        window.addEventListener('scroll', this.handleScrollBind);
    }
    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScrollBind);
    }
    componentWillMount() {
        window.clearTimeout(this.timeoutRenderItems);
        this.timeoutRenderItems = window.setTimeout(() => {
            this.state.caclulate_render = true;
            this.state.need_rerender = false;
            this.forceUpdate();
            this.items = this.props.value.parsed_data ? this.props.value.parsed_data.map((item, index) => {
                return {
                    updated: false,
                    index: index,
                    id: item.id,
                    data: item,
                    momento: this.props.grid.big_data ? null : this.renderItem(item, index)
                };
            }) : [];
            this.state.caclulate_render = false;
            this.state.need_rerender = true;
            this.forceUpdate();
        }, 0);
        let sort = this.props.grid.sort && this.props.grid.sort != this.props.value.sort ? this.props.grid.sort : (this.props.grid.sortNull ? this.props.grid.sortNull : []);
        this.props.value.sort = sort;
        this.props.page.update();
        this.parse_data();
    }
    componentWillUpdate() {
        let curr = this.state.updating || this.props.value.need_update;
        if (this.prev != curr && !curr) {
            window.clearTimeout(this.timeoutRenderItems);
            this.timeoutRenderItems = window.setTimeout(() => {
                this.state.caclulate_render = true;
                this.state.need_rerender = false;
                this.forceUpdate();
                this.items = this.props.value.parsed_data.map((item, index) => {
                    return {
                        updated: false,
                        need_info: true,
                        index: index,
                        id: item.id,
                        data: item
                    };
                });
                this.state.caclulate_render = false;
                this.state.need_rerender = true;
                this.forceUpdate();
            }, 0);
        }
        this.willUpdate();
        this.prev = curr;
    }
    reget_info() {
        if (this.props.grid.big_data) {
            if (this.items.length > 0 && this.items[0].momento) {
                let item = this.items[0];
                let elem = $(ReactDOM.findDOMNode(this.refs.tbody)).find('tr[id=' + this.id + item.index + ']');
                if (elem[0]) {
                    let min = elem.offset().top;
                    let height = elem.outerHeight();
                    let max = min + height;
                    item.elem = elem[0];
                    item.min = min;
                    item.height = height;
                    item.max = max;
                    item.need_info = false;
                    this.etaloneHeight = height;
                    this.etaloneOffset = min;
                }
            }
            this.few.forEach((item) => {
                let elem = $(ReactDOM.findDOMNode(this.refs.tbody)).find('tr[id=' + this.id + item.index + ']');
                if (elem[0]) {
                    let min = elem.offset().top;
                    let height = elem.outerHeight();
                    let max = min + height;
                    item.elem = elem[0];
                    item.min = min;
                    item.height = height;
                    item.max = max;
                    item.need_info = false;
                }
            });
        }
        else {
            this.items.forEach((item) => {
                if (!item.need_info)
                    return;
                let elem = $(ReactDOM.findDOMNode(this.refs.tbody)).find('tr[id=' + this.id + item.index + ']');
                if (elem[0]) {
                    let min = elem.offset().top;
                    let height = elem.outerHeight();
                    let max = min + height;
                    item.elem = elem[0];
                    item.min = min;
                    item.height = height;
                    item.max = max;
                    item.need_info = false;
                }
            });
        }
    }
    componentDidUpdate() {
        if (this.state.need_rerender) {
            this.reget_info();
            this.state.need_rerender = false;
            this.state.rows_updated = true;
            this.forceUpdate();
        }
        else if (this.props.grid.big_data && this.few.filter((item) => item.need_info).length > 0) {
            this.reget_info();
            this.forceUpdate();
        }
        else if (!this.props.grid.big_data && this.items.filter((item) => item.need_info).length > 0) {
            this.reget_info();
            this.forceUpdate();
        }
        else if (this.props.value.loaded && !this.state.updating && !this.props.value.need_update && this.state.rows_updated && this.items.length == this.props.value.parsed_data.length) {
            if (this.props.value.scroll && this.props.value.need_scroll) {
                const key = this.props.value.scroll.key;
                const value = this.props.value.scroll.value(this.props.content, this.props.context, this.props.page, this.props.application);
                let scrollToIndex = (index) => {
                    let windowHeight = $(window).height();
                    let offset;
                    if (this.items[index].max && this.items[index].height) {
                        offset = this.items[index].max - this.items[index].height / 2 - windowHeight / 2;
                    }
                    else {
                        offset = this.etaloneHeight * (index + 1) + this.etaloneOffset - this.etaloneHeight / 2 - windowHeight / 2;
                    }
                    let el = $('#' + this.id + index);
                    if (el[0]) {
                        this.props.value.need_scroll = false;
                        $('html, body').animate({ scrollTop: offset }, 300, () => {
                            $(el.children()).velocity({ opacity: 1.0, backgroundColor: '#e0f0ff' }, {
                                duration: 300, easing: "swing", loop: 1, complete: () => {
                                    $(el.children()).attr("style", "");
                                } });
                        });
                    }
                    else {
                        $('html, body').animate({ scrollTop: offset }, 100, () => {
                            this.forceUpdate();
                        });
                    }
                };
                this.props.value.data.forEach((row, index) => {
                    let isIn = row[key] == value;
                    if (isIn) {
                        if (this.props.value.per_page == null && this.props.value.page == 0) {
                            scrollToIndex(index);
                        }
                        else if (index < (this.props.value.page + 1) * this.props.value.per_page && index >= this.props.value.page * this.props.value.per_page) {
                            scrollToIndex(index - Math.floor(index / this.props.value.per_page) * this.props.value.per_page);
                        }
                        else {
                            this.props.value.page = Math.floor(index / this.props.value.per_page);
                            this.parse_data();
                        }
                    }
                });
            }
        }
    }
    factory_run_recurse(func_name) {
        if (this.props.value.need_update) {
            this.props.value.need_update = false;
            this[func_name]();
        }
        else {
            this.state.updating = false;
            this.forceUpdate();
        }
    }
    recurse_remote_data() {
        this.state.updating = true;
        this.forceUpdate();
        window.setTimeout(() => __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.props.grid.request(this.props.value, this.props.context, this.props.page, this.props.application);
                this.props.value.parsed_data = uniqueIds(response.data);
                this.props.value.total = response.total;
                this.props.value.page = 0;
                this.cleanSelect();
                this.props.page.update();
                this.factory_run_recurse('recurse_remote_data');
            }
            catch (message) {
                console.error(message);
                this.factory_run_recurse('recurse_remote_data');
            }
        }), 300);
    }
    recurse_remote_pagination_data() {
        this.state.updating = true;
        this.forceUpdate();
        window.setTimeout(() => __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.props.grid.request(this.props.value, this.props.context, this.props.page, this.props.application);
                this.props.value.parsed_data = uniqueIds(response.data);
                this.props.value.total = response.total;
                this.cleanPage();
                this.cleanSelect();
                this.props.page.update();
                this.factory_run_recurse('recurse_remote_pagination_data');
            }
            catch (message) {
                console.error(message);
                this.factory_run_recurse('recurse_remote_pagination_data');
            }
        }), 300);
    }
    recurse_remote_local_data() {
        this.state.updating = true;
        this.forceUpdate();
        window.setTimeout(() => __awaiter(this, void 0, void 0, function* () {
            try {
                if (!this.props.value.loaded) {
                    const response = yield this.props.grid.request(this.props.value, this.props.context, this.props.page, this.props.application);
                    this.props.value.data = uniqueIds(response.data);
                    this.props.value.loaded = true;
                    this.props.value.after_request = true;
                }
                let temp = this.props.value.data.filter(row => {
                    return !Main_1.MapPro.find(this.props.grid.fields, (form, name) => {
                        if (this.props.value.filter && this.props.value.filter[name] && form.filters && form.filters[this.props.value.filter[name].type] && form.filters[this.props.value.filter[name].type].check) {
                            const filter_type = this.props.value.filter[name].type;
                            const filter_value = this.props.value.filter[name].value;
                            const value = row[name];
                            return !form.filters[filter_type].check(value, filter_value, form, this.props.context, this.props.page, this.props.application);
                        }
                        else {
                            return false;
                        }
                    });
                });
                if (this.props.value.sort && this.props.value.sort.length > 0) {
                    this.props.value.sort.reverse().forEach((s) => {
                        let oper = s.substring(0, 1);
                        let by = s.substring(1);
                        if (oper === '-') {
                            temp = temp.sort((a, b) => {
                                return a[by] > b[by] ? 1 : 0;
                            });
                        }
                        else {
                            temp = temp.sort((a, b) => {
                                return a[by] < b[by] ? 1 : 0;
                            });
                        }
                    });
                    this.props.value.sort.reverse();
                }
                this.props.value.total = temp.length;
                this.props.value.parsed_data = temp;
                this.props.value.page = 0;
                this.cleanSelect();
                this.props.page.update();
                this.factory_run_recurse('recurse_remote_local_data');
            }
            catch (message) {
                console.error(message);
                this.factory_run_recurse('recurse_remote_local_data');
            }
        }), 300);
    }
    recurse_remote_local_pagination_data() {
        this.state.updating = true;
        this.forceUpdate();
        window.setTimeout(() => __awaiter(this, void 0, void 0, function* () {
            try {
                if (!this.props.value.loaded) {
                    const response = yield this.props.grid.request(this.props.value, this.props.context, this.props.page, this.props.application);
                    this.props.value.data = uniqueIds(response.data);
                    this.props.value.loaded = true;
                    this.props.value.after_request = true;
                }
                let temp = this.props.value.data.filter(row => {
                    return !Main_1.MapPro.find(this.props.grid.fields, (form, name) => {
                        if (this.props.value.filter && this.props.value.filter[name] && form.filters && form.filters[this.props.value.filter[name].type] && form.filters[this.props.value.filter[name].type].check) {
                            const filter_type = this.props.value.filter[name].type;
                            const filter_value = this.props.value.filter[name].value;
                            const value = row[name];
                            return !form.filters[filter_type].check(value, filter_value, form, this.props.context, this.props.page, this.props.application);
                        }
                        else {
                            return false;
                        }
                    });
                });
                if (this.props.value.sort && this.props.value.sort.length > 0) {
                    this.props.value.sort.reverse().forEach((s) => {
                        let oper = s.substring(0, 1);
                        let by = s.substring(1);
                        if (oper === '-') {
                            temp = temp.sort((a, b) => {
                                return a[by] > b[by] ? 1 : 0;
                            });
                        }
                        else {
                            temp = temp.sort((a, b) => {
                                return a[by] < b[by] ? 1 : 0;
                            });
                        }
                    });
                    this.props.value.sort.reverse();
                }
                this.props.value.total = temp.length;
                if (this.props.value.per_page != null)
                    temp = temp.filter((item, index) => {
                        return index < (this.props.value.page + 1) * this.props.value.per_page && index >= this.props.value.page * this.props.value.per_page;
                    });
                this.props.value.parsed_data = temp;
                this.cleanPage();
                this.cleanSelect();
                this.props.page.update();
                this.factory_run_recurse('recurse_remote_local_pagination_data');
            }
            catch (message) {
                console.error(message);
                this.factory_run_recurse('recurse_remote_local_pagination_data');
            }
        }), 300);
    }
    recurse_local_data() {
        this.state.updating = true;
        this.forceUpdate();
        window.setTimeout(() => {
            let temp = this.props.value.data.filter(row => {
                return !Main_1.MapPro.find(this.props.grid.fields, (form, name) => {
                    if (this.props.value.filter && this.props.value.filter[name] && form.filters && form.filters[this.props.value.filter[name].type] && form.filters[this.props.value.filter[name].type].check) {
                        const filter_type = this.props.value.filter[name].type;
                        const filter_value = this.props.value.filter[name].value;
                        const value = row[name];
                        return !form.filters[filter_type].check(value, filter_value, form, this.props.context, this.props.page, this.props.application);
                    }
                    else {
                        return false;
                    }
                });
            });
            if (this.props.value.sort && this.props.value.sort.length > 0) {
                this.props.value.sort.reverse().forEach((s) => {
                    let oper = s.substring(0, 1);
                    let by = s.substring(1);
                    if (oper === '-') {
                        temp = temp.sort((a, b) => {
                            return a[by] > b[by] ? 1 : 0;
                        });
                    }
                    else {
                        temp = temp.sort((a, b) => {
                            return a[by] < b[by] ? 1 : 0;
                        });
                    }
                });
                this.props.value.sort.reverse();
            }
            this.props.value.total = temp.length;
            this.props.value.parsed_data = temp;
            this.props.value.page = 0;
            this.cleanSelect();
            this.props.page.update();
            this.factory_run_recurse('recurse_local_data');
        }, 300);
    }
    recurse_local_pagination_data() {
        this.state.updating = true;
        this.forceUpdate();
        window.setTimeout(() => {
            let temp = this.props.value.data.filter(row => {
                return !Main_1.MapPro.find(this.props.grid.fields, (form, name) => {
                    if (this.props.value.filter && this.props.value.filter[name] && form.filters && form.filters[this.props.value.filter[name].type] && form.filters[this.props.value.filter[name].type].check) {
                        const filter_type = this.props.value.filter[name].type;
                        const filter_value = this.props.value.filter[name].value;
                        const value = row[name];
                        return !form.filters[filter_type].check(value, filter_value, form, this.props.context, this.props.page, this.props.application);
                    }
                    else {
                        return false;
                    }
                });
            });
            if (this.props.value.sort && this.props.value.sort.length > 0) {
                this.props.value.sort.reverse().forEach((s) => {
                    let oper = s.substring(0, 1);
                    let by = s.substring(1);
                    if (oper === '-') {
                        temp = temp.sort((a, b) => {
                            return a[by] > b[by] ? 1 : 0;
                        });
                    }
                    else {
                        temp = temp.sort((a, b) => {
                            return a[by] < b[by] ? 1 : 0;
                        });
                    }
                });
                this.props.value.sort.reverse();
            }
            this.props.value.total = temp.length;
            if (this.props.value.per_page != null)
                temp = temp.filter((item, index) => {
                    return index < (this.props.value.page + 1) * this.props.value.per_page && index >= this.props.value.page * this.props.value.per_page;
                });
            this.props.value.parsed_data = temp;
            this.cleanPage();
            this.cleanSelect();
            this.props.page.update();
            this.factory_run_recurse('recurse_local_pagination_data');
        }, 300);
    }
    parse_data() {
        if (this.state.updating) {
            this.props.value.need_update = true;
        }
        else if (this.props.grid.data_type == 'local') {
            this.state.rows_updated = false;
            if (this.props.grid.pagination) {
                this.recurse_local_pagination_data();
            }
            else {
                this.recurse_local_data();
            }
        }
        else if (this.props.grid.data_type == 'remote') {
            this.state.rows_updated = false;
            if (this.props.grid.pagination) {
                this.recurse_remote_pagination_data();
            }
            else {
                this.recurse_remote_data();
            }
        }
        else if (this.props.grid.data_type == 'remote_local') {
            this.state.rows_updated = false;
            if (this.props.grid.pagination) {
                this.recurse_remote_local_pagination_data();
            }
            else {
                this.recurse_remote_local_data();
            }
        }
    }
    validate(row) {
        let result = {};
        Main_1.MapPro.each(this.props.grid.fields, (form, name) => {
            let messages = {};
            if (form.error) {
                messages.error = form.error(row, this.props.content, this.props.context, this.props.page, this.props.application);
            }
            if (form.info) {
                messages.info = form.info(row, this.props.content, this.props.context, this.props.page, this.props.application);
            }
            if (form.warning) {
                messages.warning = form.warning(row, this.props.content, this.props.context, this.props.page, this.props.application);
            }
            if (form.if) {
                messages.if = form.if(row, this.props.content, this.props.context, this.props.page, this.props.application);
            }
            else {
                messages.if = true;
            }
            if (form.enabled) {
                messages.enabled = form.enabled(row, this.props.content, this.props.context, this.props.page, this.props.application);
            }
            else {
                messages.enabled = true;
            }
            result[name] = messages;
        });
        return result;
    }
    componentWillReceiveProps(nextProps) {
        const { grid, value } = this.props;
        if ((nextProps.grid !== grid || nextProps.value !== value) || (value.need_update && !this.state.updating)) {
            let sort = this.props.grid.sort && this.props.grid.sort != this.props.value.sort ? this.props.grid.sort : (this.props.grid.sortNull ? this.props.grid.sortNull : []);
            this.props.value.sort = sort;
            this.props.page.update();
            this.parse_data();
        }
    }
    get_filter_types(filters) {
        return Main_1.MapPro.filter(this.filter_types, (item, name) => { return !!filters.find((row) => { return row == name; }); });
    }
    renderItem(row, index) {
        if (!row)
            return null;
        let cells = [];
        let is_first = index == 0;
        let is_last = index == this.props.value.parsed_data.length - 1;
        let allow_edit = this.props.grid.allow_edit ? this.props.grid.allow_edit(row, this.props.context, this.props.page, this.props.application) : false;
        if (this.props.value.edit[row.id] && allow_edit) {
            let validate = this.validate(this.props.value.edit[row.id]);
            this.props.value.messages[row.id] = validate;
            Main_1.MapPro.each(this.props.grid.fields, (form, name) => {
                let message = [];
                if (validate[name].error) {
                    message.push(React.createElement("div", {className: "padding-left-1 padding-top-1 padding-bottom-d1 text mini red", key: "error"}, validate[name].error));
                }
                if (validate[name].info) {
                    message.push(React.createElement("div", {className: "padding-left-1 padding-top-1 padding-bottom-d1 text mini sub", key: "info"}, validate[name].info));
                }
                if (validate[name].warning) {
                    message.push(React.createElement("div", {className: "padding-left-1 padding-top-1 padding-bottom-d1 text mini warning", key: "warning"}, validate[name].warning));
                }
                if (form.edit_render) {
                    let valueLink = {
                        value: this.props.value.edit[row.id][name],
                        requestChange: (newValue) => {
                            this.props.value.edit[row.id][name] = newValue;
                            this.items[index].updated = false;
                            this.props.page.update();
                        }
                    };
                    cells.push(React.createElement(Popup_1.Popup, {key: name, position: "bottom center", openOnOverMove: true, closeOnOutMove: true, openOnFocus: true, closeOnBlur: true, isHidden: message.length == 0, delay: 100, isGhost: true, activeClassName: "active", element: React.createElement("td", {key: name, className: form.classEdit ? form.classEdit : ''}, form.edit_render(valueLink, form, validate[name], this.props.content, this.props.context, this.props.page, this.props.application))}, 
                        React.createElement("div", {className: "min-width-3 width-full padding-d1"}, 
                            React.createElement(StdAnim_1.StdAnim, {duration: 300, type: "fade"}, message)
                        )
                    ));
                }
                else {
                    cells.push(React.createElement("td", {key: name}));
                }
            });
        }
        else {
            Main_1.MapPro.each(this.props.grid.fields, (form, name) => {
                if (form.render) {
                    cells.push(React.createElement("td", {key: name, className: form.classRow ? form.classRow : ''}, form.render(row[name], form, this.props.content, this.props.context, this.props.page, this.props.application)));
                }
                else {
                    cells.push(React.createElement("td", {key: name}));
                }
            });
        }
        let rowMenu = [];
        let connectionPortal = {};
        if (!this.props.value.edit[row.id] && allow_edit)
            rowMenu.push(React.createElement("div", {key: "change", className: "sp-item text thick padding-horizontal-d2 padding-vertical-1", onClick: () => {
                this.props.value.edit[row.id] = Object.assign({}, row);
                this.items[index].updated = false;
                this.props.page.update();
                connectionPortal.close();
            }}, "Изменить"));
        if (this.props.value.edit[row.id] && allow_edit)
            rowMenu.push(React.createElement("div", {key: "cancel", className: "sp-item text thick padding-horizontal-d2 padding-vertical-1", onClick: () => {
                delete this.props.value.edit[row.id];
                this.items[index].updated = false;
                this.props.page.update();
                connectionPortal.close();
            }}, "Отмена"));
        if (!this.props.value.selected && this.props.grid.selectable)
            rowMenu.push(React.createElement("div", {key: "select", className: "sp-item text thick padding-horizontal-d2 padding-vertical-1", onClick: () => {
                this.props.value.selected = {};
                this.props.value.selected[row.id] = row;
                this.items[index].updated = false;
                this.props.page.update();
                connectionPortal.close();
            }}, "Выбрать"));
        if (this.props.value.selected && this.props.grid.selectable)
            rowMenu.push(React.createElement("div", {key: "unselect", className: "sp-item text thick padding-horizontal-d2 padding-vertical-1", onClick: () => {
                this.props.value.selected = null;
                this.items[index].updated = false;
                this.props.page.update();
                connectionPortal.close();
            }}, "Отменить выбор"));
        let onSelect = () => __awaiter(this, void 0, void 0, function* () {
            if (this.props.value.selected) {
                if (this.props.value.selected[row.id]) {
                    delete this.props.value.selected[row.id];
                    if (Object.keys(this.props.value.selected).length == 0)
                        this.props.value.selected = null;
                    this.items[index].updated = false;
                    this.props.page.update();
                }
                else {
                    this.props.value.selected[row.id] = row;
                    this.items[index].updated = false;
                    this.props.page.update();
                }
            }
            else if (!this.props.value.edit[row.id] && this.props.grid.on_click) {
                this.state.loading.on_click[row.id] = true;
                this.items[index].updated = false;
                this.forceUpdate();
                try {
                    yield this.props.grid.on_click(row, this.props.content, this.props.context, this.props.page, this.props.application);
                }
                catch (message) {
                    console.error(message);
                }
                delete this.state.loading.on_click[row.id];
                this.items[index].updated = false;
                this.forceUpdate();
                this.parse_data();
            }
        });
        let onKeyDown = (event) => {
            if (event.keyCode == 13 || event.keyCode == 32) {
                onSelect();
            }
            else if (event.keyCode == 40 && !is_last)
                $('#' + this.id + (index + 1)).focus();
            else if (event.keyCode == 38 && !is_first)
                $('#' + this.id + (index - 1)).focus();
            else if (event.keyCode == 37) {
                Helper_1.default.focusBack(event.target);
            }
            else if (event.keyCode == 39) {
                Helper_1.default.focusNext(event.target);
            }
        };
        if (this.props.value.edit[row.id]) {
            if (this.props.grid.edit_context_menu)
                Main_1.MapPro.each(this.props.grid.edit_context_menu, (button, name) => {
                    let _if = button.if ? button.if(this.props.value.edit[row.id], row, this.props.context, this.props.page, this.props.application) : true;
                    if (_if) {
                        let enabled = button.enabled ? button.enabled(this.props.value.edit[row.id], row, this.props.context, this.props.page, this.props.application) : true;
                        rowMenu.push(React.createElement("div", {key: name, className: "sp-item text thick padding-horizontal-d2 padding-vertical-1" + (!enabled ? ' enabled' : ''), onClick: () => __awaiter(this, void 0, void 0, function* () {
                            connectionPortal.close();
                            this.state.updating = true;
                            this.state.loading.on_create = true;
                            this.items[index].updated = false;
                            this.forceUpdate();
                            try {
                                const result = yield button.on_click(this.props.value.edit[row.id], row, this.props.context, this.props.page, this.props.application);
                                if (result == true) {
                                    delete this.props.value.edit[row.id];
                                    this.items[index].updated = false;
                                    this.props.page.update();
                                    connectionPortal.close();
                                }
                            }
                            catch (message) {
                                console.error(message);
                            }
                            this.state.updating = false;
                            this.state.loading.on_create = false;
                            this.items[index].updated = false;
                            this.forceUpdate();
                            this.parse_data();
                        })}, button.text));
                    }
                });
        }
        else {
            if (this.props.grid.row_context_menu)
                Main_1.MapPro.each(this.props.grid.row_context_menu, (button, name) => {
                    let _if = button.if ? button.if(row, this.props.context, this.props.page, this.props.application) : true;
                    if (_if) {
                        let enabled = button.enabled ? button.enabled(row, this.props.context, this.props.page, this.props.application) : true;
                        rowMenu.push(React.createElement("div", {key: name, className: "sp-item text thick padding-horizontal-d2 padding-vertical-1", onClick: () => __awaiter(this, void 0, void 0, function* () {
                            connectionPortal.close();
                            this.state.updating = true;
                            this.state.loading.on_create = true;
                            this.items[index].updated = false;
                            this.forceUpdate();
                            try {
                                yield button.on_click(row, this.props.context, this.props.page, this.props.application);
                            }
                            catch (message) {
                                console.error(message);
                            }
                            this.state.updating = false;
                            this.state.loading.on_create = false;
                            this.items[index].updated = false;
                            this.forceUpdate();
                            this.parse_data();
                        })}, button.text));
                    }
                });
        }
        if (rowMenu.length > 0) {
            return (React.createElement(Popup_1.Popup, {key: index, position: "bottom center", closeOnEsc: true, closeOnOutsideClick: true, closeOnClick: true, openOnContextMenu: true, connectionPortal: connectionPortal, activeClassName: "active", element: (React.createElement("tr", {id: this.id + index, tabIndex: 0, className: "pointer" + (this.props.value.selected && this.props.value.selected[row.id] ? ' selected' : '') + (this.props.value.edit[row.id] ? ' unselectable' : '') + (((this.state.loading.on_edit && this.state.loading.on_edit[row.id]) || this.state.loading.on_click[row.id]) ? ' loading' : ''), onClick: onSelect, onKeyDown: onKeyDown}, cells))}, 
                React.createElement("div", {className: "sp-menu-item pull-left min-width-3 width-full"}, rowMenu)
            ));
        }
        else {
            return (React.createElement("tr", {id: this.id + index, key: index, className: (this.props.value.selected && this.props.value.selected[row.id] ? ' selected' : '') + (this.props.value.edit[row.id] ? ' unselectable' : '') + (((this.state.loading.on_edit && this.state.loading.on_edit[row.id]) || this.state.loading.on_click[row.id]) ? ' loading' : '')}, cells));
        }
    }
    render() {
        const updating = !this.state.rows_updated;
        let headers = [];
        let clearSort = () => {
            let sort = this.props.grid.sort && this.props.grid.sort != this.props.value.sort ? this.props.grid.sort : (this.props.grid.sortNull ? this.props.grid.sortNull : []);
            if (sort !== this.props.value.sort) {
                this.props.value.sort = sort;
                this.props.page.update();
            }
            this.parse_data();
        };
        let sortString = this.props.value.sort.length > 0 ? (React.createElement("div", {key: "sortString", className: "padding-horizontal-d2 padding-vertical-1"}, this.props.value.sort.join(' > '))) : null;
        let i = 0;
        Main_1.MapPro.each(this.props.grid.fields, (form, name) => {
            let sortContains = this.props.value.sort.find((item) => { return item.substring(1) == name; });
            let sortOperator = sortContains ? sortContains.substring(0, 1) : null;
            let sortSet = (operator) => {
                if (sortContains && sortOperator == operator) {
                    let temp = [];
                    this.props.value.sort.forEach((item) => {
                        if (item.substring(1) != name) {
                            temp.push(item);
                        }
                    });
                    this.props.value.sort = temp;
                    this.props.page.update();
                }
                else if (sortContains && sortOperator != operator) {
                    let temp = [];
                    this.props.value.sort.forEach((item) => {
                        if (item.substring(1) != name) {
                            temp.push(item);
                        }
                        else {
                            temp.push(operator + name);
                        }
                    });
                    this.props.value.sort = temp;
                    this.props.page.update();
                }
                else if (!sortContains) {
                    let temp = [];
                    this.props.value.sort.forEach((item) => {
                        temp.push(item);
                    });
                    temp.push(operator + name);
                    this.props.value.sort = temp;
                    this.props.page.update();
                }
                this.parse_data();
            };
            let sortPanel = form.sortable ? [
                sortString,
                (React.createElement("div", {key: "sortPanel", className: "sp-table-layout"}, 
                    React.createElement("div", {className: ""}, 
                        React.createElement("div", {tabIndex: 0, className: "btn btn-default " + (sortContains && sortOperator == '+'), onClick: () => { sortSet('+'); }, onKeyDown: (e) => {
                            if (e.keyCode == 13) {
                                sortSet('+');
                                return false;
                            }
                            else if (e.keyCode == 37) {
                                Helper_1.default.focusBack(e.target);
                            }
                            else if (e.keyCode == 39) {
                                Helper_1.default.focusNext(e.target);
                            }
                        }}, 
                            React.createElement(FontAwesome, {name: "chevron-up"})
                        ), 
                        React.createElement("div", {tabIndex: 0, className: "btn btn-default " + (sortContains && sortOperator == '-'), onClick: () => { sortSet('-'); }, onKeyDown: (e) => {
                            if (e.keyCode == 13) {
                                sortSet('-');
                                return false;
                            }
                            else if (e.keyCode == 37) {
                                Helper_1.default.focusBack(e.target);
                            }
                            else if (e.keyCode == 39) {
                                Helper_1.default.focusNext(e.target);
                            }
                        }}, 
                            React.createElement(FontAwesome, {name: "chevron-down"})
                        ), 
                        React.createElement("div", {tabIndex: 0, className: "btn btn-default", onClick: () => { clearSort(); }, onKeyDown: (e) => {
                            if (e.keyCode == 13) {
                                clearSort();
                                return false;
                            }
                            else if (e.keyCode == 37) {
                                Helper_1.default.focusBack(e.target);
                            }
                            else if (e.keyCode == 39) {
                                Helper_1.default.focusNext(e.target);
                            }
                        }}, 
                            React.createElement(FontAwesome, {name: "remove"})
                        ))
                ))
            ] : null;
            let setFilter = (value) => {
                if (!this.props.value.filter[name] && value) {
                    this.props.value.filter[name] = { type: value.id };
                }
                else if (value) {
                    this.props.value.filter[name].type = value.id;
                }
                else {
                    delete this.props.value.filter[name];
                }
                this.props.page.update();
                this.parse_data();
            };
            let typePanel = null;
            let filterPanel = null;
            let filterTypes = form.filter ? this.get_filter_types(form.filter) : new Main_1.Map();
            if (Object.keys(filterTypes).length > 0) {
                let filterName = this.props.value.filter[name] ? this.props.value.filter[name].type : null;
                let filterValueLink = {
                    value: filterName ? {
                        id: filterName,
                        name: filterTypes[filterName]
                    } : null,
                    requestChange: setFilter.bind(this)
                };
                let filterData = Main_1.MapPro.map(filterTypes, (item, name) => {
                    return {
                        id: name,
                        name: item
                    };
                });
                typePanel = (React.createElement(Select_1.Select, {className: "margin-px1", level: 1, valueLink: filterValueLink, data_type: 'local', search: 'none', data: filterData}));
                if (form.filters && form.filters[filterName] && form.filters[filterName].render) {
                    let filterMValueLink = {
                        value: this.props.value.filter[name].value,
                        requestChange: (newValue) => {
                            this.props.value.filter[name].value = newValue;
                            this.props.page.update();
                            this.parse_data();
                        }
                    };
                    filterPanel = form.filters[filterName].render(filterMValueLink, form, this.props.content, this.props.context, this.props.page, this.props.application);
                }
            }
            if (typePanel || filterPanel || sortPanel) {
                headers.push(React.createElement(Dropdown_1.Dropdown, {key: name, ref: "dropdownMenu", position: "bottom right", closeOnEsc: true, closeOnOutsideClick: true, toggleOnClick: true, activeClassName: "active", element: (React.createElement("td", {className: "pointer sp-dropdown2-item"}, 
                    React.createElement("div", {className: "", style: { display: 'inline-block' }}, form.label), 
                    React.createElement(FontAwesome, {className: "sp-dropdown2-on-open", style: { float: 'right' }, name: updating ? "spinner" : "chevron-up", pulse: updating}), 
                    React.createElement(FontAwesome, {className: "sp-dropdown2-on-not-open", style: { float: 'right' }, name: updating ? "spinner" : "chevron-down", pulse: updating})))}, 
                    React.createElement("div", {className: "sp-dropdown2-menu pull-left min-width-3 width-full"}, 
                        typePanel, 
                        filterPanel, 
                        sortPanel)
                ));
            }
            else {
                headers.push(React.createElement("td", {key: name}, form.label));
            }
            i++;
        });
        let count = i;
        let allow_add = this.props.grid.allow_add ? this.props.grid.allow_add(this.props.context, this.props.page, this.props.application) : false;
        let creation = null;
        if (allow_add) {
            let create = [];
            let validate = this.validate(this.props.value.create);
            this.props.value.create_messages = validate;
            Main_1.MapPro.each(this.props.grid.fields, (form, name) => {
                let message = [];
                if (validate[name].error) {
                    message.push(React.createElement("div", {className: "padding-left-1 padding-top-1 padding-bottom-d1 text mini red", key: "error"}, validate[name].error));
                }
                if (validate[name].info) {
                    message.push(React.createElement("div", {className: "padding-left-1 padding-top-1 padding-bottom-d1 text mini sub", key: "info"}, validate[name].info));
                }
                if (validate[name].warning) {
                    message.push(React.createElement("div", {className: "padding-left-1 padding-top-1 padding-bottom-d1 text mini warning", key: "warning"}, validate[name].warning));
                }
                if (form.create_render) {
                    let valueLink = {
                        value: this.props.value.create[name],
                        requestChange: (newValue) => {
                            this.props.value.create[name] = newValue;
                            this.props.page.update();
                        }
                    };
                    create.push(React.createElement(Popup_1.Popup, {key: name, position: "bottom center", openOnOverMove: true, closeOnOutMove: true, openOnFocus: true, closeOnBlur: true, isHidden: message.length == 0, delay: 100, isGhost: true, activeClassName: "active", element: React.createElement("td", {key: name, className: form.classCreate ? form.classCreate : ''}, form.create_render(valueLink, form, validate[name], this.props.content, this.props.context, this.props.page, this.props.application))}, 
                        React.createElement("div", {className: "min-width-3 width-full padding-d1"}, 
                            React.createElement(StdAnim_1.StdAnim, {duration: 300, type: "fade"}, message)
                        )
                    ));
                }
                else {
                    create.push(React.createElement("td", {key: name}));
                }
            });
            let connectionPortal = {};
            let creationMenu = [];
            if (this.props.grid.create_context_menu)
                Main_1.MapPro.each(this.props.grid.create_context_menu, (button, name) => {
                    let _if = button.if ? button.if(this.props.value.create, this.props.context, this.props.page, this.props.application) : true;
                    if (_if) {
                        let enabled = button.enabled ? button.enabled(this.props.value.create, this.props.context, this.props.page, this.props.application) : true;
                        creationMenu.push(React.createElement("div", {key: name, className: "sp-item text thick padding-horizontal-d2 padding-vertical-1" + (!enabled ? ' disabled' : ''), onClick: () => __awaiter(this, void 0, void 0, function* () {
                            connectionPortal.close();
                            this.state.updating = true;
                            this.state.loading.on_create = true;
                            try {
                                yield button.on_click(this.props.value.create, this.props.context, this.props.page, this.props.application);
                            }
                            catch (message) {
                                console.error(message);
                            }
                            this.state.updating = false;
                            this.state.loading.on_create = false;
                            this.forceUpdate();
                            this.parse_data();
                        })}, button.text));
                    }
                });
            if (creationMenu.length > 0) {
                creation = (React.createElement(Popup_1.Popup, {position: "bottom center", closeOnEsc: true, closeOnOutsideClick: true, closeOnClick: true, openOnContextMenu: true, connectionPortal: connectionPortal, activeClassName: "active", element: (React.createElement("tr", {className: "unselectable" + (this.state.loading.on_create ? ' loading' : '')}, create))}, 
                    React.createElement("div", {className: "sp-menu-item pull-left min-width-3 width-full"}, creationMenu)
                ));
            }
            else {
                creation = (React.createElement("tr", {className: "unselectable" + (this.state.loading.on_create ? ' loading' : '')}, create));
            }
        }
        let pagination = null;
        if (this.props.grid.pagination && this.props.value.per_page != null) {
            let items = [];
            let setPage = (page) => {
                this.props.value.page = page;
                this.props.page.update();
                this.need_on_scroll = true;
                this.parse_data();
            };
            if (this.props.value.page > 0) {
                items.push(React.createElement("div", {key: 0, className: "sp-item", onClick: setPage.bind(this, 0)}, 
                    React.createElement("div", null, "1")
                ));
            }
            if (this.props.value.page > 1) {
                items.push(React.createElement("div", {key: 1, className: "sp-item", onClick: setPage.bind(this, 1)}, 
                    React.createElement("div", null, "2")
                ));
            }
            if (this.props.value.page > 3) {
                items.push(React.createElement("div", {key: 2, className: "sp-item disabled"}, 
                    React.createElement("div", null, "...")
                ));
            }
            if (this.props.value.page > 2) {
                items.push(React.createElement("div", {key: 3, className: "sp-item", onClick: setPage.bind(this, this.props.value.page - 1)}, 
                    React.createElement("div", null, this.props.value.page)
                ));
            }
            if (this.props.value.per_page < this.props.value.total) {
                items.push(React.createElement("div", {key: 4, className: "sp-item active"}, 
                    React.createElement("div", null, this.props.value.page + 1)
                ));
            }
            let maxPage = Math.ceil(this.props.value.total / this.props.value.per_page) - 1;
            if (this.props.value.page < maxPage - 2) {
                items.push(React.createElement("div", {key: 5, className: "sp-item", onClick: setPage.bind(this, this.props.value.page + 1)}, 
                    React.createElement("div", null, this.props.value.page + 2)
                ));
            }
            if (this.props.value.page < maxPage - 3) {
                items.push(React.createElement("div", {key: 6, className: "sp-item disabled"}, 
                    React.createElement("div", null, "...")
                ));
            }
            if (this.props.value.page < maxPage - 1) {
                items.push(React.createElement("div", {key: 7, className: "sp-item", onClick: setPage.bind(this, maxPage - 1)}, 
                    React.createElement("div", null, maxPage)
                ));
            }
            if (this.props.value.page < maxPage) {
                items.push(React.createElement("div", {key: 8, className: "sp-item", onClick: setPage.bind(this, maxPage)}, 
                    React.createElement("div", null, maxPage + 1)
                ));
            }
            pagination = items.length > 0 ? (React.createElement("div", {className: "sp-pagination" + (updating ? ' loading' : '')}, items)) : null;
        }
        return (React.createElement("table", {className: "sp-table"}, 
            React.createElement("thead", null, 
                React.createElement("tr", null, headers)
            ), 
            React.createElement("tbody", null, creation), 
            React.createElement("tbody", {ref: "tbody"}, this.props.grid.big_data ?
                [
                    this.items.length > 0 && this.items[0].momento ? this.items[0].momento : null,
                    (React.createElement("tr", {key: "top_space", className: "stripes", style: { height: this.t }}, this.etaloneHeight > 0 && this.t >= this.etaloneHeight ? (React.createElement("td", {colSpan: Object.keys(this.props.grid.fields).length})) : null)),
                    this.few.map(item => item.momento ? item.momento : (React.createElement("tr", {key: item.index, className: "stripes", style: { height: this.etaloneHeight }}, 
                        React.createElement("td", {colSpan: Object.keys(this.props.grid.fields).length})
                    ))),
                    (React.createElement("tr", {key: "botom_space", className: "stripes", style: { height: this.b }}, this.etaloneHeight > 0 && this.b >= this.etaloneHeight ? (React.createElement("td", {colSpan: Object.keys(this.props.grid.fields).length})) : null))
                ] : this.items.map(item => item.momento)), 
            React.createElement("tfoot", null, 
                React.createElement("tr", null, 
                    React.createElement("td", {colSpan: Object.keys(this.props.grid.fields).length, className: "padding-0"}, pagination)
                )
            )));
    }
}
Grid.defaultProps = new GridProps();
exports.Grid = Grid;
//# sourceMappingURL=Grid.js.map