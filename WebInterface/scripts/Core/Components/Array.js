"use strict";
const React = require('react');
const Main_1 = require('../Main');
class ArrayModel extends Main_1.ContentModelInit {
    render(context, page, application) {
        let array = Main_1.MapPro.map(this.items, (item, name) => { return React.createElement(Main_1.Component, { key: name, content: item, context: item, page, application }); });
        return (React.createElement("div", {className: this.class ? this.class : ''}, array));
    }
}
exports.ArrayModel = ArrayModel;
//# sourceMappingURL=Array.js.map