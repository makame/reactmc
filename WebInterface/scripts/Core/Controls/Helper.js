"use strict";
class Helper {
    static guid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + s4() + s4();
    }
    static focusBack(target) {
        let tabs = $('[tabindex="0"]');
        let finded = false;
        for (var i = tabs.length - 1; i >= 0; i--) {
            if (finded) {
                $(tabs[i]).focus();
                break;
            }
            if (target === tabs[i]) {
                finded = true;
            }
        }
        return finded;
    }
    static focusNext(target) {
        let tabs = $('[tabindex="0"]');
        let finded = false;
        for (var i = 0; i < tabs.length; i++) {
            if (finded) {
                $(tabs[i]).focus();
                break;
            }
            if (target === tabs[i]) {
                finded = true;
            }
        }
        return finded;
    }
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Helper;
//# sourceMappingURL=Helper.js.map