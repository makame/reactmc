import * as React from 'react';
import * as moment from 'moment';
import Helper from './Helper';
import { IValueLink } from '../Main'
import { Popup } from '../Controls/Popup';
import { Calendar } from './Calendar';
import { ConnectionPortal } from './Portal'

import FontAwesome = require('react-fontawesome')

export class DatePickerProps {
  valueLink?: IValueLink<moment.Moment> = {
    value: null,
    requestChange: (newValue) => {
    }
  }
  render?: (value: moment.Moment, clear: () => void, format: string) => JSX.Element
  className?: string
  weeksPerMonth?: number
  format?: string = "YYYY-MM-DD"
  weeks?: string[] = ['Пн.', 'Вт.', 'Ср.', 'Чт.', 'Пт.', 'Сб.', 'Вс.']
  locale?: string = 'ru'
  min?: moment.Moment
  max?: moment.Moment
}

export class DatePickerState {
  connectionPortal: ConnectionPortal = {}
}

export class DatePicker extends React.Component<DatePickerProps, DatePickerState> {

  public static defaultProps = new DatePickerProps()

  state = new DatePickerState()

  refs: {
    [key: string]: any;
    calendar: Calendar;
  }

  clear(): void {
    let self = this;

    self.props.valueLink.requestChange(null)
  }

  getElement(): JSX.Element {
    let self = this

    if (self.props.render) {
      return self.props.render(self.props.valueLink.value, self.clear, self.props.format)
    }
    else {
      var selected = "Не выбрано"

      if (self.props.valueLink.value) {
        selected = self.props.valueLink.value.format(self.props.format)
      }

      return (
        <div tabIndex={0} className={"sp-input-group sp-dropdown2-item " + self.props.className}>
          <div className="sp-input-group-label width-full">
            {selected}
          </div>
          <div className="sp-input-group-btn sp-as-addon square sp-dropdown2-on-open" onClick={self.clear}>
            <FontAwesome name={self.props.valueLink.value ? "remove" : "chevron-up"}/>
          </div>
          <div className="sp-input-group-addon square sp-dropdown2-on-not-open">
            <FontAwesome name={"chevron-down"}/>
          </div>
        </div>
      )
    }
  }

  render() {
    let self = this

    const { valueLink, weeksPerMonth, format, weeks, locale, min, max, } = this.props

    const props = {
      valueLink,
      weeksPerMonth,
      format,
      weeks,
      locale,
      min,
      max
    }

    return (
      <Popup
        ref="dropdownMenu"
        position="bottom right"
        positionEl="start"
        closeOnEsc closeOnOutsideClick
        toggleOnClick
        activeClassName="open"
        connectionPortal={this.state.connectionPortal}
        onShow={
          ()=>{
            self.refs.calendar.focus()
          }
        }
        element={self.getElement()}
        >
        <div className="sp-dropdown2-menu pull-left min-width-3 width-full padding-px3">
          <Calendar ref="calendar" {...props}/>
        </div>
      </Popup>
    );
  }
}
