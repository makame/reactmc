"use strict";
const React = require('react');
const moment = require('moment');
const Helper_1 = require('./Helper');
const EditorSimple_1 = require('./EditorSimple');
class Dates {
}
exports.Dates = Dates;
class CalendarProps {
    constructor() {
        this.valueLink = {
            value: null,
            requestChange: (newValue) => {
            }
        };
        this.format = "YYYY-MM-DD";
        this.weeks = ['Пн.', 'Вт.', 'Ср.', 'Чт.', 'Пт.', 'Сб.', 'Вс.'];
        this.locale = 'ru';
    }
}
exports.CalendarProps = CalendarProps;
class CalendarState {
    constructor() {
        this.input = '';
        this.now = moment().startOf('month');
    }
}
exports.CalendarState = CalendarState;
class Calendar extends React.Component {
    constructor() {
        super(...arguments);
        this.state = new CalendarState();
        this.id = Helper_1.default.guid();
    }
    componentWillReceiveProps(nextProps, nextContext) {
        const { valueLink } = this.props;
        if (nextProps.valueLink.value !== valueLink.value) {
            this.state.value = nextProps.valueLink.value;
            this.state.input = this.state.value ? this.state.value.format(this.props.format) : '';
            this.forceUpdate();
        }
        moment.locale(nextProps.locale);
    }
    componentWillMount() {
        this.state.value = this.props.valueLink.value;
        this.state.input = this.state.value ? this.state.value.format(this.props.format) : '';
        moment.locale(this.props.locale);
    }
    getDaysArrayByCalendarMonth(date) {
        let startDate = moment(date).startOf('month');
        let endDate = moment(date).endOf('month');
        let weeksInMonth = 1;
        if (this.props.weeksPerMonth) {
            weeksInMonth = this.props.weeksPerMonth;
        }
        else {
            let tempDate = moment(startDate).add(1, "weeks").startOf('isoWeek');
            for (; tempDate.month() == date.month(); weeksInMonth++) {
                tempDate.add(1, "weeks").startOf('isoWeek');
            }
        }
        let arr = [];
        for (let k = 0; k < weeksInMonth; k++) {
            let arrWeek = [];
            let currentWeek = moment(startDate).add(k, "weeks").startOf('isoWeek');
            for (let i = 0; i < 7; i++) {
                let current = moment(currentWeek).add(i, "days");
                arrWeek.push({
                    title: current.format("DD"),
                    value: current,
                    inMonth: current.month() == date.month(),
                    isInInterval: this.isInInterval(current),
                    selected: this.state.value && (current.year() == this.state.value.year() && current.month() == this.state.value.month() && current.date() == this.state.value.date()),
                });
            }
            arr.push(arrWeek);
        }
        return arr;
    }
    goPrev() {
        this.state.now = this.state.now.subtract(1, "months").startOf('month');
        this.setState({ now: this.state.now });
    }
    goNext() {
        this.state.now = this.state.now.add(1, "months").startOf('month');
        this.setState({ now: this.state.now });
    }
    selection(item) {
        if (item.isInInterval) {
            if ((this.state.value && (this.state.value.year() != item.value.year() || this.state.value.month() != item.value.month() || this.state.value.date() != item.value.date()))
                || !this.state.value) {
                this.state.value = moment(item.value);
                this.state.now = moment(this.state.value).startOf('month');
                this.setState({ now: this.state.now, value: this.state.value, input: this.state.value.format(this.props.format) });
                this.props.valueLink.requestChange(this.state.value);
            }
            else {
                this.state.value = null;
                this.state.input = '';
                this.forceUpdate();
                this.props.valueLink.requestChange(this.state.value);
            }
        }
    }
    placeCaretAtEnd(el) {
        if (!window.getSelection && !document.createRange) {
            let range = document.createRange();
            range.selectNodeContents(el);
            range.collapse(false);
            let sel = window.getSelection();
            sel.removeAllRanges();
            sel.addRange(range);
        }
    }
    focus() {
        $('#' + this.id).focus();
        this.placeCaretAtEnd($('#' + this.id)[0]);
    }
    isInInterval(value) {
        return (!this.props.max || this.props.max.diff(value, 'days') >= 0) && (!this.props.min || this.props.min.diff(value, 'days') <= 0);
    }
    isValid(string) {
        return moment(string, this.props.format, true).isValid() && (!this.props.max || this.props.max.diff(moment(string), 'days') >= 0) && (!this.props.min || this.props.min.diff(moment(string), 'days') <= 0);
    }
    render() {
        let valueLink = {
            value: this.state.input,
            requestChange: (newValue) => {
                if (this.isValid(newValue)) {
                    this.state.value = moment(newValue, this.props.format, true);
                    this.state.now = moment(this.state.value).startOf('month');
                    this.setState({ now: this.state.now, value: this.state.value, input: newValue });
                    this.props.valueLink.requestChange(this.state.value);
                }
                else {
                    this.setState({ input: newValue });
                }
            }
        };
        this.state.dates = this.getDaysArrayByCalendarMonth(this.state.now);
        let weeks = this.props.weeks.map((week) => {
            return (React.createElement("div", {key: week, className: "box-xs"}, week));
        });
        let days = this.state.dates.map((week, i) => {
            return (React.createElement("div", {key: i}, week.map((day, k) => {
                return (React.createElement("div", {key: k, className: "text box-xs pointer" + (day.isInInterval ? '' : ' disabled') + (day.inMonth ? '' : ' subsub') + (day.selected ? ' active' : ''), onClick: () => { this.selection.call(this, day); }}, day.title));
            })));
        });
        return (React.createElement("div", {className: "sp-calendar" + ' ' + (this.props.className ? this.props.className : '')}, 
            React.createElement("div", {className: "sp-table-layout text center"}, 
                React.createElement("div", null, 
                    React.createElement("div", {className: "pointer box-sm", onClick: this.goPrev.bind(this)}, '<'), 
                    React.createElement(EditorSimple_1.EditorSimple, {valueLink: valueLink, className: "input box-sm selectable" + (this.isValid(this.state.input) ? '' : ' error'), id: this.id, tagName: "div", useTitle: true, title: this.state.now.format("YYYY/MM")}), 
                    React.createElement("div", {className: "pointer box-sm", onClick: this.goNext.bind(this)}, '>'))
            ), 
            React.createElement("div", {className: "sp-table-layout text center"}, 
                React.createElement("div", null, weeks), 
                days)));
    }
}
Calendar.defaultProps = new CalendarProps();
exports.Calendar = Calendar;
//# sourceMappingURL=Calendar.js.map