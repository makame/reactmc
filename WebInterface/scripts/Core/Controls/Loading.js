"use strict";
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
const React = require('react');
const Portal_1 = require('./Portal');
class LoadingProps extends Portal_1.PortalProps {
    constructor() {
        super(...arguments);
        this.level = -1;
    }
}
exports.LoadingProps = LoadingProps;
class Loading extends React.Component {
    closePortal() {
        this.refs.portal.closePortal();
    }
    openPortal() {
        this.refs.portal.openPortal();
    }
    componentDidMount() {
        this.openPortal();
    }
    componentWillUnmount() {
        this.closePortal();
    }
    render() {
        const { className } = this.props;
        const { level, closeOnEsc, isOpened, isHidden, isGhost, connectionPortal, onElementClick, onElementOver, onElementContextMenu, onElementOut, onElementFocus, onElementBlur, onKeyDown, onOpen, onUpdate, didUpdate, beforeClose, onClose, isModal, style, element, activeClassName } = this.props;
        const props = {
            level,
            closeOnEsc,
            isOpened,
            isHidden,
            isGhost,
            connectionPortal,
            onElementClick,
            onElementOver,
            onElementContextMenu,
            onElementOut,
            onElementFocus,
            onElementBlur,
            onKeyDown,
            onOpen,
            onUpdate,
            didUpdate,
            beforeClose,
            onClose,
            isModal,
            style,
            element,
            activeClassName
        };
        return (React.createElement(Portal_1.Portal, __assign({ref: "portal"}, props, {className: "loading-screen", isModal: true, onOpen: (node) => {
            $(node).velocity({ opacity: [1.0, 0] }, { duration: 200 });
        }, beforeClose: (node, callback) => {
            $(node).velocity({ opacity: 0.0 }, { duration: 200, complete: callback });
        }}), 
            React.createElement("div", {className: "content"}, 
                React.createElement("div", null, "[Загрузка данных]"), 
                React.createElement("div", {className: "loader"}))
        ));
    }
}
Loading.defaultProps = new LoadingProps();
exports.Loading = Loading;
//# sourceMappingURL=Loading.js.map