"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const React = require('react');
const FontAwesome = require('react-fontawesome');
const Helper_1 = require('./Helper');
const Dropdown_1 = require('../Controls/Dropdown');
class SelectProps {
    constructor() {
        this.data = [];
        this.data_type = 'local';
        this.search = 'none';
        this.request = (search_string) => {
            return new Promise((resolve, reject) => {
                setTimeout(() => {
                    resolve([]);
                }, 300);
            });
        };
    }
}
exports.SelectProps = SelectProps;
class SelectState {
    constructor() {
        this.connectionPortal = {};
        this.parsed_data = [];
        this.updating = false;
        this.search_string = '';
        this.need_update_data = false;
    }
}
exports.SelectState = SelectState;
class Select extends React.Component {
    constructor() {
        super(...arguments);
        this.state = new SelectState();
        this.id = Helper_1.default.guid();
    }
    isMounted() {
        return true;
    }
    recurse_search_local_data() {
        this.state.updating = true;
        if (this.isMounted())
            this.forceUpdate();
        setTimeout(() => {
            let temp = [];
            for (let i in this.props.data) {
                let item = this.props.data[i];
                if (!this.state.search_string || item.name.toLowerCase().indexOf(this.state.search_string.toLowerCase()) !== -1) {
                    temp.push(item);
                }
            }
            this.state.parsed_data = temp;
            if (this.isMounted())
                this.forceUpdate();
            if (this.state.need_update_data) {
                this.state.need_update_data = false;
                this.recurse_search_local_data();
            }
            else {
                this.state.updating = false;
                if (this.isMounted())
                    this.forceUpdate();
            }
        }, 300);
    }
    recurse_search_remote_data() {
        this.state.updating = true;
        if (this.isMounted())
            this.forceUpdate();
        setTimeout(() => __awaiter(this, void 0, void 0, function* () {
            let run_recurse = () => {
                if (this.state.need_update_data) {
                    this.state.need_update_data = false;
                    this.recurse_search_remote_data();
                }
                else {
                    this.state.updating = false;
                    if (this.isMounted())
                        this.forceUpdate();
                }
            };
            try {
                const data = yield this.props.request(this.state.search_string);
                this.state.parsed_data = data;
                if (this.isMounted())
                    this.forceUpdate();
                run_recurse();
            }
            catch (message) {
                console.error(message);
                run_recurse();
            }
        }), 300);
    }
    recurse_search_remote_local_data() {
        this.state.updating = true;
        if (this.isMounted())
            this.forceUpdate();
        setTimeout(() => __awaiter(this, void 0, void 0, function* () {
            let run_recurse = () => {
                if (this.state.need_update_data) {
                    this.state.need_update_data = false;
                    this.recurse_search_remote_local_data();
                }
                else {
                    this.state.updating = false;
                    if (this.isMounted())
                        this.forceUpdate();
                }
            };
            try {
                const data = yield this.props.request(null);
                let temp = [];
                data.forEach((item) => {
                    if (!this.state.search_string || item.name.toLowerCase().indexOf(this.state.search_string.toLowerCase()) !== -1) {
                        temp.push(item);
                    }
                });
                this.state.parsed_data = temp;
                if (this.isMounted())
                    this.forceUpdate();
                run_recurse();
            }
            catch (message) {
                console.error(message);
                run_recurse();
            }
        }), 300);
    }
    parse_data() {
        if (this.state.updating) {
            this.state.need_update_data = true;
        }
        else if (this.props.data_type == 'local') {
            if (this.props.search == 'local') {
                this.recurse_search_local_data();
            }
            else if (this.props.search == 'remote') {
                this.state.need_update_data = false;
            }
            else if (this.props.search == 'none') {
                this.state.parsed_data = this.props.data;
                if (this.isMounted())
                    this.forceUpdate();
                this.state.need_update_data = false;
            }
        }
        else if (this.props.data_type == 'remote') {
            if (this.props.search == 'local') {
                this.recurse_search_remote_local_data();
            }
            else if (this.props.search == 'remote') {
                this.recurse_search_remote_data();
            }
            else if (this.props.search == 'none') {
                this.recurse_search_remote_data();
            }
        }
    }
    requestChange(event) {
        this.state.search_string = event.target.value;
        if (this.isMounted())
            this.forceUpdate();
        this.parse_data.call(this);
    }
    arraysEqual(a, b) {
        if (a === b)
            return true;
        if (a == null || b == null)
            return false;
        if (a.length != b.length)
            return false;
        for (let i = 0; i < a.length; ++i) {
            if (a[i] !== b[i])
                return false;
        }
        return true;
    }
    componentWillReceiveProps(nextProps, nextContext) {
        const { data, data_type, search, request, valueLink } = this.props;
        if (!this.arraysEqual(nextProps.data, data) || nextProps.data_type !== data_type || nextProps.search !== search || nextProps.request !== request || nextProps.valueLink.value !== valueLink.value) {
            this.state.value = nextProps.valueLink.value;
            this.parse_data.call(this);
        }
    }
    componentWillMount() {
        this.state.value = this.props.valueLink.value;
        setTimeout(this.parse_data.bind(this), 100);
    }
    clear() {
        this.state.value = this.props.default;
        this.props.valueLink.requestChange(this.state.value);
        if (this.isMounted())
            this.forceUpdate();
    }
    selectItem(item) {
        if (this.state.value === item || (this.state.value && item.id == this.state.value.id)) {
            this.state.value = this.props.default;
        }
        else {
            this.state.value = item;
        }
        $('#' + this.id + 'i').focus();
        this.state.connectionPortal.close();
        this.props.valueLink.requestChange(this.state.value);
        if (this.isMounted())
            this.forceUpdate();
    }
    getElement() {
        if (this.props.render) {
            return this.props.render(this.state.value, this.clear, this.state.updating);
        }
        else {
            let selected = "Не выбрано";
            if (this.state.value) {
                selected = this.state.value.name;
            }
            let icon_right = "chevron-up";
            if (this.state.value)
                icon_right = "remove";
            if (this.state.updating)
                icon_right = "spinner";
            let icon = this.props.icon ? React.createElement("div", {className: "sp-input-icon"}, 
                React.createElement(FontAwesome, {name: this.props.icon})
            ) : null;
            return (React.createElement("div", {tabIndex: 0, id: this.id + 'i', className: "sp-input-group sp-dropdown2-item" + (this.props.className ? (' ' + this.props.className) : ''), onKeyDown: (e) => {
                if (e.keyCode == 13) {
                    this.state.connectionPortal.open();
                    return false;
                }
                else if (e.keyCode == 37) {
                    Helper_1.default.focusBack(e.target);
                }
                else if (e.keyCode == 39) {
                    Helper_1.default.focusNext(e.target);
                }
            }}, 
                this.props.left, 
                icon, 
                React.createElement("div", {className: "sp-input-group-addon label width-full"}, selected), 
                React.createElement("div", {className: "sp-input-group-btn sp-as-addon square sp-dropdown2-on-open", onClick: this.clear.bind(this)}, 
                    React.createElement(FontAwesome, {name: icon_right, pulse: this.state.updating})
                ), 
                React.createElement("div", {className: "sp-input-group-addon square sp-dropdown2-on-not-open"}, 
                    React.createElement(FontAwesome, {name: this.state.updating ? "spinner" : "chevron-down", pulse: this.state.updating})
                ), 
                this.props.right));
        }
    }
    render() {
        const { className, data, data_type, search, request, valueLink, render, level } = this.props;
        let rows = [];
        let i = 0;
        this.state.parsed_data.forEach((item, index) => {
            let is_first = index == 0;
            let is_last = index == this.state.parsed_data.length - 1;
            let onKeyDown = (event) => {
                if (event.keyCode == 13 || event.keyCode == 32)
                    this.selectItem.call(this, item);
                if (event.keyCode == 40 && !is_last)
                    $('#' + this.id + (index + 1)).focus();
                if (event.keyCode == 38 && !is_first)
                    $('#' + this.id + (index - 1)).focus();
                if (event.keyCode == 38 && is_first && this.props.search != 'none')
                    $('#' + this.id).focus();
            };
            rows.push(React.createElement("div", {tabIndex: 0, key: item.id, id: this.id + index, onClick: this.selectItem.bind(this, item), onKeyDown: onKeyDown, className: "sp-item text thick padding-horizontal-d2 padding-vertical-px8" + (item === this.state.value || (this.state.value && item.id == this.state.value.id) ? " active" : "")}, item.name));
        });
        if (this.props.asInput) {
            const onKeyDown = (event) => {
                if (event.keyCode == 40 && this.state.parsed_data.length > 0) {
                    $('#' + this.id + 0).focus();
                }
            };
            let selected = "Не выбрано";
            if (this.state.value) {
                selected = this.state.value.name;
            }
            return (React.createElement("div", {className: "sp-input-group"}, 
                React.createElement(Dropdown_1.Dropdown, {ref: "dropdownMenu", position: "bottom right", closeOnEsc: true, closeOnOutsideClick: true, activeClassName: "open", connectionPortal: this.state.connectionPortal, openOnFocus: true, element: (React.createElement("input", {id: this.id, value: this.state.search_string, onChange: this.requestChange.bind(this), type: "text", className: "sp-form-control" + (this.props.className ? (' ' + this.props.className) : ''), placeholder: selected, onKeyDown: onKeyDown}))}, 
                    React.createElement("div", {className: "sp-dropdown2-menu pull-left min-width-3 width-full"}, 
                        React.createElement("div", {className: "scrollable-menu max-height-4"}, rows)
                    )
                )
            ));
        }
        else {
            let searchElem = null;
            if (this.props.search != 'none') {
                const onKeyDown = (event) => {
                    if (event.keyCode == 40 && this.state.parsed_data.length > 0) {
                        $('#' + this.id + 0).focus();
                    }
                };
                searchElem = (React.createElement("div", {className: ""}, 
                    React.createElement("div", {className: "sp-input-group sp-sm"}, 
                        React.createElement("input", {tabIndex: 0, id: this.id, value: this.state.search_string, onChange: this.requestChange.bind(this), type: "text", className: "sp-form-control", placeholder: "Поиск", onKeyDown: onKeyDown})
                    )
                ));
            }
            return (React.createElement(Dropdown_1.Dropdown, {ref: "dropdownMenu", position: "bottom right", closeOnEsc: true, closeOnOutsideClick: true, toggleOnClick: true, activeClassName: "open", connectionPortal: this.state.connectionPortal, level: level, onKeyDown: (e) => {
                if (e.keyCode == 27) {
                    $('#' + this.id + 'i').focus();
                }
            }, onShow: () => {
                if (this.props.search == 'none') {
                    if (this.state.parsed_data.length > 0)
                        $('#' + this.id + 0).focus();
                }
                else {
                    $('#' + this.id).focus();
                }
            }, element: this.getElement.call(this)}, 
                React.createElement("div", {className: "sp-dropdown2-menu pull-left min-width-3 width-full"}, 
                    searchElem, 
                    React.createElement("div", {className: "scrollable-menu max-height-4"}, rows))
            ));
        }
    }
}
Select.defaultProps = new SelectProps();
exports.Select = Select;
//# sourceMappingURL=Select.js.map