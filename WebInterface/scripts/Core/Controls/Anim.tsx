import * as React from 'react'
import * as ReactDOM from 'react-dom';

import * as ReactTransitionGroup from 'react-addons-transition-group';

export class AnimChildProps {
  willAppear?: (el: JQuery, callback: () => void) => void
  willUnmount?: (el: JQuery) => void
  didAppear?: (el: JQuery) => void
  willEnter?: (el: JQuery, callback: () => void) => void
  didEnter?: (el: JQuery) => void
  willLeave?: (el: JQuery, callback: () => void) => void
  didLeave?: (el: JQuery) => void
}

export class AnimChildState {
}

export class AnimChild extends React.Component<AnimChildProps, AnimChildState> {

  componentWillAppear(callback) {
    if (this.props.willAppear) {
      var el = $(ReactDOM.findDOMNode(this))
      this.props.willAppear(el, callback)
    }
  }

  componentWillUnmount() {
    if (this.props.willUnmount) {
      var el = $(ReactDOM.findDOMNode(this))
      this.props.willUnmount(el)
    }
  }

  componentDidAppear() {
    if (this.props.didAppear) {
      var el = $(ReactDOM.findDOMNode(this))
      this.props.didAppear(el)
    }
  }

  componentWillEnter(callback) {
    if (this.props.willEnter) {
      var el = $(ReactDOM.findDOMNode(this))
      this.props.willEnter(el, callback)
    }
  }

  componentDidEnter() {
    if (this.props.didEnter) {
      var el = $(ReactDOM.findDOMNode(this))
      this.props.didEnter(el)
    }
  }

  componentWillLeave(callback) {
    if (this.props.willLeave) {
      var el = $(ReactDOM.findDOMNode(this))
      this.props.willLeave(el, callback)
    }
  }

  componentDidLeave() {
    if (this.props.didLeave) {
      var el = $(ReactDOM.findDOMNode(this))
      this.props.didLeave(el)
    }
  }

  render() {
    if (this.props.children) {
      return React.Children.only(this.props.children)
    }
    else {
      return null
    }
  }
}

export class AnimProps {
  willAppear?: (el: JQuery, callback: () => void) => void
  willUnmount?: (el: JQuery) => void
  didAppear?: (el: JQuery) => void
  willEnter?: (el: JQuery, callback: () => void) => void
  didEnter?: (el: JQuery) => void
  willLeave?: (el: JQuery, callback: () => void) => void
  didLeave?: (el: JQuery) => void
  className?: string
}

export class AnimState {
}

export class Anim extends React.Component<AnimProps, AnimState> {

  render() {
    var childCount = React.Children.count(this.props.children);
    var children = React.Children.map(this.props.children, function(child, idx) {
      return (
        <AnimChild
          {...this.props}>
          {child}
        </AnimChild>
      )
    }.bind(this));

    return (
      <ReactTransitionGroup className={this.props.className}>
        {children}
      </ReactTransitionGroup>
    )
  }
}
