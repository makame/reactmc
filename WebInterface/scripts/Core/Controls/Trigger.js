"use strict";
const React = require('react');
const Helper_1 = require('./Helper');
class TriggerProps {
    constructor() {
        this.valueLink = {
            value: null,
            requestChange: function (newValue) {
            }
        };
    }
}
exports.TriggerProps = TriggerProps;
class Trigger extends React.Component {
    constructor() {
        super(...arguments);
        this.id = Helper_1.default.guid();
    }
    requestChange(event) {
        this.props.valueLink.requestChange(!this.props.valueLink.value);
        this.forceUpdate();
    }
    render() {
        return (React.createElement("span", {className: "sp-form-trigger-container"}, 
            React.createElement("input", {className: "sp-form-trigger" + (this.props.className ? (' ' + this.props.className) : ''), type: "checkbox", name: "toggle", id: this.id, onChange: this.requestChange.bind(this), checked: this.props.valueLink.value, tabIndex: 0, onKeyDown: (e) => {
                if (e.keyCode == 13) {
                    this.requestChange.call(this, e);
                    return false;
                }
                else if (e.keyCode == 37) {
                    Helper_1.default.focusBack(e.target);
                }
                else if (e.keyCode == 39) {
                    Helper_1.default.focusNext(e.target);
                }
            }}), 
            React.createElement("label", {className: "label", htmlFor: this.id, "data-on": "Вкл", "data-off": "Выкл"})));
    }
}
Trigger.defaultProps = new TriggerProps();
exports.Trigger = Trigger;
//# sourceMappingURL=Trigger.js.map