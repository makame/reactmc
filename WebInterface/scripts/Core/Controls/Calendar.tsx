import * as React from 'react'
import * as moment from 'moment';
import Helper from './Helper';
import { EditorSimple } from './EditorSimple';
import { IValueLink } from '../Main'

export class Dates {
    title: string
    value: moment.Moment
    inMonth: boolean
    isInInterval: boolean
    selected: boolean
}

export class CalendarProps {
  valueLink: IValueLink<moment.Moment> = {
    value: null,
    requestChange: (newValue) => {
    }
  }
  format?: string = "YYYY-MM-DD"
  weeks?: string[] = ['Пн.', 'Вт.', 'Ср.', 'Чт.', 'Пт.', 'Сб.', 'Вс.']
  locale?: string = 'ru'
  min?: moment.Moment
  max?: moment.Moment
  weeksPerMonth?: number
  className?: string
}

export class CalendarState {
  value?: moment.Moment
  input?: string = ''
  now?: moment.Moment = moment().startOf('month')
  dates?: Dates[][]
}

export class Calendar extends React.Component<CalendarProps, CalendarState> {

  public static defaultProps = new CalendarProps()

  state = new CalendarState()

  id: string = Helper.guid()

  componentWillReceiveProps(nextProps, nextContext) {
    const { valueLink } = this.props
    if (nextProps.valueLink.value !== valueLink.value) {
      this.state.value = nextProps.valueLink.value
      this.state.input = this.state.value ? this.state.value.format(this.props.format) : ''
      this.forceUpdate()
    }

    moment.locale(nextProps.locale)
  }

  componentWillMount() {
    this.state.value = this.props.valueLink.value
    this.state.input = this.state.value ? this.state.value.format(this.props.format) : ''

    moment.locale(this.props.locale)
  }

  getDaysArrayByCalendarMonth(date): Dates[][] {
    let startDate = moment(date).startOf('month');

    let endDate = moment(date).endOf('month');

    let weeksInMonth = 1;
    if (this.props.weeksPerMonth) {
      weeksInMonth = this.props.weeksPerMonth;
    }
    else {
      let tempDate = moment(startDate).add(1, "weeks").startOf('isoWeek');

      for (; tempDate.month() == date.month(); weeksInMonth++) {
        tempDate.add(1, "weeks").startOf('isoWeek');
      }
    }

    let arr: Dates[][] = [];

    for (let k = 0; k < weeksInMonth; k++) {
      let arrWeek: Dates[] = [];

      let currentWeek = moment(startDate).add(k, "weeks").startOf('isoWeek');

      for (let i = 0; i < 7; i++) {
        let current = moment(currentWeek).add(i, "days");
        arrWeek.push({
          title: current.format("DD"),
          value: current,
          inMonth: current.month() == date.month(),
          isInInterval: this.isInInterval(current),
          selected: this.state.value && (current.year() == this.state.value.year() && current.month() == this.state.value.month() && current.date() == this.state.value.date()),
        });
      }

      arr.push(arrWeek);
    }

    return arr;
  }

  goPrev() {
    this.state.now = this.state.now.subtract(1, "months").startOf('month')
    this.setState({now: this.state.now})
  }

  goNext() {
    this.state.now = this.state.now.add(1, "months").startOf('month')
    this.setState({now: this.state.now})
  }

  selection(item) {
    if (item.isInInterval) {
      if (
        (
          this.state.value && (
            this.state.value.year() != item.value.year() || this.state.value.month() != item.value.month() || this.state.value.date() != item.value.date()
          )
        )
        || !this.state.value
      ) {
        this.state.value = moment(item.value)
        this.state.now = moment(this.state.value).startOf('month')
        this.setState({now: this.state.now, value: this.state.value, input: this.state.value.format(this.props.format)})

        this.props.valueLink.requestChange(this.state.value)
      }
      else {
        this.state.value = null
        this.state.input = ''
        this.forceUpdate()
        
        this.props.valueLink.requestChange(this.state.value)
      }
    }
  }

  placeCaretAtEnd(el) {
    if (!window.getSelection && !document.createRange) {
      let range = document.createRange();
      range.selectNodeContents(el);
      range.collapse(false);
      let sel = window.getSelection();
      sel.removeAllRanges();
      sel.addRange(range);
    }
    // else if (!document.body.createTextRange) {
    //   let textRange = document.body.createTextRange();
    //   textRange.moveToElementText(el);
    //   textRange.collapse(false);
    //   textRange.select();
    // }
  }

  focus() {
    $('#' + this.id).focus()

    this.placeCaretAtEnd($('#' + this.id)[0])
  }

  isInInterval(value) {
    return (!this.props.max || this.props.max.diff(value, 'days') >= 0) && (!this.props.min || this.props.min.diff(value, 'days') <= 0)
  }

  isValid(string) {
    return moment(string, this.props.format, true).isValid() && (!this.props.max || this.props.max.diff(moment(string), 'days') >= 0) && (!this.props.min || this.props.min.diff(moment(string), 'days') <= 0)
  }

  render() {
    let valueLink = {
      value: this.state.input,
      requestChange: (newValue) => {
        if (this.isValid(newValue)) {
          this.state.value = moment(newValue, this.props.format, true)
          this.state.now = moment(this.state.value).startOf('month')
          this.setState({now: this.state.now, value: this.state.value, input: newValue})

          this.props.valueLink.requestChange(this.state.value)
        }
        else {
          this.setState({input: newValue})
        }
      }
    }

    this.state.dates = this.getDaysArrayByCalendarMonth(this.state.now);

    let weeks = this.props.weeks.map((week)=>{return (
      <div key={week} className="box-xs">
        {week}
      </div>
    )})

    let days = this.state.dates.map((week, i)=>{
      return (
        <div key={i}>
          {week.map((day, k)=>{
            return (
              <div key={k} className={"text box-xs pointer" + (day.isInInterval ? '' : ' disabled') + (day.inMonth ? '' : ' subsub') + (day.selected ? ' active' : '')} onClick={()=>{this.selection.call(this, day)}}>
                {day.title}
              </div>
            )
          })}
        </div>
      )
    })

    return (
      <div className={"sp-calendar" + ' ' + (this.props.className ? this.props.className : '')}>
        <div className="sp-table-layout text center">
          <div>
            <div className="pointer box-sm" onClick={this.goPrev.bind(this)}>
              {'<'}
            </div>
            <EditorSimple valueLink={valueLink} className={"input box-sm selectable" + (this.isValid(this.state.input) ? '' : ' error')} id={this.id} tagName="div" useTitle title={this.state.now.format("YYYY/MM")}>
            </EditorSimple>
            <div className="pointer box-sm" onClick={this.goNext.bind(this)}>
              {'>'}
            </div>
          </div>
        </div>
        <div className="sp-table-layout text center">
          <div>
            {weeks}
          </div>
          {days}
        </div>
      </div>
    );
  }
}