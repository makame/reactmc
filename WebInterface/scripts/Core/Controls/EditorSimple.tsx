import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { ContentEditable, ContentEditableProps } from './ContentEditable';
import { IValueLink } from '../Main'

export class EditorSimpleProps extends ContentEditableProps {
  valueLink?: IValueLink<string>
  onEnter?: (obj: EditorSimple) => void
  onBack?: (obj: EditorSimple) => void
  onLeft?: (obj: EditorSimple) => void
  onRight?: (obj: EditorSimple) => void
}

export class EditorSimpleState {
  value: string = ""
}

export class EditorSimple extends React.Component<EditorSimpleProps, EditorSimpleState> {

  public static defaultProps = new EditorSimpleProps()

  state = new EditorSimpleState()

  handleChange(e) {
    let value = this.calcHtml(e.target.value)

    if (value != this.state.value) {
      this.state.value = value
      this.setState({value: this.state.value})

      if (this.props.valueLink) {
        this.props.valueLink.requestChange(this.state.value)
      }

    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.valueLink) {
      this.setState({value: nextProps.valueLink.value})
    }
  }

  componentDidMount() {
    if (this.props.valueLink) {
      this.setState({value: this.props.valueLink.value})
    }
  }

  calcHtml(value) {
    let self = this

    let result = "";

    $('<div/>').append($.parseHTML(value)).contents().each(function(index, itemRow) {
      let jitemRow = $(itemRow)
      let tagRow = jitemRow.prop("tagName")

      if (tagRow == 'DIV' || tagRow == 'SPAN' || tagRow == 'A') {
        result += self.getContent(self, jitemRow)
      }
      else {
        result += $('<div/>').append(jitemRow.clone()).html()
      }
    })

    result = result.replace(/<br>/g, '')

    return result
  }

  getContent(self, jitemRow) {
    let result = "";

    jitemRow.contents().each(function(index, item) {
      let jitem = $(item)
      let tag = jitem.prop("tagName")

      if (tag == 'DIV' || tag == 'SPAN' || tag == 'A') {
        result += self.getContent(self, jitem)
      }
      else {
        result += $('<div/>').append(jitem.clone()).html()
      }
    })

    return result
  }

  onKeyDown(e) {
  }

  onKeyUp(e) {
    if (e.which == 13) {
      if (this.props.onEnter) {
        this.props.onEnter(this)
      }
    }
    else if (e.which == 8) {
      if (this.props.onBack) {
        this.props.onBack(this)
      }
    }
    else if (e.which == 37) {
      if (this.props.onLeft) {
        this.props.onLeft(this)
      }
    }
    else if (e.which == 39) {
      if (this.props.onRight) {
        this.props.onRight(this)
      }
    }
  }

  render() {
    let self = this

    const { id, useTitle, title, tagName, className } = this.props

    let html = this.state.value

    const props = {
      id,
      className,
      tagName,
      title,
      useTitle,
      html: html,
      onKeyDown: this.onKeyDown.bind(this),
      onKeyUp: this.onKeyUp.bind(this),
      onChange: this.handleChange.bind(this)
    }

    return React.createElement(ContentEditable, props);
  }
}