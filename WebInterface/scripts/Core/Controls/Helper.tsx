export default class Helper {
  public static guid(): string {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
    }
    return s4() + s4() + s4() + s4();
  }

  public static focusBack(target: HTMLElement):boolean {
    let tabs = $('[tabindex="0"]')
    let finded = false
    for (var i = tabs.length - 1; i >= 0; i--) {
      if (finded) {
        $(tabs[i]).focus()
        break;
      }
      if (target === tabs[i]) {
        finded = true
      }
    }
    return finded
  }

  public static focusNext(target: HTMLElement):boolean {
    let tabs = $('[tabindex="0"]')
    let finded = false
    for (var i = 0; i < tabs.length; i++) {
      if (finded) {
        $(tabs[i]).focus()
        break;
      }
      if (target === tabs[i]) {
        finded = true
      }
    }
    return finded
  }
}
