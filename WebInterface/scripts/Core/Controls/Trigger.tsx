import * as React from 'react';
import Helper from './Helper';
import { IValueLink } from '../Main'

export class TriggerProps {
  valueLink: IValueLink<boolean> = {
    value: null,
    requestChange: function(newValue) {
    }
  }
  className?: string
  icon?: string
  text?: string
}

export class Trigger extends React.Component<TriggerProps, {}> {

  public static defaultProps = new TriggerProps()

  id: string = Helper.guid()

  requestChange(event) {
    this.props.valueLink.requestChange(!this.props.valueLink.value)
    this.forceUpdate()
  }

  render() {
    return (
      <span className="sp-form-trigger-container">
        <input className={"sp-form-trigger" + (this.props.className ? (' ' + this.props.className) : '')} type="checkbox" name="toggle" id={this.id} onChange={this.requestChange.bind(this)} checked={this.props.valueLink.value} tabIndex={0}
        onKeyDown={(e)=>{
            if (e.keyCode == 13) {
              this.requestChange.call(this, e)
              return false
            }
            else if (e.keyCode == 37) {
              Helper.focusBack(e.target as HTMLElement)
            }
            else if (e.keyCode == 39) {
              Helper.focusNext(e.target as HTMLElement)
            }
          }
        }/>
        <label className="label" htmlFor={this.id} data-on="Вкл" data-off="Выкл"></label>
      </span>
    );
  }
}