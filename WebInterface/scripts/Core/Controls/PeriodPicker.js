"use strict";
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
const React = require('react');
const FontAwesome = require('react-fontawesome');
const Popup_1 = require('../Controls/Popup');
const Calendar_1 = require('./Calendar');
class MomentPeriod {
}
exports.MomentPeriod = MomentPeriod;
class PeriodPickerProps {
    constructor() {
        this.valueLink = {
            value: {
                start: null,
                end: null,
            },
            requestChange: (newValue) => {
            }
        };
        this.format = "YYYY-MM-DD";
        this.weeks = ['Пн.', 'Вт.', 'Ср.', 'Чт.', 'Пт.', 'Сб.', 'Вс.'];
        this.locale = 'ru';
    }
}
exports.PeriodPickerProps = PeriodPickerProps;
class PeriodPickerState {
    constructor() {
        this.connectionPortal = {};
    }
}
exports.PeriodPickerState = PeriodPickerState;
class PeriodPicker extends React.Component {
    constructor() {
        super(...arguments);
        this.state = new PeriodPickerState();
    }
    clear() {
        let self = this;
        self.props.valueLink.value.start = null;
        self.props.valueLink.value.end = null;
        self.props.valueLink.requestChange(self.props.valueLink.value);
    }
    getElement() {
        let self = this;
        if (self.props.render) {
            return self.props.render(self.props.valueLink.value, self.clear, self.props.format);
        }
        else {
            var selected = "Не выбрано";
            if (self.props.valueLink.value.start && self.props.valueLink.value.end) {
                selected = "с " + self.props.valueLink.value.start.format(self.props.format) + " по " + self.props.valueLink.value.end.format(self.props.format);
            }
            else if (self.props.valueLink.value.start && !self.props.valueLink.value.end) {
                selected = "с " + self.props.valueLink.value.start.format(self.props.format);
            }
            else if (!self.props.valueLink.value.start && self.props.valueLink.value.end) {
                selected = "по " + self.props.valueLink.value.end.format(self.props.format);
            }
            return (React.createElement("div", {tabIndex: 0, className: "sp-input-group sp-dropdown2-item " + self.props.className}, 
                React.createElement("div", {className: "sp-input-group-label width-full"}, selected), 
                React.createElement("div", {className: "sp-input-group-btn sp-as-addon square sp-dropdown2-on-open", onClick: self.clear}, 
                    React.createElement(FontAwesome, {name: self.props.valueLink.value ? "remove" : "chevron-up"})
                ), 
                React.createElement("div", {className: "sp-input-group-addon square sp-dropdown2-on-not-open"}, 
                    React.createElement(FontAwesome, {name: "chevron-down"})
                )));
        }
    }
    render() {
        let self = this;
        const { valueLink, weeksPerMonth, format, weeks, locale, min, max } = this.props;
        const propsStart = {
            weeksPerMonth,
            format,
            weeks,
            locale,
            min,
            max: self.props.valueLink.value.end
        };
        const propsEnd = {
            weeksPerMonth,
            format,
            weeks,
            locale,
            min: self.props.valueLink.value.start,
            max,
        };
        return (React.createElement(Popup_1.Popup, {ref: "dropdownMenu", position: "bottom right", positionEl: "start", className: "sp-auto", closeOnEsc: true, closeOnOutsideClick: true, toggleOnClick: true, activeClassName: "open", connectionPortal: this.state.connectionPortal, onShow: () => {
            self.refs.calendarStart.focus();
        }, element: self.getElement()}, 
            React.createElement("div", {className: "sp-table-layout min-width-3 width-full padding-px3"}, 
                React.createElement("div", null, 
                    React.createElement("div", {style: { borderRight: "solid 1px #d9ecf2" }}, 
                        React.createElement(Calendar_1.Calendar, __assign({ref: "calendarStart", valueLink: {
                            value: self.props.valueLink.value.start,
                            requestChange: function (newValue) {
                                self.props.valueLink.value.start = newValue;
                                self.props.valueLink.requestChange(self.props.valueLink.value);
                            }
                        }}, propsStart))
                    ), 
                    React.createElement("div", null, 
                        React.createElement(Calendar_1.Calendar, __assign({ref: "calendarEnd", valueLink: {
                            value: self.props.valueLink.value.end,
                            requestChange: function (newValue) {
                                self.props.valueLink.value.end = newValue;
                                self.props.valueLink.requestChange(self.props.valueLink.value);
                            }
                        }}, propsEnd))
                    ))
            )
        ));
    }
}
PeriodPicker.defaultProps = new PeriodPickerProps();
exports.PeriodPicker = PeriodPicker;
//# sourceMappingURL=PeriodPicker.js.map