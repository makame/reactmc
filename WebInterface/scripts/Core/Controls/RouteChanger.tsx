import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Helper from './Helper';

import * as ReactTransitionGroup from 'react-addons-transition-group';
import { browserHistory } from 'react-router'

export class RouteChangerChildProps {
  animateDuration: number
  location: any
  shouldUpdate: boolean
}

export class RouteChangerChild extends React.Component<RouteChangerChildProps, {}> {

  id: string = Helper.guid()
  
  shouldComponentUpdate(nextProps) {
    return !!nextProps.shouldUpdate;
  }

  componentWillAppear(callback) {
    this._animateIn(callback)
  }

  componentDidAppear() {
  }

  componentWillEnter(callback) {
    this._animateInDelay(callback)
  }

  componentDidMount() {
  }

  componentDidEnter() {
  }

  componentWillLeave(callback) {
    this._animateOut(callback)
  }

  componentWillUnmount() {
    var el = ReactDOM.findDOMNode(this);
    $(el).velocity("stop");
  }

  componentDidLeave() {
  }

  _animateInDelay(callback) {
    var el = ReactDOM.findDOMNode(this);
    $(el).css('display', "none")
    $(el).velocity({ opacity: [1.0, 0] }, { display: "block", duration: this.props.animateDuration / 2, delay: this.props.animateDuration / 2, complete: callback })
  }

  _animateIn(callback) {
    var el = ReactDOM.findDOMNode(this);
    $(el).css('display', "none")
    $(el).velocity({ opacity: [1.0, 0] }, { display: "block", duration: this.props.animateDuration / 2, complete: callback })
  }

  _animateOut(callback) {
    var el = ReactDOM.findDOMNode(this);
    $(el).velocity({ opacity: 0 }, { display: "none", duration: this.props.animateDuration / 2, complete: callback })
  }

  render() {
    var child = this.props.children;
    if (child === null || child === false) {
      return null;
    }

    const { shouldUpdate, animateDuration, location, children } = this.props

    return React.cloneElement(this.props.children as React.ReactElement<any>, { location }, React.Children.only(this.props.children));
  }
}

export class RouteChangerProps {
  duration?: number = 300
  location: any
}

export class RouteChangerState {
  previousPathname?: string
  shouldUpdate: boolean = false
}

export class RouteChanger extends React.Component<RouteChangerProps, RouteChangerState> {

  public static defaultProps = new RouteChangerProps()

  state = new RouteChangerState()

  componentWillReceiveProps(nextProps, nextContext) {
    if (nextProps.location.pathname !== this.props.location.pathname) {
      this.setState({ shouldUpdate: true })
    }
  }

  componentDidUpdate() {
    if (this.state.previousPathname !== this.props.location.pathname) {
      this.setState({ previousPathname: this.props.location.pathname, shouldUpdate: false })
    }
  }

  render() {
    const { shouldUpdate, previousPathname } = this.state
    const { duration, children } = this.props

    var routes = React.Children.map(children, function(child, idx) {
      return (
        <RouteChangerChild
          key={previousPathname || this.props.location.pathname}
          shouldUpdate={!previousPathname}
          animateDuration={duration}
          location={location}>
          {child}
        </RouteChangerChild>
      )
    }.bind(this));

    return (
      <ReactTransitionGroup>
        {routes}
      </ReactTransitionGroup>
    )
  }
}
