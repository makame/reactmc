import * as React from 'react'
import * as ReactDOM from 'react-dom';
import { Portal, PortalProps } from './Portal';

export class ModalProps extends PortalProps {
  level?: number = -1
  closeOnOutsideClick?: boolean
  dialog?: boolean
  message?: JSX.Element
  bottom?: JSX.Element
}

export class Modal extends React.Component<ModalProps, {}> {

  public static defaultProps = new ModalProps()

  refs: {
    [key: string]: any;
    portal: Portal;
  }

  closePortal() {
    this.refs.portal.closePortal();
  }

  openPortal() {
    this.refs.portal.openPortal();
  }

  componentDidMount() {
    this.openPortal();
  }

  componentWillUnmount() {
    this.closePortal();
  }

  handleOutsideClick(e) {
    if (this.props.closeOnOutsideClick && this.refs.portal.state.active) {
      e.preventDefault();
      e.stopPropagation();
      this.refs.portal.closePortal();
    }
    else if (this.props.onOutsideClick) {
      if ($(e.target).hasClass('sp-modal-back') || !$(e.target).find(ReactDOM.findDOMNode(this))) {
        this.props.onOutsideClick(e)
      }
    }
  }

  render() {
    const {children, className, onOutsideClick, closeOnOutsideClick, dialog, message, bottom } = this.props

    const {
      level,
      closeOnEsc,
      isOpened,
      isHidden,
      isGhost,
      connectionPortal,
      onElementClick,
      onElementOver,
      onElementContextMenu,
      onElementOut,
      onElementFocus,
      onElementBlur,
      onKeyDown,
      onOpen,
      onUpdate,
      didUpdate,
      beforeClose,
      onClose,
      isModal,
      style,
      element,
      activeClassName 
    } = this.props

    const props = {
      level,
      closeOnEsc,
      isOpened,
      isHidden,
      isGhost,
      connectionPortal,
      onElementClick,
      onElementOver,
      onElementContextMenu,
      onElementOut,
      onElementFocus,
      onElementBlur,
      onKeyDown,
      onOpen,
      onUpdate,
      didUpdate,
      beforeClose,
      onClose,
      isModal,
      style,
      element,
      activeClassName
    }

    var messageEl = null
    if (message) {
      messageEl = (
        <div className="sp-message">
          {message}
        </div>
      )
    }

    var bottomEl = null
    if (bottom) {
      bottomEl = (
        <div className="sp-bottom">
          {bottom}
        </div>
      )
    }

    return (
      <Portal ref="portal" {...props} className="sp-modal-back" isModal={true}
        onOpen={(node)=>{
          $(node).velocity({ opacity: 1.0 }, { display: 'block', duration: 200 });
          $(node).children().velocity({ opacity: [1.0, 0], translateX: ["-50%", "-50%"], translateY: [0, "-4rem"], }, { display: 'block', duration: 200 });
        }}
        beforeClose={(node, callback)=>{
          $(node).velocity({ opacity: 0.0 }, { duration: 200, display: 'none', complete: callback});
        }}
        onOutsideClick={closeOnOutsideClick || onOutsideClick ? this.handleOutsideClick.bind(this) : null}>
        <div className={"sp-dialog"}>
          <div className={"sp-modal" + (dialog ? ' sp-quiz' : '') + (className ? (' ' + className) : '')}>
            {this.props.children}
            {messageEl}
            {bottomEl}
          </div>
        </div>
      </Portal>
    )
  }
}
