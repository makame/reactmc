import * as React from 'react'
import * as ReactDOM from 'react-dom'

export class LImageProps {
  url?: string
  zoomable?: boolean = true
  overalable?: boolean = true
  overflow?: boolean = true
  zoom?: number = 1
  defaultSize?: {
    height: number,
    width: number
  } = {
    height: 50,
    width: 50
  }
}

export class LImageState {
  src?: string

  loading: boolean = false
  error: boolean = false

  width: number = 0
  height: number = 0

  x: number = 0
  y: number = 0
  zoom: number = 1

  mouseOver: boolean = false
  
  style: React.CSSProperties = {
    position: 'absolute'
  }
}

export class LImage extends React.Component<LImageProps, LImageState> {

  public static defaultProps = new LImageProps()

  state = new LImageState()

  componentWillReceiveProps(nextProps, nextContext) {
    const { url } = this.props
    if (nextProps.url !== url) {
      this.state.zoom = nextProps.zoom
      this.load()
    }
  }

  componentWillMount() {
    this.state.zoom = this.props.zoom
    this.load()
  }

  componentDidMount() {
    const container = $(ReactDOM.findDOMNode(this))
    if (this.props.overalable) container.mousemove(this.onMouseMove.bind(this)).mouseover(this.onMouseOver.bind(this)).mouseout(this.onMouseOut.bind(this))
    if (this.props.zoomable) container.bind('mousewheel', this.onMouseWheel.bind(this));
  }

  onMouseWheel(e) {
    if ((e.originalEvent as WheelEvent).wheelDelta / 120 > 0) {
      this.state.zoom = Math.min(this.state.zoom + 1, 15);
      this.calcPosition()
    }
    else {
      this.state.zoom = Math.max(this.state.zoom - 1, 1);
      this.calcPosition()
    }
    return false;
  }

  componentWillUnmount() {
  }

  onLoadImage(elem: HTMLImageElement, ev: Event) {
    this.state.loading = false
    this.state.src = (elem as HTMLImageElement).src
    this.forceUpdate()
  }

  load() {
    if (this.props.url) {
      this.state.loading = true
      this.forceUpdate()
      const img = new Image();
      img.onload = this.onLoadImage.bind(this, img)
      img.onerror = (ev: ErrorEvent) => {
        this.state.loading = false
        this.state.error = true
        console.error(ev);
      }
      img.src = this.props.url
    }
  }

  onMouseOver(e) {
    const container = $(ReactDOM.findDOMNode(this))

    this.state.width = container.width()
    this.state.height = container.height()

    this.state.mouseOver = true

    this.state.x = e.offsetX
    this.state.y = e.offsetY
    
    this.calcPosition()
  }

  onMouseOut(e) {
    this.state.mouseOver = false

    this.forceUpdate()
  }

  onMouseMove(e) {
    this.state.x = e.offsetX
    this.state.y = e.offsetY

    this.calcPosition()
  }

  calcPosition() {
    if (!this.state.mouseOver) return;

    const container = $(ReactDOM.findDOMNode(this))

    this.state.style.height = this.state.height * this.state.zoom
    this.state.style.width = this.state.width * this.state.zoom

    let left = - this.state.x * this.state.zoom + this.state.width / 2
    let top = - this.state.y * this.state.zoom + this.state.height / 2

    left = Math.max(left, this.state.width * (1 - this.state.zoom))
    left = Math.min(left, 0)

    top = Math.max(top, this.state.height * (1 - this.state.zoom))
    top = Math.min(top, 0)

    this.state.style.left = left
    this.state.style.top = top
    
    this.forceUpdate()
  }

  render() {
    return (
      <div className={"sp-image" + (this.state.mouseOver ? (this.props.overflow ? ' overflow-hidden' : ' overflow-visible') : '') + (this.state.loading ? ' loading' : '')}
      style={this.state.mouseOver && !this.state.loading && this.state.src ? {
        cursor: this.state.error ? 'pointer' : (this.props.overalable ? 'crosshair' : (this.props.zoomable ? 'zoom-in' : '')),
        minHeight: this.state.height,
        minWidth: this.state.width,
        maxHeight: this.state.height,
        maxWidth: this.state.width,
        height: this.state.height,
        width: this.state.width
      } : (
        this.props.defaultSize && !this.state.src ? {
          cursor: this.state.error ? 'pointer' : '',
          height: this.props.defaultSize.height,
          width: this.props.defaultSize.width
        } : {
          cursor: this.state.error ? 'pointer' : ''
        })} onClick={()=>{if (this.state.error) this.load.call(this)}}>
        <img src={this.state.src} style={this.state.mouseOver ? this.state.style : {}}/>
      </div>
    );
  }
}