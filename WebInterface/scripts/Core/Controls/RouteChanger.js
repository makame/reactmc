"use strict";
const React = require('react');
const ReactDOM = require('react-dom');
const Helper_1 = require('./Helper');
const ReactTransitionGroup = require('react-addons-transition-group');
class RouteChangerChildProps {
}
exports.RouteChangerChildProps = RouteChangerChildProps;
class RouteChangerChild extends React.Component {
    constructor() {
        super(...arguments);
        this.id = Helper_1.default.guid();
    }
    shouldComponentUpdate(nextProps) {
        return !!nextProps.shouldUpdate;
    }
    componentWillAppear(callback) {
        this._animateIn(callback);
    }
    componentDidAppear() {
    }
    componentWillEnter(callback) {
        this._animateInDelay(callback);
    }
    componentDidMount() {
    }
    componentDidEnter() {
    }
    componentWillLeave(callback) {
        this._animateOut(callback);
    }
    componentWillUnmount() {
        var el = ReactDOM.findDOMNode(this);
        $(el).velocity("stop");
    }
    componentDidLeave() {
    }
    _animateInDelay(callback) {
        var el = ReactDOM.findDOMNode(this);
        $(el).css('display', "none");
        $(el).velocity({ opacity: [1.0, 0] }, { display: "block", duration: this.props.animateDuration / 2, delay: this.props.animateDuration / 2, complete: callback });
    }
    _animateIn(callback) {
        var el = ReactDOM.findDOMNode(this);
        $(el).css('display', "none");
        $(el).velocity({ opacity: [1.0, 0] }, { display: "block", duration: this.props.animateDuration / 2, complete: callback });
    }
    _animateOut(callback) {
        var el = ReactDOM.findDOMNode(this);
        $(el).velocity({ opacity: 0 }, { display: "none", duration: this.props.animateDuration / 2, complete: callback });
    }
    render() {
        var child = this.props.children;
        if (child === null || child === false) {
            return null;
        }
        const { shouldUpdate, animateDuration, location, children } = this.props;
        return React.cloneElement(this.props.children, { location }, React.Children.only(this.props.children));
    }
}
exports.RouteChangerChild = RouteChangerChild;
class RouteChangerProps {
    constructor() {
        this.duration = 300;
    }
}
exports.RouteChangerProps = RouteChangerProps;
class RouteChangerState {
    constructor() {
        this.shouldUpdate = false;
    }
}
exports.RouteChangerState = RouteChangerState;
class RouteChanger extends React.Component {
    constructor() {
        super(...arguments);
        this.state = new RouteChangerState();
    }
    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.location.pathname !== this.props.location.pathname) {
            this.setState({ shouldUpdate: true });
        }
    }
    componentDidUpdate() {
        if (this.state.previousPathname !== this.props.location.pathname) {
            this.setState({ previousPathname: this.props.location.pathname, shouldUpdate: false });
        }
    }
    render() {
        const { shouldUpdate, previousPathname } = this.state;
        const { duration, children } = this.props;
        var routes = React.Children.map(children, function (child, idx) {
            return (React.createElement(RouteChangerChild, {key: previousPathname || this.props.location.pathname, shouldUpdate: !previousPathname, animateDuration: duration, location: location}, child));
        }.bind(this));
        return (React.createElement(ReactTransitionGroup, null, routes));
    }
}
RouteChanger.defaultProps = new RouteChangerProps();
exports.RouteChanger = RouteChanger;
//# sourceMappingURL=RouteChanger.js.map