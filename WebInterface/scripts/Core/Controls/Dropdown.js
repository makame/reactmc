"use strict";
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
const React = require('react');
const ReactDOM = require('react-dom');
const Portal_1 = require('./Portal');
class DropdownProps extends Portal_1.PortalProps {
    constructor() {
        super(...arguments);
        this.level = 0;
        this.padding_window = 0;
        this.offset = 0;
        this.timeout = 0;
        this.activeClassName = 'active';
        this.delay = 0;
    }
}
exports.DropdownProps = DropdownProps;
class DropdownState {
}
exports.DropdownState = DropdownState;
class Dropdown extends React.Component {
    constructor() {
        super(...arguments);
        this.state = new DropdownState();
    }
    closePortal() {
        this.refs.portal.closePortal();
    }
    openPortal() {
        this.refs.portal.openPortal();
    }
    didUpdate(popupNode, elementNode) {
        var popup = $(ReactDOM.findDOMNode(this.refs.popup));
        var element = $(elementNode);
        if (popup.length == 0 || !this.refs.portal.state.active || this.refs.portal.props.isHidden) {
            return;
        }
        var container = $(document.body);
        var min_x = Math.max($(window).scrollLeft(), container.offset().left);
        var min_y = Math.max($(window).scrollTop(), container.offset().top);
        var max_x = Math.min($(window).width() + $(window).scrollLeft(), container.offset().left + container.outerWidth());
        var max_y = Math.min($(window).height() + $(window).scrollTop(), container.offset().top + container.outerHeight());
        var this_y = element.offset().top;
        var this_x = element.offset().left;
        var this_height = element.outerHeight();
        var this_width = element.outerWidth();
        var this_y_middle = this_y + this_height / 2;
        var this_x_middle = this_x + this_width / 2;
        var popup_height = popup.outerHeight();
        var popup_width = popup.outerWidth();
        var top = this_y;
        var left = this_x;
        var result_y = 'auto';
        var result_x = 'auto';
        if (this.props.position == 'top center' || this.props.position == 'top') {
            top = this_y - popup_height - this.props.offset;
            left = this_x + this_width / 2 - popup_width / 2;
            result_y = "top";
            result_x = "center";
            if (left + popup_width > max_x - this.props.padding_window) {
                left = max_x - this.props.padding_window - popup_width;
                result_x = "left";
            }
            else if (left < min_x + this.props.padding_window) {
                left = min_x + this.props.padding_window;
                result_x = "right";
            }
        }
        if (this.props.position == 'top left') {
            top = this_y - popup_height - this.props.offset;
            left = this_x - popup_width + this_width;
            result_y = "top";
            result_x = "left";
            if (left + popup_width > max_x - this.props.padding_window) {
                left = max_x - this.props.padding_window - popup_width;
                result_x = "left";
            }
            else if (left < min_x + this.props.padding_window) {
                left = min_x + this.props.padding_window;
                result_x = "right";
            }
        }
        if (this.props.position == 'top right') {
            top = this_y - popup_height - this.props.offset;
            left = this_x;
            result_y = "top";
            result_x = "right";
            if (left + popup_width > max_x - this.props.padding_window) {
                left = max_x - this.props.padding_window - popup_width;
                result_x = "left";
            }
            else if (left < min_x + this.props.padding_window) {
                left = min_x + this.props.padding_window;
                result_x = "right";
            }
        }
        if (this.props.position == 'bottom center' || this.props.position == 'bottom') {
            top = this_y + this_height + this.props.offset;
            left = this_x + this_width / 2 - popup_width / 2;
            result_y = "bottom";
            result_x = "center";
            if (left + popup_width > max_x - this.props.padding_window) {
                left = max_x - this.props.padding_window - popup_width;
                result_x = "left";
            }
            else if (left < min_x + this.props.padding_window) {
                left = min_x + this.props.padding_window;
                result_x = "right";
            }
        }
        if (this.props.position == 'bottom left') {
            top = this_y + this_height + this.props.offset;
            left = this_x - popup_width + this_width;
            result_y = "bottom";
            result_x = "left";
            if (left + popup_width > max_x - this.props.padding_window) {
                left = max_x - this.props.padding_window - popup_width;
                result_x = "left";
            }
            else if (left < min_x + this.props.padding_window) {
                left = min_x + this.props.padding_window;
                result_x = "right";
            }
        }
        if (this.props.position == 'bottom right') {
            top = this_y + this_height + this.props.offset;
            left = this_x;
            result_y = "bottom";
            result_x = "right";
            if (left + popup_width > max_x - this.props.padding_window) {
                left = max_x - this.props.padding_window - popup_width;
                result_x = "left";
            }
            else if (left < min_x + this.props.padding_window) {
                left = min_x + this.props.padding_window;
                result_x = "right";
            }
        }
        popup.css('min-width', this_width);
        popup.offset({ top: top, left: left });
    }
    handleWrapperClick(e) {
        if (this.props.toggleOnClick && !this.refs.portal.props.isHidden) {
            e.preventDefault();
            e.stopPropagation();
            window.clearTimeout(this.state.timer);
            this.state.timer = window.setTimeout(() => {
                this.refs.portal.togglePortal();
            }, this.props.delay);
        }
        else if (this.props.openOnClick && !this.refs.portal.state.active && !this.refs.portal.props.isHidden) {
            e.preventDefault();
            e.stopPropagation();
            window.clearTimeout(this.state.timer);
            this.state.timer = window.setTimeout(() => {
                this.refs.portal.openPortal();
            }, this.props.delay);
        }
        else if (this.props.closeOnClick && this.refs.portal.state.active) {
            e.preventDefault();
            e.stopPropagation();
            window.clearTimeout(this.state.timer);
            this.state.timer = window.setTimeout(() => {
                this.refs.portal.closePortal();
            }, this.props.timeout);
        }
    }
    handleWrapperContextMenu(e) {
        if (this.props.toggleOnContextMenu) {
            e.preventDefault();
            e.stopPropagation();
            window.clearTimeout(this.state.timer);
            this.state.timer = window.setTimeout(() => {
                this.refs.portal.togglePortal();
            }, this.props.delay);
        }
        else if (this.props.openOnContextMenu && !this.refs.portal.state.active) {
            e.preventDefault();
            e.stopPropagation();
            window.clearTimeout(this.state.timer);
            this.state.timer = window.setTimeout(() => {
                this.refs.portal.openPortal();
            }, this.props.delay);
        }
        else if (this.props.closeOnContextMenu && this.refs.portal.state.active) {
            e.preventDefault();
            e.stopPropagation();
            window.clearTimeout(this.state.timer);
            this.state.timer = window.setTimeout(() => {
                this.refs.portal.closePortal();
            }, this.props.timeout);
        }
    }
    handleWrapperOver(e) {
        if (this.props.closeOnOutMove) {
            window.clearTimeout(this.state.timer);
            this.state.timer = null;
        }
        if (this.props.openOnOverMove && !this.refs.portal.state.active) {
            window.clearTimeout(this.state.timer);
            this.state.timer = window.setTimeout(() => {
                this.refs.portal.openPortal();
            }, this.props.delay);
        }
    }
    handleWrapperOut(e) {
        if (this.props.openOnOverMove) {
            window.clearTimeout(this.state.timer);
            this.state.timer = null;
        }
        if (this.props.closeOnOutMove && this.refs.portal.state.active) {
            window.clearTimeout(this.state.timer);
            this.state.timer = window.setTimeout(() => {
                this.refs.portal.closePortal();
            }, this.props.timeout);
        }
    }
    handleWrapperFocus(e) {
        if (this.props.openOnFocus && !this.refs.portal.state.active) {
            window.clearTimeout(this.state.timer);
            this.state.timer = window.setTimeout(() => {
                this.refs.portal.openPortal();
            }, this.props.delay);
        }
    }
    handleWrapperBlur(e) {
        if (this.props.closeOnBlur && this.refs.portal.state.active) {
            window.clearTimeout(this.state.timer);
            this.state.timer = window.setTimeout(() => {
                this.refs.portal.closePortal();
            }, this.props.delay);
        }
    }
    handleOutsideClick(e) {
        if (this.props.closeOnOutsideClick && this.refs.portal.state.active) {
            e.preventDefault();
            e.stopPropagation();
            window.clearTimeout(this.state.timer);
            this.state.timer = window.setTimeout(() => {
                this.refs.portal.closePortal();
            }, this.props.timeout);
        }
        else if (this.props.onOutsideClick) {
            this.props.onOutsideClick(e);
        }
    }
    render() {
        let self = this;
        const { children, className, closeOnOutsideClick, onOutsideClick } = this.props;
        const { level, closeOnEsc, isOpened, isHidden, isGhost, connectionPortal, onElementClick, onElementOver, onElementContextMenu, onElementOut, onElementFocus, onElementBlur, onKeyDown, onOpen, onUpdate, didUpdate, beforeClose, onClose, isModal, style, element, activeClassName } = this.props;
        const props = {
            level,
            closeOnEsc,
            isOpened,
            isHidden,
            isGhost,
            connectionPortal,
            onElementClick,
            onElementOver,
            onElementContextMenu,
            onElementOut,
            onElementFocus,
            onElementBlur,
            onKeyDown,
            onOpen,
            didUpdate,
            beforeClose,
            onClose,
            isModal,
            style,
            element,
            activeClassName
        };
        var modifedActiveClassName = activeClassName + ' open';
        return (React.createElement(Portal_1.Portal, __assign({ref: "portal"}, props, {className: "sp-popup-back", didUpdate: this.didUpdate.bind(this), onUpdate: this.refs.portal ? this.refs.portal.update : null, activeClassName: modifedActiveClassName, onOpen: (node) => {
            if (self.props.onShow)
                self.props.onShow();
            $(node).velocity("stop");
            $(node).children().velocity("stop");
            $(node).velocity({ opacity: [1.0, 0] }, { duration: 200 });
            $(node).children().velocity({ opacity: [1.0, 0], translateY: [0, "2rem"] }, { duration: 200 });
        }, beforeClose: (node, callback) => {
            $(node).velocity("stop");
            $(node).children().velocity("stop");
            $(node).velocity({ opacity: 0.0 }, { duration: 200, complete: callback });
        }, onElementOut: this.handleWrapperOut.bind(this), onElementOver: this.handleWrapperOver.bind(this), onElementFocus: this.handleWrapperFocus.bind(this), onElementBlur: this.handleWrapperBlur.bind(this), onElementClick: this.handleWrapperClick.bind(this), onOutsideClick: closeOnOutsideClick || onOutsideClick ? this.handleOutsideClick.bind(this) : null, onElementContextMenu: this.handleWrapperContextMenu.bind(this)}), 
            React.createElement("div", {ref: "popup", className: "sp-dropdown2 " + className, onMouseOver: this.handleWrapperOver.bind(this), onMouseOut: this.handleWrapperOut.bind(this)}, children)
        ));
    }
}
Dropdown.defaultProps = new DropdownProps();
exports.Dropdown = Dropdown;
//# sourceMappingURL=Dropdown.js.map