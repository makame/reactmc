import * as React from 'react';
import * as moment from 'moment';

import FontAwesome = require('react-fontawesome')

import Helper from './Helper';
import { IValueLink } from '../Main';
import { ConnectionPortal } from './Portal'

import { Dropdown } from '../Controls/Dropdown';

export type SelectDataType = 'remote' | 'local'
export type SelectSearchType = 'none' | 'local' | 'remote'

export interface ISelectData {
  id: string
  name: string
}

export class SelectProps {
  data?: ISelectData[] = []
  data_type?: SelectDataType = 'local'
  search?: SelectSearchType = 'none'
  request?: (search_string: string) => Promise<ISelectData[]> | ISelectData[] = (search_string: string) => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve([])
      }, 300)
    })
  }
  default?: ISelectData
  asInput?: boolean
  render?: (value: ISelectData, clear: () => void, updating: boolean) => JSX.Element

  valueLink: IValueLink<ISelectData>
  icon?: string
  className?: string
  left?: JSX.Element[]
  right?: JSX.Element[]
  level?: number
}

export class SelectState {
  connectionPortal: ConnectionPortal = {}
  parsed_data?: ISelectData[] = []
  updating: boolean = false
  search_string: string = ''
  need_update_data: boolean = false
  value: ISelectData
}

export class Select extends React.Component<SelectProps, SelectState> {

  public static defaultProps = new SelectProps()

  state = new SelectState()

  id: string = Helper.guid()

  isMounted() {
    return true
  }

  recurse_search_local_data() {
    this.state.updating = true
    if (this.isMounted()) this.forceUpdate()

    setTimeout(() => {
      let temp = []
      for (let i in this.props.data) {
        let item = this.props.data[i]

        if (!this.state.search_string || item.name.toLowerCase().indexOf(this.state.search_string.toLowerCase()) !== -1) {
          temp.push(item)
        }
      }

      this.state.parsed_data = temp
      if (this.isMounted()) this.forceUpdate()

      if (this.state.need_update_data) {
        this.state.need_update_data = false

        this.recurse_search_local_data()
      }
      else {
        this.state.updating = false
        if (this.isMounted()) this.forceUpdate()
      }
    }, 300)
  }

  recurse_search_remote_data() {
    this.state.updating = true
    if (this.isMounted()) this.forceUpdate()

    setTimeout(async () => {
      let run_recurse = () => {
        if (this.state.need_update_data) {
          this.state.need_update_data = false

          this.recurse_search_remote_data()
        }
        else {
          this.state.updating = false
          if (this.isMounted()) this.forceUpdate()
        }
      }

      try {
        const data = await this.props.request(this.state.search_string)
        this.state.parsed_data = data as ISelectData[]
        if (this.isMounted()) this.forceUpdate()

        run_recurse()
      }
      catch (message) {
        console.error(message)

        run_recurse()
      }
    }, 300)
  }

  recurse_search_remote_local_data() {
    this.state.updating = true
    if (this.isMounted()) this.forceUpdate()

    setTimeout(async () => {
      let run_recurse = () => {
        if (this.state.need_update_data) {
          this.state.need_update_data = false

          this.recurse_search_remote_local_data()
        }
        else {
          this.state.updating = false
          if (this.isMounted()) this.forceUpdate()
        }
      }

      try {
        const data = await this.props.request(null)
        let temp = []

        data.forEach((item) => {
          if (!this.state.search_string || item.name.toLowerCase().indexOf(this.state.search_string.toLowerCase()) !== -1) {
            temp.push(item)
          }
        })

        this.state.parsed_data = temp
        if (this.isMounted()) this.forceUpdate()

        run_recurse()
      }
      catch (message) {
        console.error(message)

        run_recurse()
      }
    }, 300)
  }

  parse_data() {
    if (this.state.updating) {
      this.state.need_update_data = true
    }
    else if (this.props.data_type == 'local') {
      if (this.props.search == 'local') {
        this.recurse_search_local_data()
      }
      else if (this.props.search == 'remote') {
        this.state.need_update_data = false
        // throw Exeption('imposible to search as remote with local data')
      }
      else if (this.props.search == 'none') {
        this.state.parsed_data = this.props.data;
        if (this.isMounted()) this.forceUpdate()
        this.state.need_update_data = false
      }
    }
    else if (this.props.data_type == 'remote') {
      if (this.props.search == 'local') {
        this.recurse_search_remote_local_data()
      }
      else if (this.props.search == 'remote') {
        this.recurse_search_remote_data()
      }
      else if (this.props.search == 'none') {
        this.recurse_search_remote_data()
      }
    }
  }

  requestChange(event) {
    this.state.search_string = event.target.value
    if (this.isMounted()) this.forceUpdate()
    this.parse_data.call(this)
  }

  arraysEqual(a, b) {
    if (a === b) return true;
    if (a == null || b == null) return false;
    if (a.length != b.length) return false;

    for (let i = 0; i < a.length; ++i) {
      if (a[i] !== b[i]) return false;
    }
    return true;
  }

  componentWillReceiveProps(nextProps, nextContext) {
    // const { next_data, next_data_type, next_search, next_request, next_validate, next_valueLink } = nextProps
    const { data, data_type, search, request, valueLink } = this.props
    if (!this.arraysEqual(nextProps.data, data) || nextProps.data_type !== data_type || nextProps.search !== search || nextProps.request !== request || nextProps.valueLink.value !== valueLink.value) {
      this.state.value = nextProps.valueLink.value

      this.parse_data.call(this)
    }
  }

  componentWillMount() {
    this.state.value = this.props.valueLink.value
    setTimeout(this.parse_data.bind(this), 100)
  }

  clear() {
    this.state.value = this.props.default

    this.props.valueLink.requestChange(this.state.value)

    if (this.isMounted()) this.forceUpdate();
  }

  selectItem(item) {
    if (this.state.value === item || (this.state.value && item.id == this.state.value.id)) {
      this.state.value = this.props.default
    }
    else {
      this.state.value = item
    }

    $('#' + this.id + 'i').focus()

    this.state.connectionPortal.close()

    this.props.valueLink.requestChange(this.state.value)

    if (this.isMounted()) this.forceUpdate();
  }

  getElement() {
    if (this.props.render) {
      return this.props.render(this.state.value, this.clear, this.state.updating)
    }
    else {
      let selected = "Не выбрано"

      if (this.state.value) {
        selected = this.state.value.name
      }

      let icon_right = "chevron-up"
      if (this.state.value) icon_right = "remove"
      if (this.state.updating) icon_right = "spinner"

      let icon = this.props.icon ? <div className={"sp-input-icon"}><FontAwesome name={this.props.icon}/></div> : null

      return (
        <div tabIndex={0} id={this.id + 'i'} className={"sp-input-group sp-dropdown2-item" + (this.props.className ? (' ' + this.props.className) : '')} onKeyDown={(e)=>{
            if (e.keyCode == 13) {
              this.state.connectionPortal.open()
              return false
            }
            else if (e.keyCode == 37) {
              Helper.focusBack(e.target as HTMLElement)
            }
            else if (e.keyCode == 39) {
              Helper.focusNext(e.target as HTMLElement)
            }
          }}>
          {this.props.left}
          {icon}
          <div className="sp-input-group-addon label width-full">
            {selected}
          </div>
          <div className="sp-input-group-btn sp-as-addon square sp-dropdown2-on-open" onClick={this.clear.bind(this)}>
            <FontAwesome name={icon_right} pulse={this.state.updating}/>
          </div>
          <div className="sp-input-group-addon square sp-dropdown2-on-not-open">
            <FontAwesome name={this.state.updating ? "spinner" : "chevron-down"} pulse={this.state.updating}/>
          </div>
          {this.props.right}
        </div>
      )
    }
  }

  render() {
    const { className, data, data_type, search, request, valueLink, render, level } = this.props

    let rows = []

    let i = 0

    this.state.parsed_data.forEach((item, index) => {
      let is_first = index == 0
      let is_last = index == this.state.parsed_data.length - 1

      let onKeyDown = (event) => {
        if (event.keyCode == 13 || event.keyCode == 32) this.selectItem.call(this, item)
        if (event.keyCode == 40 && !is_last) $('#' + this.id + (index + 1)).focus()
        if (event.keyCode == 38 && !is_first) $('#' + this.id + (index - 1)).focus()
        if (event.keyCode == 38 && is_first && this.props.search != 'none') $('#' + this.id).focus()
      }

      rows.push(
        <div tabIndex={0} key={item.id} id={this.id + index} onClick={this.selectItem.bind(this, item)} onKeyDown={onKeyDown} className={"sp-item text thick padding-horizontal-d2 padding-vertical-px8" + (item === this.state.value || (this.state.value && item.id == this.state.value.id) ? " active" : "")}>{item.name}</div>
      )
    })

    if (this.props.asInput) {

      const onKeyDown = (event) => {
        if (event.keyCode == 40 && this.state.parsed_data.length > 0) {
          $('#' + this.id + 0).focus()
        }
      }

      let selected = "Не выбрано"

      if (this.state.value) {
        selected = this.state.value.name
      }

      return (
        <div className={"sp-input-group"}>
          <Dropdown
            ref="dropdownMenu"
            position="bottom right"
            closeOnEsc closeOnOutsideClick
            activeClassName="open"
            connectionPortal={this.state.connectionPortal}
            openOnFocus
            element={(
              <input id={this.id} value={this.state.search_string} onChange={this.requestChange.bind(this)} type="text" className={"sp-form-control" + (this.props.className ? (' ' + this.props.className) : '')} placeholder={selected} onKeyDown={onKeyDown}/>
            )}
            >
            <div className="sp-dropdown2-menu pull-left min-width-3 width-full">
              <div className="scrollable-menu max-height-4">
                {rows}
              </div>
            </div>
          </Dropdown>
        </div>
      );
    }
    else {
      let searchElem = null

      if (this.props.search != 'none') {
        const onKeyDown = (event) => {
          if (event.keyCode == 40 && this.state.parsed_data.length > 0) {
            $('#' + this.id + 0).focus()
          }
        }

        searchElem = (
          <div className="">
            <div className={"sp-input-group sp-sm"}>
              <input tabIndex={0} id={this.id} value={this.state.search_string} onChange={this.requestChange.bind(this)} type="text" className="sp-form-control" placeholder="Поиск" onKeyDown={onKeyDown}/>
            </div>
          </div>
        )
      }

      return (
        <Dropdown
          ref="dropdownMenu"
          position="bottom right"
          closeOnEsc closeOnOutsideClick
          toggleOnClick
          activeClassName="open"
          connectionPortal={this.state.connectionPortal}
          level={level}
          onKeyDown={(e)=>{
            if (e.keyCode == 27) {
              $('#' + this.id + 'i').focus()
            }
          }}
          onShow={
            ()=>{
              if (this.props.search == 'none') {
                if (this.state.parsed_data.length > 0) $('#' + this.id + 0).focus()
              } else {
                $('#' + this.id).focus()
              }
            }
          }
          element={this.getElement.call(this)}
          >
          <div className="sp-dropdown2-menu pull-left min-width-3 width-full">
            {searchElem}
            <div className="scrollable-menu max-height-4">
              {rows}
            </div>
          </div>
        </Dropdown>
      );
    }
  }
}
