import * as React from 'react';
import * as moment from 'moment';

import FontAwesome = require('react-fontawesome')

import Helper from './Helper';
import { IValueLink } from '../Main';
import { Popup } from '../Controls/Popup';
import { Calendar } from './Calendar';
import { ConnectionPortal } from './Portal'

export class MomentPeriod {
  start?: moment.Moment
  end?: moment.Moment
}

export class PeriodPickerProps {
  valueLink?: IValueLink<MomentPeriod> = {
    value: {
      start: null,
      end: null,
    },
    requestChange: (newValue) => {
    }
  }
  render?: (value: MomentPeriod, clear: () => void, format: string) => JSX.Element
  className?: string
  weeksPerMonth?: number
  format?: string = "YYYY-MM-DD"
  weeks?: string[] = ['Пн.', 'Вт.', 'Ср.', 'Чт.', 'Пт.', 'Сб.', 'Вс.']
  locale?: string = 'ru'
  min?: moment.Moment
  max?: moment.Moment
}

export class PeriodPickerState {
  connectionPortal: ConnectionPortal = {}
}

export class PeriodPicker extends React.Component<PeriodPickerProps, PeriodPickerState> {

  public static defaultProps = new PeriodPickerProps()

  state = new PeriodPickerState()

  refs: {
    [key: string]: any;
    calendarStart: Calendar;
    calendarEnd: Calendar;
  }

  clear() {
    let self = this;

    self.props.valueLink.value.start = null
    self.props.valueLink.value.end = null
    self.props.valueLink.requestChange(self.props.valueLink.value)
  }

  getElement() {
    let self = this

    if (self.props.render) {
      return self.props.render(self.props.valueLink.value, self.clear, self.props.format)
    }
    else {
      var selected = "Не выбрано"

      if (self.props.valueLink.value.start && self.props.valueLink.value.end) {
        selected = "с " + self.props.valueLink.value.start.format(self.props.format) + " по " + self.props.valueLink.value.end.format(self.props.format)
      }
      else if (self.props.valueLink.value.start && !self.props.valueLink.value.end) {
        selected = "с " + self.props.valueLink.value.start.format(self.props.format)
      }
      else if (!self.props.valueLink.value.start && self.props.valueLink.value.end) {
        selected = "по " + self.props.valueLink.value.end.format(self.props.format)
      }

      return (
        <div tabIndex={0} className={"sp-input-group sp-dropdown2-item " + self.props.className}>
          <div className="sp-input-group-label width-full">
            {selected}
          </div>
          <div className="sp-input-group-btn sp-as-addon square sp-dropdown2-on-open" onClick={self.clear}>
            <FontAwesome name={self.props.valueLink.value ? "remove" : "chevron-up"}/>
          </div>
          <div className="sp-input-group-addon square sp-dropdown2-on-not-open">
            <FontAwesome name={"chevron-down"}/>
          </div>
        </div>
      )
    }
  }

  render() {
    let self = this

    const { valueLink, weeksPerMonth, format, weeks, locale, min, max } = this.props

    const propsStart = {
      weeksPerMonth,
      format,
      weeks,
      locale,
      min,
      max: self.props.valueLink.value.end
    }

    const propsEnd = {
      weeksPerMonth,
      format,
      weeks,
      locale,
      min: self.props.valueLink.value.start,
      max,
    }

    return (
      <Popup
        ref="dropdownMenu"
        position="bottom right"
        positionEl="start"
        className="sp-auto"
        closeOnEsc closeOnOutsideClick
        toggleOnClick
        activeClassName="open"
        connectionPortal={this.state.connectionPortal}
        onShow={
          ()=>{
            self.refs.calendarStart.focus()
          }
        }
        element={self.getElement()}
        >
        <div className="sp-table-layout min-width-3 width-full padding-px3">
          <div>
            <div style={{borderRight: "solid 1px #d9ecf2"}}>
              <Calendar ref="calendarStart" valueLink={
                  {
                    value: self.props.valueLink.value.start,
                    requestChange: function(newValue) {
                      self.props.valueLink.value.start = newValue
                      self.props.valueLink.requestChange(self.props.valueLink.value)
                    }
                  }
                }
                {...propsStart}/>
            </div>
            <div>
              <Calendar ref="calendarEnd" valueLink={
                  {
                    value: self.props.valueLink.value.end,
                    requestChange: function(newValue) {
                      self.props.valueLink.value.end = newValue
                      self.props.valueLink.requestChange(self.props.valueLink.value)
                    }
                  }
                }
                {...propsEnd}/>
            </div>
          </div>
        </div>
      </Popup>
    );
  }
}
