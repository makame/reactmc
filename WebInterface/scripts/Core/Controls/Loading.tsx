import * as React from 'react'
import * as ReactDOM from 'react-dom';

import { Portal, PortalProps } from './Portal';

export class LoadingProps extends PortalProps {
  level?: number = -1
}

export class Loading extends React.Component<LoadingProps, {}> {

  public static defaultProps = new LoadingProps()

  refs: {
    [key: string]: any;
    portal: Portal;
  }

  closePortal() {
    this.refs.portal.closePortal();
  }

  openPortal() {
    this.refs.portal.openPortal();
  }

  componentDidMount() {
    this.openPortal();
  }

  componentWillUnmount() {
    this.closePortal();
  }

  render() {
    const { className } = this.props

    const {
      level,
      closeOnEsc,
      isOpened,
      isHidden,
      isGhost,
      connectionPortal,
      onElementClick,
      onElementOver,
      onElementContextMenu,
      onElementOut,
      onElementFocus,
      onElementBlur,
      onKeyDown,
      onOpen,
      onUpdate,
      didUpdate,
      beforeClose,
      onClose,
      isModal,
      style,
      element,
      activeClassName 
    } = this.props

    const props = {
      level,
      closeOnEsc,
      isOpened,
      isHidden,
      isGhost,
      connectionPortal,
      onElementClick,
      onElementOver,
      onElementContextMenu,
      onElementOut,
      onElementFocus,
      onElementBlur,
      onKeyDown,
      onOpen,
      onUpdate,
      didUpdate,
      beforeClose,
      onClose,
      isModal,
      style,
      element,
      activeClassName
    }

    return (
      <Portal ref="portal" {...props} className="loading-screen" isModal={true}
        onOpen={(node)=>{
          $(node).velocity({ opacity: [1.0, 0] }, { duration: 200 });
        }}
        beforeClose={(node, callback)=>{
          $(node).velocity({ opacity: 0.0 }, { duration: 200, complete: callback});
        }}>
        <div className="content">
          <div>[Загрузка данных]</div>
          <div className="loader"></div>
        </div>
      </Portal>
    )
  }
}
