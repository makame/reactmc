"use strict";
const React = require('react');
const Anim_1 = require('./Anim');
class StdAnimProps {
    constructor() {
        this.duration = 300;
    }
}
exports.StdAnimProps = StdAnimProps;
class StdAnim extends React.Component {
    render() {
        const self = this;
        const { duration, type, display, children, className } = this.props;
        return (React.createElement(Anim_1.Anim, {className: className, willEnter: function (el, callback) {
            el.hide();
            if (type == "fade") {
                $(el).velocity({ opacity: 1.0 }, { display: display ? display : "block", duration: duration, complete: callback });
            }
            else if (type == "fadeRScaleX") {
                $(el).velocity({ opacity: 0.5, scaleX: 0.9 }, { display: display ? display : "block", duration: duration / 2 });
                $(el).velocity({ opacity: 1.0, scaleX: 1 }, { duration: duration / 2, complete: callback });
            }
            else if (type == "fadeScaleX") {
                $(el).velocity({ opacity: 0.5, scaleX: 1.15 }, { display: display ? display : "block", duration: duration / 2 });
                $(el).velocity({ opacity: 1.0, scaleX: 1 }, { duration: duration / 2, complete: callback });
            }
            else if (type == "tags") {
                $(el).velocity({ opacity: 0.5, scale: 1.15 }, { display: display ? display : "inline-block", duration: duration / 2 });
                $(el).velocity({ opacity: 1.0, scale: 1 }, { duration: duration / 2, complete: callback });
            }
            else {
                $(el).velocity({ opacity: 0.5, scale: 0.9 }, { display: display ? display : "block", duration: duration / 2 });
                $(el).velocity({ opacity: 1.0, scale: 1 }, { duration: duration / 2, complete: callback });
            }
        }, willAppear: function (el, callback) {
            el.hide();
            if (type == "fade") {
                $(el).velocity({ opacity: 1.0 }, { display: display ? display : "block", duration: duration, complete: callback });
            }
            else if (type == "fadeRScaleX") {
                $(el).velocity({ opacity: 0.5, scaleX: 0.9 }, { display: display ? display : "block", duration: duration / 2 });
                $(el).velocity({ opacity: 1.0, scaleX: 1 }, { duration: duration / 2, complete: callback });
            }
            else if (type == "fadeScaleX") {
                $(el).velocity({ opacity: 0.5, scaleX: 1.15 }, { display: display ? display : "block", duration: duration / 2 });
                $(el).velocity({ opacity: 1.0, scaleX: 1 }, { duration: duration / 2, complete: callback });
            }
            else if (type == "tags") {
                $(el).velocity({ opacity: 0.5, scale: 1.15 }, { display: display ? display : "inline-block", duration: duration / 2 });
                $(el).velocity({ opacity: 1.0, scale: 1 }, { duration: duration / 2, complete: callback });
            }
            else {
                $(el).velocity({ opacity: 0.5, scale: 0.9 }, { display: display ? display : "block", duration: duration / 2 });
                $(el).velocity({ opacity: 1.0, scale: 1 }, { duration: duration / 2, complete: callback });
            }
        }, willLeave: function (el, callback) {
            if (type == "tags") {
                $(el).velocity({ opacity: 0 }, { display: "none", duration: duration, complete: callback });
            }
            else if (type == "spec") {
                $(el).velocity({ opacity: 0, height: 0, maxHeight: 0, padding: 0, margin: 0 }, { display: "none", duration: duration, complete: callback });
            }
            else {
                $(el).velocity({ opacity: 0 }, { display: "none", duration: duration, complete: callback });
            }
        }, willUnmount: function (el) {
            $(el).velocity("stop");
        }}, children));
    }
}
StdAnim.defaultProps = new StdAnimProps();
exports.StdAnim = StdAnim;
//# sourceMappingURL=StdAnim.js.map