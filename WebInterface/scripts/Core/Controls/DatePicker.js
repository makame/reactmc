"use strict";
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
const React = require('react');
const Popup_1 = require('../Controls/Popup');
const Calendar_1 = require('./Calendar');
const FontAwesome = require('react-fontawesome');
class DatePickerProps {
    constructor() {
        this.valueLink = {
            value: null,
            requestChange: (newValue) => {
            }
        };
        this.format = "YYYY-MM-DD";
        this.weeks = ['Пн.', 'Вт.', 'Ср.', 'Чт.', 'Пт.', 'Сб.', 'Вс.'];
        this.locale = 'ru';
    }
}
exports.DatePickerProps = DatePickerProps;
class DatePickerState {
    constructor() {
        this.connectionPortal = {};
    }
}
exports.DatePickerState = DatePickerState;
class DatePicker extends React.Component {
    constructor() {
        super(...arguments);
        this.state = new DatePickerState();
    }
    clear() {
        let self = this;
        self.props.valueLink.requestChange(null);
    }
    getElement() {
        let self = this;
        if (self.props.render) {
            return self.props.render(self.props.valueLink.value, self.clear, self.props.format);
        }
        else {
            var selected = "Не выбрано";
            if (self.props.valueLink.value) {
                selected = self.props.valueLink.value.format(self.props.format);
            }
            return (React.createElement("div", {tabIndex: 0, className: "sp-input-group sp-dropdown2-item " + self.props.className}, 
                React.createElement("div", {className: "sp-input-group-label width-full"}, selected), 
                React.createElement("div", {className: "sp-input-group-btn sp-as-addon square sp-dropdown2-on-open", onClick: self.clear}, 
                    React.createElement(FontAwesome, {name: self.props.valueLink.value ? "remove" : "chevron-up"})
                ), 
                React.createElement("div", {className: "sp-input-group-addon square sp-dropdown2-on-not-open"}, 
                    React.createElement(FontAwesome, {name: "chevron-down"})
                )));
        }
    }
    render() {
        let self = this;
        const { valueLink, weeksPerMonth, format, weeks, locale, min, max, } = this.props;
        const props = {
            valueLink,
            weeksPerMonth,
            format,
            weeks,
            locale,
            min,
            max
        };
        return (React.createElement(Popup_1.Popup, {ref: "dropdownMenu", position: "bottom right", positionEl: "start", closeOnEsc: true, closeOnOutsideClick: true, toggleOnClick: true, activeClassName: "open", connectionPortal: this.state.connectionPortal, onShow: () => {
            self.refs.calendar.focus();
        }, element: self.getElement()}, 
            React.createElement("div", {className: "sp-dropdown2-menu pull-left min-width-3 width-full padding-px3"}, 
                React.createElement(Calendar_1.Calendar, __assign({ref: "calendar"}, props))
            )
        ));
    }
}
DatePicker.defaultProps = new DatePickerProps();
exports.DatePicker = DatePicker;
//# sourceMappingURL=DatePicker.js.map