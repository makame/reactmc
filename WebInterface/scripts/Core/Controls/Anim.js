"use strict";
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
const React = require('react');
const ReactDOM = require('react-dom');
const ReactTransitionGroup = require('react-addons-transition-group');
class AnimChildProps {
}
exports.AnimChildProps = AnimChildProps;
class AnimChildState {
}
exports.AnimChildState = AnimChildState;
class AnimChild extends React.Component {
    componentWillAppear(callback) {
        if (this.props.willAppear) {
            var el = $(ReactDOM.findDOMNode(this));
            this.props.willAppear(el, callback);
        }
    }
    componentWillUnmount() {
        if (this.props.willUnmount) {
            var el = $(ReactDOM.findDOMNode(this));
            this.props.willUnmount(el);
        }
    }
    componentDidAppear() {
        if (this.props.didAppear) {
            var el = $(ReactDOM.findDOMNode(this));
            this.props.didAppear(el);
        }
    }
    componentWillEnter(callback) {
        if (this.props.willEnter) {
            var el = $(ReactDOM.findDOMNode(this));
            this.props.willEnter(el, callback);
        }
    }
    componentDidEnter() {
        if (this.props.didEnter) {
            var el = $(ReactDOM.findDOMNode(this));
            this.props.didEnter(el);
        }
    }
    componentWillLeave(callback) {
        if (this.props.willLeave) {
            var el = $(ReactDOM.findDOMNode(this));
            this.props.willLeave(el, callback);
        }
    }
    componentDidLeave() {
        if (this.props.didLeave) {
            var el = $(ReactDOM.findDOMNode(this));
            this.props.didLeave(el);
        }
    }
    render() {
        if (this.props.children) {
            return React.Children.only(this.props.children);
        }
        else {
            return null;
        }
    }
}
exports.AnimChild = AnimChild;
class AnimProps {
}
exports.AnimProps = AnimProps;
class AnimState {
}
exports.AnimState = AnimState;
class Anim extends React.Component {
    render() {
        var childCount = React.Children.count(this.props.children);
        var children = React.Children.map(this.props.children, function (child, idx) {
            return (React.createElement(AnimChild, __assign({}, this.props), child));
        }.bind(this));
        return (React.createElement(ReactTransitionGroup, {className: this.props.className}, children));
    }
}
exports.Anim = Anim;
//# sourceMappingURL=Anim.js.map