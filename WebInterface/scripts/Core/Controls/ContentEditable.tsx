import * as React from 'react';
import * as ReactDOM from 'react-dom';

export class ContentEditableProps {
  id?: string
  html?: string
  tagName?: string = "span"
  title?: string
  useTitle?: boolean
  disabled?: boolean
  className?: string

  onKeyDown?: (e: any) => void
  onKeyUp?: (e: any) => void
  
  onInput?: (e: any) => void
  onFocus?: (e: any) => void
  onBlur?: (e: any) => void
  onChange?: (e: any) => void
}

export class ContentEditableState {
  focused: boolean = false
  htmlEl?: Element
  rp?: () => void
  lastHtml?: string
}

export class ContentEditable extends React.Component<ContentEditableProps, ContentEditableState> {

  public static defaultProps = new ContentEditableProps()

  state = new ContentEditableState()

  shouldComponentUpdate(nextProps, nextState) {
    return !this.state.htmlEl || this.state.htmlEl.innerHTML == '' || nextProps.html !== this.state.htmlEl.innerHTML ||
    this.props.disabled !== nextProps.disabled || nextState.focused !== this.state.focused || nextProps.title !== this.props.title || nextProps.className != this.props.className;
  }

  componentWillUpdate() {
    if (this.state.focused) {
      try {
        this.state.rp = this.saveCaretPosition(this.state.htmlEl)
      }
      catch (error) {}
    }
    else {
      this.state.rp = undefined
    }
  }

  componentDidUpdate() {
    if (this.state.htmlEl && (this.props.html !== this.state.htmlEl.innerHTML || this.state.htmlEl.innerHTML == '' || this.state.htmlEl.innerHTML == '<br>')) {
      this.state.htmlEl.innerHTML = !this.state.focused && this.props.useTitle ? this.props.title : (this.props.html.length > 0 ? this.props.html : ' ');
    }
    if (this.state.focused && this.state.rp) {
      this.state.rp()
    }
  }

  onInput(evt) {
    if (this.props.onInput) {
      this.props.onInput(evt)
    }
    this.emitChange(evt)

    this.forceUpdate()
  }

  onFocus(evt) {
    if (this.props.onFocus) {
      this.props.onFocus(evt)
    }

    this.state.focused = true
    this.forceUpdate()
  }

  onBlur(evt) {
    if (this.props.onBlur) {
      this.props.onBlur(evt)
    }
    this.emitChange(evt)

    this.state.focused = false
    this.forceUpdate()
  }

  emitChange(evt) {
    if (!this.state.htmlEl) return;

    var html = this.state.htmlEl.innerHTML;

    if (this.props.onChange && html !== this.state.lastHtml && !(!this.state.focused && this.props.useTitle)) {
      evt.target = { value: html };
      this.props.onChange(evt);
    }

    this.state.lastHtml = html;
  }

  saveCaretPosition(context) {
    var selection = window.getSelection();
    var range = selection.getRangeAt(0);
    range.setStart(context, 0);
    var len = range.toString().length;

    return () => {
      var pos = this.getTextNodeAtPosition(context, len);
      selection.removeAllRanges();
      var range = new Range();
      range.setStart(pos.node, pos.position);
      selection.addRange(range);
    }
  }

  getTextNodeAtPosition(root, index) {
    var lastNode = null;

    var treeWalker = document.createTreeWalker(root, NodeFilter.SHOW_TEXT, {
      acceptNode: (elem: HTMLElement): number => {
        if(index > elem.textContent.length){
          index -= elem.textContent.length;
          lastNode = elem;
          return NodeFilter.FILTER_REJECT
        }
        return NodeFilter.FILTER_ACCEPT;
      }
    });

    var c = treeWalker.nextNode();
    
    return {
      node: c? c: root,
      position: c? index:  0
    };
  }

  render() {
    var self = this

    const { children, id, html, useTitle, title, tagName } = this.props

    return React.createElement(
      tagName,
      {
        id,
        ref: (e) => this.state.htmlEl = e,
        onInput: this.onInput.bind(this),
        onBlur: this.onBlur.bind(this),
        className: this.props.className,
        onFocus: this.onFocus.bind(this),
        contentEditable: !this.props.disabled,
        dangerouslySetInnerHTML: {__html: !this.state.focused && this.props.useTitle ? this.props.title : (this.props.html.length > 0 ? this.props.html : ' ')}
      },
      children
    );
  }
}
