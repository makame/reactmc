"use strict";
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
const React = require('react');
const ReactDOM = require('react-dom');
const Portal_1 = require('./Portal');
class ModalProps extends Portal_1.PortalProps {
    constructor() {
        super(...arguments);
        this.level = -1;
    }
}
exports.ModalProps = ModalProps;
class Modal extends React.Component {
    closePortal() {
        this.refs.portal.closePortal();
    }
    openPortal() {
        this.refs.portal.openPortal();
    }
    componentDidMount() {
        this.openPortal();
    }
    componentWillUnmount() {
        this.closePortal();
    }
    handleOutsideClick(e) {
        if (this.props.closeOnOutsideClick && this.refs.portal.state.active) {
            e.preventDefault();
            e.stopPropagation();
            this.refs.portal.closePortal();
        }
        else if (this.props.onOutsideClick) {
            if ($(e.target).hasClass('sp-modal-back') || !$(e.target).find(ReactDOM.findDOMNode(this))) {
                this.props.onOutsideClick(e);
            }
        }
    }
    render() {
        const { children, className, onOutsideClick, closeOnOutsideClick, dialog, message, bottom } = this.props;
        const { level, closeOnEsc, isOpened, isHidden, isGhost, connectionPortal, onElementClick, onElementOver, onElementContextMenu, onElementOut, onElementFocus, onElementBlur, onKeyDown, onOpen, onUpdate, didUpdate, beforeClose, onClose, isModal, style, element, activeClassName } = this.props;
        const props = {
            level,
            closeOnEsc,
            isOpened,
            isHidden,
            isGhost,
            connectionPortal,
            onElementClick,
            onElementOver,
            onElementContextMenu,
            onElementOut,
            onElementFocus,
            onElementBlur,
            onKeyDown,
            onOpen,
            onUpdate,
            didUpdate,
            beforeClose,
            onClose,
            isModal,
            style,
            element,
            activeClassName
        };
        var messageEl = null;
        if (message) {
            messageEl = (React.createElement("div", {className: "sp-message"}, message));
        }
        var bottomEl = null;
        if (bottom) {
            bottomEl = (React.createElement("div", {className: "sp-bottom"}, bottom));
        }
        return (React.createElement(Portal_1.Portal, __assign({ref: "portal"}, props, {className: "sp-modal-back", isModal: true, onOpen: (node) => {
            $(node).velocity({ opacity: 1.0 }, { display: 'block', duration: 200 });
            $(node).children().velocity({ opacity: [1.0, 0], translateX: ["-50%", "-50%"], translateY: [0, "-4rem"], }, { display: 'block', duration: 200 });
        }, beforeClose: (node, callback) => {
            $(node).velocity({ opacity: 0.0 }, { duration: 200, display: 'none', complete: callback });
        }, onOutsideClick: closeOnOutsideClick || onOutsideClick ? this.handleOutsideClick.bind(this) : null}), 
            React.createElement("div", {className: "sp-dialog"}, 
                React.createElement("div", {className: "sp-modal" + (dialog ? ' sp-quiz' : '') + (className ? (' ' + className) : '')}, 
                    this.props.children, 
                    messageEl, 
                    bottomEl)
            )
        ));
    }
}
Modal.defaultProps = new ModalProps();
exports.Modal = Modal;
//# sourceMappingURL=Modal.js.map