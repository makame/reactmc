"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const React = require('react');
const react_router_1 = require('react-router');
const history_1 = require('history');
const Modal_1 = require('./Controls/Modal');
const Loading_1 = require('./Controls/Loading');
class Map {
}
exports.Map = Map;
class MapPro {
    static map(map, iterator) {
        let keys = Object.keys(map);
        let result = [];
        keys.forEach((name, index) => {
            result.push(iterator(map[name], name, index));
        });
        return result;
    }
    static each(map, iterator) {
        let keys = Object.keys(map);
        keys.forEach((name, index) => {
            iterator(map[name], name, index);
        });
    }
    static find(map, iterator) {
        let keys = Object.keys(map);
        let result;
        keys.forEach((name, index) => {
            if (iterator(map[name], name, index) && !result) {
                result = map[name];
            }
        });
        return result;
    }
    static findName(map, iterator) {
        let keys = Object.keys(map);
        let result;
        keys.forEach((name, index) => {
            if (iterator(map[name], name, index) && !result) {
                result = name;
            }
        });
        return result;
    }
    static filter(map, iterator) {
        let keys = Object.keys(map);
        let result = {};
        keys.forEach((name, index) => {
            if (iterator(map[name], name, index)) {
                result[name] = map[name];
            }
        });
        return result;
    }
}
exports.MapPro = MapPro;
class ContextModel {
}
exports.ContextModel = ContextModel;
class IContentModel extends ContextModel {
}
exports.IContentModel = IContentModel;
class ContentModel extends ContextModel {
    render(context, page, application, containerFactory) {
        return null;
    }
}
exports.ContentModel = ContentModel;
class Init {
    constructor(initializer) {
        if (initializer === undefined) {
            return;
        }
        Object.keys(initializer).forEach((key, index) => {
            this[key] = initializer[key];
        });
    }
}
exports.Init = Init;
class ContentModelInit extends ContentModel {
    constructor(initializer) {
        super();
        if (initializer === undefined) {
            return;
        }
        Object.keys(initializer).forEach((key, index) => {
            this[key] = initializer[key];
        });
    }
}
exports.ContentModelInit = ContentModelInit;
class ModalModel {
}
exports.ModalModel = ModalModel;
class RouteModel {
}
exports.RouteModel = RouteModel;
class ApplicationModel {
}
exports.ApplicationModel = ApplicationModel;
class ContainerAdapter {
}
exports.ContainerAdapter = ContainerAdapter;
class ComponentProps {
}
exports.ComponentProps = ComponentProps;
class Component extends React.Component {
    getValue(array, parent) {
        if (array.length > 1) {
            return this.getValue(array.slice(1), parent[array[0]]);
        }
        else if (array.length == 1) {
            return parent[array[0]];
        }
        else {
            throw new RangeError("Context haven't current SetGet");
        }
    }
    parseContainer(str, context) {
        return str.split('>').map((step1) => {
            let in_brackets_req = step1.match(/\((.*?)\)/);
            let in_brackets = in_brackets_req ? in_brackets_req[1] : '';
            let step2 = step1.replace(in_brackets, '').replace('(', '').replace(')', '').split('.');
            let type = step2[0];
            let classes = step2.length > 1 ? step2.slice(1) : [];
            let parameters = in_brackets.split(',').map((parameterRaw) => {
                let parameter = parameterRaw.trim();
                if (parameter.indexOf('$') == 0) {
                    let value = parameter.slice(1).split('.');
                    return this.getValue(value, context);
                }
                else if (parameter.indexOf("'") == 0) {
                    const pp = parameter.replace("'", '').replace("'", '');
                    const result = {
                        get: (context, page, application) => {
                            return pp;
                        }
                    };
                    return result;
                }
                else {
                    const result = {
                        get: (context, page, application) => {
                            return parameter;
                        }
                    };
                    return result;
                }
            });
            const adapter = { type, class: classes, parameters };
            return adapter;
        });
    }
    getContainer(model, content, context, page, application) {
        if (model.type) {
            return application.containers[model.type].render(model, content, context, page, application);
        }
        else
            return content;
    }
    createContainer(array, content, context, page, application) {
        if (array.length > 0) {
            return this.createContainer(array.slice(1), this.getContainer(array[0], content, context, page, application), context, page, application);
        }
        else {
            return content;
        }
    }
    containerFactory(container, inner, context, page, application) {
        let containerModel = this.parseContainer(container, context);
        return this.createContainer(containerModel.reverse(), inner, context, page, application);
    }
    componentWillReceiveProps(nextProps, nextContext) {
        const { content, page, application } = this.props;
        page.params = nextProps.params ? nextProps.params : (page.params ? page.params : {});
        page.query = nextProps.location && nextProps.location.query ? nextProps.location.query : (page.query ? page.query : {});
        page.location = nextProps.location ? nextProps.location : page.location;
    }
    init() {
        return __awaiter(this, void 0, void 0, function* () {
            const { context, page, application } = this.props;
            if (context.init && !context.inited && !context.loading) {
                context.loading = true;
                page.update();
                yield context.init(context, page, application);
                context.loading = false;
                context.inited = true;
                page.update();
            }
        });
    }
    componentWillMount() {
        const { content, context, page, application } = this.props;
        page.params = this.props.params ? this.props.params : (page.params ? page.params : {});
        page.query = this.props.location && this.props.location.query ? this.props.location.query : (page.query ? page.query : {});
        page.location = this.props.location ? this.props.location : page.location;
        this.init();
    }
    componentWillUpdate() {
        return __awaiter(this, void 0, void 0, function* () {
            this.init();
        });
    }
    render() {
        const { content, context, page, application } = this.props;
        let modals = content && content.modals ? MapPro.map(content.modals, (item, name) => {
            item.close = () => {
                if (item.before_close) {
                    item.before_close(() => {
                        delete content.modals[name];
                        page.update();
                    }, content, item, page, application);
                }
                else {
                    delete content.modals[name];
                    page.update();
                }
            };
            return (React.createElement(Modal_1.Modal, {key: name, dialog: item.dialog, className: item.class, message: item.message ? React.createElement(Component, { content: item.message, context: item, page: page, application }) : null, bottom: item.bottom ? React.createElement(Component, { content: item.bottom, context: item, page: page, application }) : null, onOutsideClick: (e) => {
                e.preventDefault();
                e.stopPropagation();
                if (item.on_outside) {
                    item.on_outside(content, item, page, application);
                }
                else {
                    item.close();
                }
            }}, React.createElement(Component, { content: item.content, context: item, page: page, application })));
        }) : null;
        let container = (element) => {
            let inner = (React.createElement("div", {className: "sp-module" + (content.loading ? ' loading' : '')}, 
                element, 
                modals, 
                React.createElement("div", {className: "message"}, 
                    React.createElement("div", {className: "loader"})
                )));
            if (content.container) {
                return this.containerFactory(content.container, inner, context, page, application);
            }
            else {
                return inner;
            }
        };
        return container(content.render(context, page, application, (contaner, inner, context) => {
            if (contaner) {
                return this.containerFactory(contaner, inner, context, page, application);
            }
            else {
                return inner;
            }
        }));
    }
}
Component.defaultProps = new ComponentProps();
exports.Component = Component;
class PageProps {
}
class Page extends React.Component {
    init() {
        return __awaiter(this, void 0, void 0, function* () {
            const { page, application } = this.props;
            if (page.init && !page.inited && !page.loading) {
                page.loading = true;
                page.update();
                yield page.init(page, application);
                page.loading = false;
                page.inited = true;
                page.update();
            }
        });
    }
    componentWillMount() {
        this.init();
    }
    componentWillUpdate() {
        this.init();
    }
    render() {
        const { children, page, application } = this.props;
        if (page.loading) {
            return (React.createElement("div", null, 
                React.createElement(Loading_1.Loading, null)
            ));
        }
        else if (page.page) {
            return page.page(page, application);
        }
        else {
            const condition = page.condition ? page.condition(page, application) : null;
            const content = condition ? condition : page.content;
            if (content) {
                return React.createElement(Component, { content, context: content, page, application }, children);
            }
            else {
                return React.createElement("div", null, children);
            }
        }
    }
}
Page.defaultProps = new PageProps();
class LayoutMask {
}
class LayoutProps {
}
class Layout extends React.Component {
    componentWillMount() {
        const { page } = this.mask;
        page.update = () => { this.setState({}); };
    }
    shouldComponentUpdate(nextProps, nextState) {
        const { page, application } = this.mask;
        if (page.check)
            return page.check(page, application) !== false;
        else
            return true;
    }
    render() {
        const { location } = this.props;
        const { page, application } = this.mask;
        const children = React.createElement(Page, { page, application }, this.props.children);
        if (page.layout) {
            return page.layout(page, application, location, children);
        }
        else {
            return React.createElement("div", null, children);
        }
    }
}
Layout.defaultProps = new LayoutProps();
class ApplicationProps {
}
exports.ApplicationProps = ApplicationProps;
class ApplicationState {
}
exports.ApplicationState = ApplicationState;
class Application extends React.Component {
    constructor() {
        super(...arguments);
        this.state = new ApplicationState();
    }
    parseRoute(route, name, application) {
        var routes = [];
        if (route.routes)
            MapPro.each(route.routes, (item, name) => {
                routes.push(this.parseRoute(item, name, application));
            });
        var index = null;
        if (route.index) {
            index = (React.createElement(react_router_1.IndexRedirect, {to: route.index}));
        }
        const params = { page: route, name, application };
        let component = class LayoutMask extends Layout {
            constructor() {
                super(...arguments);
                this.mask = {
                    page: route,
                    application: application
                };
            }
        }
        ;
        return (React.createElement(react_router_1.Route, {key: name, path: route.path, component: component}, 
            index, 
            routes));
    }
    init() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.props.application.init) {
                this.props.application.initing = true;
                yield this.props.application.init(this.props.application);
                this.props.application.initing = false;
                this.setState({});
            }
        });
    }
    componentWillMount() {
        let basename = this.props.application.basename ? this.props.application.basename(window.location) : window.location.origin;
        this.state.browserHistory = react_router_1.useRouterHistory(history_1.createHistory)({
            basename: basename
        });
        this.props.application.browserHistory = this.state.browserHistory;
        this.init();
    }
    render() {
        if (this.props.application.initing) {
            return (React.createElement(Loading_1.Loading, null));
        }
        else {
            var routes = [];
            MapPro.each(this.props.application.routes, (item, name) => {
                routes.push(this.parseRoute(item, name, this.props.application));
            });
            return (React.createElement(react_router_1.Router, {history: this.state.browserHistory, routes: routes}));
        }
    }
}
Application.defaultProps = new ApplicationProps();
exports.Application = Application;
//# sourceMappingURL=Main.js.map