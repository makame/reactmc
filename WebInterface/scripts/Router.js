"use strict";
const React = require('react');
const react_dom_1 = require('react-dom');
const Velocity = require('velocity-animate');
$.fn.velocity = Velocity;
const Core_1 = require('./Core');
const App_1 = require('./Model/App');
$(window).keydown(function (e) { if (e.keyCode == 123)
    debugger; });
react_dom_1.render(React.createElement(Core_1.Application, { application: App_1.application }), document.getElementById("app-container"));
$(window).load(function () {
    $('body #loading').children().velocity('fadeOut', { duration: 100, complete: function () { $('body #loading').velocity('fadeOut', { duration: 500, complete: function () { $('body #loading').remove(); } }); } });
});
if (typeof window['require'] !== "undefined") {
    let electron = window['require']("electron");
    let ipcRenderer = electron.ipcRenderer;
    console.log("ipc renderer", ipcRenderer);
    let remote = electron.remote;
    document.getElementById("min-btn").addEventListener("click", function (e) {
        let window = remote.getCurrentWindow();
        window.minimize();
    });
    document.getElementById("max-btn").addEventListener("click", function (e) {
        let window = remote.getCurrentWindow();
        if (!window.isMaximized()) {
            window.maximize();
        }
        else {
            window.unmaximize();
        }
    });
    document.getElementById("close-btn").addEventListener("click", function (e) {
        var window = remote.getCurrentWindow();
        window.close();
    });
}
//# sourceMappingURL=Router.js.map