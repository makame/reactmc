import { NavigationModel } from './Model/Navigation'
import { ApplicationModel, ContentModel, Map } from './Core'

export interface UserModel {
  username: string
  avatar_url?: string
  first_name?: string
  last_name?: string
  id: string
  notifications: ContentModel[]
}

export class SheriffModel extends ApplicationModel {
  auth: (application: SheriffModel) => Promise<void>
  user?: UserModel
  menu: Map<NavigationModel>
}