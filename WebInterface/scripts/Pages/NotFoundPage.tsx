import * as React from 'react'

import { RouteModel, ApplicationModel, LImage } from '../Core'

class NotFoundPage extends React.Component<{}, {}> {

  render() {
    return (
      <div className="container align-center padding-3">
        <h1 className="margin-bottom-d3">[404]</h1>
        <h2 className="margin-bottom-1">Page Not Found</h2>
        <div>You can try another route...</div>
        <div className="width-10 margin-top-d2">
          <LImage url={"test.jpg"}/>
        </div>
      </div>
    )
  }
}

export default (page: RouteModel, application: ApplicationModel, children?: JSX.Element): JSX.Element => {
  return (
    <NotFoundPage/>
  )
}