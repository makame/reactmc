"use strict";
const React = require('react');
const Core_1 = require('../Core');
class NotFoundPage extends React.Component {
    render() {
        return (React.createElement("div", {className: "container align-center padding-3"}, 
            React.createElement("h1", {className: "margin-bottom-d3"}, "[404]"), 
            React.createElement("h2", {className: "margin-bottom-1"}, "Page Not Found"), 
            React.createElement("div", null, "You can try another route..."), 
            React.createElement("div", {className: "width-10 margin-top-d2"}, 
                React.createElement(Core_1.LImage, {url: "test.jpg"})
            )));
    }
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = (page, application, children) => {
    return (React.createElement(NotFoundPage, null));
};
//# sourceMappingURL=NotFoundPage.js.map