import { FormItemInputModel, FormItemInputAddonTextModel, FormItemInputAddonButtonModel,
  FormItemSelectModel,
  FormItemMSelectModel,
  FormItemButtonsModel, FormItemButtonModel } from '../Extends/Form'
import { SheriffModel } from '../ISheriff'
import { ISetGet, FormModel } from '../Core'

export const AccordForm = new FormModel({
  container: "accord($value.title, $value.visible).margin-bottom-1>div.padding-top-1.cols",

  value: {
    title: <ISetGet> {
      get: (context, page, application) => {
        return context.value.name ? 'Новый элемент: ' + context.value.name : 'Создание нового элемента' + (page.params.id ? page.params.id : '')
      }
    },
    visible: <ISetGet> {
      value: true,
      get: (context, page, application) => {
        return context.value.visible.value
      },
      set: (value, context, page, application) => {
        context.value.visible.value = value
      }
    },
    name: '',
    select1: {
      id: 'test1',
      name: 'Test 1'
    },
    select3: [
      {
        id: 'test1',
        name: 'Test 1'
      },
      {
        id: 'test2',
        name: 'Test 2'
      },
      {
        id: 'test3',
        name: 'Test 3'
      },
    ],
  },

  form: {
    name: new FormItemInputModel({
      container: "labeled('Название')",

      right: {
        question: new FormItemInputAddonTextModel({
          class: 'square',
          text: '?',
        }),
        button: new FormItemInputAddonButtonModel({
          class: 'danger',
          text: 'Стереть',
        }),
      },
      class: 'white',
      icon: 'home',
      placeholder: 'Что-то полезное...',

      type: 'text',
      error: function(value, model, context, page, application: SheriffModel) {
        return !value || value.length == 0 ? 'Поле должно быть заполнено' : value.length < 2 ? 'Поле должно быть больше 2 знаков' : null;
      },
      warning: function(value, model, context, page, application: SheriffModel) {
        return !value || (value.length < 8 && value.length >= 2) ? 'Поле слишком простое' : null;
      },
      enabled: function(value, model, context, page, application: SheriffModel) {
        return true;
      },
    }),
    select1: new FormItemSelectModel({
      container: "labeled('Тип без поиска').padding-top-1",

      data_type: 'local',
      data: [
        {
          id: 'test1',
          name: 'Test 1'
        },
        {
          id: 'test2',
          name: 'Test 2'
        },
      ],
      search: 'none',
      error: function(value, form_data) {
        return value ? null : 'Поле должно быть выбрано';
      }
    }),
    select3: new FormItemMSelectModel({
      container: "labeled('Подтип').padding-top-1",

      data_type: 'remote',
      search: 'remote',
      request: function(search_string) {
        return new Promise(function (resolve, reject) {
          setTimeout(function () {
            resolve([
              {
                id: 'test1',
                name: 'Test 1'
              },
              {
                id: 'test2',
                name: 'Test 2'
              },
              {
                id: 'test3',
                name: 'Test 3'
              },
            ])
          }, 300)
        })
      },
      error: function(value, form_data) {
        return value.length > 0 ? null : 'Должно быть выбран больше 1го значения';
      },
    }),
    buttons: new FormItemButtonsModel({
      container: "div.col-xs-12.padding-top-2.padding-bottom-1",
      items: {
        first: new FormItemButtonModel({
          class: 'btn-primary square',
          icon: 'home',
        }),
        second: new FormItemButtonModel({
          class: 'square',
          text: '?',
        }),
        third: new FormItemButtonModel({
          class: 'btn-danger',
          text: 'Стереть',
        }),
        fourth: new FormItemButtonModel({
          class: 'btn-default',
          icon: 'home',
          text: 'Стереть',
        }),
      },
    }),
  },
})