import { NavigationModel, Navigation } from './Navigation'

// import Grid from './Grid'
// import TestGrid from './TestGrid'
// import TestInfo from './TestInfo'
// import TestAccord from './TestAccord'

import { ApplicationModel, ContextModel, ContentModel, Map, RouteModel, rest } from '../Core'

import { UserModel, SheriffModel } from '../ISheriff'

import { Login } from './Login'

const login: RouteModel = {
  path: 'login',
  check: (page, application: SheriffModel) => {
    if (application.user) {
      application.browserHistory.push('/')
      return false
    }
  },
  content: Login,
}

import { Restore } from './Restore'

const restore: RouteModel = {
  path: 'restore',
  check: (page, application: SheriffModel) => {
    if (application.user) {
      application.browserHistory.push('/')
      return false
    }
  },
  content: Restore,
}

import { TestForm } from './TestForm'

const form: RouteModel = {
  path: 'form',
  content: TestForm,
}

import { TabsModel, TabModel } from '../Core/Components/Tabs'

const tabs: RouteModel = {
  path: 'tabs',
  content: new TabsModel({
    container: "div.padding-horizontal-2.padding-bottom-2",
    active_tab: 'form',
    tabs: {
      form: new TabModel({
        label: 'Form',
        content: TestForm
      }),
      form2: new TabModel({
        label: 'Form Alt',
        content: TestForm
      }),
    }
  }),
}

import { ArrayModel } from '../Core/Components/Array'
import { AccordForm } from './AccordForm'

const accord: RouteModel = {
  path: 'accord',
  content: new ArrayModel({
    container: "div.padding-horizontal-2.padding-bottom-2",

    items: {
      grid: AccordForm,
      two: AccordForm,
      three: AccordForm,
    }
  }),
}

import {
  ModalModel,
  MenuModel,
  GridModel, IGridValue, IGridRow,
  TextModel,
  ButtonsModel
} from '../Core'
import { MenuItemHeaderModel, MenuItemDividerModel, MenuItemInputModel, MenuItemSpaceModel, MenuItemButtonModel, MenuItemDateModel, MenuItemEnumModel, MenuItemEnumModelItem } from '../Extends/Menu'
import { GridItemTextModel } from '../Extends/Grid'

const help: RouteModel = {
  path: 'help',
  content: new ArrayModel({
    container: "div.padding-horizontal-2.padding-bottom-2",

    items: {
      menu: new MenuModel({
        items: {
          header: new MenuItemHeaderModel({
            text: 'fsdfs',
          }),
          divider0: new MenuItemDividerModel(),
          date: new MenuItemDateModel({
            icon: 'calendar',
            value: null,
            get: (content: MenuModel, context, page, application) => {
              return (content.items['date'] as MenuItemDateModel).value
            },
            set: (newValue, content: MenuModel, context, page, application) => {
              (content.items['date'] as MenuItemDateModel).value = newValue
              page.update()
            },
          }),
          input: new MenuItemInputModel({
            icon: 'search',
            value: 'fsdfs',
            placeholder: 'Поиск'
          }),
          enum: new MenuItemEnumModel({
            get: (item: MenuItemEnumModel, content: MenuModel, context: ContentModel, page: RouteModel, application: ApplicationModel): MenuItemEnumModelItem => {
              const grid = ((page.content as ArrayModel).items['grid'] as GridModel)
              const value = grid.value.per_page
              const en: { id: any, name: string } = item.enums.find(s => s.id == value)
              
              return en ? en : {
                id: value,
                name: value.toString()
              }
            },
            set: (item: MenuItemEnumModel, newValue: MenuItemEnumModelItem, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): void => {
              const grid = ((page.content as ArrayModel).items['grid'] as GridModel)
              grid.value.per_page = newValue.id
              grid.value.need_update = true
              page.update()
            },
            enums: [
              {
                id: 25,
                name: '25'
              },
              {
                id: 50,
                name: '50'
              },
              {
                id: 250,
                name: '250'
              },
              {
                id: 1000,
                name: '1k'
              },
              {
                id: null,
                name: 'Все'
              },
            ]
          }),
          space: new MenuItemSpaceModel(),
          button: new MenuItemButtonModel({
            label: 'Demo Buton',
            on_click: async (context, page, application) => {
              await new Promise(function (resolve, reject) {
                setTimeout(() => {
                  resolve()
                }, 2000)
              })
            },
          }),
        },
      }),
      grid: new GridModel({
        container: "div.padding-horizontal-2.padding-top-2.padding-bottom-2",

        value: {
          sort: [],
          page: 0,
          per_page: null,
          total: 0
        },

        grid: {
          request: async (value: IGridValue, context: GridModel, page: RouteModel, application: ApplicationModel) => {
            context.loading = true
            page.update()

            const result = await rest.enter().one('test.json').get().do()

            context.loading = false
            page.update()

            return {
              total: result.FCT.fund.length,
              data: result.FCT.fund
            }
          },
          on_click: (row: IGridRow, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => {
            let ask: ModalModel = {
              dialog: true,
              on_outside: (content: ContentModel, item: ModalModel, page: RouteModel, application: ApplicationModel) => {
                ask.close()
              },
              content: new TextModel({
                container: "div.text.medium.padding-horizontal-2.padding-vertical-d2",
                value: 'Закрытие окна'
              }),
              message: new TextModel({
                container: "div.padding-horizontal-2.padding-vertical-d2",
                value: 'Подтвердите закрытие окна!'
              }),
              bottom: new ButtonsModel({
                container: "div.padding-horizontal-2.padding-vertical-d2",

                buttons: {
                  cancel: {
                    class: 'btn-primary',
                    text: 'Отмена',
                    on_click: async (context: ContextModel, page: RouteModel, application: ApplicationModel) => {
                      ask.close()
                    },
                  },
                  ok: {
                    class: 'btn-danger margin-left-1',
                    text: 'Закрыть',
                    on_click: async (context: ContextModel, page: RouteModel, application: ApplicationModel) => {
                      ask.close()
                      modal.close()
                    },
                  },
                }
              })
            }

            let modal: ModalModel = {
              on_outside: (content_: ContentModel, modal: ModalModel, page: RouteModel, application: ApplicationModel) => {
                content.modals['ask'] = ask
                page.update()
              },
              content: new ArrayModel({
                container: "div.text.medium.padding-horizontal-2.padding-top-2",

                items: {
                  header: new TextModel({
                    value: 'Hello!'
                  }),
                  buttons: new ButtonsModel({
                    container: "div.padding-top-2.padding-bottom-d2",

                    buttons: {
                      restore: {
                        class: 'btn-danger',
                        text: 'Закрыть',
                        on_click: async (context: ContextModel, page: RouteModel, application: ApplicationModel) => {
                          content.modals['ask'] = ask
                        },
                      },
                    }
                  })
                }
              })
            }

            content.modals = {}

            content.modals['modal_test'] = modal
          },
          sort: ['+id'],
          sortNull: [],
          data_type: 'remote_local',
          pagination: true,
          big_data: true,
          fields: {
            FamilyName: new GridItemTextModel({
              label: 'Family Name',
              filter: ['contains', 'totaly', 'not_in'],
              sortable: true,
              placeholder: 'Family Name'
            }),
            LastReturn: new GridItemTextModel({
              label: 'Last Return',
              filter: ['contains', 'totaly', 'not_in'],
              sortable: true,
              placeholder: 'Last Return'
            }),
            Name: new GridItemTextModel({
              label: 'Name',
              filter: ['contains', 'totaly', 'not_in'],
              sortable: true,
              placeholder: 'Name'
            }),
            Ticker: new GridItemTextModel({
              label: 'Ticker',
              filter: ['contains', 'totaly', 'not_in'],
              sortable: true,
              placeholder: 'Ticker'
            }),
          },
          allow_add: (context: ContextModel, page: RouteModel, application: ApplicationModel): boolean => true,
          allow_edit: (row: IGridRow, context: ContextModel, page: RouteModel, application: ApplicationModel): boolean => true,
          selectable: true,
          create_context_menu: {
            save: {
              text: 'Сохранить',
              on_click: (row: IGridRow, context: ContextModel, page: RouteModel, application: ApplicationModel): Promise<void> | void => {

              }
            }
          }
        }
      }),
    }
  }),
}

import DefaultRender from '../Layouts/Default'
import NotFoundPage from '../Pages/NotFoundPage'

const sheriff: RouteModel = {
  path: '/',
  index: 'help',
  layout: DefaultRender,
  routes: <Map<RouteModel>> {
    help: help,
    form: form,
    tabs: tabs,
    accord: accord,
    not_found: <RouteModel> {
      path: '*',
      page: NotFoundPage,
    }
  }
}

const secured: RouteModel = {
  path: '',
  condition: (page: RouteModel, application: SheriffModel): ContentModel => {
    if (!application.user) {
      return Login
    }
  },
  routes: {
    sheriff: sheriff
  }
}

import { containers } from '../Containers';

import * as api from './api';

export const application: SheriffModel = {
  containers: containers,

  basename: (location) => {
    return location.pathname.indexOf('.html') >= 0 ? location.pathname.substring(0, location.pathname.indexOf('.html') + 5) : location.origin
  },

  // user: {}, // Look below

  auth: async (application) => {

      // const client = new api.Client();
      // const b = await client.apiArcherGetEmployeesPost();
      // console.log(b);
      //var args = {};
      //soap.createClient(url, {}, function (err, client) {
      //    console.log((client as any).describe() as any)
      //    //(client as any).ping(args, function (err, result) {
      //    //    console.log(result);
      //    //});
      //});

    //return new Promise<void>(function (resolve, reject) {
    //  setTimeout(function () {
        application.user = {
          id: 'sdfsdf',
          avatar_url: rest.getDir() + 'images/avatar-empty.png',
          username: 'makame',
          notifications: [],
        }
      //  resolve()
      //}, 1000)
    //})
  },

  init: async (application: SheriffModel) => {
    await application.auth(application)
  },

  menu: Navigation,

  routes: <Map<RouteModel>> {
    login: login,
    restore: restore,
    secured: secured
  }
}


// help: {
//   path: 'help(/:ticker)',
//   init: (context, page) => {
//     return new Promise(function (resolve, reject) {
//       setTimeout(function () {
//         resolve()
//       }, 1000)
//     })
//   },
//   content: {
//     type: 'array',
//     array: {
//       menu: {
//         type: 'menu',
//         content: {
//           header: {
//             type: 'header',
//             text: 'fsdfs',
//           },
//           divider0: {
//             type: 'divider',
//           },
//           date: {
//             type: 'date',
//             icon: 'calendar',
//             value: null,
//             get: (content, context, page, application) => {
//               return page.date
//             },
//             set: (newValue, content, context, page, application) => {
//               page.date = newValue
//               page.update()
//             },
//           },
//           input: {
//             type: 'input',
//             icon: 'search',
//             value: 'fsdfs',
//             placeholder: 'Поиск'
//           },
//           enum: {
//             type: 'enum',
//             get: (content, context, page, application) => {
//               const value = page.content.array.grid.value.per_page
//               return {
//                 id: value,
//                 name: value
//               }
//             },
//             set: (newValue, content, context, page, application) => {
//               page.content.array.grid.value.per_page = newValue.id
//               page.content.array.grid.value.need_update = true
//               page.update()
//             },
//             enums: [
//               {
//                 id: 25,
//                 name: '25'
//               },
//               {
//                 id: 50,
//                 name: '50'
//               },
//               {
//                 id: 100,
//                 name: '100'
//               },
//               {
//                 id: 1000,
//                 name: '1k'
//               },
//               {
//                 id: null,
//                 name: 'Все'
//               },
//             ]
//           },
//           space: {
//             type: 'space',
//           },
//           button: {
//             type: 'button',
//             label: 'Demo Buton',
//             on_click: async (context, page, application) => {
//               await new Promise(function (resolve, reject) {
//                 setTimeout(() => {
//                   resolve()
//                 }, 2000)
//               })
//             },
//           },
//         },
//       },
//       grid: Grid,
//     }
//   },
//   // init: (context, page) => {
//   //   return new Promise(function (resolve, reject) {
//   //     setTimeout(function () {
//   //       resolve()
//   //     }, 2000)
//   //   })
//   // },
// },