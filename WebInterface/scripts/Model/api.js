"use strict";
class Client {
    constructor(baseUrl) {
        this.baseUrl = undefined;
        this.beforeSend = undefined;
        this.jsonParseReviver = undefined;
        this.baseUrl = baseUrl !== undefined ? baseUrl : "";
    }
    apiArcherGetByIdByIdPost(id) {
        return new Promise((resolve, reject) => {
            this.apiArcherGetByIdByIdPostWithCallbacks(id, (result) => resolve(result), (exception, reason) => reject(exception));
        });
    }
    apiArcherGetByIdByIdPostWithCallbacks(id, onSuccess, onFail) {
        let url_ = this.baseUrl + "/api/archer/GetById/{id}";
        if (id === undefined || id === null)
            throw new Error("The parameter 'id' must be defined.");
        url_ = url_.replace("{id}", encodeURIComponent("" + id));
        const content_ = "";
        jQuery.ajax({
            url: url_,
            beforeSend: this.beforeSend,
            type: "post",
            data: content_,
            dataType: "text",
            headers: {
                "Content-Type": "application/json; charset=UTF-8"
            }
        }).done((data, textStatus, xhr) => {
            this.processApiArcherGetByIdByIdPostWithCallbacks(url_, xhr, onSuccess, onFail);
        }).fail((xhr) => {
            this.processApiArcherGetByIdByIdPostWithCallbacks(url_, xhr, onSuccess, onFail);
        });
    }
    processApiArcherGetByIdByIdPostWithCallbacks(url, xhr, onSuccess, onFail) {
        try {
            let result = this.processApiArcherGetByIdByIdPost(xhr);
            if (onSuccess !== undefined)
                onSuccess(result);
        }
        catch (e) {
            if (onFail !== undefined)
                onFail(e, "http_service_exception");
        }
    }
    processApiArcherGetByIdByIdPost(xhr) {
        const responseText = xhr.responseText;
        const status = xhr.status;
        if (status === 200) {
            let result200 = null;
            let resultData200 = responseText === "" ? null : JSON.parse(responseText, this.jsonParseReviver);
            result200 = resultData200 !== undefined ? resultData200 : null;
            return result200;
        }
        else if (status === 400) {
            let result400 = null;
            let resultData400 = responseText === "" ? null : JSON.parse(responseText, this.jsonParseReviver);
            result400 = resultData400 !== undefined ? resultData400 : null;
            this.throwException("A server error occurred.", status, responseText, result400);
        }
        else if (status !== 200 && status !== 204) {
            this.throwException("An unexpected server error occurred.", status, responseText);
        }
        return null;
    }
    apiArcherGenerateTSInterfaceGet() {
        return new Promise((resolve, reject) => {
            this.apiArcherGenerateTSInterfaceGetWithCallbacks((result) => resolve(result), (exception, reason) => reject(exception));
        });
    }
    apiArcherGenerateTSInterfaceGetWithCallbacks(onSuccess, onFail) {
        let url_ = this.baseUrl + "/api/archer/GenerateTSInterface";
        const content_ = "";
        jQuery.ajax({
            url: url_,
            beforeSend: this.beforeSend,
            type: "get",
            data: content_,
            dataType: "text",
            headers: {
                "Content-Type": "application/json; charset=UTF-8"
            }
        }).done((data, textStatus, xhr) => {
            this.processApiArcherGenerateTSInterfaceGetWithCallbacks(url_, xhr, onSuccess, onFail);
        }).fail((xhr) => {
            this.processApiArcherGenerateTSInterfaceGetWithCallbacks(url_, xhr, onSuccess, onFail);
        });
    }
    processApiArcherGenerateTSInterfaceGetWithCallbacks(url, xhr, onSuccess, onFail) {
        try {
            let result = this.processApiArcherGenerateTSInterfaceGet(xhr);
            if (onSuccess !== undefined)
                onSuccess(result);
        }
        catch (e) {
            if (onFail !== undefined)
                onFail(e, "http_service_exception");
        }
    }
    processApiArcherGenerateTSInterfaceGet(xhr) {
        const responseText = xhr.responseText;
        const status = xhr.status;
        if (status === 200) {
            let result200 = null;
            let resultData200 = responseText === "" ? null : JSON.parse(responseText, this.jsonParseReviver);
            result200 = resultData200 !== undefined ? resultData200 : null;
            return result200;
        }
        else if (status === 400) {
            let result400 = null;
            let resultData400 = responseText === "" ? null : JSON.parse(responseText, this.jsonParseReviver);
            result400 = resultData400 !== undefined ? resultData400 : null;
            this.throwException("A server error occurred.", status, responseText, result400);
        }
        else if (status !== 200 && status !== 204) {
            this.throwException("An unexpected server error occurred.", status, responseText);
        }
        return null;
    }
    apiArcherDocUpdatePost(doc) {
        return new Promise((resolve, reject) => {
            this.apiArcherDocUpdatePostWithCallbacks(doc, (result) => resolve(result), (exception, reason) => reject(exception));
        });
    }
    apiArcherDocUpdatePostWithCallbacks(doc, onSuccess, onFail) {
        let url_ = this.baseUrl + "/api/archer/DocUpdate";
        const content_ = JSON.stringify(doc ? doc.toJS() : null);
        jQuery.ajax({
            url: url_,
            beforeSend: this.beforeSend,
            type: "post",
            data: content_,
            dataType: "text",
            headers: {
                "Content-Type": "application/json; charset=UTF-8"
            }
        }).done((data, textStatus, xhr) => {
            this.processApiArcherDocUpdatePostWithCallbacks(url_, xhr, onSuccess, onFail);
        }).fail((xhr) => {
            this.processApiArcherDocUpdatePostWithCallbacks(url_, xhr, onSuccess, onFail);
        });
    }
    processApiArcherDocUpdatePostWithCallbacks(url, xhr, onSuccess, onFail) {
        try {
            let result = this.processApiArcherDocUpdatePost(xhr);
            if (onSuccess !== undefined)
                onSuccess(result);
        }
        catch (e) {
            if (onFail !== undefined)
                onFail(e, "http_service_exception");
        }
    }
    processApiArcherDocUpdatePost(xhr) {
        const responseText = xhr.responseText;
        const status = xhr.status;
        if (status === 200) {
            let result200 = null;
            let resultData200 = responseText === "" ? null : JSON.parse(responseText, this.jsonParseReviver);
            result200 = resultData200 ? DocUpdateModel.fromJS(resultData200) : new DocUpdateModel();
            return result200;
        }
        else if (status === 400) {
            let result400 = null;
            let resultData400 = responseText === "" ? null : JSON.parse(responseText, this.jsonParseReviver);
            result400 = resultData400 !== undefined ? resultData400 : null;
            this.throwException("A server error occurred.", status, responseText, result400);
        }
        else if (status !== 200 && status !== 204) {
            this.throwException("An unexpected server error occurred.", status, responseText);
        }
        return null;
    }
    apiArcherTestExceptionPost() {
        return new Promise((resolve, reject) => {
            this.apiArcherTestExceptionPostWithCallbacks((result) => resolve(result), (exception, reason) => reject(exception));
        });
    }
    apiArcherTestExceptionPostWithCallbacks(onSuccess, onFail) {
        let url_ = this.baseUrl + "/api/archer/TestException";
        const content_ = "";
        jQuery.ajax({
            url: url_,
            beforeSend: this.beforeSend,
            type: "post",
            data: content_,
            dataType: "text",
            headers: {
                "Content-Type": "application/json; charset=UTF-8"
            }
        }).done((data, textStatus, xhr) => {
            this.processApiArcherTestExceptionPostWithCallbacks(url_, xhr, onSuccess, onFail);
        }).fail((xhr) => {
            this.processApiArcherTestExceptionPostWithCallbacks(url_, xhr, onSuccess, onFail);
        });
    }
    processApiArcherTestExceptionPostWithCallbacks(url, xhr, onSuccess, onFail) {
        try {
            let result = this.processApiArcherTestExceptionPost(xhr);
            if (onSuccess !== undefined)
                onSuccess(result);
        }
        catch (e) {
            if (onFail !== undefined)
                onFail(e, "http_service_exception");
        }
    }
    processApiArcherTestExceptionPost(xhr) {
        const responseText = xhr.responseText;
        const status = xhr.status;
        if (status === 200) {
            let result200 = null;
            let resultData200 = responseText === "" ? null : JSON.parse(responseText, this.jsonParseReviver);
            result200 = resultData200 !== undefined ? resultData200 : null;
            return result200;
        }
        else if (status === 400) {
            let result400 = null;
            let resultData400 = responseText === "" ? null : JSON.parse(responseText, this.jsonParseReviver);
            result400 = resultData400 !== undefined ? resultData400 : null;
            this.throwException("A server error occurred.", status, responseText, result400);
        }
        else if (status !== 200 && status !== 204) {
            this.throwException("An unexpected server error occurred.", status, responseText);
        }
        return null;
    }
    apiArcherDocNewPost(value) {
        return new Promise((resolve, reject) => {
            this.apiArcherDocNewPostWithCallbacks(value, (result) => resolve(result), (exception, reason) => reject(exception));
        });
    }
    apiArcherDocNewPostWithCallbacks(value, onSuccess, onFail) {
        let url_ = this.baseUrl + "/api/archer/DocNew";
        const content_ = JSON.stringify(value);
        jQuery.ajax({
            url: url_,
            beforeSend: this.beforeSend,
            type: "post",
            data: content_,
            dataType: "text",
            headers: {
                "Content-Type": "application/json; charset=UTF-8"
            }
        }).done((data, textStatus, xhr) => {
            this.processApiArcherDocNewPostWithCallbacks(url_, xhr, onSuccess, onFail);
        }).fail((xhr) => {
            this.processApiArcherDocNewPostWithCallbacks(url_, xhr, onSuccess, onFail);
        });
    }
    processApiArcherDocNewPostWithCallbacks(url, xhr, onSuccess, onFail) {
        try {
            let result = this.processApiArcherDocNewPost(xhr);
            if (onSuccess !== undefined)
                onSuccess(result);
        }
        catch (e) {
            if (onFail !== undefined)
                onFail(e, "http_service_exception");
        }
    }
    processApiArcherDocNewPost(xhr) {
        const responseText = xhr.responseText;
        const status = xhr.status;
        if (status === 200) {
            let result200 = null;
            let resultData200 = responseText === "" ? null : JSON.parse(responseText, this.jsonParseReviver);
            result200 = resultData200 !== undefined ? resultData200 : null;
            return result200;
        }
        else if (status === 400) {
            let result400 = null;
            let resultData400 = responseText === "" ? null : JSON.parse(responseText, this.jsonParseReviver);
            result400 = resultData400 !== undefined ? resultData400 : null;
            this.throwException("A server error occurred.", status, responseText, result400);
        }
        else if (status !== 200 && status !== 204) {
            this.throwException("An unexpected server error occurred.", status, responseText);
        }
        return null;
    }
    apiArcherGetEmployeesPost() {
        return new Promise((resolve, reject) => {
            this.apiArcherGetEmployeesPostWithCallbacks((result) => resolve(result), (exception, reason) => reject(exception));
        });
    }
    apiArcherGetEmployeesPostWithCallbacks(onSuccess, onFail) {
        let url_ = this.baseUrl + "/api/archer/GetEmployees";
        const content_ = "";
        jQuery.ajax({
            url: url_,
            beforeSend: this.beforeSend,
            type: "post",
            data: content_,
            dataType: "text",
            headers: {
                "Content-Type": "application/json; charset=UTF-8"
            }
        }).done((data, textStatus, xhr) => {
            this.processApiArcherGetEmployeesPostWithCallbacks(url_, xhr, onSuccess, onFail);
        }).fail((xhr) => {
            this.processApiArcherGetEmployeesPostWithCallbacks(url_, xhr, onSuccess, onFail);
        });
    }
    processApiArcherGetEmployeesPostWithCallbacks(url, xhr, onSuccess, onFail) {
        try {
            let result = this.processApiArcherGetEmployeesPost(xhr);
            if (onSuccess !== undefined)
                onSuccess(result);
        }
        catch (e) {
            if (onFail !== undefined)
                onFail(e, "http_service_exception");
        }
    }
    processApiArcherGetEmployeesPost(xhr) {
        const responseText = xhr.responseText;
        const status = xhr.status;
        if (status === 200) {
            let result200 = null;
            let resultData200 = responseText === "" ? null : JSON.parse(responseText, this.jsonParseReviver);
            result200 = resultData200 ? EmployeeResult.fromJS(resultData200) : new EmployeeResult();
            return result200;
        }
        else if (status === 400) {
            let result400 = null;
            let resultData400 = responseText === "" ? null : JSON.parse(responseText, this.jsonParseReviver);
            result400 = resultData400 !== undefined ? resultData400 : null;
            this.throwException("A server error occurred.", status, responseText, result400);
        }
        else if (status !== 200 && status !== 204) {
            this.throwException("An unexpected server error occurred.", status, responseText);
        }
        return null;
    }
    throwException(message, status, response, result) {
        if (result !== null && result !== undefined)
            throw result;
        else
            throw new SwaggerException(message, status, response);
    }
}
exports.Client = Client;
class DocUpdateModel {
    constructor(data) {
        if (data !== undefined) {
            this.user = data["user"] !== undefined ? data["user"] : null;
            this.pass = data["pass"] !== undefined ? data["pass"] : null;
        }
    }
    static fromJS(data) {
        return new DocUpdateModel(data);
    }
    toJS(data) {
        data = data === undefined ? {} : data;
        data["user"] = this.user !== undefined ? this.user : null;
        data["pass"] = this.pass !== undefined ? this.pass : null;
        return data;
    }
    toJSON() {
        return JSON.stringify(this.toJS());
    }
    clone() {
        const json = this.toJSON();
        return new DocUpdateModel(JSON.parse(json));
    }
}
exports.DocUpdateModel = DocUpdateModel;
class EmployeeResult {
    constructor(data) {
        if (data !== undefined) {
            if (data["array"] && data["array"].constructor === Array) {
                this.array = [];
                for (let item of data["array"])
                    this.array.push(Employee.fromJS(item));
            }
            this.total = data["total"] !== undefined ? data["total"] : null;
        }
    }
    static fromJS(data) {
        return new EmployeeResult(data);
    }
    toJS(data) {
        data = data === undefined ? {} : data;
        if (this.array && this.array.constructor === Array) {
            data["array"] = [];
            for (let item of this.array)
                data["array"].push(item.toJS());
        }
        data["total"] = this.total !== undefined ? this.total : null;
        return data;
    }
    toJSON() {
        return JSON.stringify(this.toJS());
    }
    clone() {
        const json = this.toJSON();
        return new EmployeeResult(JSON.parse(json));
    }
}
exports.EmployeeResult = EmployeeResult;
class Employee {
    constructor(data) {
        if (data !== undefined) {
            this.id = data["id"] !== undefined ? data["id"] : null;
            this.name = data["name"] !== undefined ? data["name"] : null;
        }
    }
    static fromJS(data) {
        return new Employee(data);
    }
    toJS(data) {
        data = data === undefined ? {} : data;
        data["id"] = this.id !== undefined ? this.id : null;
        data["name"] = this.name !== undefined ? this.name : null;
        return data;
    }
    toJSON() {
        return JSON.stringify(this.toJS());
    }
    clone() {
        const json = this.toJSON();
        return new Employee(JSON.parse(json));
    }
}
exports.Employee = Employee;
class SwaggerException extends Error {
    constructor(message, status, response, result) {
        super();
        this.message = message;
        this.status = status;
        this.response = response;
        this.result = result;
    }
}
exports.SwaggerException = SwaggerException;
//# sourceMappingURL=api.js.map