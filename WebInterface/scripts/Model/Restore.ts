import {
  ArrayModel,
  ButtonsModel,
  FormModel
} from '../Core'

import { FormItemInputModel } from '../Extends/Form'
import { SheriffModel } from '../ISheriff'

export const Restore = new ArrayModel({
  container: "div.absolute-center-container>div.width-v4.height-v4>panel('Восстановление пароля')>div.padding-horizontal-2.padding-top-2.padding-bottom-2",

  items: {
    form: new FormModel({
      value: {
        username: '',
      },
      form: {
        name: new FormItemInputModel({
          container: "labeled('Логин')",
          type: 'text',
        }),
      },
    }),
    buttons: new ButtonsModel({
      container: "div.padding-top-1.text.right",

      buttons: {
        restore: {
          class: 'margin-right-d1',
          text: 'Войти',
          on_click: async (context, page, application: SheriffModel) => {
            await new Promise(function (resolve, reject) {
              setTimeout(() => {
                resolve()
                application.browserHistory.push('/login')
              }, 500)
            })
          },
        },
        enter: {
          class: 'btn-danger',
          text: 'Восстановить',
          on_click: async (context, page, application: SheriffModel) => {
            await application.auth(application)
          },
        },
      },
    }),
  },
})
