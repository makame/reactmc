import {
  ArrayModel,
  ButtonsModel,
  FormModel
} from '../Core'

import { FormItemInputModel } from '../Extends/Form'
import { SheriffModel } from '../ISheriff'

export const Login = new ArrayModel({
  container: "div.absolute-center-container>div.width-v4.height-v4>panel('Авторизация')>div.padding-horizontal-2.padding-top-2.padding-bottom-2",
  items: {
    form: new FormModel({
      value: {
        username: '',
        password: '',
      },
      form: {
        name: new FormItemInputModel({
          container: "labeled('Логин')",
          type: 'text',
        }),
        password: new FormItemInputModel({
          container: "labeled('Пароль').padding-top-1",
          type: 'password',
        }),
      },
    }),
    buttons: new ButtonsModel({
      container: "div.padding-top-1.text.right",

      buttons: {
        restore: {
          class: 'margin-right-d1',
          text: 'Восстановить',
          on_click: async (context, page, application: SheriffModel) => {
            await new Promise(function (resolve, reject) {
              setTimeout(() => {
                resolve()
                application.browserHistory.push('/restore')
              }, 500)
            })
          },
        },
        enter: {
          class: 'btn-primary',
          text: 'Войти',
          on_click: async (context, page, application: SheriffModel) => {
            await application.auth(application)
          },
        },
      },
    }),
  },
})
