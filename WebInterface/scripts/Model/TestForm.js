"use strict";
const Core_1 = require('../Core');
const Form_1 = require('../Extends/Form');
exports.TestForm = new Core_1.FormModel({
    container: "div.padding-3>labeled($value.title)>div.padding-top-1.cols",
    value: {
        title: {
            get: (context, page, application) => {
                return context.value.name ? 'Новый элемент: ' + context.value.name : 'Создание нового элемента' + (page.params.id ? page.params.id : '');
            }
        },
        name: '',
        select1: {
            id: 'test1',
            name: 'Test 1'
        },
        select3: [
            {
                id: 'test1',
                name: 'Test 1'
            },
            {
                id: 'test2',
                name: 'Test 2'
            },
            {
                id: 'test3',
                name: 'Test 3'
            },
        ],
        group1: {
            active: true,
            name: '',
            select1: {
                id: 'test1',
                name: 'Test 1'
            },
            select3: [
                {
                    id: 'test1',
                    name: 'Test 1'
                },
                {
                    id: 'test2',
                    name: 'Test 2'
                },
                {
                    id: 'test3',
                    name: 'Test 3'
                },
            ],
        },
        group2: {
            name: '',
            select1: {
                id: 'test1',
                name: 'Test 1'
            },
            select3: [
                {
                    id: 'test1',
                    name: 'Test 1'
                },
                {
                    id: 'test2',
                    name: 'Test 2'
                },
                {
                    id: 'test3',
                    name: 'Test 3'
                },
            ],
        }
    },
    form: {
        name: new Form_1.FormItemInputModel({
            container: "labeled('Название')",
            left: {
                question: new Form_1.FormItemInputAddonTextModel({
                    class: 'square',
                    text: '?',
                }),
                icon: new Form_1.FormItemInputAddonTextModel({
                    class: 'square',
                    icon: 'home',
                }),
                question24: new Form_1.FormItemInputAddonButtonModel({
                    icon: 'home',
                    text: 'Push me',
                    class: 'default',
                }),
                question2: new Form_1.FormItemInputAddonButtonModel({
                    text: 'Push me',
                    class: 'primary',
                }),
            },
            right: {
                icon: new Form_1.FormItemInputAddonTextModel({
                    class: 'square',
                    icon: 'home',
                }),
                question: new Form_1.FormItemInputAddonTextModel({
                    class: 'square',
                    text: '?',
                }),
                button: new Form_1.FormItemInputAddonButtonModel({
                    class: 'danger',
                    text: 'Стереть',
                }),
            },
            class: 'white',
            icon: 'home',
            placeholder: 'Что-то полезное...',
            type: 'text',
            if: function (value, model, context, page, application) {
                return true;
            },
            error: function (value, model, context, page, application) {
                return !value || value.length == 0 ? 'Поле должно быть заполнено' : value.length < 2 ? 'Поле должно быть больше 2 знаков' : null;
            },
            warning: function (value, model, context, page, application) {
                return !value || (value.length < 8 && value.length >= 2) ? 'Поле слишком простое' : null;
            },
            enabled: function (value, model, context, page, application) {
                return true;
            },
        }),
        select1: new Form_1.FormItemSelectModel({
            container: "labeled('Тип без поиска').padding-top-1",
            data_type: 'local',
            data: [
                {
                    id: 'test1',
                    name: 'Test 1'
                },
                {
                    id: 'test2',
                    name: 'Test 2'
                },
            ],
            search: 'none',
            error: function (value, form_data) {
                return value ? null : 'Поле должно быть выбрано';
            }
        }),
        select3: new Form_1.FormItemMSelectModel({
            container: "labeled('Подтип').padding-top-1",
            data_type: 'remote',
            search: 'remote',
            request: function (search_string) {
                return new Promise(function (resolve, reject) {
                    setTimeout(function () {
                        resolve([
                            {
                                id: 'test1',
                                name: 'Test 1'
                            },
                            {
                                id: 'test2',
                                name: 'Test 2'
                            },
                            {
                                id: 'test3',
                                name: 'Test 3'
                            },
                        ]);
                    }, 300);
                });
            },
            error: function (value, form_data) {
                return value.length > 0 ? null : 'Должно быть выбран больше 1го значения';
            },
        }),
        group1: new Form_1.FormItemGroupModel({
            container: "div.padding-top-1.col-xs-6>panel('Группа 1')>div.padding-1",
            items: {
                active: new Form_1.FormItemTriggerModel({
                    container: "labeled('Активность').padding-top-1",
                }),
                select1: new Form_1.FormItemSelectModel({
                    container: "labeled('Тип без поиска').padding-top-1",
                    data_type: 'local',
                    data: [
                        {
                            id: 'test1',
                            name: 'Test 1'
                        },
                        {
                            id: 'test2',
                            name: 'Test 2'
                        },
                    ],
                    search: 'none',
                    error: function (value, form_data) {
                        return value ? null : 'Поле должно быть выбрано';
                    },
                    enabled: function (value, model, context, page, application) {
                        return model.value.active;
                    },
                }),
                select3: new Form_1.FormItemMSelectModel({
                    container: "labeled('Подтип').padding-top-1",
                    data_type: 'remote',
                    search: 'remote',
                    request: function (search_string) {
                        return new Promise(function (resolve, reject) {
                            setTimeout(function () {
                                resolve([
                                    {
                                        id: 'test1',
                                        name: 'Test 1'
                                    },
                                    {
                                        id: 'test2',
                                        name: 'Test 2'
                                    },
                                    {
                                        id: 'test3',
                                        name: 'Test 3'
                                    },
                                ]);
                            }, 300);
                        });
                    },
                    error: function (value, form_data) {
                        return value.length > 0 ? null : 'Должно быть выбран больше 1го значения';
                    },
                    enabled: function (value, model, context, page, application) {
                        return model.value.active;
                    },
                }),
            }
        }),
        group2: new Form_1.FormItemGroupModel({
            container: "div.padding-top-1.col-xs-6>panel('Группа 2')>div.padding-1",
            items: {
                select1: new Form_1.FormItemSelectModel({
                    container: "labeled('Тип без поиска').padding-top-1",
                    data_type: 'local',
                    data: [
                        {
                            id: 'test1',
                            name: 'Test 1'
                        },
                        {
                            id: 'test2',
                            name: 'Test 2'
                        },
                    ],
                    search: 'none',
                    error: function (value, form_data) {
                        return value ? null : 'Поле должно быть выбрано';
                    }
                }),
                select3: new Form_1.FormItemMSelectModel({
                    container: "labeled('Подтип').padding-top-1",
                    data_type: 'remote',
                    search: 'remote',
                    request: function (search_string) {
                        return new Promise(function (resolve, reject) {
                            setTimeout(function () {
                                resolve([
                                    {
                                        id: 'test1',
                                        name: 'Test 1'
                                    },
                                    {
                                        id: 'test2',
                                        name: 'Test 2'
                                    },
                                    {
                                        id: 'test3',
                                        name: 'Test 3'
                                    },
                                ]);
                            }, 300);
                        });
                    },
                    error: function (value, form_data) {
                        return value.length > 0 ? null : 'Должно быть выбран больше 1го значения';
                    },
                }),
            }
        }),
        buttons: new Form_1.FormItemButtonsModel({
            container: "div.col-xs-12.padding-top-2.padding-bottom-1",
            items: {
                first: new Form_1.FormItemButtonModel({
                    class: 'btn-primary square',
                    icon: 'home',
                }),
                second: new Form_1.FormItemButtonModel({
                    class: 'square',
                    text: '?',
                }),
                third: new Form_1.FormItemButtonModel({
                    class: 'btn-danger',
                    text: 'Стереть',
                }),
                fourth: new Form_1.FormItemButtonModel({
                    class: 'btn-default',
                    icon: 'home',
                    text: 'Стереть',
                }),
            },
        }),
    },
});
//# sourceMappingURL=TestForm.js.map