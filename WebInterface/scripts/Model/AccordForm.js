"use strict";
const Form_1 = require('../Extends/Form');
const Core_1 = require('../Core');
exports.AccordForm = new Core_1.FormModel({
    container: "accord($value.title, $value.visible).margin-bottom-1>div.padding-top-1.cols",
    value: {
        title: {
            get: (context, page, application) => {
                return context.value.name ? 'Новый элемент: ' + context.value.name : 'Создание нового элемента' + (page.params.id ? page.params.id : '');
            }
        },
        visible: {
            value: true,
            get: (context, page, application) => {
                return context.value.visible.value;
            },
            set: (value, context, page, application) => {
                context.value.visible.value = value;
            }
        },
        name: '',
        select1: {
            id: 'test1',
            name: 'Test 1'
        },
        select3: [
            {
                id: 'test1',
                name: 'Test 1'
            },
            {
                id: 'test2',
                name: 'Test 2'
            },
            {
                id: 'test3',
                name: 'Test 3'
            },
        ],
    },
    form: {
        name: new Form_1.FormItemInputModel({
            container: "labeled('Название')",
            right: {
                question: new Form_1.FormItemInputAddonTextModel({
                    class: 'square',
                    text: '?',
                }),
                button: new Form_1.FormItemInputAddonButtonModel({
                    class: 'danger',
                    text: 'Стереть',
                }),
            },
            class: 'white',
            icon: 'home',
            placeholder: 'Что-то полезное...',
            type: 'text',
            error: function (value, model, context, page, application) {
                return !value || value.length == 0 ? 'Поле должно быть заполнено' : value.length < 2 ? 'Поле должно быть больше 2 знаков' : null;
            },
            warning: function (value, model, context, page, application) {
                return !value || (value.length < 8 && value.length >= 2) ? 'Поле слишком простое' : null;
            },
            enabled: function (value, model, context, page, application) {
                return true;
            },
        }),
        select1: new Form_1.FormItemSelectModel({
            container: "labeled('Тип без поиска').padding-top-1",
            data_type: 'local',
            data: [
                {
                    id: 'test1',
                    name: 'Test 1'
                },
                {
                    id: 'test2',
                    name: 'Test 2'
                },
            ],
            search: 'none',
            error: function (value, form_data) {
                return value ? null : 'Поле должно быть выбрано';
            }
        }),
        select3: new Form_1.FormItemMSelectModel({
            container: "labeled('Подтип').padding-top-1",
            data_type: 'remote',
            search: 'remote',
            request: function (search_string) {
                return new Promise(function (resolve, reject) {
                    setTimeout(function () {
                        resolve([
                            {
                                id: 'test1',
                                name: 'Test 1'
                            },
                            {
                                id: 'test2',
                                name: 'Test 2'
                            },
                            {
                                id: 'test3',
                                name: 'Test 3'
                            },
                        ]);
                    }, 300);
                });
            },
            error: function (value, form_data) {
                return value.length > 0 ? null : 'Должно быть выбран больше 1го значения';
            },
        }),
        buttons: new Form_1.FormItemButtonsModel({
            container: "div.col-xs-12.padding-top-2.padding-bottom-1",
            items: {
                first: new Form_1.FormItemButtonModel({
                    class: 'btn-primary square',
                    icon: 'home',
                }),
                second: new Form_1.FormItemButtonModel({
                    class: 'square',
                    text: '?',
                }),
                third: new Form_1.FormItemButtonModel({
                    class: 'btn-danger',
                    text: 'Стереть',
                }),
                fourth: new Form_1.FormItemButtonModel({
                    class: 'btn-default',
                    icon: 'home',
                    text: 'Стереть',
                }),
            },
        }),
    },
});
//# sourceMappingURL=AccordForm.js.map