"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const Core_1 = require('../Core');
const Form_1 = require('../Extends/Form');
exports.Login = new Core_1.ArrayModel({
    container: "div.absolute-center-container>div.width-v4.height-v4>panel('Авторизация')>div.padding-horizontal-2.padding-top-2.padding-bottom-2",
    items: {
        form: new Core_1.FormModel({
            value: {
                username: '',
                password: '',
            },
            form: {
                name: new Form_1.FormItemInputModel({
                    container: "labeled('Логин')",
                    type: 'text',
                }),
                password: new Form_1.FormItemInputModel({
                    container: "labeled('Пароль').padding-top-1",
                    type: 'password',
                }),
            },
        }),
        buttons: new Core_1.ButtonsModel({
            container: "div.padding-top-1.text.right",
            buttons: {
                restore: {
                    class: 'margin-right-d1',
                    text: 'Восстановить',
                    on_click: (context, page, application) => __awaiter(this, void 0, void 0, function* () {
                        yield new Promise(function (resolve, reject) {
                            setTimeout(() => {
                                resolve();
                                application.browserHistory.push('/restore');
                            }, 500);
                        });
                    }),
                },
                enter: {
                    class: 'btn-primary',
                    text: 'Войти',
                    on_click: (context, page, application) => __awaiter(this, void 0, void 0, function* () {
                        yield application.auth(application);
                    }),
                },
            },
        }),
    },
});
//# sourceMappingURL=Login.js.map