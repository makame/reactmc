import { Map } from '../Core'

export class NavigationModel {
  label: string
  icon?: string
  url?: string
  path?: string[]
  strong?: boolean
  children?: Map<NavigationModel>
  color?: string
  bud?: number | string
}

export const Navigation: Map<NavigationModel> = {
  news: {
    label: 'Новости',
    icon: 'home',
    url: '/',
    // path: ['/'],
    strong: true,
  },
  documents: {
    label: 'Документы',
    icon: 'book',
    path: ['/documents', '/document'],
    children: {
      microsoft: {
        label: 'Microsoft',
        icon: 'home',
        url: '/documents/doc',
      },
      autodesk: {
        label: 'Autodesk',
        url: '/documents/autodesk',
      },
    },
  },
  test: {
    label: 'Тестирование',
    icon: 'briefcase',
    path: ['/help', '/form', '/panes', '/tabs', '/accord'],
    children: {
      microsoft: {
        label: 'Грид',
        icon: 'home',
        url: '/help',
        path: ['/help'],
      },
      autodesk: {
        label: 'Форма',
        url: '/form',
        path: ['/form'],
      },
      tabs: {
        label: 'Табы',
        icon: 'home',
        url: '/tabs',
        path: ['/tabs'],
      },
      accord: {
        label: 'Аккордеон',
        icon: 'home',
        url: '/accord',
        path: ['/accord'],
      },
      groupred: {
        label: 'PANE',
        color: 'danger',
        children: {
          microsoft: {
            label: 'Грид',
            icon: 'home',
            url: '/panes/help',
            path: ['/panes/help'],
          },
          autodesk: {
            label: 'Форма',
            url: '/panes/form',
            path: ['/panes/form'],
          },
        },
      },
    },
  },
}