"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const Navigation_1 = require('./Navigation');
const Core_1 = require('../Core');
const Login_1 = require('./Login');
const login = {
    path: 'login',
    check: (page, application) => {
        if (application.user) {
            application.browserHistory.push('/');
            return false;
        }
    },
    content: Login_1.Login,
};
const Restore_1 = require('./Restore');
const restore = {
    path: 'restore',
    check: (page, application) => {
        if (application.user) {
            application.browserHistory.push('/');
            return false;
        }
    },
    content: Restore_1.Restore,
};
const TestForm_1 = require('./TestForm');
const form = {
    path: 'form',
    content: TestForm_1.TestForm,
};
const Tabs_1 = require('../Core/Components/Tabs');
const tabs = {
    path: 'tabs',
    content: new Tabs_1.TabsModel({
        container: "div.padding-horizontal-2.padding-bottom-2",
        active_tab: 'form',
        tabs: {
            form: new Tabs_1.TabModel({
                label: 'Form',
                content: TestForm_1.TestForm
            }),
            form2: new Tabs_1.TabModel({
                label: 'Form Alt',
                content: TestForm_1.TestForm
            }),
        }
    }),
};
const Array_1 = require('../Core/Components/Array');
const AccordForm_1 = require('./AccordForm');
const accord = {
    path: 'accord',
    content: new Array_1.ArrayModel({
        container: "div.padding-horizontal-2.padding-bottom-2",
        items: {
            grid: AccordForm_1.AccordForm,
            two: AccordForm_1.AccordForm,
            three: AccordForm_1.AccordForm,
        }
    }),
};
const Core_2 = require('../Core');
const Menu_1 = require('../Extends/Menu');
const Grid_1 = require('../Extends/Grid');
const help = {
    path: 'help',
    content: new Array_1.ArrayModel({
        container: "div.padding-horizontal-2.padding-bottom-2",
        items: {
            menu: new Core_2.MenuModel({
                items: {
                    header: new Menu_1.MenuItemHeaderModel({
                        text: 'fsdfs',
                    }),
                    divider0: new Menu_1.MenuItemDividerModel(),
                    date: new Menu_1.MenuItemDateModel({
                        icon: 'calendar',
                        value: null,
                        get: (content, context, page, application) => {
                            return content.items['date'].value;
                        },
                        set: (newValue, content, context, page, application) => {
                            content.items['date'].value = newValue;
                            page.update();
                        },
                    }),
                    input: new Menu_1.MenuItemInputModel({
                        icon: 'search',
                        value: 'fsdfs',
                        placeholder: 'Поиск'
                    }),
                    enum: new Menu_1.MenuItemEnumModel({
                        get: (item, content, context, page, application) => {
                            const grid = page.content.items['grid'];
                            const value = grid.value.per_page;
                            const en = item.enums.find(s => s.id == value);
                            return en ? en : {
                                id: value,
                                name: value.toString()
                            };
                        },
                        set: (item, newValue, content, context, page, application) => {
                            const grid = page.content.items['grid'];
                            grid.value.per_page = newValue.id;
                            grid.value.need_update = true;
                            page.update();
                        },
                        enums: [
                            {
                                id: 25,
                                name: '25'
                            },
                            {
                                id: 50,
                                name: '50'
                            },
                            {
                                id: 250,
                                name: '250'
                            },
                            {
                                id: 1000,
                                name: '1k'
                            },
                            {
                                id: null,
                                name: 'Все'
                            },
                        ]
                    }),
                    space: new Menu_1.MenuItemSpaceModel(),
                    button: new Menu_1.MenuItemButtonModel({
                        label: 'Demo Buton',
                        on_click: (context, page, application) => __awaiter(this, void 0, void 0, function* () {
                            yield new Promise(function (resolve, reject) {
                                setTimeout(() => {
                                    resolve();
                                }, 2000);
                            });
                        }),
                    }),
                },
            }),
            grid: new Core_2.GridModel({
                container: "div.padding-horizontal-2.padding-top-2.padding-bottom-2",
                value: {
                    sort: [],
                    page: 0,
                    per_page: null,
                    total: 0
                },
                grid: {
                    request: (value, context, page, application) => __awaiter(this, void 0, void 0, function* () {
                        context.loading = true;
                        page.update();
                        const result = yield Core_1.rest.enter().one('test.json').get().do();
                        context.loading = false;
                        page.update();
                        return {
                            total: result.FCT.fund.length,
                            data: result.FCT.fund
                        };
                    }),
                    on_click: (row, content, context, page, application) => {
                        let ask = {
                            dialog: true,
                            on_outside: (content, item, page, application) => {
                                ask.close();
                            },
                            content: new Core_2.TextModel({
                                container: "div.text.medium.padding-horizontal-2.padding-vertical-d2",
                                value: 'Закрытие окна'
                            }),
                            message: new Core_2.TextModel({
                                container: "div.padding-horizontal-2.padding-vertical-d2",
                                value: 'Подтвердите закрытие окна!'
                            }),
                            bottom: new Core_2.ButtonsModel({
                                container: "div.padding-horizontal-2.padding-vertical-d2",
                                buttons: {
                                    cancel: {
                                        class: 'btn-primary',
                                        text: 'Отмена',
                                        on_click: (context, page, application) => __awaiter(this, void 0, void 0, function* () {
                                            ask.close();
                                        }),
                                    },
                                    ok: {
                                        class: 'btn-danger margin-left-1',
                                        text: 'Закрыть',
                                        on_click: (context, page, application) => __awaiter(this, void 0, void 0, function* () {
                                            ask.close();
                                            modal.close();
                                        }),
                                    },
                                }
                            })
                        };
                        let modal = {
                            on_outside: (content_, modal, page, application) => {
                                content.modals['ask'] = ask;
                                page.update();
                            },
                            content: new Array_1.ArrayModel({
                                container: "div.text.medium.padding-horizontal-2.padding-top-2",
                                items: {
                                    header: new Core_2.TextModel({
                                        value: 'Hello!'
                                    }),
                                    buttons: new Core_2.ButtonsModel({
                                        container: "div.padding-top-2.padding-bottom-d2",
                                        buttons: {
                                            restore: {
                                                class: 'btn-danger',
                                                text: 'Закрыть',
                                                on_click: (context, page, application) => __awaiter(this, void 0, void 0, function* () {
                                                    content.modals['ask'] = ask;
                                                }),
                                            },
                                        }
                                    })
                                }
                            })
                        };
                        content.modals = {};
                        content.modals['modal_test'] = modal;
                    },
                    sort: ['+id'],
                    sortNull: [],
                    data_type: 'remote_local',
                    pagination: true,
                    big_data: true,
                    fields: {
                        FamilyName: new Grid_1.GridItemTextModel({
                            label: 'Family Name',
                            filter: ['contains', 'totaly', 'not_in'],
                            sortable: true,
                            placeholder: 'Family Name'
                        }),
                        LastReturn: new Grid_1.GridItemTextModel({
                            label: 'Last Return',
                            filter: ['contains', 'totaly', 'not_in'],
                            sortable: true,
                            placeholder: 'Last Return'
                        }),
                        Name: new Grid_1.GridItemTextModel({
                            label: 'Name',
                            filter: ['contains', 'totaly', 'not_in'],
                            sortable: true,
                            placeholder: 'Name'
                        }),
                        Ticker: new Grid_1.GridItemTextModel({
                            label: 'Ticker',
                            filter: ['contains', 'totaly', 'not_in'],
                            sortable: true,
                            placeholder: 'Ticker'
                        }),
                    },
                    allow_add: (context, page, application) => true,
                    allow_edit: (row, context, page, application) => true,
                    selectable: true,
                    create_context_menu: {
                        save: {
                            text: 'Сохранить',
                            on_click: (row, context, page, application) => {
                            }
                        }
                    }
                }
            }),
        }
    }),
};
const Default_1 = require('../Layouts/Default');
const NotFoundPage_1 = require('../Pages/NotFoundPage');
const sheriff = {
    path: '/',
    index: 'help',
    layout: Default_1.default,
    routes: {
        help: help,
        form: form,
        tabs: tabs,
        accord: accord,
        not_found: {
            path: '*',
            page: NotFoundPage_1.default,
        }
    }
};
const secured = {
    path: '',
    condition: (page, application) => {
        if (!application.user) {
            return Login_1.Login;
        }
    },
    routes: {
        sheriff: sheriff
    }
};
const Containers_1 = require('../Containers');
exports.application = {
    containers: Containers_1.containers,
    basename: (location) => {
        return location.pathname.indexOf('.html') >= 0 ? location.pathname.substring(0, location.pathname.indexOf('.html') + 5) : location.origin;
    },
    auth: (application) => __awaiter(this, void 0, void 0, function* () {
        application.user = {
            id: 'sdfsdf',
            avatar_url: Core_1.rest.getDir() + 'images/avatar-empty.png',
            username: 'makame',
            notifications: [],
        };
    }),
    init: (application) => __awaiter(this, void 0, void 0, function* () {
        yield application.auth(application);
    }),
    menu: Navigation_1.Navigation,
    routes: {
        login: login,
        restore: restore,
        secured: secured
    }
};
//# sourceMappingURL=app.js.map