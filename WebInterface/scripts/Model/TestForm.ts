import {
  ISetGet,
  FormModel
} from '../Core'

import { FormItemInputModel, FormItemInputAddonTextModel, FormItemInputAddonButtonModel,
  FormItemTriggerModel,
  FormItemSelectModel,
  FormItemMSelectModel,
  FormItemGroupModel,
  FormItemButtonsModel, FormItemButtonModel } from '../Extends/Form'
import { SheriffModel } from '../ISheriff'

export const TestForm = new FormModel({
  container: "div.padding-3>labeled($value.title)>div.padding-top-1.cols",

  value: {
    title: <ISetGet> {
      get: (context, page, application) => {
        return context.value.name ? 'Новый элемент: ' + context.value.name : 'Создание нового элемента' + (page.params.id ? page.params.id : '')
      }
    },
    name: '',
    select1: {
      id: 'test1',
      name: 'Test 1'
    },
    select3: [
      {
        id: 'test1',
        name: 'Test 1'
      },
      {
        id: 'test2',
        name: 'Test 2'
      },
      {
        id: 'test3',
        name: 'Test 3'
      },
    ],
    group1: {
      active: true,
      name: '',
      select1: {
        id: 'test1',
        name: 'Test 1'
      },
      select3: [
        {
          id: 'test1',
          name: 'Test 1'
        },
        {
          id: 'test2',
          name: 'Test 2'
        },
        {
          id: 'test3',
          name: 'Test 3'
        },
      ],
    },
    group2: {
      name: '',
      select1: {
        id: 'test1',
        name: 'Test 1'
      },
      select3: [
        {
          id: 'test1',
          name: 'Test 1'
        },
        {
          id: 'test2',
          name: 'Test 2'
        },
        {
          id: 'test3',
          name: 'Test 3'
        },
      ],
    }
    // repeat: [{
    //   id: 'kjhjk',
    //   name: 'hgfhgfg',
    //   value: true,
    //   price: 16.7,
    // }],
  },

  form: {
    name: new FormItemInputModel({
      container: "labeled('Название')",

      left: {
        question: new FormItemInputAddonTextModel({
          class: 'square',
          text: '?',
        }),
        icon: new FormItemInputAddonTextModel({
          class: 'square',
          icon: 'home',
        }),
        question24: new FormItemInputAddonButtonModel({
          icon: 'home',
          text: 'Push me',
          class: 'default',
        }),
        question2: new FormItemInputAddonButtonModel({
          text: 'Push me',
          class: 'primary',
        }),
      },

      right: {
        icon: new FormItemInputAddonTextModel({
          class: 'square',
          icon: 'home',
        }),
        question: new FormItemInputAddonTextModel({
          class: 'square',
          text: '?',
        }),
        button: new FormItemInputAddonButtonModel({
          class: 'danger',
          text: 'Стереть',
        }),
      },
      class: 'white',
      icon: 'home',
      placeholder: 'Что-то полезное...',

      type: 'text',
      if: function(value, model, context, page, application: SheriffModel) {
        return true;
      },
      error: function(value, model, context, page, application: SheriffModel) {
        return !value || value.length == 0 ? 'Поле должно быть заполнено' : value.length < 2 ? 'Поле должно быть больше 2 знаков' : null;
      },
      warning: function(value, model, context, page, application: SheriffModel) {
        return !value || (value.length < 8 && value.length >= 2) ? 'Поле слишком простое' : null;
      },
      enabled: function(value, model, context, page, application: SheriffModel) {
        return true;
      },
    }),
    select1: new FormItemSelectModel({
      container: "labeled('Тип без поиска').padding-top-1",

      data_type: 'local',
      data: [
        {
          id: 'test1',
          name: 'Test 1'
        },
        {
          id: 'test2',
          name: 'Test 2'
        },
      ],
      search: 'none',
      error: function(value, form_data) {
        return value ? null : 'Поле должно быть выбрано';
      }
    }),
    select3: new FormItemMSelectModel({
      container: "labeled('Подтип').padding-top-1",

      data_type: 'remote',
      search: 'remote',
      request: function(search_string) {
        return new Promise(function (resolve, reject) {
          setTimeout(function () {
            resolve([
              {
                id: 'test1',
                name: 'Test 1'
              },
              {
                id: 'test2',
                name: 'Test 2'
              },
              {
                id: 'test3',
                name: 'Test 3'
              },
            ])
          }, 300)
        })
      },
      error: function(value, form_data) {
        return value.length > 0 ? null : 'Должно быть выбран больше 1го значения';
      },
    }),
    group1: new FormItemGroupModel({
      container: "div.padding-top-1.col-xs-6>panel('Группа 1')>div.padding-1",

      items: {
        active: new FormItemTriggerModel({
          container: "labeled('Активность').padding-top-1",
        }),
        select1: new FormItemSelectModel({
          container: "labeled('Тип без поиска').padding-top-1",

          data_type: 'local',
          data: [
            {
              id: 'test1',
              name: 'Test 1'
            },
            {
              id: 'test2',
              name: 'Test 2'
            },
          ],
          search: 'none',
          error: function(value, form_data) {
            return value ? null : 'Поле должно быть выбрано';
          },
          enabled: function(value, model, context, page, application: SheriffModel) {
            return (model.value as any).active;
          },
        }),
        select3: new FormItemMSelectModel({
          container: "labeled('Подтип').padding-top-1",

          data_type: 'remote',
          search: 'remote',
          request: function(search_string) {
            return new Promise(function (resolve, reject) {
              setTimeout(function () {
                resolve([
                  {
                    id: 'test1',
                    name: 'Test 1'
                  },
                  {
                    id: 'test2',
                    name: 'Test 2'
                  },
                  {
                    id: 'test3',
                    name: 'Test 3'
                  },
                ])
              }, 300)
            })
          },
          error: function(value, form_data) {
            return value.length > 0 ? null : 'Должно быть выбран больше 1го значения';
          },
          enabled: function(value, model, context, page, application: SheriffModel) {
            return (model.value as any).active;
          },
        }),
      }
    }),
    group2: new FormItemGroupModel({
      container: "div.padding-top-1.col-xs-6>panel('Группа 2')>div.padding-1",

      items: {
        select1: new FormItemSelectModel({
          container: "labeled('Тип без поиска').padding-top-1",

          data_type: 'local',
          data: [
            {
              id: 'test1',
              name: 'Test 1'
            },
            {
              id: 'test2',
              name: 'Test 2'
            },
          ],
          search: 'none',
          error: function(value, form_data) {
            return value ? null : 'Поле должно быть выбрано';
          }
        }),
        select3: new FormItemMSelectModel({
          container: "labeled('Подтип').padding-top-1",

          data_type: 'remote',
          search: 'remote',
          request: function(search_string) {
            return new Promise(function (resolve, reject) {
              setTimeout(function () {
                resolve([
                  {
                    id: 'test1',
                    name: 'Test 1'
                  },
                  {
                    id: 'test2',
                    name: 'Test 2'
                  },
                  {
                    id: 'test3',
                    name: 'Test 3'
                  },
                ])
              }, 300)
            })
          },
          error: function(value, form_data) {
            return value.length > 0 ? null : 'Должно быть выбран больше 1го значения';
          },
        }),
      }
    }),
    // group1: {
    //   container: "div.padding-top-1.col-xs-6>panel('Группа 1')>div.padding-1",

    //   type: 'group',
    //   array: {
    //     select1: {
    //       container: "labeled('Тип')",

    //       type: 'select',
    //       data_type: 'local',
    //       data: [
    //         {
    //           id: 'test1',
    //           name: 'Test 1'
    //         },
    //         {
    //           id: 'test2',
    //           name: 'Test 2'
    //         },
    //       ],
    //       search: 'local',
    //       validate: function(value, form_data) {
    //         return value ? null : 'Поле должно быть выбрано';
    //       }
    //     },
    //     select11: {
    //       container: "labeled('Тип без поиска').padding-top-1",

    //       type: 'select',
    //       data_type: 'local',
    //       data: [
    //         {
    //           id: 'test1',
    //           name: 'Test 1'
    //         },
    //         {
    //           id: 'test2',
    //           name: 'Test 2'
    //         },
    //       ],
    //       search: 'none',
    //       validate: function(value, form_data) {
    //         return value ? null : 'Поле должно быть выбрано';
    //       }
    //     },
    //   },
    // },
    // group2: {
    //   container: "div.padding-top-1.col-xs-6>panel('Группа 2')>div.padding-1",

    //   type: 'group',
    //   array: {
    //     select2: {
    //       container: "labeled('Подтип')",

    //       type: 'select',
    //       data_type: 'remote',
    //       search: 'remote',
    //       if: function(form_data) {
    //         return !form_data.select1 || form_data.select1.id != 'test1';
    //       },
    //       request: function(form_data, search_string) {
    //         return new Promise(function (resolve, reject) {
    //           setTimeout(function () {
    //             resolve([
    //               {
    //                 id: 'test1',
    //                 name: 'Test 1'
    //               },
    //               {
    //                 id: 'test2',
    //                 name: 'Test 2'
    //               },
    //               {
    //                 id: 'test3',
    //                 name: 'Test 3'
    //               },
    //             ])
    //           }, 300)
    //         })
    //       },
    //       validate: function(value, form_data) {
    //         return value ? null : 'Поле должно быть выбрано';
    //       }
    //     },
    //   },
    // },
    // repeat: {
    //   container: "labeled('Действия').col-xs-12.padding-top-2>div.padding-bottom-1",
    //   row_container: "panel('Позиция').margin-bottom-1>div.padding-1.cols",

    //   type: 'repeat',
    //   default: {
    //     id: '',
    //     name: '',
    //     value: true,
    //     price: 16.7,
    //   },
    //   row: {
    //     name: {
    //       container: "labeled('Наименование')",

    //       type: 'text',
    //       validate: function(value, form_data) {
    //         return value.length < 2 ? 'Поле должно быть больше 2 знаков' : null;
    //       }
    //     },
    //     value: {
    //       container: "labeled('Активность').padding-top-1",

    //       type: 'bool'
    //     },
    //     price: {
    //       container: "labeled('Цена').padding-top-1",

    //       type: 'text',
    //     },
    //     buttons: {
    //       container: "div.col-xs-12.padding-top-2.padding-bottom-1",
    //       type: 'buttons',
    //       buttons: {
    //         add: {
    //           class: 'btn-primary square',
    //           icon: 'plus',
    //           on_click: (level, context, page, application) => {
    //             var temp = []

    //             _.each(level.parent.valueLink.value.repeat, (it) => {
    //               if (it === level.valueLink.value) {
    //                 temp.push(it)
    //                 temp.push(Object.assign({}, level.parent.form.repeat.default))
    //               }
    //               else {
    //                 temp.push(it)
    //               }
    //             })

    //             level.parent.valueLink.value.repeat = temp
    //             level.parent.valueLink.requestChange(level.parent.valueLink.value)
    //           },
    //         },
    //         remove: {
    //           class: 'btn-danger square',
    //           icon: 'minus',
    //           on_click: (level, context, page, application) => {
    //             var temp = []

    //             _.each(level.parent.valueLink.value.repeat, (it) => {
    //               if (it !== level.valueLink.value) {
    //                 temp.push(it)
    //               }
    //             })

    //             level.parent.valueLink.value.repeat = temp
    //             level.parent.valueLink.requestChange(level.parent.valueLink.value)
    //           },
    //           if: function(level, context, page, application) {
    //             return level.parent.valueLink.value.repeat && level.parent.valueLink.value.repeat[0] && level.parent.valueLink.value.repeat[0] !== level.valueLink.value;
    //           },
    //         },
    //       },
    //     },
    //   },
    // },

    buttons: new FormItemButtonsModel({
      container: "div.col-xs-12.padding-top-2.padding-bottom-1",
      items: {
        first: new FormItemButtonModel({
          class: 'btn-primary square',
          icon: 'home',
        }),
        second: new FormItemButtonModel({
          class: 'square',
          text: '?',
        }),
        third: new FormItemButtonModel({
          class: 'btn-danger',
          text: 'Стереть',
        }),
        fourth: new FormItemButtonModel({
          class: 'btn-default',
          icon: 'home',
          text: 'Стереть',
        }),
      },
    }),
  },
})