"use strict";
const React = require('react');
const RouteChanger_1 = require('../Core/Controls/RouteChanger');
class ClearProps {
}
exports.ClearProps = ClearProps;
class ClearState {
}
exports.ClearState = ClearState;
class Clear extends React.Component {
    render() {
        return (React.createElement(RouteChanger_1.RouteChanger, {duration: 500, location: this.props.location}, this.props.children));
    }
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Clear;
//# sourceMappingURL=Clear.js.map