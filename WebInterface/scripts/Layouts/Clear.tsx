import * as React from 'react';
import { RouteChanger } from '../Core/Controls/RouteChanger'

export class ClearProps {
  location?: any
}

export class ClearState {
}

export default class Clear extends React.Component<ClearProps, ClearState> {

  render() {
    return (
      <RouteChanger duration={500} location={this.props.location}>
        {this.props.children}
      </RouteChanger>
    )
  }
}
