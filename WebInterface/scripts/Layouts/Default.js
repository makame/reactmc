"use strict";
const React = require('react');
const FontAwesome = require('react-fontawesome');
const Core_1 = require('../Core');
class DefaultProps {
}
exports.DefaultProps = DefaultProps;
class DefaultState {
    constructor() {
        this.leftMenu = true;
        this.selected = null;
    }
}
exports.DefaultState = DefaultState;
class Default extends React.Component {
    constructor() {
        super(...arguments);
        this.state = new DefaultState();
    }
    toggleLeftMenu() {
        this.setState({ leftMenu: !this.state.leftMenu });
    }
    isActived(item) {
        var active = false;
        if (item.path)
            item.path.forEach((row) => {
                if (this.context.router.isActive(row, item.strong))
                    active = true;
            });
        return active;
    }
    render() {
        let self = this;
        const { page, application } = this.props;
        const { selected } = this.state;
        const menu = Core_1.MapPro.map(application.menu, (top, topName) => {
            let iconTop = top.icon ? (React.createElement(FontAwesome, {name: top.icon})) : null;
            let budTop = top.bud ? (React.createElement("div", {className: "bud"}, top.bud)) : null;
            if (top.children) {
                let children = Core_1.MapPro.map(top.children, (row, rowName) => {
                    if (row.children) {
                        let group = Core_1.MapPro.map(row.children, (col, colName) => {
                            let iconCol = col.icon ? (React.createElement(FontAwesome, {name: col.icon})) : null;
                            let budCol = col.bud ? (React.createElement("div", {className: "bud"}, col.bud)) : null;
                            return (React.createElement("div", {key: colName, className: "sp-item" + (self.isActived(col) ? ' active' : ''), onClick: () => {
                                if (col.url)
                                    application.browserHistory.push(col.url);
                            }}, 
                                React.createElement("div", null, 
                                    iconCol, 
                                    col.label, 
                                    budCol)
                            ));
                        });
                        return (React.createElement("div", {key: rowName, className: "sp-group" + (row.color ? (' sp-' + row.color) : '') + (self.isActived(row) ? ' active' : ''), "data-label": row.label}, group));
                    }
                    else {
                        let iconRow = row.icon ? (React.createElement(FontAwesome, {name: row.icon})) : null;
                        let budRow = row.bud ? (React.createElement("div", {className: "bud"}, row.bud)) : null;
                        return (React.createElement("div", {key: rowName, className: "sp-item" + (self.isActived(row) ? ' active' : ''), onClick: () => {
                            if (row.url)
                                application.browserHistory.push(row.url);
                        }}, 
                            React.createElement("div", null, 
                                iconRow, 
                                row.label, 
                                budRow)
                        ));
                    }
                });
                return (React.createElement("div", {key: topName, className: "sp-item" + ((self.isActived(top) && !selected) || selected === top ? ' active' : '') + (self.isActived(top) && selected && selected !== top ? ' actived' : ''), onClick: () => {
                    self.state.selected = self.state.selected == top ? null : top;
                    self.forceUpdate();
                }}, 
                    React.createElement("label", null, 
                        iconTop, 
                        top.label, 
                        budTop), 
                    React.createElement("div", {className: "sp-submenu"}, children)));
            }
            else {
                return (React.createElement("div", {key: topName, className: "sp-item" + ((self.isActived(top) && !selected) || selected === top ? ' active' : '') + (self.isActived(top) && selected && selected !== top ? ' actived' : ''), onClick: () => {
                    self.state.selected = self.state.selected == top ? null : top;
                    self.forceUpdate();
                    if (top.url)
                        application.browserHistory.push(top.url);
                }}, 
                    React.createElement("div", null, 
                        iconTop, 
                        top.label, 
                        budTop)
                ));
            }
        });
        return (React.createElement("div", {className: "sp-layout"}, 
            React.createElement("div", null, 
                React.createElement("div", {className: "sp-logo", style: { display: this.state.leftMenu ? '' : 'none' }}, 
                    React.createElement("div", null, 
                        React.createElement("div", null, 
                            React.createElement("div", {className: "sp-logo-container"}, 
                                React.createElement("div", {className: "padding-left-2"}, 
                                    React.createElement("img", {src: Core_1.rest.getDir() + "images/logo.svg", alt: ""})
                                ), 
                                React.createElement("div", {className: "padding-left-d2 padding-right-2 sp-header"}, 
                                    React.createElement("div", null, "React MD")
                                ))
                        )
                    )
                ), 
                React.createElement("div", {className: "sp-top-menu"}, 
                    React.createElement("div", null, 
                        React.createElement("div", {className: "sp-item sp-big sp-square", onClick: this.toggleLeftMenu.bind(this)}, 
                            React.createElement("span", {className: "fa fa-th-list"})
                        ), 
                        React.createElement("div", {className: "sp-header padding-left-2"}), 
                        React.createElement("div", {className: "sp-space"}), 
                        React.createElement("div", {className: "sp-item"}, "Обратная связь"), 
                        React.createElement("div", {className: "sp-divider"}), 
                        React.createElement("div", {className: "sp-input"}, 
                            React.createElement(FontAwesome, {name: "search"}), 
                            React.createElement("input", {type: "text", placeholder: "Поиск..."})), 
                        React.createElement("div", {className: "sp-divider"}), 
                        React.createElement(Core_1.Popup, {position: "bottom left", toggleOnClick: true, closeOnEsc: true, closeOnOutsideClick: true, isHidden: !(application.user && application.user.notifications && application.user.notifications.length > 0), activeClassName: "active", element: (React.createElement("div", {className: "sp-item sp-avatar sp-square"}, 
                            React.createElement("img", {src: application.user && application.user.avatar_url ? application.user.avatar_url : "../../../../../../../images/avatar-empty.png"}), 
                            application.user && application.user.notifications && application.user.notifications.length > 0 ? (React.createElement("div", {className: "bud"}, application.user.notifications.length)) : null))}, 
                            React.createElement("div", {className: "min-width-3 width-full padding-d1"}, 
                                React.createElement(Core_1.StdAnim, {duration: 300, type: "fade"}, (application.user ? application.user.notifications : []).map((notification, index) => {
                                    return React.createElement(Core_1.Component, { key: index, content: notification, context: notification, page, application });
                                }))
                            )
                        ), 
                        React.createElement(Core_1.Dropdown, {position: "bottom left", openOnClick: true, closeOnEsc: true, closeOnOutsideClick: true, activeClassName: "open", element: (React.createElement("div", {className: "sp-item sp-big padding-right-4 sp-dropdown2-item"}, 
                            React.createElement(FontAwesome, {name: "ellipsis-v"})
                        ))}, 
                            React.createElement("div", {className: "sp-dropdown2-menu pull-right min-width-3"}, 
                                React.createElement("div", {className: "sp-header text micro thick padding-horizontal-2 padding-vertical-px8"}, application.user && application.user.username ? application.user.username : "Общие"), 
                                React.createElement("div", {className: "sp-item text micro thick padding-horizontal-d2 padding-vertical-px8"}, "Сводка"), 
                                React.createElement("div", {className: "sp-item text micro thick padding-horizontal-d2 padding-vertical-px8"}, "Настройки"), 
                                React.createElement("div", {className: "sp-header text micro thick padding-horizontal-2 padding-vertical-px8"}, "Система"), 
                                React.createElement("div", {className: "sp-item text micro thick padding-horizontal-d2 padding-vertical-px8"}, "Статистика"), 
                                React.createElement("div", {className: "sp-item text micro thick padding-horizontal-d2 padding-vertical-px8"}, "Логи"))
                        ))
                )), 
            React.createElement("div", null, 
                React.createElement("div", {className: "sp-left-menu", style: { display: this.state.leftMenu ? '' : 'none' }}, menu), 
                React.createElement("div", {className: "sp-vertical-layout"}, this.props.children))));
    }
}
Default.contextTypes = {
    router: React.PropTypes.object.isRequired
};
exports.Default = Default;
const RouteChanger_1 = require('../Core/Controls/RouteChanger');
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = (page, application, location, children) => {
    return (React.createElement(Default, {page: page, application: application}, 
        React.createElement(RouteChanger_1.RouteChanger, {duration: 500, location: location}, children)
    ));
};
//# sourceMappingURL=Default.js.map