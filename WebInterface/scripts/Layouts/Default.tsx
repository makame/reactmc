import * as React from 'react'

import FontAwesome = require('react-fontawesome')

import {
  RouteModel, ApplicationModel, Component, MapPro,
  Dropdown, Popup, StdAnim,
  rest
} from '../Core'

import { SheriffModel } from '../ISheriff'

export class DefaultProps {
  page: RouteModel
  application: SheriffModel
}

export class DefaultState {
  leftMenu: boolean = true
  selected?: Object = null
}

export class Default extends React.Component<DefaultProps, DefaultState> {

  static contextTypes = {
    router: React.PropTypes.object.isRequired
  }

  state = new DefaultState()

  toggleLeftMenu() {
    this.setState({leftMenu: !this.state.leftMenu});
  }

  isActived(item) {
    var active = false
    if (item.path) item.path.forEach((row)=>{
      if (this.context.router.isActive(row, item.strong)) active = true
    })
    return active
  }

  render() {
    let self = this

    const { page, application } = this.props

    const {selected} = this.state

    const menu = MapPro.map(application.menu, (top, topName) => {
      let iconTop = top.icon ? (<FontAwesome name={top.icon}/>) : null
      let budTop = top.bud ? (<div className="bud">{top.bud}</div>) : null

      if (top.children) {
        let children = MapPro.map(top.children, (row, rowName) => {
          if (row.children) {
            let group = MapPro.map(row.children, (col, colName) => {
              let iconCol = col.icon ? (<FontAwesome name={col.icon}/>) : null
              let budCol = col.bud ? (<div className="bud">{col.bud}</div>) : null

              return (
                <div key={colName} className={"sp-item" + (self.isActived(col) ? ' active' : '')} onClick={()=>{
                    if (col.url) application.browserHistory.push(col.url)
                  }
                }><div>{iconCol}{col.label}{budCol}</div></div>
              )
            })

            return (
              <div key={rowName} className={"sp-group" + (row.color ? (' sp-' + row.color) : '') + (self.isActived(row) ? ' active' : '')} data-label={row.label}>
                {group}
              </div>
            )
          }
          else {
            let iconRow = row.icon ? (<FontAwesome name={row.icon}/>) : null
            let budRow = row.bud ? (<div className="bud">{row.bud}</div>) : null

            return (
              <div key={rowName} className={"sp-item" + (self.isActived(row) ? ' active' : '')} onClick={()=>{
                  if (row.url) application.browserHistory.push(row.url)
                }
              }><div>{iconRow}{row.label}{budRow}</div></div>
            )
          }
        })

        return (
          <div key={topName} className={"sp-item" + ((self.isActived(top) && !selected) || selected === top ? ' active' : '') + (self.isActived(top) && selected && selected !== top ? ' actived' : '')} onClick={()=>{
              self.state.selected = self.state.selected == top ? null : top
              self.forceUpdate()
            }}>
            <label>{iconTop}{top.label}{budTop}</label>
            <div className="sp-submenu">
              {children}
            </div>
          </div>
        )
      }
      else {
        return (
          <div key={topName} className={"sp-item" + ((self.isActived(top) && !selected) || selected === top ? ' active' : '') + (self.isActived(top) && selected && selected !== top ? ' actived' : '')} onClick={()=>{
              self.state.selected = self.state.selected == top ? null : top
              self.forceUpdate()
              if (top.url) application.browserHistory.push(top.url)
            }}>
            <div>{iconTop}{top.label}{budTop}</div>
          </div>
        )
      }
    })

    return (
      <div className="sp-layout">
        <div>
          <div className="sp-logo" style={{display: this.state.leftMenu ? '' : 'none'}}>
            <div>
              <div>
                <div className="sp-logo-container">
                  <div className="padding-left-2">
                    <img src={rest.getDir() + "images/logo.svg"} alt=""/>
                  </div>
                  <div className="padding-left-d2 padding-right-2 sp-header">
                    <div>React MD</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="sp-top-menu">
            <div>
              <div className="sp-item sp-big sp-square" onClick={this.toggleLeftMenu.bind(this)}>
                <span className="fa fa-th-list"/>
              </div>
              <div className="sp-header padding-left-2"></div>
              <div className="sp-space"></div>
              <div className="sp-item">Обратная связь</div>
              <div className="sp-divider"></div>
              <div className="sp-input"><FontAwesome name="search" /><input type="text" placeholder="Поиск..."/></div>
              <div className="sp-divider"></div>
              <Popup
                position="bottom left"
                toggleOnClick
                closeOnEsc closeOnOutsideClick
                isHidden={!(application.user && application.user.notifications && application.user.notifications.length > 0)}
                activeClassName="active"
                element={
                  (
                    <div className="sp-item sp-avatar sp-square">
                      <img src={application.user && application.user.avatar_url ? application.user.avatar_url : "../../../../../../../images/avatar-empty.png"}/>
                      {application.user && application.user.notifications && application.user.notifications.length > 0 ? (<div className="bud">{application.user.notifications.length}</div>) : null}
                    </div>
                  )
                }
                >
                <div className="min-width-3 width-full padding-d1">
                  <StdAnim duration={300} type="fade">{
                      (application.user ? application.user.notifications : []).map((notification, index)=>{
                        return React.createElement(Component, { key: index, content: notification, context: notification, page, application })
                      })
                    }</StdAnim>
                </div>
              </Popup>
              <Dropdown
                position="bottom left"
                openOnClick
                closeOnEsc closeOnOutsideClick
                activeClassName="open"
                element={(
                  <div className="sp-item sp-big padding-right-4 sp-dropdown2-item">
                    <FontAwesome name="ellipsis-v"/>
                  </div>
                )}
                >
                <div className="sp-dropdown2-menu pull-right min-width-3">
                  <div className="sp-header text micro thick padding-horizontal-2 padding-vertical-px8">{application.user && application.user.username ? application.user.username : "Общие"}</div>
                  <div className="sp-item text micro thick padding-horizontal-d2 padding-vertical-px8">Сводка</div>
                  <div className="sp-item text micro thick padding-horizontal-d2 padding-vertical-px8">Настройки</div>
                  <div className="sp-header text micro thick padding-horizontal-2 padding-vertical-px8">Система</div>
                  <div className="sp-item text micro thick padding-horizontal-d2 padding-vertical-px8">Статистика</div>
                  <div className="sp-item text micro thick padding-horizontal-d2 padding-vertical-px8">Логи</div>
                </div>
              </Dropdown>
            </div>
          </div>
        </div>
        <div>
          <div className="sp-left-menu" style={{display: this.state.leftMenu ? '' : 'none'}}>
            {menu}
          </div>

          <div className="sp-vertical-layout">
            {this.props.children}
          </div>
        </div>
      </div>
    )
  }
}

import { RouteChanger } from '../Core/Controls/RouteChanger'

export default (page: RouteModel, application: ApplicationModel, location: any, children?: JSX.Element): JSX.Element => {
  return (
    <Default page={page} application={application as SheriffModel}>
      <RouteChanger duration={500} location={location}>
        {children}
      </RouteChanger>
    </Default>
  )
}