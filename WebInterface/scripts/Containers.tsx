import * as React from 'react'

import FontAwesome = require('react-fontawesome')

import { Map, IContainerModel, ContainerAdapter, ContextModel, RouteModel, ApplicationModel } from './Core'

export const containers: Map<IContainerModel> = {
  panel: {
    render: (model: ContainerAdapter, content: JSX.Element, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element => {
      let titleSetGet = model.parameters[0]

      let titleValue = titleSetGet.get(context, page, application)

      let title = titleValue ? (<div className="sp-pane-header">{titleValue}</div>) : null

      return (
        <div className={"sp-pane" + (model.class.length > 0 ? (' ' + model.class.join(' ')) : '')}>
          {title}
          {content}
        </div>
      )
    }
  },
  labeled: {
    render: (model: ContainerAdapter, content: JSX.Element, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element => {
      let titleSetGet = model.parameters[0]

      let titleValue = titleSetGet.get(context, page, application)

      let title = titleValue ? (<div className="padding-left-1 padding-bottom-d1 text mini thick">{titleValue}</div>) : null

      return (
        <div className={model.class.join(' ')}>
          {title}
          {content}
        </div>
      )
    }
  },
  div: {
    render: (model: ContainerAdapter, content: JSX.Element, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element => {
      return (
        <div className={model.class.join(' ')}>
          {content}
        </div>
      )
    }
  },
  accord: {
    render: (model: ContainerAdapter, content: JSX.Element, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element => {
      let titleSetGet = model.parameters[0]
      let visibleSetGet = model.parameters[1]

      let titleValue = titleSetGet.get(context, page, application)

      let visibleValue = visibleSetGet.get(context, page, application)

      let title = (
        <div className={"sp-pane-header sp-accord" + (visibleValue ? " active" : "")} onClick={(e)=>{
            visibleSetGet.set(!visibleValue, context, page, application)
            page.update()
          }}>
          {titleValue}
          <div className="sp-header-toolbar"><div className="sp-button"><FontAwesome name={visibleValue ? "chevron-up" : "chevron-down"}/></div></div>
        </div>
      )

      return (
        <div className={"sp-pane" + (model.class.length > 0 ? (' ' + model.class.join(' ')) : '')}>
          {title}
          {visibleValue ? content : null}
        </div>
      )
    }
  },
}
