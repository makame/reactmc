/// <reference types="react" />
import * as moment from 'moment';
import { ContentModel, ContextModel, RouteModel, ApplicationModel } from '../Core';
import { IMenuItemModel } from '../Core/Components/Menu';
export declare class MenuItemSpaceModel implements IMenuItemModel {
    render(name: string, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
}
export declare class MenuItemDividerModel implements IMenuItemModel {
    render(name: string, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
}
export interface IMenuItemHeaderModel {
    text: string;
}
export declare class MenuItemHeaderModel implements IMenuItemHeaderModel, IMenuItemModel {
    text: string;
    constructor(initializer?: IMenuItemHeaderModel);
    render(name: string, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
}
export interface IMenuItemInputModel {
    value?: string;
    icon?: string;
    placeholder?: string;
}
export declare class MenuItemInputModel implements IMenuItemInputModel, IMenuItemModel {
    value?: string;
    icon?: string;
    placeholder?: string;
    constructor(initializer?: IMenuItemInputModel);
    render(name: string, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
}
export interface IMenuItemButtonModel {
    label?: string;
    icon?: string;
    placeholder?: string;
    loading?: boolean;
    on_click: (context: ContextModel, page: RouteModel, application: ApplicationModel) => Promise<void> | void;
}
export declare class MenuItemButtonModel implements IMenuItemButtonModel, IMenuItemModel {
    label?: string;
    icon?: string;
    placeholder?: string;
    loading?: boolean;
    on_click: (context: ContextModel, page: RouteModel, application: ApplicationModel) => Promise<void> | void;
    constructor(initializer?: IMenuItemButtonModel);
    render(name: string, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
}
export interface IMenuItemDateModel {
    get?: (content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => moment.Moment;
    set?: (newValue: moment.Moment, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => void;
    value?: moment.Moment;
    icon?: string;
}
export declare class MenuItemDateModel implements IMenuItemDateModel, IMenuItemModel {
    get?: (content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => moment.Moment;
    set?: (newValue: moment.Moment, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => void;
    value?: moment.Moment;
    icon?: string;
    constructor(initializer?: IMenuItemDateModel);
    render(name: string, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
}
export interface MenuItemEnumModelItem {
    id: any;
    name: string;
    icon?: string;
}
export interface IMenuItemEnumModel {
    get?: (item: IMenuItemModel, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => MenuItemEnumModelItem;
    set?: (item: IMenuItemModel, newValue: MenuItemEnumModelItem, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => void;
    value?: MenuItemEnumModelItem;
    enums?: MenuItemEnumModelItem[];
}
export declare class MenuItemEnumModel implements IMenuItemEnumModel, IMenuItemModel {
    get?: (item: IMenuItemModel, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => MenuItemEnumModelItem;
    set?: (item: IMenuItemModel, newValue: MenuItemEnumModelItem, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => void;
    value?: MenuItemEnumModelItem;
    enums?: MenuItemEnumModelItem[];
    constructor(initializer?: IMenuItemEnumModel);
    render(name: string, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
}
