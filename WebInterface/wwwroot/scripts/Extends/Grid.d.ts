/// <reference types="react" />
import { IValueLink, Map, ContentModel, ContextModel, RouteModel, ApplicationModel, ISelectData, SelectDataType, SelectSearchType, IMSelectData } from '../Core';
import { GridMessages, IGridItemFilter, IGridItemModel, GridItemModel } from '../Core/Components/Grid';
export interface IGridItemTriggerModel extends IGridItemModel<boolean> {
}
export declare class GridItemTriggerModel extends GridItemModel<boolean> implements IGridItemTriggerModel {
    constructor(initializer?: IGridItemTriggerModel);
    filters: Map<IGridItemFilter<boolean>>;
    render(value: boolean, field: GridItemModel<boolean>, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
    create_render(valueLink: IValueLink<boolean>, field: GridItemModel<boolean>, messages: GridMessages, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
    edit_render(valueLink: IValueLink<boolean>, field: GridItemModel<boolean>, messages: GridMessages, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
}
export interface IGridItemTextModel extends IGridItemModel<string> {
    icon?: string;
    placeholder?: string;
}
export declare class GridItemTextModel extends GridItemModel<string> implements IGridItemTextModel {
    icon?: string;
    placeholder?: string;
    classEdit: string;
    classCreate: string;
    constructor(initializer?: IGridItemTextModel);
    filters: Map<IGridItemFilter<string>>;
    render(value: string, field: GridItemModel<string>, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
    create_render(valueLink: IValueLink<string>, field: GridItemModel<string>, messages: GridMessages, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
    edit_render(valueLink: IValueLink<string>, field: GridItemModel<string>, messages: GridMessages, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
}
export interface IGridItemSelectModel extends IGridItemModel<ISelectData> {
    icon?: string;
    class?: string;
    data?: ISelectData[];
    data_type?: SelectDataType;
    search?: SelectSearchType;
    request?: (search_string: string) => Promise<ISelectData[]> | ISelectData[];
    default?: ISelectData;
    as_input?: boolean;
    render_element?: (value: ISelectData, clear: () => void, updating: boolean) => JSX.Element;
}
export declare class GridItemSelectModel extends GridItemModel<ISelectData> implements IGridItemSelectModel {
    icon?: string;
    class?: string;
    classEdit: string;
    classCreate: string;
    data?: ISelectData[];
    data_type?: SelectDataType;
    search?: SelectSearchType;
    request?: (search_string: string) => Promise<ISelectData[]> | ISelectData[];
    default?: ISelectData;
    as_input?: boolean;
    render_element?: (value: ISelectData, clear: () => void, updating: boolean) => JSX.Element;
    constructor(initializer?: IGridItemSelectModel);
    filters: Map<IGridItemFilter<ISelectData>>;
    render(value: ISelectData, field: GridItemModel<ISelectData>, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
    create_render(valueLink: IValueLink<ISelectData>, field: GridItemModel<ISelectData>, messages: GridMessages, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
    edit_render(valueLink: IValueLink<ISelectData>, field: GridItemModel<ISelectData>, messages: GridMessages, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
}
export interface IGridItemMSelectModel extends IGridItemModel<IMSelectData[]> {
    icon?: string;
    class?: string;
    data?: IMSelectData[];
    data_type?: SelectDataType;
    search?: SelectSearchType;
    request?: (search_string: string) => Promise<IMSelectData[]> | IMSelectData[];
    default?: IMSelectData[];
    as_input?: boolean;
    render_element?: (value: IMSelectData[], clear: () => void, updating: boolean) => JSX.Element;
}
export declare class GridItemMSelectModel extends GridItemModel<IMSelectData[]> implements IGridItemMSelectModel {
    icon?: string;
    class?: string;
    classEdit: string;
    classCreate: string;
    data?: IMSelectData[];
    data_type?: SelectDataType;
    search?: SelectSearchType;
    request?: (search_string: string) => Promise<IMSelectData[]> | IMSelectData[];
    default?: IMSelectData[];
    as_input?: boolean;
    render_element?: (value: IMSelectData[], clear: () => void, updating: boolean) => JSX.Element;
    constructor(initializer?: IGridItemSelectModel);
    filters: Map<IGridItemFilter<IMSelectData[]>>;
    render(value: IMSelectData[], field: GridItemModel<IMSelectData[]>, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
    create_render(valueLink: IValueLink<IMSelectData[]>, field: GridItemModel<IMSelectData[]>, messages: GridMessages, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
    edit_render(valueLink: IValueLink<IMSelectData[]>, field: GridItemModel<IMSelectData[]>, messages: GridMessages, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
}
