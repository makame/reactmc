/// <reference types="react" />
import { IValueLink, Map, ContentModel, ContextModel, RouteModel, ApplicationModel, ISelectData, SelectDataType, SelectSearchType, IMSelectData } from '../Core';
import { IFormLink, IFormItemModel, FormItemModel } from '../Core/Components/Form';
export interface IFormItemGroupModel extends IFormItemModel<Map<any>> {
    items: Map<FormItemModel<any>>;
}
export declare class FormItemGroupModel extends FormItemModel<Map<any>> {
    items: Map<FormItemModel<any>>;
    constructor(initializer?: IFormItemGroupModel);
    render(name: string, valueLink: IValueLink<Map<any>>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel, containerFactory: (contaner: string, inner: JSX.Element, context: ContextModel) => JSX.Element): JSX.Element;
}
export interface IFormItemInputAddonTextModel {
    icon?: string;
    text?: string;
    class?: string;
}
export interface IFormItemInputAddonModelRender {
    render(name: string, field: FormItemModel<string>, valueLink: IValueLink<string>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
}
export declare class FormItemInputAddonTextModel implements IFormItemInputAddonModelRender, IFormItemInputAddonTextModel {
    icon?: string;
    text?: string;
    class?: string;
    constructor(initializer?: IFormItemInputAddonTextModel);
    render(name: string, field: FormItemModel<string>, valueLink: IValueLink<string>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
}
export interface IFormItemInputAddonButtonModel {
    icon?: string;
    text?: string;
    class?: string;
}
export declare class FormItemInputAddonButtonModel implements IFormItemInputAddonModelRender, IFormItemInputAddonButtonModel {
    icon?: string;
    text?: string;
    on_click?: (name: string, field: FormItemModel<string>, valueLink: IValueLink<string>, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => Promise<void> | void;
    loading?: boolean;
    class?: string;
    enabled?(name: string, field: FormItemModel<string>, valueLink: IValueLink<string>, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): boolean;
    constructor(initializer?: IFormItemInputAddonButtonModel);
    render(name: string, field: FormItemModel<string>, valueLink: IValueLink<string>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
}
export interface IFormItemInputModel extends IFormItemModel<string> {
    left?: Map<IFormItemInputAddonModelRender>;
    right?: Map<IFormItemInputAddonModelRender>;
    placeholder?: string;
    icon?: string;
    class?: string;
    type?: string;
}
export declare class FormItemInputModel extends FormItemModel<string> implements IFormItemInputModel {
    left?: Map<IFormItemInputAddonModelRender>;
    right?: Map<IFormItemInputAddonModelRender>;
    placeholder?: string;
    icon?: string;
    class?: string;
    type?: string;
    constructor(initializer?: IFormItemInputModel);
    render(name: string, valueLink: IValueLink<string>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
}
export interface IFormItemRowAddonTextModel {
    icon?: string;
    text?: string;
    class?: string;
}
export interface IFormItemRowAddonModelRender {
    render(name: string, field: FormItemModel<Map<any>>, valueLink: IValueLink<Map<any>>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
}
export declare class FormItemRowAddonTextModel implements IFormItemRowAddonModelRender, IFormItemRowAddonTextModel {
    icon?: string;
    text?: string;
    class?: string;
    constructor(initializer?: IFormItemRowAddonTextModel);
    render(name: string, field: FormItemModel<Map<any>>, valueLink: IValueLink<Map<any>>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
}
export interface IFormItemRowAddonButtonModel {
    icon?: string;
    text?: string;
    on_click?: (name: string, field: FormItemModel<Map<any>>, valueLink: IValueLink<Map<any>>, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => Promise<void> | void;
    loading?: boolean;
    class?: string;
    enabled?(name: string, field: FormItemModel<Map<any>>, valueLink: IValueLink<Map<any>>, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): boolean;
}
export declare class FormItemRowAddonButtonModel implements IFormItemRowAddonModelRender, IFormItemRowAddonButtonModel {
    icon?: string;
    text?: string;
    on_click?: (name: string, field: FormItemModel<Map<any>>, valueLink: IValueLink<Map<any>>, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => Promise<void> | void;
    loading?: boolean;
    class?: string;
    enabled?(name: string, field: FormItemModel<Map<any>>, valueLink: IValueLink<Map<any>>, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): boolean;
    render(name: string, field: FormItemModel<Map<any>>, valueLink: IValueLink<Map<any>>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
}
export interface IFormItemRowAddonInputModel {
    icon?: string;
    placeholder?: string;
    class?: string;
    type?: string;
    enabled?(name: string, field: FormItemModel<Map<any>>, valueLink: IValueLink<Map<any>>, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): boolean;
}
export declare class FormItemRowAddonInputModel implements IFormItemRowAddonModelRender, IFormItemRowAddonInputModel {
    icon?: string;
    placeholder?: string;
    class?: string;
    type?: string;
    render(name: string, field: FormItemModel<Map<any>>, valueLink: IValueLink<Map<any>>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
}
export declare class FormItemRowModel extends FormItemModel<Map<any>> {
    items?: Map<IFormItemRowAddonModelRender>;
    class?: string;
    render(name: string, valueLink: IValueLink<Map<any>>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
}
export interface IFormItemButtonModelRender {
    if?: (name: string, field: FormItemModel<Map<any>>, valueLink: IValueLink<Map<any>>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => boolean;
    render(name: string, field: FormItemModel<Map<any>>, valueLink: IValueLink<Map<any>>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
}
export interface IFormItemButtonModel {
    icon?: string;
    text?: string;
    on_click?: (name: string, field: FormItemModel<Map<any>>, valueLink: IValueLink<Map<any>>, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => Promise<void> | void;
    loading?: boolean;
    class?: string;
    enabled?(name: string, field: FormItemModel<Map<any>>, valueLink: IValueLink<Map<any>>, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): boolean;
}
export declare class FormItemButtonModel implements IFormItemButtonModelRender, IFormItemButtonModel {
    icon?: string;
    text?: string;
    on_click?: (name: string, field: FormItemModel<Map<any>>, valueLink: IValueLink<Map<any>>, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => Promise<void> | void;
    loading?: boolean;
    class?: string;
    if?: (name: string, field: FormItemModel<Map<any>>, valueLink: IValueLink<Map<any>>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => boolean;
    enabled?(name: string, field: FormItemModel<Map<any>>, valueLink: IValueLink<Map<any>>, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): boolean;
    constructor(initializer?: IFormItemButtonModel);
    render(name: string, field: FormItemModel<Map<any>>, valueLink: IValueLink<Map<any>>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
}
export interface IFormItemButtonsModel extends IFormItemModel<Map<any>> {
    items?: Map<IFormItemButtonModelRender>;
    class?: string;
}
export declare class FormItemButtonsModel extends FormItemModel<Map<any>> implements IFormItemButtonsModel {
    items?: Map<IFormItemButtonModelRender>;
    class?: string;
    constructor(initializer?: IFormItemButtonsModel);
    render(name: string, valueLink: IValueLink<Map<any>>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
}
export interface IFormItemTriggerModel extends IFormItemModel<boolean> {
    icon?: string;
    class?: string;
    text?: string;
}
export declare class FormItemTriggerModel extends FormItemModel<boolean> implements IFormItemTriggerModel {
    icon?: string;
    class?: string;
    text?: string;
    constructor(initializer?: IFormItemTriggerModel);
    render(name: string, valueLink: IValueLink<boolean>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
}
export interface IFormItemSelectAddonTextModel {
    icon?: string;
    text?: string;
    class?: string;
}
export interface IFormItemSelectAddonModelRender {
    render(name: string, field: FormItemModel<ISelectData>, valueLink: IValueLink<ISelectData>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
}
export declare class FormItemSelectAddonTextModel implements IFormItemSelectAddonModelRender, IFormItemSelectAddonTextModel {
    icon?: string;
    text?: string;
    class?: string;
    constructor(initializer?: IFormItemSelectAddonTextModel);
    render(name: string, field: FormItemModel<ISelectData>, valueLink: IValueLink<ISelectData>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
}
export interface IFormItemSelectAddonButtonModel {
    icon?: string;
    text?: string;
    class?: string;
}
export declare class FormItemSelectAddonButtonModel implements IFormItemSelectAddonModelRender, IFormItemSelectAddonButtonModel {
    icon?: string;
    text?: string;
    on_click?: (name: string, field: FormItemModel<ISelectData>, valueLink: IValueLink<ISelectData>, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => Promise<void> | void;
    loading?: boolean;
    class?: string;
    enabled?(name: string, field: FormItemModel<ISelectData>, valueLink: IValueLink<ISelectData>, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): boolean;
    constructor(initializer?: IFormItemSelectAddonButtonModel);
    render(name: string, field: FormItemModel<ISelectData>, valueLink: IValueLink<ISelectData>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
}
export interface IFormItemSelectModel extends IFormItemModel<ISelectData> {
    left?: Map<IFormItemSelectAddonModelRender>;
    right?: Map<IFormItemSelectAddonModelRender>;
    icon?: string;
    class?: string;
    data?: ISelectData[];
    data_type?: SelectDataType;
    search?: SelectSearchType;
    request?: (search_string: string) => Promise<ISelectData[]> | ISelectData[];
    default?: ISelectData;
    asInput?: boolean;
    render_element?: (value: ISelectData, clear: () => void, updating: boolean) => JSX.Element;
}
export declare class FormItemSelectModel extends FormItemModel<ISelectData> implements IFormItemSelectModel {
    left?: Map<IFormItemSelectAddonModelRender>;
    right?: Map<IFormItemSelectAddonModelRender>;
    icon?: string;
    class?: string;
    data?: ISelectData[];
    data_type?: SelectDataType;
    search?: SelectSearchType;
    request?: (search_string: string) => Promise<ISelectData[]> | ISelectData[];
    default?: ISelectData;
    as_input?: boolean;
    render_element?: (value: ISelectData, clear: () => void, updating: boolean) => JSX.Element;
    constructor(initializer?: IFormItemSelectModel);
    render(name: string, valueLink: IValueLink<ISelectData>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
}
export interface IFormItemMSelectAddonTextModel {
    icon?: string;
    text?: string;
    class?: string;
}
export interface IFormItemMSelectAddonModelRender {
    render(name: string, field: FormItemModel<IMSelectData[]>, valueLink: IValueLink<IMSelectData[]>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
}
export declare class FormItemMSelectAddonTextModel implements IFormItemMSelectAddonModelRender, IFormItemMSelectAddonTextModel {
    icon?: string;
    text?: string;
    class?: string;
    constructor(initializer?: IFormItemMSelectAddonTextModel);
    render(name: string, field: FormItemModel<IMSelectData[]>, valueLink: IValueLink<IMSelectData[]>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
}
export interface IFormItemMSelectAddonButtonModel {
    icon?: string;
    text?: string;
    class?: string;
}
export declare class FormItemMSelectAddonButtonModel implements IFormItemMSelectAddonModelRender, IFormItemMSelectAddonButtonModel {
    icon?: string;
    text?: string;
    on_click?: (name: string, field: FormItemModel<IMSelectData[]>, valueLink: IValueLink<IMSelectData[]>, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => Promise<void> | void;
    loading?: boolean;
    class?: string;
    enabled?(name: string, field: FormItemModel<IMSelectData[]>, valueLink: IValueLink<IMSelectData[]>, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): boolean;
    constructor(initializer?: IFormItemMSelectAddonButtonModel);
    render(name: string, field: FormItemModel<IMSelectData[]>, valueLink: IValueLink<IMSelectData[]>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
}
export interface IFormItemMSelectModel extends IFormItemModel<IMSelectData[]> {
    left?: Map<IFormItemMSelectAddonModelRender>;
    right?: Map<IFormItemMSelectAddonModelRender>;
    icon?: string;
    class?: string;
    data?: IMSelectData[];
    data_type?: SelectDataType;
    search?: SelectSearchType;
    request?: (search_string: string) => Promise<IMSelectData[]> | IMSelectData[];
    default?: IMSelectData[];
    asInput?: boolean;
    render_element?: (value: IMSelectData[], clear: () => void, updating: boolean) => JSX.Element;
}
export declare class FormItemMSelectModel extends FormItemModel<IMSelectData[]> implements IFormItemMSelectModel {
    left?: Map<IFormItemMSelectAddonModelRender>;
    right?: Map<IFormItemMSelectAddonModelRender>;
    icon?: string;
    class?: string;
    data?: IMSelectData[];
    data_type?: SelectDataType;
    search?: SelectSearchType;
    request?: (search_string: string) => Promise<IMSelectData[]> | IMSelectData[];
    default?: IMSelectData[];
    as_input?: boolean;
    render_element?: (value: IMSelectData[], clear: () => void, updating: boolean) => JSX.Element;
    constructor(initializer?: IFormItemMSelectModel);
    render(name: string, valueLink: IValueLink<IMSelectData[]>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
}
