import { Map } from '../Core';
export declare class NavigationModel {
    label: string;
    icon?: string;
    url?: string;
    path?: string[];
    strong?: boolean;
    children?: Map<NavigationModel>;
    color?: string;
    bud?: number | string;
}
export declare const Navigation: Map<NavigationModel>;
