export declare class Client {
    baseUrl: string;
    beforeSend: any;
    protected jsonParseReviver: ((key: string, value: any) => any) | undefined;
    constructor(baseUrl?: string);
    apiArcherGetByIdByIdPost(id: number): Promise<string>;
    private apiArcherGetByIdByIdPostWithCallbacks(id, onSuccess?, onFail?);
    private processApiArcherGetByIdByIdPostWithCallbacks(url, xhr, onSuccess?, onFail?);
    protected processApiArcherGetByIdByIdPost(xhr: any): string;
    apiArcherDocUpdatePost(doc: DocUpdateModel): Promise<DocUpdateModel>;
    private apiArcherDocUpdatePostWithCallbacks(doc, onSuccess?, onFail?);
    private processApiArcherDocUpdatePostWithCallbacks(url, xhr, onSuccess?, onFail?);
    protected processApiArcherDocUpdatePost(xhr: any): DocUpdateModel;
    apiArcherTestExceptionPost(): Promise<string>;
    private apiArcherTestExceptionPostWithCallbacks(onSuccess?, onFail?);
    private processApiArcherTestExceptionPostWithCallbacks(url, xhr, onSuccess?, onFail?);
    protected processApiArcherTestExceptionPost(xhr: any): string;
    apiArcherDocNewPost(value: string): Promise<string>;
    private apiArcherDocNewPostWithCallbacks(value, onSuccess?, onFail?);
    private processApiArcherDocNewPostWithCallbacks(url, xhr, onSuccess?, onFail?);
    protected processApiArcherDocNewPost(xhr: any): string;
    apiArcherGetEmployeesPost(): Promise<EmployeeResult>;
    private apiArcherGetEmployeesPostWithCallbacks(onSuccess?, onFail?);
    private processApiArcherGetEmployeesPostWithCallbacks(url, xhr, onSuccess?, onFail?);
    protected processApiArcherGetEmployeesPost(xhr: any): EmployeeResult;
    protected throwException(message: string, status: number, response: string, result?: any): any;
}
export declare class DocUpdateModel {
    user?: string;
    pass?: string;
    constructor(data?: any);
    static fromJS(data: any): DocUpdateModel;
    toJS(data?: any): any;
    toJSON(): string;
    clone(): DocUpdateModel;
}
export declare class EmployeeResult {
    array?: Employee[];
    total?: number;
    constructor(data?: any);
    static fromJS(data: any): EmployeeResult;
    toJS(data?: any): any;
    toJSON(): string;
    clone(): EmployeeResult;
}
export declare class Employee {
    id?: number;
    name?: string;
    constructor(data?: any);
    static fromJS(data: any): Employee;
    toJS(data?: any): any;
    toJSON(): string;
    clone(): Employee;
}
export declare class SwaggerException extends Error {
    message: string;
    status: number;
    response: string;
    result?: any;
    constructor(message: string, status: number, response: string, result?: any);
}
