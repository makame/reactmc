/// <reference types="react" />
import * as React from 'react';
import { RouteModel, ApplicationModel } from '../Core';
import { SheriffModel } from '../ISheriff';
export declare class DefaultProps {
    page: RouteModel;
    application: SheriffModel;
}
export declare class DefaultState {
    leftMenu: boolean;
    selected?: Object;
}
export declare class Default extends React.Component<DefaultProps, DefaultState> {
    static contextTypes: {
        router: React.Validator<any>;
    };
    state: DefaultState;
    toggleLeftMenu(): void;
    isActived(item: any): boolean;
    render(): JSX.Element;
}
declare var _default: (page: RouteModel, application: ApplicationModel, location: any, children?: JSX.Element) => JSX.Element;
export default _default;
