/// <reference types="react" />
import * as React from 'react';
export declare class ClearProps {
    location?: any;
}
export declare class ClearState {
}
export default class Clear extends React.Component<ClearProps, ClearState> {
    render(): JSX.Element;
}
