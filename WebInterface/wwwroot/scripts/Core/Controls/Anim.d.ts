/// <reference types="jquery" />
/// <reference types="velocity-animate" />
/// <reference types="react" />
import * as React from 'react';
export declare class AnimChildProps {
    willAppear?: (el: JQuery, callback: () => void) => void;
    willUnmount?: (el: JQuery) => void;
    didAppear?: (el: JQuery) => void;
    willEnter?: (el: JQuery, callback: () => void) => void;
    didEnter?: (el: JQuery) => void;
    willLeave?: (el: JQuery, callback: () => void) => void;
    didLeave?: (el: JQuery) => void;
}
export declare class AnimChildState {
}
export declare class AnimChild extends React.Component<AnimChildProps, AnimChildState> {
    componentWillAppear(callback: any): void;
    componentWillUnmount(): void;
    componentDidAppear(): void;
    componentWillEnter(callback: any): void;
    componentDidEnter(): void;
    componentWillLeave(callback: any): void;
    componentDidLeave(): void;
    render(): React.ReactElement<any>;
}
export declare class AnimProps {
    willAppear?: (el: JQuery, callback: () => void) => void;
    willUnmount?: (el: JQuery) => void;
    didAppear?: (el: JQuery) => void;
    willEnter?: (el: JQuery, callback: () => void) => void;
    didEnter?: (el: JQuery) => void;
    willLeave?: (el: JQuery, callback: () => void) => void;
    didLeave?: (el: JQuery) => void;
    className?: string;
}
export declare class AnimState {
}
export declare class Anim extends React.Component<AnimProps, AnimState> {
    render(): JSX.Element;
}
