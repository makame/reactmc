/// <reference types="react" />
import * as React from 'react';
import { Portal, PortalProps } from './Portal';
export declare class PopupProps extends PortalProps {
    level?: number;
    closeOnOutsideClick?: boolean;
    toggleOnClick?: boolean;
    openOnClick?: boolean;
    closeOnClick?: boolean;
    toggleOnContextMenu?: boolean;
    openOnContextMenu?: boolean;
    closeOnContextMenu?: boolean;
    closeOnOutMove?: boolean;
    openOnOverMove?: boolean;
    openOnFocus?: boolean;
    openOnBlur?: boolean;
    closeOnBlur?: boolean;
    closeOnOutMovePop?: boolean;
    padding_window?: number;
    offset?: number;
    timeout?: number;
    activeClassName?: string;
    delay?: number;
    onShow?: () => void;
    onClose?: () => void;
    positionEl?: string;
    position?: string;
}
export declare class PopupState {
    timer?: number;
}
export declare class Popup extends React.Component<PopupProps, PopupState> {
    static defaultProps: PopupProps;
    state: PopupState;
    refs: {
        [key: string]: any;
        portal: Portal;
        popup: HTMLElement;
        barrow: HTMLElement;
        farrow: HTMLElement;
    };
    closePortal(): void;
    openPortal(): void;
    didUpdate(popupNode: any, elementNode: any): void;
    handleWrapperClick(e: any): void;
    handleWrapperContextMenu(e: any): void;
    handleWrapperOver(e: any): void;
    handleWrapperOut(e: any): void;
    handleWrapperFocus(e: any): void;
    handleWrapperBlur(e: any): void;
    handleOutsideClick(e: any): void;
    render(): JSX.Element;
}
