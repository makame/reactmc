/// <reference types="react" />
import * as React from 'react';
import * as moment from 'moment';
import { IValueLink } from '../Main';
import { Calendar } from './Calendar';
import { ConnectionPortal } from './Portal';
export declare class MomentPeriod {
    start?: moment.Moment;
    end?: moment.Moment;
}
export declare class PeriodPickerProps {
    valueLink?: IValueLink<MomentPeriod>;
    render?: (value: MomentPeriod, clear: () => void, format: string) => JSX.Element;
    className?: string;
    weeksPerMonth?: number;
    format?: string;
    weeks?: string[];
    locale?: string;
    min?: moment.Moment;
    max?: moment.Moment;
}
export declare class PeriodPickerState {
    connectionPortal: ConnectionPortal;
}
export declare class PeriodPicker extends React.Component<PeriodPickerProps, PeriodPickerState> {
    static defaultProps: PeriodPickerProps;
    state: PeriodPickerState;
    refs: {
        [key: string]: any;
        calendarStart: Calendar;
        calendarEnd: Calendar;
    };
    clear(): void;
    getElement(): JSX.Element;
    render(): JSX.Element;
}
