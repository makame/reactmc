/// <reference types="react" />
import * as React from 'react';
import { IValueLink } from '../Main';
import { ConnectionPortal } from './Portal';
export declare type SelectDataType = 'remote' | 'local';
export declare type SelectSearchType = 'none' | 'local' | 'remote';
export interface ISelectData {
    id: string;
    name: string;
}
export declare class SelectProps {
    data?: ISelectData[];
    data_type?: SelectDataType;
    search?: SelectSearchType;
    request?: (search_string: string) => Promise<ISelectData[]> | ISelectData[];
    default?: ISelectData;
    asInput?: boolean;
    render?: (value: ISelectData, clear: () => void, updating: boolean) => JSX.Element;
    valueLink: IValueLink<ISelectData>;
    icon?: string;
    className?: string;
    left?: JSX.Element[];
    right?: JSX.Element[];
    level?: number;
}
export declare class SelectState {
    connectionPortal: ConnectionPortal;
    parsed_data?: ISelectData[];
    updating: boolean;
    search_string: string;
    need_update_data: boolean;
    value: ISelectData;
}
export declare class Select extends React.Component<SelectProps, SelectState> {
    static defaultProps: SelectProps;
    state: SelectState;
    id: string;
    isMounted(): boolean;
    recurse_search_local_data(): void;
    recurse_search_remote_data(): void;
    recurse_search_remote_local_data(): void;
    parse_data(): void;
    requestChange(event: any): void;
    arraysEqual(a: any, b: any): boolean;
    componentWillReceiveProps(nextProps: any, nextContext: any): void;
    componentWillMount(): void;
    clear(): void;
    selectItem(item: any): void;
    getElement(): JSX.Element;
    render(): JSX.Element;
}
