/// <reference types="react" />
import * as React from 'react';
export declare class ContentEditableProps {
    id?: string;
    html?: string;
    tagName?: string;
    title?: string;
    useTitle?: boolean;
    disabled?: boolean;
    className?: string;
    onKeyDown?: (e: any) => void;
    onKeyUp?: (e: any) => void;
    onInput?: (e: any) => void;
    onFocus?: (e: any) => void;
    onBlur?: (e: any) => void;
    onChange?: (e: any) => void;
}
export declare class ContentEditableState {
    focused: boolean;
    htmlEl?: Element;
    rp?: () => void;
    lastHtml?: string;
}
export declare class ContentEditable extends React.Component<ContentEditableProps, ContentEditableState> {
    static defaultProps: ContentEditableProps;
    state: ContentEditableState;
    shouldComponentUpdate(nextProps: any, nextState: any): boolean;
    componentWillUpdate(): void;
    componentDidUpdate(): void;
    onInput(evt: any): void;
    onFocus(evt: any): void;
    onBlur(evt: any): void;
    emitChange(evt: any): void;
    saveCaretPosition(context: any): () => void;
    getTextNodeAtPosition(root: any, index: any): {
        node: any;
        position: any;
    };
    render(): React.DOMElement<{
        id: string;
        ref: (e: Element) => Element;
        onInput: any;
        onBlur: any;
        className: string;
        onFocus: any;
        contentEditable: boolean;
        dangerouslySetInnerHTML: {
            __html: string;
        };
    }, Element>;
}
