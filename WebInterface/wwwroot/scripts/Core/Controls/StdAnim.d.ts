/// <reference types="react" />
import * as React from 'react';
export declare type StdAnimType = "fade" | "fadeRScaleX" | "fadeScaleX" | "tags" | "spec";
export declare class StdAnimProps {
    duration?: number;
    type?: string;
    display?: StdAnimType;
    className?: string;
}
export declare class StdAnim extends React.Component<StdAnimProps, {}> {
    static defaultProps: StdAnimProps;
    render(): JSX.Element;
}
