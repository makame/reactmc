/// <reference types="react" />
import * as React from 'react';
import { Portal, PortalProps } from './Portal';
export declare class LoadingProps extends PortalProps {
    level?: number;
}
export declare class Loading extends React.Component<LoadingProps, {}> {
    static defaultProps: LoadingProps;
    refs: {
        [key: string]: any;
        portal: Portal;
    };
    closePortal(): void;
    openPortal(): void;
    componentDidMount(): void;
    componentWillUnmount(): void;
    render(): JSX.Element;
}
