/// <reference types="react" />
import * as React from 'react';
import { IValueLink } from '../Main';
export declare class TriggerProps {
    valueLink: IValueLink<boolean>;
    className?: string;
    icon?: string;
    text?: string;
}
export declare class Trigger extends React.Component<TriggerProps, {}> {
    static defaultProps: TriggerProps;
    id: string;
    requestChange(event: any): void;
    render(): JSX.Element;
}
