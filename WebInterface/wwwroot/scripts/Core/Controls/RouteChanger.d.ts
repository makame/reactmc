/// <reference types="react" />
import * as React from 'react';
export declare class RouteChangerChildProps {
    animateDuration: number;
    location: any;
    shouldUpdate: boolean;
}
export declare class RouteChangerChild extends React.Component<RouteChangerChildProps, {}> {
    id: string;
    shouldComponentUpdate(nextProps: any): boolean;
    componentWillAppear(callback: any): void;
    componentDidAppear(): void;
    componentWillEnter(callback: any): void;
    componentDidMount(): void;
    componentDidEnter(): void;
    componentWillLeave(callback: any): void;
    componentWillUnmount(): void;
    componentDidLeave(): void;
    _animateInDelay(callback: any): void;
    _animateIn(callback: any): void;
    _animateOut(callback: any): void;
    render(): React.ReactElement<any>;
}
export declare class RouteChangerProps {
    duration?: number;
    location: any;
}
export declare class RouteChangerState {
    previousPathname?: string;
    shouldUpdate: boolean;
}
export declare class RouteChanger extends React.Component<RouteChangerProps, RouteChangerState> {
    static defaultProps: RouteChangerProps;
    state: RouteChangerState;
    componentWillReceiveProps(nextProps: any, nextContext: any): void;
    componentDidUpdate(): void;
    render(): JSX.Element;
}
