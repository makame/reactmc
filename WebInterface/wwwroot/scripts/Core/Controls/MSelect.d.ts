/// <reference types="react" />
import * as React from 'react';
import { IValueLink } from '../Main';
import { ConnectionPortal } from './Portal';
export declare type MSelectDataType = 'remote' | 'local';
export declare type MSelectSearchType = 'none' | 'local' | 'remote';
export interface IMSelectData {
    id: string;
    name: string;
}
export declare class MSelectProps {
    data?: IMSelectData[];
    data_type?: MSelectDataType;
    search?: MSelectSearchType;
    request?: (search_string: string) => Promise<IMSelectData[]> | IMSelectData[];
    valueLink: IValueLink<IMSelectData[]>;
    render?: (value: IMSelectData[], clear: () => void, updating: boolean) => JSX.Element;
    asInput?: boolean;
    icon?: string;
    className?: string;
    left?: JSX.Element[];
    right?: JSX.Element[];
}
export declare class MSelectState {
    connectionPortal: ConnectionPortal;
    parsed_data?: IMSelectData[];
    updating: boolean;
    search_string: string;
    need_update_data: boolean;
    value: IMSelectData[];
}
export declare class MSelect extends React.Component<MSelectProps, MSelectState> {
    static defaultProps: MSelectProps;
    state: MSelectState;
    id: string;
    isMounted(): boolean;
    recurse_search_local_data(): void;
    recurse_search_remote_data(): void;
    recurse_search_remote_local_data(): void;
    parse_data(): void;
    requestChange(event: any): void;
    arraysEqual(a: any, b: any): boolean;
    componentWillReceiveProps(nextProps: any, nextContext: any): void;
    componentWillMount(): void;
    clear(): void;
    selectItem(item: any): void;
    getElement(): JSX.Element;
    render(): JSX.Element;
}
