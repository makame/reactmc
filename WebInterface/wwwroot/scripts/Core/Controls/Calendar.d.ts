/// <reference types="react" />
import * as React from 'react';
import * as moment from 'moment';
import { IValueLink } from '../Main';
export declare class Dates {
    title: string;
    value: moment.Moment;
    inMonth: boolean;
    isInInterval: boolean;
    selected: boolean;
}
export declare class CalendarProps {
    valueLink: IValueLink<moment.Moment>;
    format?: string;
    weeks?: string[];
    locale?: string;
    min?: moment.Moment;
    max?: moment.Moment;
    weeksPerMonth?: number;
    className?: string;
}
export declare class CalendarState {
    value?: moment.Moment;
    input?: string;
    now?: moment.Moment;
    dates?: Dates[][];
}
export declare class Calendar extends React.Component<CalendarProps, CalendarState> {
    static defaultProps: CalendarProps;
    state: CalendarState;
    id: string;
    componentWillReceiveProps(nextProps: any, nextContext: any): void;
    componentWillMount(): void;
    getDaysArrayByCalendarMonth(date: any): Dates[][];
    goPrev(): void;
    goNext(): void;
    selection(item: any): void;
    placeCaretAtEnd(el: any): void;
    focus(): void;
    isInInterval(value: any): boolean;
    isValid(string: any): boolean;
    render(): JSX.Element;
}
