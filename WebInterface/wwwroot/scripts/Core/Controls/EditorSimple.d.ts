/// <reference types="react" />
import * as React from 'react';
import { ContentEditable, ContentEditableProps } from './ContentEditable';
import { IValueLink } from '../Main';
export declare class EditorSimpleProps extends ContentEditableProps {
    valueLink?: IValueLink<string>;
    onEnter?: (obj: EditorSimple) => void;
    onBack?: (obj: EditorSimple) => void;
    onLeft?: (obj: EditorSimple) => void;
    onRight?: (obj: EditorSimple) => void;
}
export declare class EditorSimpleState {
    value: string;
}
export declare class EditorSimple extends React.Component<EditorSimpleProps, EditorSimpleState> {
    static defaultProps: EditorSimpleProps;
    state: EditorSimpleState;
    handleChange(e: any): void;
    componentWillReceiveProps(nextProps: any): void;
    componentDidMount(): void;
    calcHtml(value: any): string;
    getContent(self: any, jitemRow: any): string;
    onKeyDown(e: any): void;
    onKeyUp(e: any): void;
    render(): React.ComponentElement<{
        children?: React.ReactNode;
    } & ContentEditableProps, ContentEditable>;
}
