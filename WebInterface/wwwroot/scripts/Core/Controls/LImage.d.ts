/// <reference types="react" />
import * as React from 'react';
export declare class LImageProps {
    url?: string;
    zoomable?: boolean;
    overalable?: boolean;
    overflow?: boolean;
    zoom?: number;
    defaultSize?: {
        height: number;
        width: number;
    };
}
export declare class LImageState {
    src?: string;
    loading: boolean;
    error: boolean;
    width: number;
    height: number;
    x: number;
    y: number;
    zoom: number;
    mouseOver: boolean;
    style: React.CSSProperties;
}
export declare class LImage extends React.Component<LImageProps, LImageState> {
    static defaultProps: LImageProps;
    state: LImageState;
    componentWillReceiveProps(nextProps: any, nextContext: any): void;
    componentWillMount(): void;
    componentDidMount(): void;
    onMouseWheel(e: any): boolean;
    componentWillUnmount(): void;
    onLoadImage(elem: HTMLImageElement, ev: Event): void;
    load(): void;
    onMouseOver(e: any): void;
    onMouseOut(e: any): void;
    onMouseMove(e: any): void;
    calcPosition(): void;
    render(): JSX.Element;
}
