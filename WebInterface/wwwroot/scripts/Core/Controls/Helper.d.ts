export default class Helper {
    static guid(): string;
    static focusBack(target: HTMLElement): boolean;
    static focusNext(target: HTMLElement): boolean;
}
