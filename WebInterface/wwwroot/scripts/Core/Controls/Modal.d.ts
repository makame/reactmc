/// <reference types="react" />
import * as React from 'react';
import { Portal, PortalProps } from './Portal';
export declare class ModalProps extends PortalProps {
    level?: number;
    closeOnOutsideClick?: boolean;
    dialog?: boolean;
    message?: JSX.Element;
    bottom?: JSX.Element;
}
export declare class Modal extends React.Component<ModalProps, {}> {
    static defaultProps: ModalProps;
    refs: {
        [key: string]: any;
        portal: Portal;
    };
    closePortal(): void;
    openPortal(): void;
    componentDidMount(): void;
    componentWillUnmount(): void;
    handleOutsideClick(e: any): void;
    render(): JSX.Element;
}
