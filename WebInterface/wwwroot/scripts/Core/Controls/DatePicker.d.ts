/// <reference types="react" />
import * as React from 'react';
import * as moment from 'moment';
import { IValueLink } from '../Main';
import { Calendar } from './Calendar';
import { ConnectionPortal } from './Portal';
export declare class DatePickerProps {
    valueLink?: IValueLink<moment.Moment>;
    render?: (value: moment.Moment, clear: () => void, format: string) => JSX.Element;
    className?: string;
    weeksPerMonth?: number;
    format?: string;
    weeks?: string[];
    locale?: string;
    min?: moment.Moment;
    max?: moment.Moment;
}
export declare class DatePickerState {
    connectionPortal: ConnectionPortal;
}
export declare class DatePicker extends React.Component<DatePickerProps, DatePickerState> {
    static defaultProps: DatePickerProps;
    state: DatePickerState;
    refs: {
        [key: string]: any;
        calendar: Calendar;
    };
    clear(): void;
    getElement(): JSX.Element;
    render(): JSX.Element;
}
