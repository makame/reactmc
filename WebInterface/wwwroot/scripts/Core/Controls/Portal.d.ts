/// <reference types="react" />
import * as React from 'react';
export declare class ConnectionPortal {
    open?: () => void;
    close?: () => void;
    render?: () => void;
}
export declare class PortalProps {
    level?: number;
    closeOnEsc?: boolean;
    isOpened?: boolean;
    isHidden?: boolean;
    isGhost?: boolean;
    connectionPortal?: ConnectionPortal;
    onElementClick?: (e: any) => void;
    onElementOver?: (e: any) => void;
    onElementContextMenu?: (e: any) => void;
    onElementOut?: (e: any) => void;
    onElementFocus?: (e: any) => void;
    onElementBlur?: (e: any) => void;
    onOutsideClick?: (e: any) => void;
    onKeyDown?: (e: any) => void;
    onOpen?: (node?: any) => void;
    onUpdate?: () => void;
    didUpdate?: (portal: HTMLElement, element: Element) => void;
    beforeClose?: (node: HTMLElement, resetPortalState: () => void) => void;
    onClose?: (node?: HTMLElement) => void;
    isModal?: boolean;
    className?: string;
    style?: any;
    element?: React.ReactElement<any>;
    activeClassName?: string;
}
export declare class PortalState {
    portal?: any;
    node?: HTMLElement;
    active: boolean;
    onResize?: (s: any) => void;
    forceClosed?: boolean;
}
export declare class Portal extends React.Component<PortalProps, PortalState> {
    static portals: Portal[];
    state: PortalState;
    static defaultProps: PortalProps;
    refs: {
        [key: string]: Element;
        elem: HTMLElement;
    };
    constructor();
    update(): void;
    componentDidMount(): void;
    componentWillReceiveProps(newProps: any): void;
    componentWillUnmount(): void;
    handleWrapperClick(e: any): void;
    handleWrapperOver(e: any): void;
    handleContextMenu(e: any): void;
    handleWrapperOut(e: any): void;
    handleWrapperFocus(e: any): void;
    handleWrapperBlur(e: any): void;
    togglePortal(): void;
    openPortal(props?: {
        children?: React.ReactNode;
    } & PortalProps): void;
    closePortal(isUnmounted?: boolean): void;
    renderPortal(props?: {
        children?: React.ReactNode;
    } & PortalProps): void;
    tryExpectClose(): void;
    handleKeydown(e: any): void;
    handleOutsideMouseClick(e: any): void;
    componentDidUpdate(): void;
    getElement(): React.ReactElement<any>;
    render(): React.ReactElement<any>;
}
