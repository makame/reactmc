/// <reference types="react" />
import * as React from 'react';
import { Map, IValueLink, IContentModel, ContentModel, ContextModel, RouteModel, ApplicationModel } from '../Main';
export declare type GridDataType = 'local' | 'remote' | 'remote_local';
export declare class GridMessages {
    error?: string;
    info?: string;
    warning?: string;
    if?: boolean;
    enabled?: boolean;
}
export interface IGridItemModel<T> {
    container?: string;
    filter?: string[];
    classRow?: string;
    classEdit?: string;
    classCreate?: string;
    sortable?: boolean;
    label: string;
    if?: (value: T, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => boolean;
    enabled?: (value: T, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => boolean;
    error?: (value: T, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => string;
    info?: (value: T, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => string;
    warning?: (value: T, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => string;
}
export interface IGridItemFilter<T> {
    check(value: T, filter_value: any, field: GridItemModel<T>, context: ContextModel, page: RouteModel, application: ApplicationModel): boolean;
    render?(valueLink: IValueLink<any>, field: GridItemModel<T>, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
}
export declare class GridItemModel<T> implements IGridItemModel<T> {
    container?: string;
    filter?: string[];
    classRow?: string;
    classEdit?: string;
    classCreate?: string;
    sortable?: boolean;
    label: string;
    if?: (value: T, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => boolean;
    enabled?: (value: T, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => boolean;
    error?: (value: T, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => string;
    info?: (value: T, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => string;
    warning?: (value: T, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => string;
    filters?: Map<IGridItemFilter<T>>;
    render?(value: T, field: GridItemModel<T>, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
    create_render?(valueLink: IValueLink<T>, field: GridItemModel<T>, messages: GridMessages, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
    edit_render?(valueLink: IValueLink<T>, field: GridItemModel<T>, messages: GridMessages, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
}
export interface IGridFilterType {
    type: string;
    value?: any;
}
export interface IGridRow {
    [K: string]: any;
    id: any;
}
export interface IGridFilter {
    type: string;
    value?: any;
}
export declare class IGridValue {
    data?: IGridRow[];
    parsed_data?: IGridRow[];
    messages?: Map<Map<GridMessages>>;
    create_messages?: Map<GridMessages>;
    create?: IGridRow;
    edit?: Map<IGridRow>;
    selected?: Map<IGridRow>;
    sort?: string[];
    filter?: Map<IGridFilter>;
    per_page: number;
    page: number;
    total: number;
    need_update?: boolean;
    loaded?: boolean;
    scroll?: {
        key: any;
        value: (content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => any;
    };
    need_scroll?: boolean;
    after_request?: boolean;
    constructor(initializer?: IGridValue);
}
export interface IGridResponce {
    total: number;
    data: IGridRow[];
}
export declare class GridContextCreateButton {
    text: string;
    icon?: string;
    if?: (row: IGridRow, context: ContextModel, page: RouteModel, application: ApplicationModel) => boolean;
    enabled?: (row: IGridRow, context: ContextModel, page: RouteModel, application: ApplicationModel) => boolean;
    on_click: (row: IGridRow, context: ContextModel, page: RouteModel, application: ApplicationModel) => Promise<void> | void;
}
export declare class GridContextRowButton {
    text: string;
    icon?: string;
    if?: (row: IGridRow, context: ContextModel, page: RouteModel, application: ApplicationModel) => boolean;
    enabled?: (row: IGridRow, context: ContextModel, page: RouteModel, application: ApplicationModel) => boolean;
    on_click: (row: IGridRow, context: ContextModel, page: RouteModel, application: ApplicationModel) => Promise<void> | void;
}
export declare class GridContextChangeButton {
    text: string;
    icon?: string;
    if?: (row: IGridRow, old_row: IGridRow, context: ContextModel, page: RouteModel, application: ApplicationModel) => boolean;
    enabled?: (row: IGridRow, old_row: IGridRow, context: ContextModel, page: RouteModel, application: ApplicationModel) => boolean;
    on_click: (row: IGridRow, old_row: IGridRow, context: ContextModel, page: RouteModel, application: ApplicationModel) => Promise<boolean> | boolean;
}
export declare class IGrid {
    fields: Map<GridItemModel<any>>;
    pagination?: boolean;
    big_data?: boolean;
    selectable?: boolean;
    on_scroll?: (row: IGridRow, context: ContextModel, page: RouteModel, application: ApplicationModel) => void;
    allow_edit?: (row: IGridRow, context: ContextModel, page: RouteModel, application: ApplicationModel) => boolean;
    allow_add?: (context: ContextModel, page: RouteModel, application: ApplicationModel) => boolean;
    row_context_menu?: Map<GridContextRowButton>;
    create_context_menu?: Map<GridContextCreateButton>;
    edit_context_menu?: Map<GridContextChangeButton>;
    sortNull?: string[];
    sort?: string[];
    on_click?: (row: IGridRow, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel) => Promise<void> | void;
    data_type: GridDataType;
    request?: (value: IGridValue, context: ContextModel, page: RouteModel, application: ApplicationModel) => Promise<IGridResponce> | IGridResponce;
    constructor(initializer?: IGrid);
}
export declare class IGridModel extends IContentModel {
    class?: string;
    value?: IGridValue;
    grid: IGrid;
}
export declare class GridModel extends ContentModel {
    class?: string;
    value?: IGridValue;
    grid: IGrid;
    constructor(initializer?: IGridModel);
    render(context: ContextModel, page: RouteModel, application: ApplicationModel, containerFactory: (contaner: string, inner: JSX.Element, context: ContextModel) => JSX.Element): JSX.Element;
}
export declare class GridProps {
    value: IGridValue;
    grid: IGrid;
    containerFactory: (contaner: string, inner: JSX.Element, context: ContextModel) => JSX.Element;
    content: ContentModel;
    context: ContextModel;
    page: RouteModel;
    application: ApplicationModel;
}
export declare class GridState {
    if: Map<boolean>;
    enabled: Map<boolean>;
    caclulate_render: boolean;
    need_rerender: boolean;
    updating: boolean;
    rows_updated: boolean;
    loading: {
        on_create: boolean;
        on_edit: Map<boolean>;
        on_click: Map<boolean>;
    };
}
export declare class Grid extends React.Component<GridProps, GridState> implements ContentModel {
    filter_types: Map<string>;
    static defaultProps: GridProps;
    state: GridState;
    id: string;
    refs: {
        [key: string]: any;
        tbody: HTMLElement;
    };
    items: any[];
    few: any[];
    etaloneHeight: number;
    etaloneOffset: number;
    t: number;
    b: number;
    need_on_scroll: boolean;
    timerScroll?: number;
    timeoutRenderItems?: number;
    prev: boolean;
    cleanSelect(): void;
    cleanPage(): void;
    onScroll(): void;
    willUpdate(): void;
    handleScroll(event: any): void;
    handleScrollBind: any;
    componentDidMount(): void;
    componentWillUnmount(): void;
    componentWillMount(): void;
    componentWillUpdate(): void;
    reget_info(): void;
    componentDidUpdate(): void;
    factory_run_recurse(func_name: any): void;
    recurse_remote_data(): void;
    recurse_remote_pagination_data(): void;
    recurse_remote_local_data(): void;
    recurse_remote_local_pagination_data(): void;
    recurse_local_data(): void;
    recurse_local_pagination_data(): void;
    parse_data(): void;
    validate(row: IGridRow): Map<GridMessages>;
    componentWillReceiveProps(nextProps: any): void;
    get_filter_types(filters: string[]): Map<string>;
    renderItem(row: IGridRow, index: number): JSX.Element;
    render(): JSX.Element;
}
