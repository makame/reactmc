/// <reference types="react" />
import { Map, IContentModel, ContentModelInit, ContextModel, RouteModel, ApplicationModel } from '../Main';
export interface ITabModel extends IContentModel {
    label?: string;
    class?: string;
}
export declare class TabModel extends ContentModelInit<ITabModel> implements ITabModel {
    label?: string;
    class?: string;
}
export interface ITabsModel extends IContentModel {
    tabs?: Map<TabModel>;
    class?: string;
    active_tab?: string;
}
export declare class TabsModel extends ContentModelInit<ITabsModel> implements ITabsModel {
    tabs?: Map<TabModel>;
    class?: string;
    active_tab?: string;
    render(context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
}
