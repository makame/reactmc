/// <reference types="react" />
import { Map, IContentModel, ContentModelInit, ContextModel, RouteModel, ApplicationModel } from '../Main';
export declare class ButtonModel {
    text?: string;
    icon?: string;
    enabled?: (context: ContextModel, page: RouteModel, application: ApplicationModel) => Promise<boolean> | boolean;
    on_click: (context: ContextModel, page: RouteModel, application: ApplicationModel) => Promise<void> | void;
    if?: (context: ContextModel, page: RouteModel, application: ApplicationModel) => Promise<boolean> | boolean;
    loading?: boolean;
    class?: string;
}
export interface IButtonsModel extends IContentModel {
    buttons: Map<ButtonModel>;
    class?: string;
}
export declare class ButtonsModel extends ContentModelInit<IButtonsModel> implements IButtonsModel {
    buttons: Map<ButtonModel>;
    class?: string;
    render(context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
}
