/// <reference types="react" />
import * as React from 'react';
import { Map, IValueLink, IContentModel, ContentModelInit, ContentModel, ContextModel, RouteModel, ApplicationModel } from '../Main';
export declare class FormMessages {
    error?: string;
    info?: string;
    warning?: string;
}
export interface IFormItemModel<T> {
    container?: string;
    if?: (value: T, model: IFormLink, context: ContextModel, page: RouteModel, application: ApplicationModel) => boolean;
    enabled?: (value: T, model: IFormLink, context: ContextModel, page: RouteModel, application: ApplicationModel) => boolean;
    error?: (value: T, model: IFormLink, context: ContextModel, page: RouteModel, application: ApplicationModel) => string;
    info?: (value: T, model: IFormLink, context: ContextModel, page: RouteModel, application: ApplicationModel) => string;
    warning?: (value: T, model: IFormLink, context: ContextModel, page: RouteModel, application: ApplicationModel) => string;
}
export declare class FormItemModel<T> implements IFormItemModel<T> {
    messages?: FormMessages;
    validated?: boolean;
    container?: string;
    if?: (value: T, model: IFormLink, context: ContextModel, page: RouteModel, application: ApplicationModel) => boolean;
    enabled?: (value: T, model: IFormLink, context: ContextModel, page: RouteModel, application: ApplicationModel) => boolean;
    error?: (value: T, model: IFormLink, context: ContextModel, page: RouteModel, application: ApplicationModel) => string;
    info?: (value: T, model: IFormLink, context: ContextModel, page: RouteModel, application: ApplicationModel) => string;
    warning?: (value: T, model: IFormLink, context: ContextModel, page: RouteModel, application: ApplicationModel) => string;
    render(name: string, valueLink: IValueLink<T>, enabled: boolean, model: IFormLink, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel, containerFactory: (contaner: string, inner: JSX.Element, context: ContextModel) => JSX.Element): JSX.Element;
}
export declare class IFormModel extends IContentModel {
    class?: string;
    value?: Map<any>;
    form: Map<FormItemModel<any>>;
    validated?: boolean;
}
export declare class IFormLink {
    value: Map<any>;
    form: Map<FormItemModel<any>>;
    parent?: IFormLink;
}
export declare class FormModel extends ContentModelInit<IFormModel> {
    class?: string;
    value: Map<any>;
    form: Map<FormItemModel<any>>;
    validated?: boolean;
    render(context: ContextModel, page: RouteModel, application: ApplicationModel, containerFactory: (contaner: string, inner: JSX.Element, context: ContextModel) => JSX.Element): JSX.Element;
}
export declare class FormProps {
    value: Map<any>;
    form: Map<FormItemModel<any>>;
    model: IFormLink;
    parent?: IFormLink;
    validated?: boolean;
    enabled?: boolean;
    containerFactory: (contaner: string, inner: JSX.Element, context: ContextModel) => JSX.Element;
    content: ContentModel;
    context: ContextModel;
    page: RouteModel;
    application: ApplicationModel;
}
export declare class FormState {
    if: Map<boolean>;
    enabled: Map<boolean>;
}
export declare class Form extends React.Component<FormProps, FormState> implements ContentModel {
    static defaultProps: FormProps;
    state: FormState;
    validate(): void;
    componentWillMount(): void;
    componentWillUpdate(): void;
    render(): JSX.Element;
}
