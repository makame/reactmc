/// <reference types="react" />
import { Map, IContentModel, ContentModelInit, ContentModel, ContextModel, RouteModel, ApplicationModel } from '../Main';
export interface IMenuItemModel {
    render(name: string, content: ContentModel, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
    if?: (context: ContextModel, page: RouteModel, application: ApplicationModel) => boolean;
}
export interface IMenuModel extends IContentModel {
    items: Map<IMenuItemModel>;
    class?: string;
}
export declare class MenuModel extends ContentModelInit<IMenuModel> implements IMenuModel {
    items: Map<IMenuItemModel>;
    class?: string;
    render(context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
}
