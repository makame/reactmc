/// <reference types="react" />
import { ContentModelInit, IContentModel, ContextModel, RouteModel, ApplicationModel } from '../Main';
export interface ITextModel extends IContentModel {
    value?: string;
    class?: string;
}
export declare class TextModel extends ContentModelInit<ITextModel> implements ITextModel {
    value?: string;
    class?: string;
    render(context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
}
