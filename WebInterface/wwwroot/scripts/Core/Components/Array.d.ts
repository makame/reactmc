/// <reference types="react" />
import { Map, IContentModel, ContentModelInit, ContentModel, ContextModel, RouteModel, ApplicationModel } from '../Main';
export interface IArrayModel extends IContentModel {
    items: Map<ContentModel>;
    class?: string;
}
export declare class ArrayModel extends ContentModelInit<IArrayModel> implements IArrayModel {
    items: Map<ContentModel>;
    class?: string;
    render(context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
}
