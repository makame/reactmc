/// <reference types="react" />
import * as React from 'react';
export interface ISetGet {
    get?: <T>(context: ContextModel, page: RouteModel, application: ApplicationModel) => T;
    set?: <T>(newValue: T, context: ContextModel, page: RouteModel, application: ApplicationModel) => void | Promise<void>;
}
export interface IValueLink<T> {
    value: T;
    requestChange(newValue: T): any;
}
export declare class Map<T> {
    [K: string]: T;
}
export declare class MapPro {
    static map<T, S>(map: Map<T>, iterator: (item: T, name: string, index: number) => S): S[];
    static each<T, S>(map: Map<T>, iterator: (item: T, name: string, index: number) => S): void;
    static find<T>(map: Map<T>, iterator: (item: T, name: string, index: number) => boolean): T;
    static findName<T>(map: Map<T>, iterator: (item: T, name: string, index: number) => boolean): string;
    static filter<T>(map: Map<T>, iterator: (item: T, name: string, index: number) => boolean): Map<T>;
}
export declare class ContextModel implements ISetGet {
    content?: ContentModel;
    init?(context: ContextModel, page: RouteModel, application: ApplicationModel): Promise<any> | void;
    loading?: boolean;
    inited?: boolean;
}
export declare class IContentModel extends ContextModel {
    container?: string;
    loading?: boolean;
    modals?: Map<ModalModel>;
}
export declare class ContentModel extends ContextModel implements IContentModel {
    container?: string;
    loading?: boolean;
    modals?: Map<ModalModel>;
    render(context: ContextModel, page: RouteModel, application: ApplicationModel, containerFactory: (contaner: string, inner: JSX.Element, context: ContextModel) => JSX.Element): JSX.Element;
}
export declare class Init<T> {
    constructor(initializer?: T);
}
export declare class ContentModelInit<T> extends ContentModel {
    constructor(initializer?: T);
}
export declare class ModalModel {
    message?: ContentModel;
    bottom?: ContentModel;
    dialog?: boolean;
    content: ContentModel;
    class?: string;
    close?: () => void;
    before_close?: (close: () => void, content: ContentModel, item: ModalModel, page: RouteModel, application: ApplicationModel) => void;
    on_outside?: (content: ContentModel, item: ModalModel, page: RouteModel, application: ApplicationModel) => void;
}
export declare class RouteModel {
    routes?: Map<RouteModel>;
    index?: string;
    path: string;
    layout?: (page: RouteModel, application: ApplicationModel, location: any, children?: JSX.Element) => JSX.Element;
    page?: (page: RouteModel, application: ApplicationModel, children?: JSX.Element) => JSX.Element;
    update?(): void;
    check?(page: RouteModel, application: ApplicationModel): void | boolean;
    condition?(page: RouteModel, application: ApplicationModel): ContentModel;
    content?: ContentModel;
    init?(page: RouteModel, application: ApplicationModel): Promise<any> | void;
    loading?: boolean;
    inited?: boolean;
    params?: Map<string>;
    query?: Map<string>;
    location?: any;
}
export declare class ApplicationModel {
    basename?(location: Location): any;
    browserHistory?: any;
    initing?: boolean;
    init?(application: ApplicationModel): Promise<any> | void;
    routes?: Map<RouteModel>;
    containers: Map<IContainerModel>;
}
export interface IContainerModel {
    render(model: ContainerAdapter, content: JSX.Element, context: ContextModel, page: RouteModel, application: ApplicationModel): JSX.Element;
}
export declare class ContainerAdapter {
    type: string;
    class: string[];
    parameters: ISetGet[];
}
export declare class ComponentProps {
    page: RouteModel;
    application: ApplicationModel;
    content?: ContentModel;
    context?: ContextModel;
    params?: Map<string>;
    query?: Map<string>;
    location?: any;
}
export declare class Component extends React.Component<ComponentProps, {}> {
    static defaultProps: ComponentProps;
    private getValue(array, parent);
    private parseContainer(str, context);
    private getContainer<T>(model, content, context, page, application);
    private createContainer(array, content, context, page, application);
    private containerFactory(container, inner, context, page, application);
    componentWillReceiveProps(nextProps: any, nextContext: any): void;
    init(): Promise<void>;
    componentWillMount(): void;
    componentWillUpdate(): Promise<void>;
    render(): JSX.Element;
}
export declare class ApplicationProps {
    application: ApplicationModel;
}
export declare class ApplicationState {
    browserHistory?: any;
}
export declare class Application extends React.Component<ApplicationProps, ApplicationState> {
    static defaultProps: ApplicationProps;
    state: ApplicationState;
    private parseRoute(route, name, application);
    init(): Promise<void>;
    componentWillMount(): void;
    render(): JSX.Element;
}
