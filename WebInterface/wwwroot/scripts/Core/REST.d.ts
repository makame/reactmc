export declare class METHOD {
    private rest;
    protected _url: string;
    protected _data: string;
    protected _method: string;
    protected _type: string;
    protected _crossDomain: boolean;
    constructor(rest: REST, url?: string);
    cross(crossDomain: any): this;
    url(url: any): this;
    method(method: any): this;
    type(type: any): this;
    data(data: any): this;
    do(): any;
}
export declare class GET extends METHOD {
    constructor(rest: REST, url?: string);
}
export declare class ONE {
    rest: REST;
    _one: string[];
    constructor(rest: REST, one?: string[]);
    one(one: any): this;
    back(one: any): this;
    get(): GET;
    method(): METHOD;
}
export declare class Task {
    func: (resolve: (reason?: any) => void, reject: (value?: any | PromiseLike<{}>) => void) => void;
    resolve: (value?: any | PromiseLike<{}>) => void;
    reject: (reason?: any) => void;
    run: boolean;
    constructor(func: (resolve: (reason?: any) => void, reject: (value?: any | PromiseLike<{}>) => void) => void, resolve: (value?: any | PromiseLike<{}>) => void, reject: (reason?: any) => void, run: boolean);
}
export declare class REST {
    private tasks;
    _enter: string;
    constructor(enter?: string);
    new(enter: any): REST;
    getDir(): string;
    enter(enter?: string): this;
    one(one: string): ONE;
    nextTask(): void;
    addTask(func: any): Promise<{}>;
    get(): GET;
    method(): METHOD;
}
export declare const rest: REST;
