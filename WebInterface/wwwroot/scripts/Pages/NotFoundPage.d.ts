/// <reference types="react" />
import { RouteModel, ApplicationModel } from '../Core';
declare var _default: (page: RouteModel, application: ApplicationModel, children?: JSX.Element) => JSX.Element;
export default _default;
