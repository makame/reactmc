﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NSwag;
using NSwag.CodeGeneration.CodeGenerators.TypeScript;

namespace WebInterface.Controllers
{
    public class ExceptionFilter : Microsoft.AspNetCore.Mvc.Filters.ExceptionFilterAttribute
    {
        public override void OnException(Microsoft.AspNetCore.Mvc.Filters.ExceptionContext context)
        {
            var exception = context.Exception;
            context.HttpContext.Response.StatusCode = 400;
            context.Result = new JsonResult(exception.Message);
        }
    }

    /// <summary>
    /// Archer Project
    /// </summary>
    [Route("api/archer")]
    public class ArcherController : Controller
    {
        /// <summary>
        /// This Get by Id
        /// </summary>
        [HttpPost]
        [Consumes("text/json")]
        [Produces(typeof(string))]
        [ProducesResponseType(typeof(string), 400)]
        [ExceptionFilter]
        [Route("[action]/{id:int}")]
        public string GetById(int id)
        {
            return "item " + id;
        }

		/// <summary>
		/// Generate TypeScript Interface
		/// </summary>
		[HttpGet]
		[Consumes("text/json")]
		[Produces(typeof(string))]
		[ProducesResponseType(typeof(string), 400)]
		[ExceptionFilter]
		[Route("[action]")]
		public async Task<string> GenerateTSInterface()
        {
            var schema = string.Format("{0}://{1}{2}", Request.Scheme,
            Request.Host, "/swagger/v1/swagger.json");

            string contents;

			using (var wc = new System.Net.Http.HttpClient())
			{
               contents = await wc.GetStringAsync(schema);
			}
			
			var document = SwaggerDocument.FromJson(contents);

			var settings = new SwaggerToTypeScriptClientGeneratorSettings
			{
				ClassName = "{controller}Client",
				PromiseType = PromiseType.Promise,
				Template = TypeScriptTemplate.JQueryPromises
			};

			var generator = new SwaggerToTypeScriptClientGenerator(document, settings);
			var code = generator.GenerateFile();

			return code;
		}

	    /// <summary>
	    /// Archer Project
	    /// </summary>
        public class DocUpdateModel
        {
			/// <summary>
			/// Archer Project
			/// </summary>
			public string User { get; set; }
			/// <summary>
			/// Archer Project
			/// </summary>
			public string Pass { get; set; }
        }

        /// <summary>
        /// Test Update
        /// </summary>
        [HttpPost]
        [Consumes("application/json")]
        [Produces(typeof(DocUpdateModel))]
        [ProducesResponseType(typeof(string), 400)]
        [ExceptionFilter]
        [Route("[action]")]
        public IActionResult DocUpdate([FromBody] DocUpdateModel doc)
        {
            return new JsonResult(doc);
        }

        /// <summary>
        /// Test Update
        /// </summary>
        [HttpPost]
        [Consumes("application/json")]
        [Produces(typeof(string))]
        [ProducesResponseType(typeof(string), 400)]
        [ExceptionFilter]
        [Route("[action]")]
        public IActionResult TestException()
        {
            //this.Response.StatusCode = 400;
            throw new Exception("TEST EX");
        }

        /// <summary>
        /// Test Update
        /// </summary>
        [HttpPost]
        [Consumes("text/json")]
        [Produces(typeof(string))]
        [ProducesResponseType(typeof(string), 400)]
        [ExceptionFilter]
        [Route("[action]")]
        public IActionResult DocNew([FromBody] string value)
        {
            return new JsonResult(new { text = "item " + value });
        }

        /// <summary>
        /// Test Employee
        /// </summary>
        public class Employee
        {
            /// <summary>
            /// Test Id
            /// </summary>
            public long Id;
            /// <summary>
            /// Test Name
            /// </summary>
            public string Name { set; get; }
        }

        /// <summary>
        /// Get Employees
        /// </summary>
        public class EmployeeResult
        {
            public Employee[] array { set; get; }
            public ulong total { set; get; }
        }

        [HttpPost]
        [Consumes("application/json")]
        [Produces(typeof(EmployeeResult))]
        [ProducesResponseType(typeof(string), 400)]
        [ExceptionFilter]
        [Route("[action]")]
        public IActionResult GetEmployees()
        {
            Core.Client.test();
            return new JsonResult(new EmployeeResult() {
                array = new Employee[] {
                    new Employee { Id = 1, Name = "John Doe" },
                    new Employee { Id = 2, Name = "Jane Doe" }
                },
                total = 2
            });
        }
    }
}
