var http = require('http');
var fs = require('fs');

var argv = require('minimist')(process.argv.slice(2));

var options = {
  host: argv.h,
  path: argv.u,
  port: argv.p,
  method: argv.m
};

callback = function(response) {
  var str = ''
  response.setEncoding('utf8')

  response.on('data', function (chunk) {
    str += chunk;
  });

  response.on('end', function () {
    //   str.replace(/\\n/g, '\r\n')
    fs.writeFile(argv.f, str, function (err) {
        if (err) throw err;
        console.log('It\'s saved!');
    });
  });
}

var req = http.request(options, callback);

req.end();