using System;
using FluentNHibernate.Mapping;
namespace Core
{
	public class ClientContact
	{
		public virtual int Id { get; set; }
		public virtual string Type { get; set; }
		public virtual string Value { get; set; }
		public virtual Client Client { get; set; }
	}

	public class ClientContactMap : ClassMap<ClientContact>
	{
		public ClientContactMap()
		{
			Table("client_contact");
			Id(x => x.Id).Column("id");
			Map(x => x.Type).Column("type");
			Map(x => x.Value).Column("value");
			References(x => x.Client).Column("client_id").Cascade.SaveUpdate();
		}
	}
}
