﻿using System;
using System.Collections.Generic;
using FluentNHibernate.Mapping;
using NHibernate;

namespace Core
{
	public class Client
	{
		public virtual int Id { get; set; }
		public virtual string FirstName { get; set; }
		public virtual string LastName { get; set; }

		private IList<ClientContact> _contacts;
		public virtual IList<ClientContact> Contacts
		{
			get
			{
				return _contacts ?? (_contacts = new List<ClientContact>());
			}
			set { _contacts = value; }
		}

		public static void test() {
            int id;

            using (ISession session = NHibernateHelper.OpenSession()) {
                Client client = new Client() {
                    FirstName = "Maxim",
                    LastName = "Karpov"
                };
                   
                // Сохрананяем объект в сессии
                session.Save(client);

                id = client.Id;
            }

            using (ISession session = NHibernateHelper.OpenSession()) {
                Client client1 = session.Get<Client>(id);

                Console.WriteLine(client1.Id);

                IList<Client> clients = session.QueryOver<Client>().List();

                Console.WriteLine(clients.Count);
            }
		}
	}

	public class ClientMap: ClassMap<Client>
	{
		public ClientMap()
		{
			Table("client");
			Id(x => x.Id).Column("id");
			Map(x => x.FirstName).Column("first_name");
			Map(x => x.LastName).Column("last_name");
			HasMany(x => x.Contacts).Inverse();
		}
	}
}
