﻿using System;
using System.Reflection;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Cfg.MappingSchema;
using NHibernate.Dialect;
using NHibernate.Mapping.ByCode;
using NHibernate.Tool.hbm2ddl;

namespace Core
{
	public class NHibernateHelper
	{
		public static ISession OpenSession()
		{
			ISessionFactory sessionFactory = Fluently.Configure().Database(MySQLConfiguration.Standard.ConnectionString(c => c.FromConnectionStringWithKey("Server=127.0.0.1;Port=3306;Database=test;Uid=root;Pwd=vfrfv;")))
			.Mappings(m => m.FluentMappings.AddFromAssemblyOf<ClientMap>())
			.Mappings(m => m.FluentMappings.AddFromAssemblyOf<ClientContactMap>())
         	.ExposeConfiguration(cfg => new SchemaUpdate(cfg).Execute(false, true))
			.BuildSessionFactory();
			
			return sessionFactory.OpenSession();
		}
	}
}
